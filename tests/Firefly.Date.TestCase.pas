{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for Firefly.Date)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2011-06-17  pk  10m  Code Audit
// 2009-09-29  pk   --  Added this header (from Code Test Browser)
// 2007-04-05  pk  10m  FileTimeToDateTime for FPC
// 2007-01-30  pk  10m  Moved SchaltJahr, DayOfJahr, DaysOfYear, DaysSinceYear from snlNumbers
// *****************************************************************************
   )
}

unit Firefly.Date.TestCase;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$endif FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.Date;

type

   TTestCase_Firefly_Date = class(TFireflyTestCase)
   published
      procedure TestSchaltJahr;
      procedure TestDaysOfYear;
      procedure TestDaysSinceYear;
      procedure TestDateTimeToRFC822;
      procedure TestConvertUNIXTime;
      procedure TestGetWindowsInstallationDate;
      procedure TestGetFirstDayOfWeek;
   end;

implementation

{ TTestCase_Firefly_Date }

procedure TTestCase_Firefly_Date.TestConvertUNIXTime;
var
   dt, dt2: TDateTime;
   c: cardinal;
begin
   dt := 35065;
   {$IFDEF MSWindows}
   c := ConvertDateTimeToUNIXTime(dt);
   dt2 := ConvertUNIXTimeToDateTime(c);
   {$ELSE MSWindows}
   c := ConvertDateTimeToUNIXTimeRaw(dt);
   dt2 := ConvertUNIXTimeToDateTimeRaw(c);
   {$ENDIF MSWindows}
   Status('ConvertDateTimeToUNIXTime: ' + IntToStr(c));
   Status('ConvertUNIXTimeToDateTime: ' + DateTimeToRFC822(dt2));
   CheckEquals(dt, dt2, 'ConvertDateTimeToUNIXTime/ConvertUNIXTimeToDateTime');
end;

procedure TTestCase_Firefly_Date.TestDateTimeToRFC822;
const
   SDate = 'Mon, 01 Jan 1996 00:00:00 +0100';
var
   s: string;
begin
   s := DateTimeToRFC822(35065);
   Status('DateTimeToRFC822(35065): ' + s);
   CheckEquals(s, SDate, 'DateTimeToRFC822');
   Status('DateTimeToRFC822(Now): ' + DateTimeToRFC822(Now));
end;

procedure TTestCase_Firefly_Date.TestDaysOfYear;
begin
   CheckEquals(DaysOfYear(1900), 365, 'DaysOfYear(1900)');
   CheckEquals(DaysOfYear(2000), 366, 'DaysOfYear(2000)');
   CheckEquals(DaysOfYear(2004), 366, 'DaysOfYear(2004)');
end;

procedure TTestCase_Firefly_Date.TestDaysSinceYear;
begin
   Status(Format('DaysSinceYear(2005): %d', [DaysSinceYear(2005)]));
   Status(Format('DaysSinceYear(2007): %d', [DaysSinceYear(2007)]));
end;

procedure TTestCase_Firefly_Date.TestGetFirstDayOfWeek;
begin
   Check(pkGetFirstDayOfWeek < 7);
end;

procedure TTestCase_Firefly_Date.TestGetWindowsInstallationDate;
var
   dt: TDateTime;
begin
   {$IFDEF MSWindows}
   Check(GetWindowsInstallationDate(dt), 'GetWindowsInstallationDate');
   {$ELSE MSWindows}
   Check(true);
   {$ENDIF MSWindows}
end;

procedure TTestCase_Firefly_Date.TestSchaltJahr;
begin
   CheckEquals(SchaltJahr(1900), 0, 'SchaltJahr(1900)');
   CheckEquals(SchaltJahr(2000), 1, 'SchaltJahr(2000)');
   CheckEquals(SchaltJahr(2004), 1, 'SchaltJahr(2004)');
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_Date.Suite);

end.
