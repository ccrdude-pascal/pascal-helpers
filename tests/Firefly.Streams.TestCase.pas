{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for PepiMK.Files.Streams.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2003-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-04  pk   5m  Test runs successful on Lazarus 1.6 / FPC 3.0.0
// 2016-05-04  pk   5m  Test runs successful on Delphi
// 2016-05-04  pk  10m  Wrote test case
// *****************************************************************************
   )
}

unit Firefly.Streams.TestCase;

interface

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   Windows, // Delphi needs this for FileClose, DeleteFile
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.Streams;

type

   TTestCase_Firefly_Streams = class(TFireflyTestCase)
   private
      function CreateTestFile: boolean;
   published
      procedure TestBaseTFileStream;
      procedure TestTFileHandleStream;
      procedure TestCopyDataFromHandleToStream;
   end;

implementation

const
   STestFileName: string = 'snlStreamsDeleteTest.txt';
   STestFileNameA: ansistring = 'snlStreamsDeleteTest.txt';

{ TTestCase_Firefly_Streams }

procedure TTestCase_Firefly_Streams.TestCopyDataFromHandleToStream;
var
   saTest: ansistring;
   sFilename: string;
   saReal: ansistring;
   msReal, msTest: TMemoryStream;
   hFile: THandle;
begin
   sFilename := ParamStr(0);
   msReal := TMemoryStream.Create;
   msTest := TMemoryStream.Create;
   try
      msReal.LoadFromFile(sFilename);
      msReal.Seek(0, soFromBeginning);
      hFile := FileOpen(sFilename, fmOpenRead or fmShareDenyWrite);
      FileSeek(hFile, 0, 0);
      if hFile > 0 then begin
         try
            CopyDataFromHandleToStream(hFile, msTest, 0, msReal.Size);
            msTest.Seek(0, soFromBeginning);
            CheckEquals(msTest.Size, msReal.Size, 'CopyDataFromHandleToStream: stream size');
            saTest := '';
            SetLength(saTest, 100);
            saReal := '';
            SetLength(saReal, 100);
            msTest.Read(saTest[1], 100);
            msReal.Read(saReal[1], 100);
            CheckEquals(saTest, saReal, 'CopyDataFromHandleToStream: first 100 bytes');
            msTest.Seek(0, soFromEnd);
            msReal.Seek(0, soFromEnd);
            msTest.Read(saTest[1], 100);
            msReal.Read(saReal[1], 100);
            CheckEquals(saTest, saReal, 'CopyDataFromHandleToStream: last 100 bytes');
         finally
            FileClose(hFile);
         end;
      end;
   finally
      msReal.Free;
      msTest.Free;
   end;
end;

function TTestCase_Firefly_Streams.CreateTestFile: boolean;
var
   sl: TStringList;
begin
   sl := TStringList.Create;
   try
      sl.Add(ansistring('Hallo Welt'));
      sl.Add(ansistring('Hello World'));
      sl.Add(ansistring('Salve Mundi'));
      sl.SaveToFile(TempTestFolder + STestFileName);
   finally
      sl.Free;
   end;
   Result := FileExists(TempTestFolder + STestFileName);
end;

procedure TTestCase_Firefly_Streams.TestBaseTFileStream;
var
   fs1: TFileStream;
   iTestLen, iReadLen, iWriteLen: integer;
   saTest: ansistring;
begin
   // 2016-09-06  pk  I removed this test since both compilers do currently fail here
   Exit;
   // Issue in Delphi:
   // http://qc.embarcadero.com/wc/qcmain.aspx?d=45628
   // Issue in FreePascal, regression issue:
   // http://bugs.freepascal.org/view.php?id=8898
   CheckTrue(CreateTestFile, 'CreateTestFile #1');
   iTestLen := Length(STestFileNameA);
   fs1 := nil;
   try
      fs1 := TFileStream.Create(TempTestFolder + STestFileName, fmOpenReadWrite or fmShareDenyNone);
      CheckTrue(True, 'TFileStream.Create');
      // A bug in Delphi prevents this from working
      // A bug regression in FreePascal prevents this from working as well
      {$IFDEF VER220}
      Fail('Two tests discarded, due to a bug in Delphi XE (fmShareDenyNone)!');
      //CheckTrue(DeleteFile(PChar(TempTestFolder + STestFileName)), 'DeleteFile #1');
      //CheckTrue(not FileExists(TempTestFolder + STestFileName), 'FileExists #1');
      {$ENDIF VER220}
      {$IFDEF FPC}
      Fail('Two tests discarded, due to a bug regression in FreePascal 3.0 (fmShareDenyNone)!');
      //CheckTrue(DeleteFile(PChar(TempTestFolder + STestFileName)), 'DeleteFile #1');
      //CheckTrue(not FileExists(TempTestFolder + STestFileName), 'FileExists #1');
      {$ENDIF FPC}
      iWriteLen := fs1.Write(STestFileNameA[1], iTestLen);
      CheckEquals(iTestLen, iWriteLen, 'TFileStream.Write');
      fs1.Seek(0, soFromBeginning);
      saTest := '';
      SetLength(saTest, iTestLen);
      iReadLen := fs1.Read(saTest[1], iTestLen);
      CheckEquals(iTestLen, iReadLen, 'TFileStream.Read#Count');
      CheckEquals(STestFileNameA, saTest, 'TFileStream.Read#Text');
   finally
      fs1.Free;
   end;
end;

procedure TTestCase_Firefly_Streams.TestTFileHandleStream;
var
   fhs: TFileHandleStream;
   iTestLen, iReadLen, iWriteLen: integer;
   saTest: ansistring;
begin
   CheckTrue(CreateTestFile, 'CreateTestFile #2');
   try
      try
         DeleteFile(PChar(TempTestFolder + STestFileName));
         fhs := TFileHandleStream.Create(TempTestFolder + STestFileName, fmCreate or fmShareDenyNone);
         Status(TempTestFolder + STestFileName);
         CheckTrue(True, 'TFileHandleStream.Create');
         CheckTrue(DeleteFile(PChar(TempTestFolder + STestFileName)), 'DeleteFile #2');
         {$IFNDEF VER220}
         CheckTrue(not FileExists(TempTestFolder + STestFileName), 'FileExists #2');
         {$ELSE VER220}
         Status('One test discarded, due to bug in Delphi XE (fmShareDenyNone)!');
         {$ENDIF VER220}
         iTestLen := Length(STestFileNameA);
         iWriteLen := fhs.Write(STestFileNameA[1], iTestLen);
         CheckEquals(iTestLen, iWriteLen, 'TpkFileStream.Write');
         fhs.Seek(0, soFromBeginning);
         saTest := '';
         SetLength(saTest, iTestLen);
         iReadLen := fhs.Read(saTest[1], iTestLen);
         CheckEquals(iTestLen, iReadLen, 'TpkFileStream.Read#Count');
         CheckEquals(STestFileNameA, saTest, 'TpkFileStream.Read#Text');
      finally
         fhs.Free;
      end;
   except
      CheckTrue(False, 'TFileHandleStream.Create');
   end;
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_Streams.Suite);

end.
