{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for Firefly.Parser.Boolean.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-02-28  --  ---  Renamed from PepiMK.Parser.boolean.TestCase to PepiMK.Parser.Boolean.TestCase
// *****************************************************************************
   )
}

unit Firefly.Parser.boolean.TestCase;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$endif FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.Parser.boolean;

type

   { TTestCase_Firefly_Parser_Boolean }

   TTestCase_Firefly_Parser_Boolean = class(TFireflyTestCase)
   published
      procedure TestOpOR;
      procedure TestOpAND;
      procedure TestOpNOT;
   end;

implementation

{ TTestCase_Firefly_Parser_Boolean }

procedure TTestCase_Firefly_Parser_Boolean.TestOpOR;
var
   ruleSet: TCompareRule;
begin
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString('Hallo OR Hello');
      CheckTrue(ruleSet.StringFits('Hallo Welt'));
      CheckTrue(ruleSet.StringFits('Hello World'));
      CheckFalse(ruleSet.StringFits('Salve Mundae'));
   finally
      ruleSet.Free;
   end;
end;

procedure TTestCase_Firefly_Parser_Boolean.TestOpAND;
var
   ruleSet: TCompareRule;
begin
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString('Hallo AND grüne');
      CheckTrue(ruleSet.StringFits('Hallo grüne Welt'));
      CheckFalse(ruleSet.StringFits('Hallo gelbe Welt'));
      CheckFalse(ruleSet.StringFits('Salve Mundae'));
   finally
      ruleSet.Free;
   end;
end;

procedure TTestCase_Firefly_Parser_Boolean.TestOpNOT;
var
   ruleSet: TCompareRule;
begin
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString('NOT gelb');
      CheckTrue(ruleSet.StringFits('Hallo grüne Welt'));
      CheckFalse(ruleSet.StringFits('Hallo gelbe Welt'));
      CheckTrue(ruleSet.StringFits('Salve Mundae'));
   finally
      ruleSet.Free;
   end;
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_Parser_Boolean.Suite);

end.
