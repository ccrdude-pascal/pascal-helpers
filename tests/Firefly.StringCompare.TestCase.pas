﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for PepiMK.Firefly.StringCompare.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2016-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-17  pk  20m  Ported test case
// *****************************************************************************
   )
}

unit Firefly.StringCompare.TestCase;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.StringCompare;

type

   { TTestCase_Firefly_StringCompare }

   TTestCase_Firefly_StringCompare = class(TFireflyTestCase)
   private
      GetFilePathsWideTemplate: TStringList;
      function CompareTwoStrings(const ABaseText: string; const ACompareRule: string; const AStyle: TCompareStyle; const AIgnoreCase, AShouldBeGood: boolean): boolean;
   protected
      procedure SetUp; override;
      procedure TearDown; override;
   published
      procedure TestAhoCorasickStringReplaceASimple;
      procedure TestAhoCorasickStringReplaceAAdvanced;
      procedure TestAhoCorasickStringReplaceAPaths;
      procedure TestCompareTwoStringsSubString;
      procedure TestCompareTwoStringsStartingBytes;
      procedure TestCompareTwoStringsWildcards;
      procedure TestCompareTwoStringsBoolean;
      procedure TestCompareTwoStringsRegExpr;
      procedure TestCompareTwoStringsPerlRegExpr;
      procedure TestCompareTwoStringsBoyerMoore;
      procedure TestLevensthein;
      //procedure TestAhoCorasickStringReplaceA;
   end;

implementation


{ TTestCase_Firefly_StringCompare }

function TTestCase_Firefly_StringCompare.CompareTwoStrings(const ABaseText: string; const ACompareRule: string; const AStyle: TCompareStyle; const AIgnoreCase, AShouldBeGood: boolean): boolean;
begin
   Result := pkCompareString(ABaseText, ACompareRule, AStyle, AIgnoreCase);
   if AShouldBeGood then begin
      CheckTrue(Result, 'pkCompareString');
   end else begin
      CheckTrue(not Result, 'pkCompareString');
   end;
end;

procedure TTestCase_Firefly_StringCompare.SetUp;
var
   slSub1, slSub2, slSub3: TStringList;
begin
   inherited SetUp;
   GetFilePathsWideTemplate := TStringList.Create;
   slSub1 := TStringList.Create;
   slSub1.Add('C:\Windows');
   slSub2 := TStringList.Create;
   slSub2.Add('C:\Windows\system32');
   slSub3 := TStringList.Create;
   slSub3.Add('C:\Users\Georg\Desktop');
   GetFilePathsWideTemplate.AddObject('<$WINDIR>', slSub1);
   GetFilePathsWideTemplate.AddObject('<$SYSDIR>', slSub2);
   GetFilePathsWideTemplate.AddObject('<$DESKTOP>', slSub3);
end;

procedure TTestCase_Firefly_StringCompare.TearDown;
begin
   GetFilePathsWideTemplate.Objects[0].Free;
   GetFilePathsWideTemplate.Objects[1].Free;
   GetFilePathsWideTemplate.Objects[2].Free;
   GetFilePathsWideTemplate.Free;
   inherited TearDown;
end;

procedure TTestCase_Firefly_StringCompare.TestAhoCorasickStringReplaceASimple;
const
   SInput1  = 'Hallo dumme doofe Welt!';
   SOutput1 = 'Hallo schoene bunte Welt!';
var
   slPatterns: TStringList;
   s: string;
begin
   slPatterns := TStringList.Create;
   try
      slPatterns.Add('dumme=schoene');
      slPatterns.Add('doofe=bunte');
      s := AhoCorasickStringReplaceA(SInput1, slPatterns, []);
      CheckTrue(s = SOutput1, 'call');
      Status(s);
   finally
      slPatterns.Free;
   end;
end;

procedure TTestCase_Firefly_StringCompare.TestAhoCorasickStringReplaceAAdvanced;
var
   slPatterns, slSub1, slSub2, slResults: TStringList;
const
   SInput4 = 'Hallo <$MODE> <$MORE> Welt!';
begin
   slPatterns := TStringList.Create;
   slResults := TStringList.Create;
   slSub1 := TStringList.Create;
   slSub2 := TStringList.Create;
   try
      slSub1.Add('schoene');
      slSub1.Add('bunte');
      slSub1.Add('neue');
      slSub2.Add('gelbe');
      slSub2.Add('gruene');
      slPatterns.AddObject('<$MODE>', slSub1);
      slPatterns.AddObject('<$MORE>', slSub2);
      AhoCorasickStringReplaceA(SInput4, slPatterns, [], slResults);
      CheckEquals(6, slResults.Count);
      Status(slResults);
   finally
      slPatterns.Free;
      slResults.Free;
      slSub1.Free;
      slSub2.Free;
   end;
end;

procedure TTestCase_Firefly_StringCompare.TestAhoCorasickStringReplaceAPaths;
const
   SInput5: ansistring = 'Files at <$WINDIR>, not <$DESKTOP>!';
   SInput6             = '<regexpr><$WINDIR>\\[0-9a-f]+';
   SInput7             = '<regexpr><$DESKTOP>\\[0-9a-f]+';
var
   slResults: TStringList;
begin
   slResults := TStringList.Create;
   try
      AhoCorasickStringReplaceA(SInput5, GetFilePathsWideTemplate, [], slResults);
      Status(slResults);
   finally
      slResults.Free;
   end;
   slResults := TStringList.Create;
   try
      AhoCorasickStringReplaceA(SInput6, GetFilePathsWideTemplate, [], slResults);
      Status(slResults);
   finally
      slResults.Free;
   end;
   slResults := TStringList.Create;
   try
      AhoCorasickStringReplaceA(SInput7, GetFilePathsWideTemplate, [], slResults);
      Status(slResults);
   finally
      slResults.Free;
   end;
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsSubString;
begin
   CompareTwoStrings('Hallo Welt', 'Hallo', csSubString, True, True);
   CompareTwoStrings('Hallo Welt', 'Hallo', csSubString, False, True);
   CompareTwoStrings('Hallo Welt', 'hallo', csSubString, False, False);
   CompareTwoStrings('Hallo Welt', 'world', csSubString, False, False);
   CompareTwoStrings('Hallo Welt', 'world', csSubString, True, False);
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsStartingBytes;
begin
   CompareTwoStrings('Hallo Welt', 'Hallo', csStartingBytes, True, True);
   CompareTwoStrings('Hallo Welt', 'Hallo', csStartingBytes, False, True);
   CompareTwoStrings('Hallo Welt', 'hallo', csStartingBytes, False, False);
   CompareTwoStrings('Hallo Welt', 'world', csStartingBytes, False, False);
   CompareTwoStrings('Hallo Welt', 'world', csStartingBytes, True, False);
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsWildcards;
begin
   CompareTwoStrings('Hallo Welt', 'Hallo*', csWildcards, True, True);
   CompareTwoStrings('Hallo Welt', 'Hallo*', csWildcards, False, True);
   CompareTwoStrings('Hallo Welt', '*Welt', csWildcards, False, True);
   CompareTwoStrings('Hallo Welt', 'H*Welt', csWildcards, False, True);
   CompareTwoStrings('Hallo Welt', '????? Welt', csWildcards, False, True);
   CompareTwoStrings('Hallo Welt', 'H?l?? Welt', csWildcards, False, True);
   CompareTwoStrings('Hallo Welt', '?????Welt', csWildcards, False, False);
   CompareTwoStrings('Hallo Welt', 'hallo*', csWildcards, False, False);
   CompareTwoStrings('Hallo Welt', '*world', csWildcards, False, False);
   CompareTwoStrings('Hallo Welt', '*world', csWildcards, True, False);
   CheckTrue(not WildcardCompareString('Safer Networking Limited', 'Surfer Networking*', True), 'WildcardCompareString');
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsBoolean;
begin
   CompareTwoStrings('Hallo Welt', 'Hallo OR Welt', csBoolean, True, True);
   CompareTwoStrings('Hallo Welt', 'Hallo AND Welt', csBoolean, True, True);
   CompareTwoStrings('Hallo Welt', 'hallo OR Welt', csBoolean, False, True);
   CompareTwoStrings('Hallo Welt', 'hallo AND Welt', csBoolean, False, False);
   CompareTwoStrings('Hallo Welt', 'Hallo AND World', csBoolean, True, False);
   CompareTwoStrings('Hallo Welt', 'hallo', csBoolean, True, True);
   CompareTwoStrings('Hallo Welt', 'world', csBoolean, False, False);
   CompareTwoStrings('Hallo Welt', 'Hallo AND Welt OR holla', csBoolean, True, True);
   CompareTwoStrings('Hallo Welt', 'Hallo OR Hello AND Welt', csBoolean, True, True);
   CompareTwoStrings('Hallo Welt', 'Hello OR Hallo AND Welt', csBoolean, True, True);
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsRegExpr;
begin
   CompareTwoStrings('Hallo Welt', '(Hallo )(Welt)', csRegExp, True, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('Hallo Welt', '[Hh]allo', csRegExp, False, True);
   CompareTwoStrings('Hallo Welt', 'Hallo', csRegExp, True, True);
   CompareTwoStrings('Hallo Welt', '[a-z ]*(Welt)', csRegExp, True, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('Hallo Welt', '(Ha[l]*o) We..', csRegExp, False, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('Halo Welt', 'Ha[l]*o We..', csRegExp, False, True);
   CompareTwoStrings('Hallo Welt', 'hallo', csRegExp, False, False);
   CompareTwoStrings('Hallo Welt', '[a-zA-Z ]', csRegExp, False, True);
   CompareTwoStrings('verifydummy11', '[a-z]*11', csRegExp, False, True);
   CompareTwoStrings('192.168.13.2', '([0-9]{1,3})\.([0-9]{3})\.([0-9]*)\.([0-9]*)', csRegExp, False, True);
   CompareTwoStrings('C:\Windows\NOTEPAD.EXE', 'C:\\Windows\\[a-z]*.EXE', csRegExp, True, True);
   CompareTwoStrings('Z:\Tools\Tests\testfiles\SBITests\blindman5.exe', 'Z:\\Tools\\Tests\\testfiles\\SBITests\\([5a-z]*\.exe)', csRegExp, False, True);
   CompareTwoStrings('{0000002F-0000-0000-C000-000000000046}', '\{[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}\}', csRegExp, True, True);
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsPerlRegExpr;
begin
   {$IFDEF UsePerlRegExpr}
   CompareTwoStrings('Hallo Welt', '(Hallo )(Welt)', csPerlRegExp, True, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('Hallo Welt', '[Hh]allo', csPerlRegExp, False, True);
   CompareTwoStrings('Hallo Welt', 'Hallo', csPerlRegExp, True, True);
   CompareTwoStrings('Hallo Welt', '[a-z ]*(Welt)', csPerlRegExp, True, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('Hallo Welt', '(Ha[l]*o) We..', csPerlRegExp, False, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('Halo Welt', 'Ha[l]*o We..', csPerlRegExp, False, True);
   CompareTwoStrings('Hallo Welt', 'hallo', csPerlRegExp, False, False);
   CompareTwoStrings('Hallo Welt', '[a-zA-Z ]', csPerlRegExp, False, True);
   CompareTwoStrings('verifydummy11', '[a-z]*11', csPerlRegExp, False, True);
   CompareTwoStrings('192.168.13.2', '([0-9]{1,3})\.([0-9]{3})\.([0-9]*)\.([0-9]*)', csPerlRegExp, False, True);
   CompareTwoStrings('C:\Windows\NOTEPAD.EXE', 'C:\\Windows\\[a-z]*.EXE', csPerlRegExp, True, True);
   CompareTwoStrings('G:\Tools\Tests\testfiles\SBITests\blindman5.exe', 'G:\\Tools\\Tests\\testfiles\\SBITests\\([5a-z]*\.exe)', csPerlRegExp, False, True);
   Status(RegExpResults.LastResults);
   CompareTwoStrings('{0000002F-0000-0000-C000-000000000046}', '\{[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}\}', csPerlRegExp, True, True);
   {$ENDIF UsePerlRegExpr}
end;

procedure TTestCase_Firefly_StringCompare.TestCompareTwoStringsBoyerMoore;
begin
   CompareTwoStrings('Dies ist die umgebende Hallo Welt Textumgebung!', 'Hallo', csBoyerMoore, True, True);
   CompareTwoStrings('Dies ist die umgebende Hallo Welt Textumgebung!', 'Hallo', csBoyerMoore, False, True);

   CompareTwoStrings('Hallo Welt', 'Hallo', csBoyerMoore, True, True);
   CompareTwoStrings('Hallo Welt', 'Hallo', csBoyerMoore, False, True);
   CompareTwoStrings('Hallo Welt', 'hallo', csBoyerMoore, False, False);
   CompareTwoStrings('Hallo Welt', 'world', csBoyerMoore, False, False);
   CompareTwoStrings('Hallo Welt', 'world', csBoyerMoore, True, False);
   CheckTrue(pkCompareIDString('Z:\Tools\Tests\testfiles\SBITests\blindman5.exe', '<regexpr>Z:\\Tools\\Tests\\testfiles\\SBITests\\([5a-z]*\.exe)', True), 'pkCompareIDString(filename stuff)');
   CheckTrue(pkCompareIDString('{0000002F-0000-0000-C000-000000000046}', '<regexpr>\{[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}\}', True),
      'pkCompareIDString(cslid stuff))');
end;

procedure TTestCase_Firefly_StringCompare.TestLevensthein;
begin
   Status(Format('Hallo vs. Hallo: %d', [Levenshtein('Hallo', 'Hallo')]));
   Status(Format('Hallo vs. Hello: %d', [Levenshtein('Hallo', 'Hello')]));
   Status(Format('Welt vs. World: %d', [Levenshtein('Welt', 'World')]));
   Status(Format('Hallo vs. Salve: %d', [Levenshtein('Hallo', 'Salve')]));
   Status(Format('Bernsteinzimmer vs. Erdbeermarmelade: %d', [Levenshtein('Bernsteinzimmer', 'Erdbeermarmelade')]));
   Status(Format('Hey vs. Hi: %d', [Levenshtein('Hey', 'Hi')]));
end;

 //procedure TTestCase_Firefly_StringCompare.TestAhoCorasickStringReplaceA;
 //const
//   SInput2 = 'File:"<$FILE_LIBRARY>","<$SYSDIR>\midepoba.dll","filesize>=20000,filesize<=750000,build>=20080128,ignore=2,filetype=PE,exists[authx509]=0,fastclose[handle]=1,silentregreboot=1,setenv=Virtummonde:midepoba.dll"';
//   SOutput2 = 'File:"<$FILE_LIBRARY>","<$SYSDIR>\midepoba.dll","size[file]>=20000,size[file]<=750000,build>=20080128,ignore=2,type[file]=PE,exists[authx509]=0,fastclose[handle]=1,addsilent[reboot]=1,set[env]=Virtummonde:midepoba.dll"';
//   SInput3 = 'File:"<$FILE_LIBRARY>","<$SYSDIR>\midepoba.dll","pM>=20000,pM<=750000,build>=20080128,ignore=2,xm=PE,rF=0,handle.fastclose=1,jb=1,pX=Virtummonde:midepoba.dll"';
//   SOutput3 = 'File:"<$FILE_LIBRARY>","<$SYSDIR>\midepoba.dll","file.size>=20000,file.size<=750000,build>=20080128,ignore=2,file.type=PE,authx509.exists=0,handle.fastclose=1,reboot.addSilent=1,env.set=Virtummonde:midepoba.dll"';
 //   SInput4 = 'Hallo <$MODE> <$MORE> Welt!';
 //var
 //   slPatterns: TStringList;
 //   s: string;
 //begin
 //slPatterns := TStringList.Create;
 //try
 //   Status(SInput2);
 //   SBIUpgradeLinesBuildTransformers(slPatterns);
 //   s := AhoCorasickSpybotAFPStringReplaceA(SInput2, slPatterns, []);
 //   CheckTrue(s = SOutput2, 'call');
 //finally
 //   slPatterns.Free;
 //end;
 //slPatterns := TStringList.Create;
 //try
 //   Status(SInput3);
 //   SBIUpgradeOOLinesBuildTransformers(slPatterns);
 //   (*
 //     slPatterns.SaveToFile('C:\ooupgrade-1.txt');
 //     slPatterns.Clear;
 //     SBIUpgradeOOLinesBuildTransformers(slPatterns);
 //     slPatterns.SaveToFile('C:\ooupgrade-2.txt');
 //   *)
 //   s := AhoCorasickSpybotAFPStringReplaceA(SInput3, slPatterns, []);
 //   CheckTrue(s = SOutput3, 'call', SOutput3, s);
 //   Status(s);
 //finally
 //   slPatterns.Free;
 //end;


//end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_StringCompare.Suite);

end.
