{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for PepiMK.FileFormats.Base.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-09  jh  ---  added Test from snlFFBaseTest
// 2017-03-06  pk  ---  Test case created by PascalHeaderUpdate
// *****************************************************************************
   )
}

unit SimpleFileReader.Base.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Firefly.Tests,
   SimpleFileReader.Base;

type

   {
     TTestCase_SimpleFileReader_Base is a test class for classes, functions and procedures
     from unit PepiMK.FileFormats.Base.
   }
   TTestCase_SimpleFileReader_Base = class(TFireflyTestCase)
   published
      {
        TestCompile is a dummy that simply fills in code to check if
        this test unit, and the associated tested unit, get compiled.
      }
      procedure TestCompile;
      procedure snlFFBaseTestExecute;
   end;

implementation

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   SysUtils;

{ TTestCase_SimpleFileReader_Base }

procedure TTestCase_SimpleFileReader_Base.TestCompile;
begin
   CheckTrue(True);
   // TODO : add real tests to this class
end;

procedure TTestCase_SimpleFileReader_Base.snlFFBaseTestExecute;
var
   ff: TCustomFileFormat;
begin
   //DebugLogger.LogEnterMethod(nil, 'snlFFBaseTestExecute');
   try
      try
         ff := TCustomFileFormat.Create;
         try
            CheckEquals(False, not ff.Valid, 'TCustomFileFormat.Valid');
         finally
            FreeAndNil(ff);
         end;
      finally
         //DebugLogger.LogLeaveMethod(nil, 'snlFFBaseTestExecute');
      end;
   except
      on E: Exception do begin
         //snlAddTestWarning(E.Message);
      end;
   end;
end;

initialization

   RegisterTest('FireflyCore', TTestCase_SimpleFileReader_Base.Suite);

end.
