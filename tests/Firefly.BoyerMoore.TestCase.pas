﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for Firefly.BoyerMoore.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2013-06-06  pk  10m  Ported to Dunit
// 2007-10-16  --  ---  Created by snlCodeTest
// *****************************************************************************
   )
}

unit Firefly.BoyerMoore.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Firefly.Tests,
   Firefly.BoyerMoore;

type

   TTestCase_Firefly_BoyerMoore = class(TFireflyTestCase)
   private
      function GetTestSample1Filename: string;
      function GetTestSample2Filename: string;
      procedure DoTestTextSearchClass(sc: TBMSearchAlgorithmClass);
      function TryTextInStream(const AFilename: string; SearchString: ansistring; const cTextStartOffset, cTextEndOffset: cardinal; var FoundPos: cardinal; sc: TBMSearchAlgorithmClass): boolean;
      procedure DoTestFileSearchClass(sc: TBMSearchAlgorithmClass);
      procedure DoTestStreamSearchClass(sc: TBMSearchAlgorithmClass);
   published
      procedure TestSamplesExist;
      procedure TestBoyerMooreSNLTemp;
      procedure TestBoyerMooreSNL2;
      procedure TestBoyerMooreJS;
      procedure TestBMPos;
      procedure TestBMStringReplace1;
      procedure TestBMStringReplaceUnicode;
   end;

implementation

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   SysUtils;

function BinaryToStringA(const ABinaryText: ansistring; const ASeparatorChar: ansichar): ansistring;

   function HexToNibble(const AHexCharText: ansichar): byte;
   var
      cChar: ansichar;
   begin
      cChar := UpCase(AHexCharText);
      if cChar = '0' then begin
         Result := byte(0);
      end else if cChar in ['1' .. '9'] then begin
         Result := Ord(cChar) - 48;
      end else if cChar in ['A' .. 'F'] then begin
         Result := Ord(cChar) - 55;
      end else begin
         Result := byte(0);
      end;

   end;

   function HexToByte(const AHexByteText: ansistring): byte;
   begin
      if Length(AHexByteText) <> 2 then begin
         Result := 0;
      end else begin
         Result := byte(HexToNibble(AHexByteText[2]) + (HexToNibble(AHexByteText[1]) shl 4));
      end;
   end;

var
   iIncrementor: integer;
   iChar: integer;
   sByte: ansistring;
begin
   Result := '';
   if ASeparatorChar = #0 then begin
      iIncrementor := 2;
   end else begin
      iIncrementor := 3;
   end;
   iChar := 1;
   while (iChar < Length(ABinaryText)) do begin
      sByte := Copy(ABinaryText, iChar, 2);
      Result := Result + ansichar(Chr(HexToByte(sByte)));
      Inc(iChar, iIncrementor);
   end;
end;

{ TTestCase_Firefly_BoyerMoore }

procedure TTestCase_Firefly_BoyerMoore.TestBoyerMooreSNL2;
begin
   DoTestTextSearchClass(TBoyerMoorePepiMK);
   DoTestFileSearchClass(TBoyerMoorePepiMK);
   DoTestStreamSearchClass(TBoyerMoorePepiMK);
end;

function TTestCase_Firefly_BoyerMoore.GetTestSample1Filename: string;
begin
   Result := ExtractFilePath(ParamStr(0)) + 'testfiles\Backdoor.Rbot.BMS.bin';
end;

function TTestCase_Firefly_BoyerMoore.GetTestSample2Filename: string;
begin
   Result := ExtractFilePath(ParamStr(0)) + 'testfiles\Trojan.Vundo.Gen.1.bin';
end;

procedure TTestCase_Firefly_BoyerMoore.TestBMPos;
var
   i: integer;
begin
   i := BMPos('23', 'Hallo23Welt');
   CheckEquals(i, 6, 'BMPos(''23'', ''Hallo23Welt'')');
   i := BMPos('.exe', 'notepad.exe');
   CheckEquals(i, 8, 'BMPos(''.exe'', ''notepad.exe'')');
   i := BMPos('.exe', 'notepad.exe -param');
   CheckEquals(i, 8, 'BMPos(''.exe'', ''notepad.exe -param'')');
end;

procedure TTestCase_Firefly_BoyerMoore.TestBMStringReplace1;
const
   SIn: ansistring  = 'Hallo gelbe Welt';
   Sout: ansistring = 'Hallo blaue Welt';
var
   s: ansistring;
begin
   s := BMStringReplace(SIn, 'gelbe', 'blaue', [rfReplaceAll]);
   CheckEquals(s, Sout);
end;

procedure TTestCase_Firefly_BoyerMoore.TestBMStringReplaceUnicode;
const
   SIn: ansistring     = 'Hallo gelbe Welt';
   Sout: unicodestring = 'Hallo grüne Welt';
var
   s: ansistring;
begin
   s := BMStringReplace(SIn, 'gelbe', 'grüne', [rfReplaceAll]);
   {$IFDEF Unicode}
   Check(s = ansistring(Sout));
   {$ELSE Unicode}
   CheckEquals(s, UTF8Encode(Sout));
   {$ENDIF Unicode}
end;

procedure TTestCase_Firefly_BoyerMoore.DoTestFileSearchClass(sc: TBMSearchAlgorithmClass);
var
   c: cardinal;
begin
   c := 0;
   CheckTrue(TryTextInStream(GetTestSample1Filename, '/sdbot.n3.net', 148000, 148450, c, sc), 'TryTextInStream method >= 148000 ("/sdbot.n3.net") @ ');
   CheckTrue(TryTextInStream(GetTestSample1Filename, '/sdbot.n3.net', 148230, 148450, c, sc), 'TryTextInStream method >= 148230 ("/sdbot.n3.net") @ ');
   CheckTrue(TryTextInStream(GetTestSample1Filename, 'http://sdbot.', 140005, 148450, c, sc), 'TryTextInStream method ("http://sdbot.") @ ');
   CheckTrue(TryTextInStream(GetTestSample1Filename, 'http://sdbot.n3.net', 148200, 148450, c, sc), 'TryTextInStream method ("http://sdbot.n3.net") @ ');
   CheckTrue(TryTextInStream(GetTestSample1Filename, 'FINAL WORKING FOR PLAYSTATION', 140000, 148450, c, sc), 'TryTextInStream method ("FINAL WORKING FOR PLAYSTATION") @ ');
   CheckTrue(TryTextInStream(GetTestSample1Filename, 'FINAL WORKING FOR PLAYSTATION', 0, 148450, c, sc), 'TryTextInStream method ("FINAL WORKING FOR PLAYSTATION") @ ');
   CheckTrue(TryTextInStream(GetTestSample1Filename, 'c:\sirh0t32.pif', 140000, 148450, c, sc), 'TryTextInStream method ("c:\sirh0t32.pif") @ ');
end;

procedure TTestCase_Firefly_BoyerMoore.TestSamplesExist;
begin
   Check(FileExists(GetTestSample1Filename), 'Sample 1 missing');
   Check(FileExists(GetTestSample2Filename), 'Sample 2 missing');
end;

procedure TTestCase_Firefly_BoyerMoore.DoTestStreamSearchClass(sc: TBMSearchAlgorithmClass);

   procedure TestFile(const Filename: string; const StartPos, EndPos: cardinal; const BinText: ansistring);
   var
      c: cardinal;
      b: boolean;
   begin
      c := 0;
      b := TryTextInStream(Filename, BinaryToStringA(BinText, #0), StartPos, EndPos, c, sc);
      CheckTrue(b, 'TryTextInStream');
      // if b then begin
      // snlAddTestInformation('Position: ' + IntToHex(c, 8));
      // end;
   end;

var
   sTerm: ansistring;
begin
   // fails: 2nd, 3rd, 8th
   sTerm := '594F594F2E444C4C004372656174654D61696E50726F630043726561746550726F7465637450726F6300446C6C43616E556E6C6F61644E6F7700'; // '';
   TestFile(GetTestSample2Filename, 1, 290000, sTerm);
   TestFile(GetTestSample2Filename, 2, 290000, sTerm);
   TestFile(GetTestSample2Filename, 3, 290000, sTerm);
   TestFile(GetTestSample2Filename, 4, 290000, sTerm);
   TestFile(GetTestSample2Filename, 5, 290000, sTerm);
   TestFile(GetTestSample2Filename, 6, 290000, sTerm);
   TestFile(GetTestSample2Filename, 7, 290000, sTerm);
   TestFile(GetTestSample2Filename, 8, 290000, sTerm);
end;

procedure TTestCase_Firefly_BoyerMoore.TestBoyerMooreSNLTemp;
var
   bm: TBMSearchAlgorithm;
   ms: TMemoryStream;
   s: ansistring;
begin
   s := 'Alle meine Enten abbabab blub!';
   bm := TBoyerMoorePepiMK.Create;
   bm.FindFirstOnly := True;
   ms := TMemoryStream.Create;
   try
      ms.Write(s[1], Length(s));
      // good tests
      bm.Search(ms, 'abbabab');
      CheckTrue(bm.LastPosition > 0, string(bm.Text) + ' (in: ' + string(s) + ')');
      bm.Search(s, 'abbabab');
      CheckTrue(bm.LastPosition > 0, string(bm.Text) + ' (in: ' + string(s) + ')');
   finally
      ms.Free;
      bm.Free;
   end;
end;

procedure TTestCase_Firefly_BoyerMoore.DoTestTextSearchClass(sc: TBMSearchAlgorithmClass);
var
   bm: TBMSearchAlgorithm;
   ms: TMemoryStream;
   s: ansistring;
begin
   s := 'MAN dingsda PANMAN ANPANMAN hallo welt';
   bm := sc.Create;
   bm.FindFirstOnly := True;
   ms := TMemoryStream.Create;
   try
      ms.Write(s[1], Length(s));
      // good tests
      bm.Search(ms, 'ANPANMAN');
      CheckTrue(bm.LastPosition > 0, string(bm.Text) + ' (in: ' + string(s) + ')');
      bm.Search(s, 'ANPANMAN');
      CheckTrue(bm.LastPosition > 0, string(bm.Text) + ' (in: ' + string(s) + ')');
      // bad tests
      bm.Search(ms, 'xNPxNMxN');
      CheckTrue(bm.LastPosition < 1, string(bm.Text) + ' (in: ' + string(s) + ')');
      bm.Search(s, 'xNPxNMxN');
      CheckTrue(bm.LastPosition < 1, string(bm.Text) + ' (in: ' + string(s) + ')');
   finally
      ms.Free;
      bm.Free;
   end;

   s := 'Dies ist die umgebende Hallo Welt Textumgebung!';
   bm := sc.Create;
   bm.FindFirstOnly := True;
   ms := TMemoryStream.Create;
   try
      ms.Write(s[1], Length(s));
      // good tests
      bm.Search(ms, 'Hallo Welt');
      CheckTrue(bm.LastPosition > 0, string(bm.Text) + ' (in: ' + string(s) + ')');
      // bad tests
      bm.Search(ms, 'Hallo World');
      CheckTrue(bm.LastPosition < 1, string(bm.Text) + ' (in: ' + string(s) + ')');
   finally
      ms.Free;
      bm.Free;
   end;

   s := 'xHallo Welt';
   bm := sc.Create;
   bm.FindFirstOnly := True;
   ms := TMemoryStream.Create;
   try
      ms.Write(s[1], Length(s));
      // good tests
      bm.Search(ms, 'Hallo Welt');
      CheckTrue(bm.LastPosition >= 0, string(bm.Text) + ' (in: ' + string(s) + ')');
      // bad tests
      bm.Search(ms, 'Hallo World');
      CheckTrue(bm.LastPosition < 1, string(bm.Text) + ' (in: ' + string(s) + ')');
   finally
      ms.Free;
      bm.Free;
   end;
end;

procedure TTestCase_Firefly_BoyerMoore.TestBoyerMooreJS;
begin
   // TestTextSearchClass(TBoyerMooreJS);
   DoTestFileSearchClass(TBoyerMooreJS);
   DoTestStreamSearchClass(TBoyerMooreJS);
end;

function TTestCase_Firefly_BoyerMoore.TryTextInStream(const AFilename: string; SearchString: ansistring; const cTextStartOffset, cTextEndOffset: cardinal;
   var FoundPos: cardinal; sc: TBMSearchAlgorithmClass): boolean;
var
   fs: TFileStream;
   ms: TMemoryStream;
   cCopyCount: cardinal;
   jsTextSearch: TBMSearchAlgorithm;
begin
   Result := False;
   FoundPos := 0;
   if (cTextEndOffset > cTextStartOffset) and (cTextEndOffset > 0) then begin
      fs := TFileStream.Create(AFilename, fmOpenRead or fmShareDenyNone);
      try
         ms := TMemoryStream.Create;
         try
            fs.Seek(cTextStartOffset, soFromBeginning);
            cCopyCount := cTextEndOffset - cTextStartOffset + 1;
            if cCopyCount > fs.Size - fs.Position then begin
               cCopyCount := fs.Size - fs.Position;
            end;
            ms.CopyFrom(fs, cCopyCount);
            ms.Seek(0, soFromBeginning);
            jsTextSearch := sc.Create;
            try
               jsTextSearch.FindFirstOnly := True;
               jsTextSearch.Search(ms, SearchString);
               Result := jsTextSearch.LastPosition > 0;
               if Result then begin
                  FoundPos := cardinal(jsTextSearch.LastPosition) + cTextStartOffset;
               end;
            finally
               jsTextSearch.Free;
            end;
         finally
            ms.Free;
         end;
      finally
         fs.Free;
      end;
   end;
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_BoyerMoore.Suite);

end.
