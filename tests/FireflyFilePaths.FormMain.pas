unit FireflyFilePaths.FormMain;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ComCtrls,
   Generics.Collections,
   Firefly.FilePaths.FillersBase,
   Firefly.FilePaths.FillersWindows,
   OVM.Listview.Attributes,
   OVM.ListView;

type

   { TPathEntry }

   TPathEntry = class
   private
      FPath: string;
      FShortcut: string;
      FSources: TPathSources;
      FTemplateName: string;
   published
      property TemplateName: string read FTemplateName;
      property Shortcut: string read FShortcut;
      property Path: string read FPath;
      property Sources: TPathSources read FSources;
   end;

   { TPathEntryList }

   TPathEntryList = class(TObjectList<TPathEntry>)
   end;


   { TFormFireflyFilePaths }

   TFormFireflyFilePaths = class(TForm)
      lv: TListView;
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      FPaths: TPathEntryList;
   public

   end;

var
   FormFireflyFilePaths: TFormFireflyFilePaths;

implementation

{$R *.lfm}

uses
   Firefly.FilePaths;

   { TFormFireflyFilePaths }

procedure TFormFireflyFilePaths.FormShow(Sender: TObject);
var
   i, iPath: integer;
   pl: TPathList;
   ps: TPathSource;
   pe: TPathEntry;
begin
   for i := 0 to Pred(PathTemplates.Count) do begin
      pl := PathTemplates.PathByIndex[i];
      //m.Lines.Add(pl.BracketedTemplateName + ' (' + pl.ShortcutName + ')');
      for iPath := 0 to Pred(pl.Count) do begin
         pe := TPathEntry.Create;
         pe.FTemplateName := pl.TemplateName;
         pe.FShortcut := pl.ShortcutName;
         pe.FPath := pl.Path[iPath].Path;
         pe.FSources:=pl.Path[iPath].Sources;
         FPaths.Add(pe);
      end;
   end;
   lv.DisplayItems<TPathEntry>(FPaths);
end;

procedure TFormFireflyFilePaths.FormCreate(Sender: TObject);
begin
   FPaths := TPathEntryList.Create;
end;

procedure TFormFireflyFilePaths.FormDestroy(Sender: TObject);
begin
   FPaths.Free;
end;

end.
