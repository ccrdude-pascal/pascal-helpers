﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for Firefly.AhoCorasick.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-04-03  pk   5m  Updated to use test files inside package repository
// 2016-08-30  pk  30m  Improved test to better identify generic FPC issue
// 2016-06-27  jh  ---  FindAllInFileText fails because of case Sensitivity
// 2013-04-23  pk  10m  Ported to Dunit
// 2007-08-29  --  ---  Created by snlCodeTest
// *****************************************************************************
   )
}

unit Firefly.AhoCorasick.TestCase;

{$IFDEF FPC}
{$CODEPAGE UTF8}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Firefly.Tests,
   Firefly.AhoCorasick;

type

   { TTestCase_Firefly_AhoCorasick }

   TTestCase_Firefly_AhoCorasick = class(TFireflyTestCase)
   private
      procedure DoFindAllInFileStream(AFilenameBase: string);
      procedure DoFindAllInFileText(AFilenameBase: string);
      function GetTestSample1Filename(AFilenameBase: string): string;
      function GetTestSample1KeywordsFilename(AFilenameBase: string): string;
      function GetTestSample1ResultsBinaryFilename(AFilenameBase: string): string;
      function GetTestSample1ResultsTextFilename(AFilenameBase: string): string;
      procedure FindInStream(const ADescription: string; const AKeywords: TStrings; AContent: TStream; AExpected: TStrings);
      procedure FindInText(const ADescription: string; const AKeywords, AContent, AExpected: TStrings);
      procedure DoTestSamplesExist(AFilenameBase: string);
   protected
   published
      procedure TestFindAllInText;
      procedure TestKaramasovFindAllInFileText;
      procedure TestKaramasovFindAllInFileStream;
   end;

implementation

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   SysUtils;

{ TTestCase_Firefly_AhoCorasick }

procedure TTestCase_Firefly_AhoCorasick.FindInStream(const ADescription: string; const AKeywords: TStrings; AContent: TStream; AExpected: TStrings);

   procedure SaveTStringsFormatted(AList, APositions: TStringList);
   var
      i: integer;
      slFormatted: TStringList;
   begin
      slFormatted := TStringList.Create;
      try
         for i := 0 to Pred(AList.Count) do begin
            slFormatted.Add(APositions[i] + '=' + AList[i]);
         end;
         {$IFDEF FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results, FreePascal, Stream).txt');
         {$ELSE FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results, Delphi, Stream).txt');
         {$ENDIF FPC}
      finally
         CheckEquals(AExpected.Count, slFormatted.Count, 'results count');
         CheckEquals(AExpected.Text, slFormatted.Text, 'results content');
         slFormatted.Free;
      end;
   end;

   procedure SaveTStringSearchResultListFormatted(AList: TStringSearchResultList);
   var
      i: integer;
      slFormatted: TStringList;
   begin
      slFormatted := TStringList.Create;
      try
         for i := 0 to Pred(AList.Count) do begin
            slFormatted.Add(IntToStr(AList[i].Position) + '=' + AList[i].SearchTerm);
         end;
         {$IFDEF FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results objectlist, FreePascal, Stream).txt');
         {$ELSE FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results objectlist, Delphi, Stream).txt');
         {$ENDIF FPC}
      finally
         CheckEquals(AExpected.Count, slFormatted.Count, 'results count');
         CheckEquals(AExpected.Text, slFormatted.Text, 'results content');
         slFormatted.Free;
      end;
   end;

var
   ac: TAhoCorasick;
   slTemp: TStringList;
   slPositions: TStringList;
   slResults: TStringSearchResultList;
   sKeywordsPreview: string;
begin
   sKeywordsPreview := 'Keywords: ' + StringReplace(Trim(AKeywords.Text), #13#10, ',', [rfReplaceAll]);
   if Length(sKeywordsPreview) > 58 then begin
      sKeywordsPreview := Copy(sKeywordsPreview, 1, 55) + '...';
   end;
   Status(sKeywordsPreview);
   slTemp := TStringList.Create;
   slPositions := TStringList.Create;
   slResults := TStringSearchResultList.Create;
   try
      ac := TAhoCorasick.Create(AKeywords);
      try
         ac.CaseSensitive := False;
         AContent.Seek(0, soFromBeginning);
         ac.FindInStream(AContent, True, slTemp, slPositions);
         AContent.Seek(0, soFromBeginning);
         ac.FindInStream(AContent, True, slResults);
      finally
         ac.Free;
      end;
      Status('TAhoCorasick.FindInStream result: ' + IntToStr(slTemp.Count));
      CheckEquals(slTemp.Count, slResults.Count);
      SaveTStringsFormatted(slTemp, slPositions);
      SaveTStringSearchResultListFormatted(slResults);
   finally
      slTemp.Free;
      slPositions.Free;
      slResults.Free;
   end;
end;

procedure TTestCase_Firefly_AhoCorasick.DoFindAllInFileText(AFilenameBase: string);
var
   slKeywords, slContent, slExpected: TStringList;
begin
   slKeywords := TStringList.Create;
   slContent := TStringList.Create;
   slExpected := TStringList.Create;
   try
      slContent.LoadFromFile(GetTestSample1Filename(AFilenameBase));
      slKeywords.LoadFromFile(GetTestSample1KeywordsFilename(AFilenameBase));
      slExpected.LoadFromFile(GetTestSample1ResultsTextFilename(AFilenameBase));
      FindInText(AFilenameBase, slKeywords, slContent, slExpected);
   finally
      slExpected.Free;
      slContent.Free;
      slKeywords.Free;
   end;
end;

procedure TTestCase_Firefly_AhoCorasick.TestKaramasovFindAllInFileText;
begin
   DoFindAllInFileText('Karamasov');
end;

procedure TTestCase_Firefly_AhoCorasick.TestFindAllInText;
var
   slKeywords, slContent, slExpected: TStringList;
begin
   slKeywords := TStringList.Create;
   slContent := TStringList.Create;
   slExpected := TStringList.Create;
   try
      slKeywords.Text := 'doof';
      slContent.Text := 'Hallo doofe Welt';
      slExpected.Text := '7=doof';
      FindInText('Hallo Welt', slKeywords, slContent, slExpected);
      slKeywords.Text := 'Hallo' + #13#10 + 'Welt' + #13#10 + 'doof' + #13#10 + 'dog';
      slContent.Text := 'Diese eine Welt ist eine doofe. Hallo da draußen! Wie geht''s? And don''t forget to feed the dog!';
      // In UTF-8, the "ß" takes two btes
      slExpected.Text := '12=Welt' + #13#10 + '26=doof' + #13#10 + '33=Hallo' + #13#10 + '93=dog';
      FindInText('hallo, doof & dog', slKeywords, slContent, slExpected);
   finally
      slExpected.Free;
      slContent.Free;
      slKeywords.Free;
   end;
end;

function TTestCase_Firefly_AhoCorasick.GetTestSample1Filename(AFilenameBase: string): string;
begin
   Result := ExtractFilePath(ParamStr(0)) + Format('testfiles\%s.txt', [AFilenameBase]);
end;

function TTestCase_Firefly_AhoCorasick.GetTestSample1KeywordsFilename(AFilenameBase: string): string;
begin
   Result := ExtractFilePath(ParamStr(0)) + Format('testfiles\%s (keywords).txt', [AFilenameBase]);
end;

function TTestCase_Firefly_AhoCorasick.GetTestSample1ResultsBinaryFilename(AFilenameBase: string): string;
begin
   Result := ExtractFilePath(ParamStr(0)) + Format('testfiles\%s (results).txt', [AFilenameBase]);
end;

function TTestCase_Firefly_AhoCorasick.GetTestSample1ResultsTextFilename(AFilenameBase: string): string;
begin
   {$IFDEF Unicode}
   Result := ExtractFilePath(ParamStr(0)) + Format('testfiles\%s (results-unicode).txt', [AFilenameBase]);
   {$ELSE Unicode}
   Result := ExtractFilePath(ParamStr(0)) + Format('testfiles\%s (results).txt', [AFilenameBase]);
   {$ENDIF Unicode}
end;

procedure TTestCase_Firefly_AhoCorasick.FindInText(const ADescription: string; const AKeywords, AContent, AExpected: TStrings);

   procedure SaveTStringsFormatted(AList, APositions: TStringList);
   var
      i: integer;
      slFormatted: TStringList;
   begin
      slFormatted := TStringList.Create;
      try
         for i := 0 to Pred(AList.Count) do begin
            slFormatted.Add(APositions[i] + '=' + AList[i]);
         end;
         {$IFDEF FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results, FreePascal, Text).txt');
         {$ELSE FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results, Delphi, Text).txt');
         {$ENDIF FPC}
      finally
         CheckEquals(AExpected.Count, slFormatted.Count, 'results count');
         CheckEquals(AExpected.Text, slFormatted.Text, 'results content');
         slFormatted.Free;
      end;
   end;

   procedure SaveTStringSearchResultListFormatted(AList: TStringSearchResultList);
   var
      i: integer;
      slFormatted: TStringList;
   begin
      slFormatted := TStringList.Create;
      try
         for i := 0 to Pred(AList.Count) do begin
            slFormatted.Add(IntToStr(AList[i].Position) + '=' + AList[i].SearchTerm);
         end;
         {$IFDEF FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results objectlist, FreePascal, Text).txt');
         {$ELSE FPC}
         slFormatted.SaveToFile(ExtractFilePath(GetTestSample1ResultsBinaryFilename(ADescription)) + ADescription + ' (results objectlist, Delphi, Text).txt');
         {$ENDIF FPC}
      finally
         CheckEquals(AExpected.Count, slFormatted.Count, 'results count');
         CheckEquals(AExpected.Text, slFormatted.Text, 'results content');
         slFormatted.Free;
      end;
   end;

var
   ac: TAhoCorasick;
   slTemp: TStringList;
   slPositions: TStringList;
   slResults: TStringSearchResultList;
   sContentPreview: string;
   sKeywordsPreview: string;
begin
   sContentPreview := 'Content: ' + StringReplace(Trim(AContent.Text), #13#10, ' ', [rfReplaceAll]);
   if Length(sContentPreview) > 58 then begin
      sContentPreview := Copy(sContentPreview, 1, 55) + '...';
   end;
   Status(sContentPreview);
   sKeywordsPreview := 'Keywords: ' + StringReplace(Trim(AKeywords.Text), #13#10, ',', [rfReplaceAll]);
   if Length(sKeywordsPreview) > 58 then begin
      sKeywordsPreview := Copy(sKeywordsPreview, 1, 55) + '...';
   end;
   Status(sKeywordsPreview);
   slTemp := TStringList.Create;
   slPositions := TStringList.Create;
   slResults := TStringSearchResultList.Create;
   try
      ac := TAhoCorasick.Create(AKeywords);
      try
         ac.CaseSensitive := False;
         {$IFDEF Unicode}
         ac.FindAll(UTF8Encode(AContent.Text), slTemp);
         ac.FindAll(UTF8Encode(AContent.Text), slResults);
         {$ELSE Unicode}
         ac.FindAll(AContent.Text, slTemp, slPositions);
         ac.FindAll(AContent.Text, slResults);
         {$ENDIF Unicode}
      finally
         ac.Free;
      end;
      Status('TAhoCorasick.FindAll result: ' + IntToStr(slTemp.Count));
      CheckEquals(slTemp.Count, slResults.Count);
      SaveTStringsFormatted(slTemp, slPositions);
      SaveTStringSearchResultListFormatted(slResults);

      //if FindCmdLineSwitch('v', ['-', '/'], true) then begin
      //   Status(AExpected.Text);
      //   Status(slFormatted.Text);
      //end;
   finally
      slTemp.Free;
      slPositions.Free;
      slResults.Free;
   end;
end;

procedure TTestCase_Firefly_AhoCorasick.DoTestSamplesExist(AFilenameBase: string);
begin
   Check(FileExists(GetTestSample1Filename(AFilenameBase)), 'Sample 1 missing');
   Check(FileExists(GetTestSample1KeywordsFilename(AFilenameBase)), 'Sample 1 keywords missing');
   Check(FileExists(GetTestSample1ResultsBinaryFilename(AFilenameBase)), 'Sample 1 results missing');
end;

procedure TTestCase_Firefly_AhoCorasick.DoFindAllInFileStream(AFilenameBase: string);
var
   slKeywords, slContent, slExpected: TStringList;
   fs: TFileStream;
begin
   //Fail('This currently fails finding "Fräulein"');
   slKeywords := TStringList.Create;
   slContent := TStringList.Create;
   slExpected := TStringList.Create;
   try
      fs := TFileStream.Create(GetTestSample1Filename(AFilenameBase), fmOpenRead or fmShareDenyNone);
      try
         fs.Seek(0, soFromBeginning);
         slKeywords.LoadFromFile(GetTestSample1KeywordsFilename(AFilenameBase));
         slExpected.LoadFromFile(GetTestSample1ResultsBinaryFilename(AFilenameBase));
         FindInStream(AFilenameBase, slKeywords, fs, slExpected);
      finally
         fs.Free;
      end;
   finally
      slExpected.Free;
      slContent.Free;
      slKeywords.Free;
   end;
end;

procedure TTestCase_Firefly_AhoCorasick.TestKaramasovFindAllInFileStream;
begin
   DoFindAllInFileStream('Karamasov');
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_AhoCorasick.Suite);

end.
