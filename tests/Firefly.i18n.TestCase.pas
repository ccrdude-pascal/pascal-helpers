{
   @author(Joachim Hengelbrock)
  @abstract(Test case for PepiMK.Localizer)

   @preformatted(
// *****************************************************************************
// Copyright: © 2016-2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-06-17  jh  --  Wrote test case
// *****************************************************************************
   )
}

unit Firefly.i18n.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses

   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.i18n;

type

   TTestCase_Firefly_i18n = class(TFireflyTestCase)
   published
      procedure TestGetCurrentLanguage;
      procedure TestTranslation;
   end;

implementation

//uses
//   PepiMK.Debug,
//   PepiMK.Debug.Consts;

procedure TTestCase_Firefly_i18n.TestGetCurrentLanguage;
var
   AResult: Ansistring;
begin
   UseLanguage('en_IE');
   AResult := GetCurrentLanguage;
   {$IFDEF FPC}
   CheckEquals('en_IE', AResult, 'GetCurrentLanguage');
   {$ELSE FPC}
   CheckEquals('de_DE', AResult, 'GetCurrentLanguage');
   {$ENDIF FPC}
end;

resourcestring
  SGreeting = 'Hello World';

procedure TTestCase_Firefly_i18n.TestTranslation;
begin
   UseLanguage('de');
   // this works by taking the translation from PepiMKBaseTestsFPUI.de.po
   CheckEquals('Hallo Welt', SGreeting);
   CheckEquals('Hallo Welt', _(SGreeting));
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_i18n.Suite);

end.
