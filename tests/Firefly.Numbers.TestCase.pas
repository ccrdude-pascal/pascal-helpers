{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Test case for PepiMK.Numbers.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2003-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-04  pk  10m  Wrote test case
// *****************************************************************************
   )
}

unit Firefly.Numbers.TestCase;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.Numbers;

type

   TTestCase_Firefly_Numbers = class(TFireflyTestCase)
   published
      procedure TestIntToStr;
      procedure TestNibbleToHex;
      procedure TestByteToHex;
      procedure TestByte2KToShortIntHex;
      procedure TestPreviewChrOrHex;
      procedure TestSwap;
      procedure TestBinaryTo;
      procedure TestStringTo;
      procedure TestHexTo;
      procedure TestOctalTo;
      procedure TestDoubleTo;
      procedure TestByteTo;
      procedure TestLongIntTo;
      procedure TestLongWordTo;
      procedure TestCardinalTo;
      procedure TestInt64To;
   end;

implementation

{ TTestCase_Firefly_Numbers }

procedure TTestCase_Firefly_Numbers.TestIntToStr;
const
   ITestInteger: integer = 4711;
   ITestWord: word = 4711;
   ITestCardinal: cardinal = 4711;
   ITestInt64: int64 = 4711;
   ITestUInt64: UInt64 = 4711;
begin
   CheckEquals(snlIntToStr(ITestInteger), '4711');
   CheckEquals(snlIntToStr(ITestWord), '4711');
   CheckEquals(snlIntToStr(ITestCardinal), '4711');
   CheckEquals(snlIntToStr(ITestInt64), '4711');
   CheckEquals(snlIntToStr(ITestUInt64), '4711');
end;

procedure TTestCase_Firefly_Numbers.TestNibbleToHex;
begin
   CheckEquals(NibbleToHex(10), 'A');
   CheckEquals(NibbleToHexA(10), 'A');
   CheckEquals(NibbleToHexW(10), 'A');
end;

procedure TTestCase_Firefly_Numbers.TestByteToHex;
begin
   CheckEquals(ByteToHex(10), '0A');
   CheckEquals(ByteToHexA(10), '0A');
   CheckEquals(ByteToHexW(10), WideString('0A'));
   CheckEquals(ByteToHexLA(10), '0a');
end;

procedure TTestCase_Firefly_Numbers.TestByte2KToShortIntHex;
begin
   CheckEquals(Byte2KToShortIntHex($F0), '-10');
   CheckEquals(Byte2KToShortIntHexA($F0), '-10');
   CheckEquals(Byte2KToShortIntHexW($F0), WideString('-10'));
end;

procedure TTestCase_Firefly_Numbers.TestPreviewChrOrHex;
begin
   CheckEquals('<$1C>', PreviewChrOrHex(28), 'PreviewChrOrHex');
   CheckEquals('S', PreviewChrOrHex($53), 'PreviewChrOrHex');
end;

procedure TTestCase_Firefly_Numbers.TestSwap;
begin
   //CheckEquals(Int64ToHex(4294), '10C6', 'Int64ToHex');
   CheckEquals($01000000, CardinalSwap($00000001), 'CardinalSwap(1)');
   {$R-}
   CheckEquals($FFFFFFFF, CardinalSwap($FFFFFFFF), 'CardinalSwap(0xFFFFFFFF)');
   CheckEquals($0100000000000000, UInt64Swap(1), 'UInt64Swap(1)');
end;

procedure TTestCase_Firefly_Numbers.TestBinaryTo;
const
   sMyAnsistring: ansistring = 'abcdef';
   sHexEncodedString: ansistring = '616263646566';
   sHexEncodedStringWSeparator: ansistring = '61-62-63-64-65-66';
begin
   CheckEquals(31, BinaryToInt64('11111'), 'BinaryToInt64');
   CheckEquals(sMyAnsistring, BinaryToStringA(sHexEncodedString, #0), 'BinaryToStringA#1');
   CheckEquals(sMyAnsistring, BinaryToStringA(sHexEncodedStringWSeparator, '-'), 'BinaryToStringA#1');
   CheckEquals(455, BinaryToLongInt('111000111'), 'BinaryToLongInt');
end;

procedure TTestCase_Firefly_Numbers.TestDoubleTo;
const
   dD: double = 1.5;
begin
   CheckEquals('23DBF97EAA0ABA40', DoubleToHexA(6666.666), 'DoubleToHexA 1.5');  // ????
   CheckEquals('000000000000F83F', DoubleToHexA(dD), 'DoubleToHexA 1.5');
end;

procedure TTestCase_Firefly_Numbers.TestByteTo;
const
   bMyByteArray: array [1 .. 3] of byte = (53, 54, 1);
   bMyByte: byte = 53;
   sMystring: string = '00110101';
   sMystringW: WideString = '00110101';
   sMyByteString: string = '001101010011011000000001';
begin
   CheckEquals(sMyByteString, BytesToBits(bMyByteArray), 'BytesToBits');
   CheckEquals(sMystring, ByteToBits(bMyByte), 'ByteToBits');
   CheckEquals(sMystringW, ByteToBitsW(bMyByte), 'ByteToBits');
   // 2016-08-31  pk  added brackets
   CheckEquals('11111110', BytesToBits([254]), 'BytesToBits');
end;

procedure TTestCase_Firefly_Numbers.TestLongIntTo;
const
   iLW: longword = $FFFFFFFF; // 4294967295;
   iLI: longint = $7FFFFFFD; // 2147483645;
   iLI1: longint = $7FFFFFFF; // 2147483647;
   iLI2: longint = -15; // $FFFFFFF1
   iLI3: longint = -2147483647; // $80000001
begin
   CheckEquals('FFFFFFFF', LongIntToHexA(iLW), 'LongIntToHexA (max)'); // Bereich prüfen
   CheckEquals('7FFFFFFD', LongIntToHexA(iLI), 'LongIntToHexA #2');
   CheckEquals('7FFFFFFF', LongIntToHexA(iLI1), 'LongIntToHexA #3');
   CheckEquals('FFFFFFF1', LongIntToHexA(iLI2), 'LongIntToHexA (-15)');
   CheckEquals('80000001', LongIntToHexA(iLI3), 'LongIntToHexA (neg 2)');
end;

procedure TTestCase_Firefly_Numbers.TestLongWordTo;
begin
   CheckEquals('FDFFFFFF', LongWordToSwapHex($FFFFFFFD), 'LongWordToSwapHex');
   CheckEquals('FFFFFFFF', LongWordToSwapHexA($FFFFFFFF), 'LongWordToSwapHexA');
end;

procedure TTestCase_Firefly_Numbers.TestCardinalTo;
const
   cCardinalValue: cardinal = $19999999; // 429496729;
   sStringValue: string = '00011001100110011001100110011001';
   sStringValueNoTrailing: string = '11001100110011001100110011001';
begin
   CheckEquals(sStringValueNoTrailing, CardinalToBinary(cCardinalValue), 'CardinalToBinary');
   CheckEquals(sStringValue, CardinalToBits(cCardinalValue), 'CardinalToBits');     // ????
   CheckEquals(sStringValue, CardinalToBinary(cCardinalValue, false), 'CardinalToBinary');
end;

procedure TTestCase_Firefly_Numbers.TestInt64To;
const
   iPositive815: int64 = 815;
   sPositive815Binary: string = '1100101111';
   sPositive815Hex: string = '000000000000032F';
   iNegative815: int64 = -815;
   sNegative815Binary: string = '111111111111111111111111111111111111111111111111111110011010001';
   sNegative815Hex: string = 'FFFFFFFFFFFFFCD1';
begin
   CheckEquals(sPositive815Binary, Int64ToBinary(iPositive815), 'Int64ToBinary');
   CheckEquals(sNegative815Binary, Int64ToBinary(iNegative815), 'Int64ToBinary');
   CheckEquals(sPositive815Hex, Int64ToHex(iPositive815), 'Int64ToHex');
   CheckEquals(sNegative815Hex, Int64ToHex(iNegative815), 'Int64ToHex');
end;

procedure TTestCase_Firefly_Numbers.TestHexTo;
begin
   CheckEquals(250, HexToLongWord('0xfa'), 'HexToLongWord');
   CheckEquals(250, HexToByte('fa'), 'HexToByte');
   CheckEquals(15, HexToNibble('f'), 'HexToNibble');
   CheckEquals(255, HexToByte('FF'), 'HexToByte');
   CheckEquals(0, HexToByte('222'), 'HexToByte');
end;

procedure TTestCase_Firefly_Numbers.TestOctalTo;
begin
   CheckEquals(511, OctalToLongWord('777'), 'OctalToLongWord');
   CheckEquals(73, OctalToByte('111'), 'OctalToByte');
   CheckEquals(3, OctalDigitToByte('3'), 'OctalDigitToByte');
   CheckEquals(2, OctalDigitToByte('2'), 'OctalDigitToByte');
   // the following obviously is a bug, since octals are only 0..7!
   CheckEquals(38, OctalDigitToByte('V'), 'OctalDigitToByte');
end;

procedure TTestCase_Firefly_Numbers.TestStringTo;
const
   sMyTestString: string = 'abcd';
   sMyTestString1: string = '1234';
begin
   CheckEquals('75s76s77s78s79s7a', StringToBinarySeparatorA('uvwxyz', 's', True), 'StringToBinarySeparatorA');
   CheckEquals('75s76s77s78s79s7A', StringToBinarySeparatorA('uvwxyz', 's'), 'StringToBinarySeparatorA');
   CheckEquals('61s62s63s64', StringToBinarySeparatorA(sMyTestString, 's'), 'StringToBinarySeparatorA');
   CheckEquals('61,62,63,64', StringToRegBinaryA(AnsiString(sMyTestString)), 'StringToRegBinaryA');
   CheckEquals('31,32,33,34', StringToRegBinaryA(AnsiString(sMyTestString1)), 'StringToRegBinaryA');
   CheckEquals(WideString('0061,0062,0063,0064'), StringToRegBinaryW(WideString(sMyTestString)), 'StringToRegBinaryW');
   CheckEquals(WideString('0031,0032,0033,0034'), StringToRegBinaryW(WideString(sMyTestString1)), 'StringToRegBinaryW');
   CheckEquals('31 32 33 34', StringToBinaryA(AnsiString(sMyTestString1)), 'StringToBinaryA');
   CheckEquals('61 62 63 64', StringToBinaryA(AnsiString(sMyTestString)), 'StringToBinaryA');
   CheckEquals(WideString('31 32 33 34'), StringToBinaryW(sMyTestString1), 'StringToBinary');
   CheckEquals(WideString('61 62 63 64'), StringToBinaryW(sMyTestString), 'StringToBinary');
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_Numbers.Suite);

end.
