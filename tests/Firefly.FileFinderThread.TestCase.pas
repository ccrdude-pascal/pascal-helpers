{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test case for Firefly.FileFinderThread.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2016-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-08-29  pk   5m  Fixed paths
// 2016-05-17  pk  10m  Wrote test case
// *****************************************************************************
   )
}

unit Firefly.FileFinderThread.TestCase;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.FileFinderThread;

type

   { TTestCase_Firefly_FileFinderThread }

   TTestCase_Firefly_FileFinderThread = class(TFireflyTestCase)
   published
      procedure TestFilenameIsReserved;
      procedure TestGetFileSizeInt64;
      procedure TestGetFileSizeString;
      procedure TestFileExists;
      procedure TestExtractFilePathW;
      procedure TestGetFileDateString;
   end;

implementation

{ TTestCase_Firefly_FileFinderThread }

procedure TTestCase_Firefly_FileFinderThread.TestFilenameIsReserved;
begin
   CheckTrue(FilenameIsReserved('com1'));
   CheckTrue(FilenameIsReserved('aux'));
   CheckTrue(FilenameIsReserved('prn'));
end;

procedure TTestCase_Firefly_FileFinderThread.TestGetFileSizeInt64;
begin
   CheckEquals(321024, GetFileSizeInt64(ExtractFilePath(ParamStr(0)) + 'testfiles\Karamasov.txt'), 'GetFileSizeInt64');
end;

procedure TTestCase_Firefly_FileFinderThread.TestGetFileSizeString;
begin
   CheckEquals('321024', GetFileSizeString(ExtractFilePath(ParamStr(0)) + 'testfiles\Karamasov.txt'), 'GetFileSizeString');
end;

procedure TTestCase_Firefly_FileFinderThread.TestFileExists;
var
   sFilename: string;
begin
   sFilename := ExtractFilePath(ParamStr(0)) + 'testfiles\Karamasov.txt';
   CheckTrue(FileExists(sFilename), 'FileExists(' + sFilename + ')');
   CheckTrue(pkFileExists(sFilename), sFilename);
end;

procedure TTestCase_Firefly_FileFinderThread.TestExtractFilePathW;
begin
   CheckEquals(ExtractFilePathW('C:\bdlog.txt'), 'C:\', 'ExtractFilePathW');
   CheckEquals(ExtractFilePathW(TestFileFolder + 'readme.txt'), TestFileFolder, 'ExtractFilePathW');
end;

procedure TTestCase_Firefly_FileFinderThread.TestGetFileDateString;
const
   sExpected = 'Friday, 15 September 2006 18:00:38';
var
   s: string;
   dt: TDateTime;
begin
   // This is dependent on the OS locale settings, would fail on English OS.
   dt := EncodeDate(2006, 09, 15) + EncodeTime(18, 00, 38, 0);
   FileSetDate(ExtractFilePath(ParamStr(0)) + 'testfiles\Karamasov.txt', DateTimeToFileDate(dt));
   s := GetFileDateString(ExtractFilePath(ParamStr(0)) + 'testfiles\Karamasov.txt');
   CheckEquals(sExpected, s);
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_FileFinderThread.Suite);

end.
