{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Test case for PepiMK.Drives.Base.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-06  pk  ---  Test case created by PascalHeaderUpdate
// *****************************************************************************
   )
}

unit Firefly.Drives.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Firefly.Tests,
   Firefly.Drives;

type

   {
     TTestCase_Firefly_Drives is a test class for classes, functions and procedures
     from unit PepiMK.Drives.Base.
   }
   TTestCase_Firefly_Drives = class(TFireflyTestCase)
   published
      {
        TestCompile is a dummy that simply fills in code to check if
        this test unit, and the associated tested unit, get compiled.
      }
      procedure TestCompile;
      procedure TestFixed;
      procedure TestRemovable;
   end;

implementation

uses
   {$IFDEF MSWindows}
   MSWindows,
   {$ENDIF MSWindows}
   SysUtils;

{ TTestCase_Firefly_Drives }

procedure TTestCase_Firefly_Drives.TestCompile;
begin
   CheckTrue(true);
end;

procedure TTestCase_Firefly_Drives.TestFixed;
begin
   CheckTrue(dtFixed in FixedDriveTypes);
   CheckTrue(dtRAMDisk in FixedDriveTypes);
end;

procedure TTestCase_Firefly_Drives.TestRemovable;
begin
   CheckTrue(dtOptical in RemovableDriveTypes);
   CheckTrue(dtRemovable in RemovableDriveTypes);
   CheckTrue(dtRemote in RemovableDriveTypes);
end;

initialization

   RegisterTest('FireflyCore', TTestCase_Firefly_Drives.Suite);

end.
