{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Test case for PepiMK.Threads.Base)

   @preformatted(
// *****************************************************************************
// Copyright: © 2003-2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-18  pk  10m  Wrote test case
// *****************************************************************************
   )
}

unit Firefly.Threads.Base.TestCase;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

interface

uses
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   Classes,
   SysUtils,
   Firefly.Tests,
   Firefly.Threads;

type

   { TTestCaseThreadsBase }

   TTestCaseThreadsBase = class(TFireflyTestCase)
   published
      procedure TestEnterLeave;
   end;

implementation

{ TTestCaseThreadsBase }

procedure TTestCaseThreadsBase.TestEnterLeave;
begin
   ThreadEnterCritical;
   try
      CheckTrue(IsInThreadCritical());
   finally
      ThreadLeaveCritical;
   end;
end;

initialization

{$IFDEF FPC}
   RegisterTest(TTestCaseThreadsBase);
{$ELSE FPC}
   RegisterTest(TTestCaseThreadsBase.Suite);
{$ENDIF FPC}


end.
