# Language specific rules

## Prefix variables with their type

Example:
```
procedure DoSomeThing;
var
   iNumber: integer;
   sText: string;
```

## Each variable declared on separate line

Example:
```
procedure DoSomeThing;
var
   iNumber: integer;
   sText: string;
```

## Variable initialization in declarartion

Example:
```
procedure DoSomeThing;
var
   iNumber: integer = -1;
   sText: string = '';
```

## Wrap even one liner conditional blocks in begin/end

Example:
```
if bGood then begin
   WriteLn('all good');
end else begin
   WriteLn('oh no');
end;
