{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Serializes objects to and from JSON or XML.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2022-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-04-25  pk   5m  Added to FireflyCore package.
// 2022-10-18  pk   5m  First unit header.
// *****************************************************************************
   )
}

unit Firefly.Serializer;

{$IFDEF FPC}
{$mode Delphi}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   TypInfo,
   laz2_dom,
   laz_xmlwrite,
   fpjson,
   jsonparser;

function CreateJSONFromObject(AnObject: TObject): TJSONObject;
function CreateKeyValuePairsFromObject(AnObject: TObject): TStringList;
function CreateXMLFromObject(AnObject: TObject): TXMLDocument;

function GetJSONFromObject(AnObject: TObject): string;
function GetKeyValuePairsFromObject(AnObject: TObject): string;
function GetXMLFromObject(AnObject: TObject): string;

type

   { AnObjectExportAttribute }

   AnObjectExportAttribute = class(TCustomAttribute)
   private
      FFieldName: string;
   public
      constructor Create(AFieldName: string);
      property FieldName: string read FFieldName;
   end;

implementation

uses
   Rtti,
   Firefly.Date;

function GetPublishedFieldDataEntryName(AnObject: TObject; AProperty: PPropInfo): string;
begin
   Result := AProperty^.Name;
end;

function CreateJSONFromObject(AnObject: TObject): TJSONObject;
var
   context: TRttiContext;
   prop: TRttiProperty;
   rType: TRttiType;
   a: TCustomAttribute;
   sName: string;
   o: TJSONObject;
   e: extended;
   dt: TDateTime;
begin
   Result := TJSONObject.Create;
   context := TRttiContext.Create;
   try
      rType := context.GetType(AnObject.ClassType);

      for prop in rType.GetProperties do begin
         sName := prop.Name;
         for a in prop.GetAttributes do begin
            if a is AnObjectExportAttribute then begin
               sName := (a as AnObjectExportAttribute).FieldName;
            end;
         end;
         case prop.PropertyType.TypeKind of
            tkAString: begin
               Result.Add(sName, prop.GetValue(AnObject).AsAnsiString);
            end;
            tkWString: begin
               Result.Add(sName, prop.GetValue(AnObject).AsUnicodeString);
            end;
            tkUString: begin
               Result.Add(sName, prop.GetValue(AnObject).AsUnicodeString);
            end;
            tkInteger: begin
               Result.Add(sName, prop.GetValue(AnObject).AsInteger);
            end;
            tkInt64: begin
               Result.Add(sName, prop.GetValue(AnObject).AsInt64);
            end;
            tkFloat: begin
               e := prop.GetValue(AnObject).AsExtended;
               if (prop.PropertyType.Name = 'TDateTime') then begin
                  try
                     if (e = 0) then begin
                        Result.Add(sName, e);
                     end else begin
                        prop.GetValue(AnObject).ExtractRawData(@dt);
                        o := TJSONObject.Create;
                        try
                           o.Add('double', e);
                           try
                              o.Add('iso8601', FormatDatetime('yyyy-mm-dd', dt) + 'T' + FormatDatetime('hh:nn:ss', dt));
                           except
                              on E: Exception do begin
                                 {$IFDEF MSWindows}
                                 OutputDebugString(PChar({$I %FILE%} + ' - ' + {$I %CURRENTROUTINE%} + ' / FormatDatetime: ' + E.Message));
                                 {$ENDIF MSWindows}
                              end;
                           end;
                           try
                              o.Add('unix', ConvertDateTimeToUNIXTimeRaw(dt));
                           except
                              on E: Exception do begin
                                 {$IFDEF MSWindows}
                                 OutputDebugString(PChar({$I %FILE%} + ' - ' + {$I %CURRENTROUTINE%} + ' / ConvertDateTimeToUNIXTimeRaw: ' + E.Message));
                                 {$ENDIF MSWindows}
                              end;
                           end;
                        finally
                           Result.Add(sName, o);
                        end;
                     end;
                  except
                     on E: Exception do begin
                        {$IFDEF MSWindows}
                        OutputDebugString(PChar({$I %FILE%} + ' - ' + {$I %CURRENTROUTINE%} + ' / ConvertDateTimeToUNIXTimeRaw: ' + E.Message));
                        {$ENDIF MSWindows}
                     end;
                  end;
               end else begin
                  Result.Add(sName, e);
               end;
            end;
         end;
      end;
   finally
      context.Free;
   end;
end;

function CreateKeyValuePairsFromObject(AnObject: TObject): TStringList;
var
   propList: TPropList;
   pProp: PPropInfo;
   iCount: integer;
   iCurrent: integer;
begin
   Result := TStringList.Create;
   iCount := GetPropList(AnObject.ClassInfo, tkAny, @propList, False);
   for iCurrent := 0 to Pred(iCount) do begin
      pProp := propList[iCurrent];
      (*
      (tkUnknown,tkInteger,tkChar,tkEnumeration,tkFloat,
              tkSet,tkMethod,tkSString,tkLString,tkAString,
              tkWString,tkVariant,tkArray,tkRecord,tkInterface,
              tkClass,tkObject,tkWChar,tkBool,tkInt64,tkQWord,
              tkDynArray,tkInterfaceRaw,tkProcVar,tkUString,tkUChar,
              tkHelper,tkFile,tkClassRef,tkPointer)
      *)
      Result.Add(GetPublishedFieldDataEntryName(AnObject, pProp) + '=data');
   end;
end;

function CreateXMLFromObject(AnObject: TObject): TXMLDocument;
var
   propList: TPropList;
   pProp: PPropInfo;
   iCount: integer;
   iCurrent: integer;
   oRoot, oNode: TDOMNode;
begin
   Result := TXMLDocument.Create;
   oRoot := Result.CreateElement('browserData');
   Result.AppendChild(oRoot);
   oNode := Result.CreateElement('data');
   oRoot.AppendChild(oNode);
   iCount := GetPropList(AnObject.ClassInfo, tkAny, @propList, False);
   for iCurrent := 0 to Pred(iCount) do begin
      pProp := propList[iCurrent];
      (*
      (tkUnknown,tkInteger,tkChar,tkEnumeration,tkFloat,
              tkSet,tkMethod,tkSString,tkLString,tkAString,
              tkWString,tkVariant,tkArray,tkRecord,tkInterface,
              tkClass,tkObject,tkWChar,tkBool,tkInt64,tkQWord,
              tkDynArray,tkInterfaceRaw,tkProcVar,tkUString,tkUChar,
              tkHelper,tkFile,tkClassRef,tkPointer)
      *)
      TDOMElement(oNode).AttribStrings[GetPublishedFieldDataEntryName(AnObject, pProp)] := 'data';
   end;
end;

function GetJSONFromObject(AnObject: TObject): string;
var
   oJSONResult: TJSONObject;
begin
   oJSONResult := CreateJSONFromObject(AnObject);
   try
      Result := oJSONResult.FormatJSON();
   finally
      oJSONResult.Free;
   end;
end;

function GetKeyValuePairsFromObject(AnObject: TObject): string;
var
   slKeyValuePairResult: TStringList;
begin
   slKeyValuePairResult := CreateKeyValuePairsFromObject(AnObject);
   try
      Result := slKeyValuePairResult.Text;
   finally
      slKeyValuePairResult.Free;
   end;
end;

function GetXMLFromObject(AnObject: TObject): string;
var
   sl: TStringList;
   ms: TMemoryStream;
   xmlResult: TXMLDocument;
begin
   xmlResult := CreateXMLFromObject(AnObject);
   try
      sl := TStringList.Create;
      try
         ms := TMemoryStream.Create;
         try
            WriteXMLFile(xmlResult, ms);
            ms.Seek(0, soFromBeginning);
            sl.LoadFromStream(ms);
            Result := sl.Text;
         finally
            ms.Free;
         end;
      finally
         sl.Free;
      end;
   finally
      xmlResult.Free;
   end;

end;

{ AnObjectExportAttribute }

constructor AnObjectExportAttribute.Create(AFieldName: string);
begin
   FFieldName := AFieldName;
end;

end.
