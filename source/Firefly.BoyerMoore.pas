{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Implements Boyer-Moore(-Horspool?) search algorithm.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-05-06  pk   5m  Added credits
// 2016-05-04  pk  10m  Moved to new PepiMKBase package
// 2010-10-04  pk  20m  Implemented search in a string, fixes für Delphi XE
// 2010-09-21  pk  10m  Started update for Delphi XE (Ansi vs. Wide)
// 2008-05-09  pk  10m  Added FoundSomething property for cases of LastPosition=0
// *****************************************************************************
   )
}

unit Firefly.BoyerMoore;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$ENDIF FPC}

interface

uses
   SysUtils, // only for Format for debugging
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   {$IFDEF Unicode} AnsiStrings, {$ENDIF} // allows AnsiString functions on Delphi
   //   {$IFDEF Windows} Windows, {$ENDIF}
   Classes;

type
   {
     This event gets triggered when the algorithm found a search term.

     @param(ASender is the search class object that identified the search term.)
     @param(APosition is the position where the search term was found.)
     @param(ACancel tells the algorithm to stop search.)
   }
   TOnTextFoundEvent = procedure(ASender: TObject; APosition: int64; var ACancel: boolean) of object;

   {
     This is the standard one byte charset skip table used by Boyer-Moore
     algorithms.
   }
   TBoyerMooreSkipTable = array [0 .. 255] of integer;

   {
     TBMSearchAlgorithm is an abstract base class for implementing Boyer-Moore.
   }
   TBMSearchAlgorithm = class
   private
      FTextAnsi: ansistring;
      FTextWideRaw: rawbytestring;
      FTextWide: WideString;
      FOnFound: TOnTextFoundEvent;
      FFindFirstOnly: boolean;
      FLastPosition: integer;
      FFindLimit: integer;
      FFoundSomething: boolean;
   protected
      {
        Reports a found position.

        @param(APosition is the position something was found at.)
        @param(ACancel tells the algorithm to stop search.)
      }
      procedure DoFound(APosition: integer; var ACancel: boolean);
   public
      {
        Resets search.
      }
      procedure Clear;
      constructor Create;
      {
        Searches for an ansi term.

        @param(AStream is the stream to search.)
        @param(ASearchTerm is the term to search for.)
      }
      procedure Search({%H-}AStream: TStream; const ASearchTerm: ansistring); overload; virtual;
      {
        Searches for an unicode term.

        @param(AStream is the stream to search.)
        @param(ASearchTerm is the term to search for.)
      }
      procedure SearchWide({%H-}AStream: TStream; const ASearchTerm: WideString); overload; virtual;
      {
        Searches for an ansi term.

        @param(ASearchIn is the text to search in.)
        @param(ASearchTerm is the term to search for.)
      }
      procedure Search({%H-}ASearchIn: ansistring; const {%H-}ASearchTerm: ansistring); overload; virtual;
      property FindFirstOnly: boolean read FFindFirstOnly write FFindFirstOnly;
      property FindLimit: integer read FFindLimit write FFindLimit;
      property FoundSomething: boolean read FFoundSomething;
      property LastPosition: integer read FLastPosition write FLastPosition;
      property Text: ansistring read FTextAnsi write FTextAnsi;
      property OnFound: TOnTextFoundEvent read FOnFound write FOnFound;
   end;

   {
     TBMSearchAlgorithmClass defines Boyer-Moore implementation classes.
   }
   TBMSearchAlgorithmClass = class of TBMSearchAlgorithm;

   {
     TBoyerMoorePepiMK is my own implementation of Boyer-Moore-Horspool.
   }
   TBoyerMoorePepiMK = class(TBMSearchAlgorithm)
   private
      FoccAnsi: array [#0 .. #255] of integer;
      // FoccWide: array [#0 .. #255] of integer;
      FF, FS: array of integer;
      FAnsiLength: integer;
      // FWideLength: integer;
      FTerm: array of ansichar;
      // FBadCharShifts: TBoyerMooreSkipTable;
      FGoodSuffixShifts: array of integer;
      {
        Initializes the table used to speed up the search.

        @param(ASearchTerm is the term to search for.)
      }
      procedure InitTables(const ASearchTerm: ansistring);
      procedure bmInitocc;     //< Part of table initialization.
      procedure bmPreprocess1; //< Part of table initialization.
      procedure bmPreprocess2; //< Part of table initialization.
   public
      {
        Searches for an ansi term.

        @param(AStream is the stream to search.)
        @param(ASearchTerm is the term to search for.)
      }
      procedure Search(AStream: TStream; const ASearchTerm: ansistring); override;
      {
        Searches for an ansi term.

        @param(ASearchIn is the text to search in.)
        @param(ASearchTerm is the term to search for.)
      }
      procedure Search(ASearchIn: ansistring; const ASearchTerm: ansistring); override;
      destructor Destroy; override;
   end;

   {
     TBoyerMooreJS is a broken implementation found on the Internet,
     with endless loops in certain situations and non-found items, plus
     my own bugfixes for them that still do not lead to a good implementation.
   }
   TBoyerMooreJS = class(TBMSearchAlgorithm)
   private
      FSkipTable: TBoyerMooreSkipTable;
      {
        Initializes the table used to speed up the search.

        @param(ASearchTerm is the term to search for.)
      }
      procedure InitSkipTable(const ASearchTerm: ansistring);
   public
      {
        Searches within a stream.

        @param(AStream is the stream to search.)
        @param(ASearchTerm is the term to search for.)
      }
      procedure Search(AStream: TStream; const ASearchTerm: ansistring); override;
   end;

   {
     TPreferredBoyerMoore is a class identifying the best algorithm, so that
     we can later easily replace it if necessary.
   }
   TPreferredBoyerMoore = TBoyerMoorePepiMK;


{
  Replaces a string within a text with a replacement, using the Boyer-Moore algorithm.
  Used by Spybot's engine.

  @param(AText is the text to search in.)
  @param(AnOldPattern is the text to search for.)
  @param(ANewPattern is the term to replace with.)
  @param(AFlags are flags as used in compilers 3StringReplace.)
  @returns(Text with replaced patterns.)
}
function BMStringReplace(const AText, AnOldPattern, ANewPattern: ansistring; AFlags: TReplaceFlags): ansistring;
{
  Parses a specified path and adds files found there to the temp file list.
  Used by Spybot's engine.

  @param(ASearchTerm is the term to search for.)
  @param(AText is the text to search in.)
  @returns(Position of found item.)
}
function BMPos(const ASearchTerm, AText: ansistring): integer;

implementation

uses
   Math,
   Firefly.Credits;

procedure TBoyerMooreJS.InitSkipTable(const ASearchTerm: ansistring);
var
   iCnt: integer;
begin
   // FillChar doesn't work with array for more-than-byte-length-terms
   // FillChar(FSkipTable, SizeOf(TBoyerMooreSkipTable), Length(SearchTerm));
   for iCnt := low(FSkipTable) to high(FSkipTable) do begin
      FSkipTable[iCnt] := Length(ASearchTerm);
   end;
   // #1: Original skip: didn't work on multiple characters!
   // for iCnt := Length(SearchTerm) downto 1
   // do SkipTable[Ord(SearchTerm[iCnt])] := Length(SearchTerm) - iCnt;
   // #2: Improved skip, but wrong!
   for iCnt := 1 to Length(ASearchTerm) do begin
      FSkipTable[Ord(ASearchTerm[iCnt])] := Length(ASearchTerm) - iCnt;
   end;
   // #3: According to Wikipedia article, not Horspool one
   // for iCnt := Length(SearchTerm) downto 1
   // do if FSkipTable[Ord(SearchTerm[iCnt])] = Length(SearchTerm)
   // then FSkipTable[Ord(SearchTerm[iCnt])] := Length(SearchTerm) - iCnt;
end;

procedure TBoyerMooreJS.Search(AStream: TStream; const ASearchTerm: ansistring);
const
   iBufferSizeMax = 4096;
   // String search in stream using Boyer Moore. Results see event OnFound.
   // Count in OnFound begins at 0.
var
   CanCancel: boolean;
   TestEndOfBuffer: ansistring;
   iBufferSize, TextLen, iBufferPos, iSubStrPos, iBuffersReadCount, a: int64;
   aBuffer: array [1 .. iBufferSizeMax] of ansichar;
   maxStreamPos: int64;
begin
   inherited;
   InitSkipTable(ASearchTerm);
   CanCancel := False;
   iBuffersReadCount := 0;
   TextLen := Length(ASearchTerm);
   AStream.Seek(0, soFromBeginning);
   if FFindLimit > 0 then begin
      maxStreamPos := FFindLimit;
   end else begin
      maxStreamPos := AStream.Size;
   end;
   if maxStreamPos > AStream.Size then begin
      maxStreamPos := AStream.Size;
   end;
   {$IFDEF MSWindows}
   ZeroMemory(@aBuffer, iBufferSizeMax);
   {$ELSE MSWindows}
   FillChar(aBuffer, iBufferSizeMax, 0);
   {$ENDIF MSWindows}
   while AStream.Position < maxStreamPos do begin
      a := 0;
      iBufferPos := TextLen;
      iBufferSize := AStream.Read(aBuffer, SizeOf(aBuffer));
      repeat
         iSubStrPos := TextLen;
         repeat
            if aBuffer[iBufferPos] = ASearchTerm[iSubStrPos] then begin
               Dec(iBufferPos);
               Dec(iSubStrPos);
            end else begin
               // Hole den Sprungwert aus der Skiptabelle
               // Einfach nur springen geht nicht:
               // SourcePos := SourcePos + SkipTable[Ord(aBuffer[SourcePos])];

               // SkipTable[Ord(AText[iSubStrPos])] --- Skip from current search term char!
               // SkipTable[Ord(aBuffer[iBufferPos])] --- Skip for current content char!
               // Inc(iBufferPos, SkipTable[Ord(aBuffer[iBufferPos])]);

               // x A 0 0 0
               // A A 0 0 0

               if FSkipTable[Ord(ASearchTerm[iSubStrPos])] > FSkipTable[Ord(aBuffer[iBufferPos])] then begin
                  Inc(iBufferPos, TextLen);
               end else begin
                  Inc(iBufferPos, FSkipTable[Ord(aBuffer[iBufferPos])]); // + TextLen - iSubStrPos
               end;

               (*
                 then SourcePos := SourcePos - SkipTable[Ord(AText[SubStrPos])] + SkipTable[Ord(aBuffer[SourcePos])]
                 else SourcePos := SourcePos + SkipTable[Ord(aBuffer[SourcePos])];
               *)
               // Wenn mehrmals derselbe Buchstabe in dem gesuchten Text
               // vorkommt, kann es passieren, dass SourcePos nicht exakt auf
               // ReadLen landet. Wenn der gesuchte Text dann den
               // Block abschließt, wird der Text nicht gefunden. Dadurch das
               // SourcePos nie größer wird als ReadLen muss hier mitgezählt
               // werden, ob Source auf ReadLen gesetzt wurde.
               if iBufferPos > iBufferSize then begin
                  iBufferPos := iBufferSize;
                  Inc(a);
               end;
               iSubStrPos := TextLen;
            end;
         until (iSubStrPos = 0) or (iBufferPos > iBufferSize) or (a = 2);
         if iSubStrPos = 0 then begin // Text gefunden
            DoFound(iBuffersReadCount * SizeOf(aBuffer) + iBufferPos - iBuffersReadCount * TextLen, CanCancel);
            // Überspringe das gefundene Wort in aBuffer
            // Da der gesuchte Text aber von rechts nach links über aBuffer
            // "gezogen" wird, muss, da SourcePos um TextLen verringert wurde
            // SourcePos jetzt um 2*TextLen erhöht werden. Dann würde das
            // gefundene Wort übersprungen und SourcePos genau TextLen Positionen
            // hinter das gefundenen Wort gesetzt.
            iBufferPos := iBufferPos + 2 * TextLen;
         end;
         if CanCancel then begin
            Exit;
         end;
      until (iBufferPos > iBufferSize) or (a = 2); // Block ist abgearbeitet
      Inc(iBuffersReadCount);                      // merke die Anzahl gelesenen Blöcke
      // wg. der Blockbearbeitung kann es passieren, dass der gesuchte Text
      // zerschnitten wird. Deshalb wird aStream.Position um die Textlänge
      // nach "links" geschoben.
      // Wenn aber das gesuchte Wort and er
      // Blockgrenze abschließt wird es zweimal gefunden. Deshalb
      // darf die Position nur zurückgesetzt werden, wenn der
      // gesuchte Text NICHT den Block abschließt und der Stream.Postion<
      // als Stream.Size ist (Wenn Stream.Positon=Stream.Size ist handelt
      // es sich um den letzten Block. Dann muss nix mehr verschoben werden !!!}
      TestEndOfBuffer := Copy(aBuffer, iBufferSize - TextLen + 1, TextLen);
      if (TestEndOfBuffer <> ASearchTerm) and (AStream.Position < AStream.Size) then begin
         AStream.Seek(-(TextLen), soFromCurrent);
      end;
   end; // While aStream.Position<Filestream.Size do
end;

{ TBMSearchAlgorithm }

procedure TBMSearchAlgorithm.Clear;
begin
   FLastPosition := -1;
end;

constructor TBMSearchAlgorithm.Create;
begin
   FFindFirstOnly := False;
   FFindLimit := 0;
   Clear;
end;

procedure TBMSearchAlgorithm.DoFound(APosition: integer; var ACancel: boolean);
begin
   FFoundSomething := True;
   FLastPosition := APosition;
   if FFindFirstOnly then begin
      ACancel := True;
   end;
   if Assigned(FOnFound) then begin
      FOnFound(Self, APosition, ACancel);
   end;
end;

procedure TBMSearchAlgorithm.SearchWide({%H-}AStream: TStream; const ASearchTerm: WideString);
begin
   Clear;
   FTextAnsi := ansistring(ASearchTerm);
   FTextWide := ASearchTerm;
   SetLength(FTextWideRaw, Length(FTextWide) * 2);
   Move(FTextWide[1], FTextWideRaw[1], Length(FTextWide) * 2);
   FFoundSomething := False;
end;

procedure TBMSearchAlgorithm.Search({%H-}ASearchIn: ansistring; const {%H-}ASearchTerm: ansistring);
begin
   Clear;
   FFoundSomething := False;
end;

procedure TBMSearchAlgorithm.Search({%H-}AStream: TStream; const {%H-}ASearchTerm: ansistring);
begin
   Clear;
   FTextAnsi := ASearchTerm;
   FFoundSomething := False;
end;

{ TBoyerMoorePepiMK }

destructor TBoyerMoorePepiMK.Destroy;
begin
   SetLength(FGoodSuffixShifts, 0);
   inherited;
end;

{
  Part of table initialization.
}
procedure TBoyerMoorePepiMK.bmInitocc();
var
   j: integer;
   a: ansichar;
begin
   for a := #0 to #255 do begin
      FoccAnsi[a] := -1;
   end;
   for j := 0 to Pred(FAnsiLength) do begin
      a := FTerm[j];
      FoccAnsi[a] := j;
   end;
end;

{
  Part of table initialization.
}
procedure TBoyerMoorePepiMK.bmPreprocess1();
var
   i, j: integer;
begin
   i := FAnsiLength;
   j := FAnsiLength + 1;
   FF[i] := j;
   while i > 0 do begin
      while ((j <= FAnsiLength) and (FTerm[i - 1] <> FTerm[j - 1])) do begin
         if (FS[j] = 0) then FS[j] := j - i;
         j := FF[j];
      end;
      Dec(i);
      Dec(j);
      FF[i] := j;
   end;
end;

procedure TBoyerMoorePepiMK.bmPreprocess2();
var
   i, j: integer;
begin
   j := FF[0];
   for i := 0 to FAnsiLength do begin
      if FS[i] = 0 then FS[i] := j;
      if (i = j) then j := FF[j];
   end;
end;

procedure TBoyerMoorePepiMK.InitTables(const ASearchTerm: ansistring);
begin
   FAnsiLength := Length(ASearchTerm);
   SetLength(FF, Succ(FAnsiLength));
   SetLength(FS, Succ(FAnsiLength));
   SetLength(FTerm, FAnsiLength);
   Move(ASearchTerm[1], FTerm[0], FAnsiLength);
   bmInitocc;
   bmPreprocess1();
   bmPreprocess2();
end;

procedure TBoyerMoorePepiMK.Search(ASearchIn: ansistring; const ASearchTerm: ansistring);
var
   iBufferSize, iBuffersReadCount: int64;
   bDoCancel: boolean;

   procedure SearchInBlock;
   var
      i, j: integer;
      n: integer;
   begin
      // This algorithm doesn't match at right boundary!
      if iBufferSize < FAnsiLength then begin
         Exit;
      end;
      for i := Pred(FAnsiLength) downto 0 do begin
         if not (FTerm[i] = ASearchIn[iBufferSize - Pred(FAnsiLength) + i]) then begin
            break;
         end;
      end;
      if i <= 0 then begin
         DoFound(iBuffersReadCount * Length(ASearchIn) + iBufferSize - FAnsiLength + 1, bDoCancel);
         if bDoCancel then begin
            Exit;
         end;
      end;
      n := iBufferSize;
      i := 0;
      while (i <= n - FAnsiLength) do begin
         j := FAnsiLength - 1;
         while ((j >= 0) and (FTerm[j] = ASearchIn[i + j])) do Dec(j);
         if (j < 0) then begin
            // report i
            DoFound(iBuffersReadCount * Length(ASearchIn) + i - iBuffersReadCount * FAnsiLength, bDoCancel);
            if bDoCancel then begin
               Exit;
            end;
            i := i + FS[0];
         end else begin
            Inc(i, Max(FS[j + 1], j - FoccAnsi[ASearchIn[i + j]]));
         end;
      end;
   end;

begin
   inherited;
   bDoCancel := False;
   InitTables(ASearchTerm);
   iBuffersReadCount := 0;
   iBufferSize := Length(ASearchIn);
   SearchInBlock;
end;

procedure TBoyerMoorePepiMK.Search(AStream: TStream; const ASearchTerm: ansistring);
const
   iBufferSizeMax = 4096;
var // CanCancel: boolean;
   iBufferSize, iSearchLength, iBuffersReadCount: int64;
   aBuffer: array [1 .. iBufferSizeMax] of ansichar;
   iMaxStreamPos: int64;
   bDoCancel, bEndMatch: boolean;

   procedure SearchInBlock(var ADoCancel: boolean);
   var
      i, j: integer;
      n: integer;
   begin
      // This algorithm doesn't match at right boundary!
      if iBufferSize < FAnsiLength then begin
         Exit;
      end;
      bEndMatch := False;
      for i := Pred(FAnsiLength) downto 0 do begin
         if not (FTerm[i] = aBuffer[iBufferSize - Pred(FAnsiLength) + i]) then begin
            bEndMatch := False;
            break;
         end;
         if i = 0 then begin
            bEndMatch := True;
         end;
      end;
      if bEndMatch then begin
         DoFound(iBuffersReadCount * SizeOf(aBuffer) + iBufferSize - FAnsiLength - 1, ADoCancel);
         if ADoCancel then begin
            Exit;
         end;
      end;
      n := iBufferSize;
      i := 0;
      while (i <= n - FAnsiLength) do begin
         j := FAnsiLength - 1;
         while ((j >= 0) and (FTerm[j] = aBuffer[i + j])) do Dec(j);
         if (j < 0) then begin
            DoFound(iBuffersReadCount * SizeOf(aBuffer) + i - iBuffersReadCount * FAnsiLength - 1, ADoCancel);
            if ADoCancel then begin
               Exit;
            end;
            i := i + FS[0];
         end else begin
            Inc(i, Max(FS[j + 1], j - FoccAnsi[aBuffer[i + j]]));
         end;
      end;
   end;

begin
   inherited;
   bDoCancel := False;
   InitTables(ASearchTerm);
   iBuffersReadCount := 0;
   AStream.Seek(0, soFromBeginning);
   if FFindLimit > 0 then begin
      iMaxStreamPos := FFindLimit;
   end else begin
      iMaxStreamPos := AStream.Size;
   end;
   if iMaxStreamPos > AStream.Size then begin
      iMaxStreamPos := AStream.Size;
   end;
   iSearchLength := Length(ASearchTerm);
   {$IFDEF MSWindows}
   ZeroMemory(@aBuffer, iBufferSizeMax);
   {$ELSE MSWindows}
   FillChar(aBuffer, iBufferSizeMax, 0);
   {$ENDIF MSWindows}
   while AStream.Position < iMaxStreamPos do begin
      iBufferSize := AStream.Read(aBuffer, SizeOf(aBuffer));
      SearchInBlock(bDoCancel);
      if bDoCancel then begin
         Exit;
      end;
      Inc(iBuffersReadCount);
      if AStream.Position < iMaxStreamPos then begin
         AStream.Seek(-iSearchLength, soFromCurrent);
      end;
   end;
end;

type
   {
     TBoyerMooreStringReplace is a special class using the Boyer-Moore
     algorithm to do speed-improved string replaces.
   }
   TBoyerMooreStringReplace = class
   private
      FReplacedString: ansistring;
      FResultCount: int64;
      FResults: array of int64;
      {
        Reports a found position.

        @param(ASender    Not used.
        @param(APosition  The position something was found at.
        @param(ACancel    Tells algorithm to stop search.
      }
      procedure DoTextFound({%H-}ASender: TObject; APosition: int64; var {%H-}ACancel: boolean);
   public
      {
        Replaces a string within a text with a replacement, using the Boyer-Moore algorithm.

        @param(AText         Text to search in.
        @param(AnOldPattern  Text to search for.
        @param(ANewPattern   Term to replace with.
        @param(AFlags        Flags as used in compilers StringReplace.
        @see StringReplace
        @return               Text with replaced patterns.
      }
      function StringReplace(const AText, AOldPattern, ANewPattern: ansistring; AFlags: TReplaceFlags): ansistring;
      property ReplacedString: ansistring read FReplacedString;
   end;

{ TBoyerMooreStringReplace }

procedure TBoyerMooreStringReplace.DoTextFound(ASender: TObject; APosition: int64; var ACancel: boolean);
var
   i: integer;
begin
   i := FResultCount;
   Inc(FResultCount);
   if FResultCount >= Length(FResults) then begin
      SetLength(FResults, Length(FResults) + 100);
   end;
   FResults[i] := APosition;
end;

function TBoyerMooreStringReplace.StringReplace(const AText, AOldPattern, ANewPattern: ansistring; AFlags: TReplaceFlags): ansistring;
var
   sText, sOldPattern: ansistring;
   ms: TMemoryStream;
   bm: TBoyerMooreJS;
   i, iPos: integer;
   iLen: int64;
begin
   FResultCount := 0;
   SetLength(FResults, 100);
   if rfIgnoreCase in AFlags then begin
      sText := UpperCase(AText);
      sOldPattern := UpperCase(AOldPattern);
   end else begin
      sText := AText;
      sOldPattern := AOldPattern;
   end;
   ms := TMemoryStream.Create;
   bm := TBoyerMooreJS.Create;
   try
      ms.Write(sText[1], Length(sText));
      ms.Seek(0, soFromBeginning);
      sText := ''; // release memory
      {$IFDEF FPC}
      bm.OnFound := @DoTextFound;
      {$ELSE FPC}
      bm.OnFound := DoTextFound;
      {$ENDIF FPC}
      bm.Search(ms, sOldPattern);
      i := 0; // start with +1
      iLen := (int64(Length(ANewPattern)) - int64(Length(AOldPattern))) * (FResultCount - i);
      SetLength(FReplacedString, ms.Size + iLen);
      FillChar(FReplacedString[1], Length(FReplacedString), 0);
      // ZeroMemory(@(FReplacedString[1]), Length(FReplacedString));
      iPos := 1;
      ms.Seek(0, soFromBeginning);
      while i < FResultCount do begin
         ms.Read(FReplacedString[iPos], FResults[i] - iPos + i + 1);
         Inc(iPos, FResults[i] - iPos + i + 1);
         Move(ANewPattern[1], FReplacedString[iPos], Length(ANewPattern));
         ms.Seek(Length(AOldPattern), soFromCurrent);
         Inc(i);
         Inc(iPos, Length(ANewPattern));
      end;
      ms.Read(FReplacedString[iPos], ms.Size - ms.Position);
      Result := FReplacedString;
   finally
      bm.Free;
      ms.Free;
   end;
end;

function BMStringReplace(const AText, AnOldPattern, ANewPattern: ansistring; AFlags: TReplaceFlags): ansistring;
var
   sr: TBoyerMooreStringReplace;
begin
   sr := TBoyerMooreStringReplace.Create;
   try
      sr.StringReplace(AText, AnOldPattern, ANewPattern, AFlags);
      Result := sr.ReplacedString;
   finally
      sr.Free;
   end;
end;

function BMPos(const ASearchTerm, AText: ansistring): integer;
var
   bm: TBMSearchAlgorithm;
begin
   bm := TPreferredBoyerMoore.Create;
   try
      bm.FindFirstOnly := True;
      bm.Search(AText, ASearchTerm);
      Result := bm.LastPosition;
      if Result < 0 then begin
         Result := 0;
      end;
   finally
      bm.Free;
   end;
end;

begin
  RegisterLicense('Boyer Moore Implementation', 'Jens Schuhmann (DelphiPraxis.net)', 'https://www.delphipraxis.net/108604-post5.html', ltPublicDomain, vEndUser);
  RegisterLicense('Boyer Moore Implementation', 'Patrick Kolla (Safer-Networking Ltd.)', '', ltPublicDomain, vEndUser);
end.
