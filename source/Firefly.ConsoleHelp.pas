{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Handling command line parameter help at the console.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2011-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-04  pk  10m  Ported to new package PepiMKBase
// 2011-03-10  --  ---  Unit created by CollectionTemplater
// *****************************************************************************
   )
}

unit Firefly.ConsoleHelp;

{$DEFINE CollectionSorting}// sorting of the list

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   SysUtils,
   Classes;

type
   TCLPGroupEnum = (clpgGroupName, clpgDescription, clpgNecessity, clpgParameters);
   TCLPGroupSet = set of TCLPGroupEnum;
   TGroupNecessities = (gnAllMandatory, gnExactlyOneMandatory, gnAllOptional, gnOneOreMore, gnZeroOrOne);

   TCommandLineParameterItems = class;

   { TCommandLineParameterItem }

   TCommandLineParameterItem = class(TCollectionItem)
   private
      FOwner: TCommandLineParameterItems;
      /// TCommandLineParameterItems owning this TCommandLineParameterItem.
      FParameterName: String;
      FSyntax: String;
      FDescription: String;
      FIsSet: boolean;
      procedure SetParameterName(const Value: String);
      procedure SetSyntax(const Value: String);
      procedure SetDescription(const Value: String);
      function GetIsSet: boolean;
   protected
      function GetDisplayName: string; override;
   public
      constructor Create(ACollection: TCollection); override;
      destructor Destroy; override;
      function MatchesString(AText: String): boolean; overload;
      procedure Assign(Source: TPersistent); override;
      property ParameterName: String read FParameterName write SetParameterName;
      property Syntax: String read FSyntax write SetSyntax;
      property Description: String read FDescription write SetDescription;
      property IsSet: boolean read GetIsSet;
   end;

   { TCommandLineParameterItems }

   TCommandLineParameterItems = class(TCollection)
   private
      function GetItem(AIndex: integer): TCommandLineParameterItem;
      procedure SetItem(AIndex: integer; AValue: TCommandLineParameterItem);
   public
      constructor Create;
      destructor Destroy; override;
      function Add: TCommandLineParameterItem;
      function FindParameterName(const AParameterName: String): TCommandLineParameterItem;
      function FindSyntax(const ASyntax: String): TCommandLineParameterItem;
      function FindDescription(const ADescription: String): TCommandLineParameterItem;
      {$IFDEF CollectionSorting}
      procedure SortParameterName;
      procedure SortSyntax;
      procedure SortDescription;
      {$ENDIF CollectionSorting}
      property Items[index: integer]: TCommandLineParameterItem read GetItem write SetItem; default;
   end;

   TCommandLineParameters = class;

   { TCLPGroup }

   TCLPGroup = class(TCollectionItem)
   private
      FOwner: TCommandLineParameters;
      /// TCommandLineParameters owning this TCLPGroup.
      FGroupName: String;
      FDescription: String;
      FNecessity: TGroupNecessities;
      FParameters: TCommandLineParameterItems;
      procedure SetGroupName(const Value: String);
      procedure SetDescription(const Value: String);
      procedure SetNecessity(const Value: TGroupNecessities);
      procedure SetParameters(const Value: TCommandLineParameterItems);
      function GetItemText: String;
   protected
      function GetDisplayName: string; override;
   public
      constructor Create(ACollection: TCollection); override;
      destructor Destroy; override;
      procedure Assign(Source: TPersistent); override;
      function AddParameter(AParameterName, ADescription: String): TCommandLineParameterItem;
      property GroupName: String read FGroupName write SetGroupName;
      property Description: String read FDescription write SetDescription;
      property Necessity: TGroupNecessities read FNecessity write SetNecessity;
      property Parameters: TCommandLineParameterItems read FParameters write SetParameters;
      property ItemText: String read GetItemText;
   end;

   { TCommandLineParameters }

   TCommandLineParameters = class(TCollection)
   private
      function GetItem(AIndex: integer): TCLPGroup;
      procedure SetItem(AIndex: integer; AValue: TCLPGroup);
   public
      constructor Create;
      destructor Destroy; override;
      function Add: TCLPGroup;
      function Insert(AIndex: integer): TCLPGroup;
      function FindGroupName(const AGroupName: String): TCLPGroup;
      function FindDescription(const ADescription: String): TCLPGroup;
      function FindNecessity(const ANecessity: TGroupNecessities): TCLPGroup;
      function FindParameters(const AParameters: TCommandLineParameterItems): TCLPGroup;
      procedure ToLines(ALines: TStrings);
      procedure TestWriteLn;
      procedure WriteLn;
      {$IFDEF CollectionSorting}
      procedure SortGroupName;
      procedure SortDescription;
      procedure SortNecessity;
      procedure SortParameters;
      {$ENDIF CollectionSorting}
      property Items[index: integer]: TCLPGroup read GetItem write SetItem; default;
   end;

function CommandLineParameters: TCommandLineParameters;
function CLPGroupBasic: TCLPGroup;
function CLPGroupLogs: TCLPGroup;
function CLPGroupAuto: TCLPGroup;

var
   CLPAutomationVerbose: TCommandLineParameterItem;
   CLPAutomationSilent: TCommandLineParameterItem;

implementation

uses
   Firefly.Collections,
   Firefly.Date;

var
   CommandLineParametersInt: TCommandLineParameters;
   CLPGroupBasicInt: TCLPGroup;
   CLPGroupLogsInt: TCLPGroup;
   CLPGroupAutoInt: TCLPGroup;

function CommandLineParameters: TCommandLineParameters;
begin
   if not Assigned(CommandLineParametersInt) then begin
      CommandLineParametersInt := TCommandLineParameters.Create;
   end;
   Result := CommandLineParametersInt;
end;

function CLPGroupBasic: TCLPGroup;
begin
   if not Assigned(CLPGroupBasicInt) then begin
      CLPGroupBasicInt := CommandLineParameters.Add;
      ;
      with CLPGroupBasicInt do begin
         GroupName := 'Basic';
         Description := '';
         Necessity := gnAllOptional;
      end;
   end;
   Result := CLPGroupBasicInt;
end;

function CLPGroupLogs: TCLPGroup;
begin
   if not Assigned(CLPGroupLogsInt) then begin
      CLPGroupLogsInt := CommandLineParameters.Add;
      ;
      with CLPGroupLogsInt do begin
         GroupName := 'Logs';
         Description := 'About logging information';
         Necessity := gnAllOptional;
      end;
   end;
   Result := CLPGroupBasicInt;
end;

function CLPGroupAuto: TCLPGroup;
begin
   if not Assigned(CLPGroupAutoInt) then begin
      CLPGroupAutoInt := CommandLineParameters.Add;
      ;
      with CLPGroupAutoInt do begin
         GroupName := 'Automation';
         Description := 'Options to automate this program';
         Necessity := gnAllOptional;
      end;
   end;
   Result := CLPGroupAutoInt;
end;

{$IFDEF CollectionSorting}

{ *------------------------------------------------------------------------------
  Comparison of 2 TCommandLineParameterItem items by ParameterName for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItemCompareParameterName(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCommandLineParameterItem items by ParameterName for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := WideCompareText(TCommandLineParameterItem(item1).ParameterName, TCommandLineParameterItem(item2).ParameterName);
end;

{ *------------------------------------------------------------------------------
  Comparison of 2 TCommandLineParameterItem items by Syntax for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItemCompareSyntax(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCommandLineParameterItem items by Syntax for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := WideCompareText(TCommandLineParameterItem(item1).Syntax, TCommandLineParameterItem(item2).Syntax);
end;

{ *------------------------------------------------------------------------------
  Comparison of 2 TCommandLineParameterItem items by Description for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItemCompareDescription(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCommandLineParameterItem items by Description for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := WideCompareText(TCommandLineParameterItem(item1).Description, TCommandLineParameterItem(item2).Description);
end;

{ *------------------------------------------------------------------------------
  Comparison of 2 TCLPGroup items by GroupName for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCLPGroupCompareGroupName(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCLPGroup items by GroupName for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := WideCompareText(TCLPGroup(item1).GroupName, TCLPGroup(item2).GroupName);
end;

{ *------------------------------------------------------------------------------
  Comparison of 2 TCLPGroup items by Description for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCLPGroupCompareDescription(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCLPGroup items by Description for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := WideCompareText(TCLPGroup(item1).Description, TCLPGroup(item2).Description);
end;

{ *------------------------------------------------------------------------------
  Comparison of 2 TCLPGroup items by Necessity for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCLPGroupCompareNecessity(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCLPGroup items by Necessity for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := integer(TCLPGroup(item1).Necessity) - integer(TCLPGroup(item2).Necessity);
end;

{ *------------------------------------------------------------------------------
  Comparison of 2 TCLPGroup items by Parameters for sorting purposes.

  @param item1  Item to be compared
  @param item2  Item to compare against
  @return  Either zero or a negativ or positive value.
  ------------------------------------------------------------------------------- }
function TCLPGroupCompareParameters(item1, item2: pointer): integer;
   // Purpose: Comparison of 2 TCLPGroup items by Parameters for sorting purposes.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   // If no comparison method is available here, the compiler should complain.
   Result := WideCompareText(TCLPGroup(item1).ItemText, TCLPGroup(item2).ItemText);
end;

{$ENDIF CollectionSorting}
{ TCommandLineParameterItem }

{ *------------------------------------------------------------------------------
  Assigns properties of one TCommandLineParameterItem to another.

  @param Source  Object which properties should be copied.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItem.Assign(Source: TPersistent);
// Purpose: Assigns properties of one TCommandLineParameterItem to another.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if Source is TCommandLineParameterItem then
      with Source as TCommandLineParameterItem do begin
         Self.FOwner := FOwner;
         Self.FParameterName := FParameterName;
         Self.FSyntax := FSyntax;
         Self.FDescription := FDescription;
      end else
      inherited Assign(Source);
end;

{ *------------------------------------------------------------------------------
  Constructor for TCommandLineParameterItem

  @param ACollection  The collection object this TCommandLineParameterItem is inserted to.
  ------------------------------------------------------------------------------- }
constructor TCommandLineParameterItem.Create(ACollection: TCollection);
   // Purpose: Constructor for TCommandLineParameterItem.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited Create(ACollection);
   FOwner := TCommandLineParameterItems(ACollection);
end;

{ *------------------------------------------------------------------------------
  Destructor for TCommandLineParameterItem
  ------------------------------------------------------------------------------- }
destructor TCommandLineParameterItem.Destroy;
   // Purpose: Destructor for TCommandLineParameterItem.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited Destroy;
end;

function TCommandLineParameterItem.MatchesString(AText: String): boolean;
begin
   AText := LowerCase(AText);
   if Copy(AText, 1, 1) = '/' then begin
      Delete(AText, 1, 1);
   end;
   if Copy(AText, 1, 2) = '--' then begin
      Delete(AText, 1, 2);
   end;
   if Copy(AText, 1, 1) = '-' then begin
      Delete(AText, 1, 1);
   end;
   Result := WideSameText(AText, FParameterName);
end;

{ *------------------------------------------------------------------------------
  Returns string representing this item.

  @return  Name of item
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItem.GetDisplayName: string;
   // Purpose: Returns string representing this item.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   Result := UTF8Encode(FParameterName);
   if Result = '' then begin
      Result := inherited GetDisplayName;
   end;
end;

function TCommandLineParameterItem.GetIsSet: boolean;
begin
   Result := FIsSet;
end;

{ *------------------------------------------------------------------------------
  Sets the ParameterName property of TCommandLineParameterItem.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItem.SetParameterName(const Value: String);
// Purpose: Sets the ParameterName property of TCommandLineParameterItem.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FParameterName <> Value) then begin
      // implement custom actions
      if Length(FSyntax) = 0 then begin
         FSyntax := Value;
      end;
   end;
   FParameterName := Value;
   FIsSet := FindCmdLineSwitch(UTF8Encode(FParameterName));
end;

{ *------------------------------------------------------------------------------
  Sets the Syntax property of TCommandLineParameterItem.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItem.SetSyntax(const Value: String);
// Purpose: Sets the Syntax property of TCommandLineParameterItem.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FSyntax <> Value) then begin
      // implement custom actions
   end;
   FSyntax := Value;
end;

{ *------------------------------------------------------------------------------
  Sets the Description property of TCommandLineParameterItem.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItem.SetDescription(const Value: String);
// Purpose: Sets the Description property of TCommandLineParameterItem.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FDescription <> Value) then begin
      // implement custom actions
   end;
   FDescription := Value;
end;

{ TCommandLineParameterItems }

{ *------------------------------------------------------------------------------
  Adds a new item of type TCommandLineParameterItem to list TCommandLineParameterItems.

  @return  Returns the added object of type TCommandLineParameterItem.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItems.Add: TCommandLineParameterItem;
   // Purpose: Adds a new item of type TCommandLineParameterItem to list TCommandLineParameterItems.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   Result := TCommandLineParameterItem(inherited Add);
end;

{ *------------------------------------------------------------------------------
  Constructor for TCommandLineParameterItems.
  ------------------------------------------------------------------------------- }
constructor TCommandLineParameterItems.Create;
   // Purpose: Constructor for TCommandLineParameterItems.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited Create(TCommandLineParameterItem);
end;

{ *------------------------------------------------------------------------------
  Destructor for TCommandLineParameterItems.
  ------------------------------------------------------------------------------- }
destructor TCommandLineParameterItems.Destroy;
   // Purpose: Destructor for TCommandLineParameterItems.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited;
end;

{ *------------------------------------------------------------------------------
  Returns first TCommandLineParameterItem matching search parameters.

  @param   AParameterName  The contents that is to be searched for.
  @return  Found item that matches ParameterName, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItems.FindParameterName(const AParameterName: String): TCommandLineParameterItem;
   // Purpose: Returns first TCommandLineParameterItem matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).ParameterName = AParameterName then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns first TCommandLineParameterItem matching search parameters.

  @param   ASyntax  The contents that is to be searched for.
  @return  Found item that matches Syntax, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItems.FindSyntax(const ASyntax: String): TCommandLineParameterItem;
   // Purpose: Returns first TCommandLineParameterItem matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).Syntax = ASyntax then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns first TCommandLineParameterItem matching search parameters.

  @param   ADescription  The contents that is to be searched for.
  @return  Found item that matches Description, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItems.FindDescription(const ADescription: String): TCommandLineParameterItem;
   // Purpose: Returns first TCommandLineParameterItem matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).Description = ADescription then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns one TCommandLineParameterItem by index.

  @param   Index  Index of item to be retrieved.
  @return  Item of type TCommandLineParameterItem identified by Index.
  ------------------------------------------------------------------------------- }
function TCommandLineParameterItems.GetItem(AIndex: integer): TCommandLineParameterItem;
   // Purpose: Returns one TCommandLineParameterItem by index.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   Result := TCommandLineParameterItem(inherited GetItem(AIndex));
end;

{ *------------------------------------------------------------------------------
  Sets one TCommandLineParameterItem by index.

  @param   Index  Index of item to be set.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItems.SetItem(AIndex: integer; AValue: TCommandLineParameterItem);
// Purpose: Sets one TCommandLineParameterItem by index.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited SetItem(AIndex, AValue);
end;

{$IFDEF CollectionSorting}

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameterItems by ParameterName.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItems.SortParameterName;
// Purpose: Sorts TCommandLineParameterItems by ParameterName.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCommandLineParameterItemCompareParameterName);
end;

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameterItems by Syntax.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItems.SortSyntax;
// Purpose: Sorts TCommandLineParameterItems by Syntax.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCommandLineParameterItemCompareSyntax);
end;

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameterItems by Description.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameterItems.SortDescription;
// Purpose: Sorts TCommandLineParameterItems by Description.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCommandLineParameterItemCompareDescription);
end;

{$ENDIF CollectionSorting}
{ TCLPGroup }

{ *------------------------------------------------------------------------------
  Assigns properties of one TCLPGroup to another.

  @param Source  Object which properties should be copied.
  ------------------------------------------------------------------------------- }
function TCLPGroup.AddParameter(AParameterName, ADescription: String): TCommandLineParameterItem;
begin
   Result := Parameters.Add;
   with Result do begin
      ParameterName := AParameterName;
      Description := ADescription;
   end;
end;

procedure TCLPGroup.Assign(Source: TPersistent);
// Purpose: Assigns properties of one TCLPGroup to another.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if Source is TCLPGroup then
      with Source as TCLPGroup do begin
         Self.FOwner := FOwner;
         Self.FNecessity := FNecessity;
         Self.FParameters.Assign(FParameters);
         Self.FGroupName := FGroupName;
         Self.FDescription := FDescription;
      end else
      inherited Assign(Source);
end;

{ *------------------------------------------------------------------------------
  Constructor for TCLPGroup

  @param ACollection  The collection object this TCLPGroup is inserted to.
  ------------------------------------------------------------------------------- }
constructor TCLPGroup.Create(ACollection: TCollection);
   // Purpose: Constructor for TCLPGroup.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited Create(ACollection);
   FOwner := TCommandLineParameters(ACollection);
   FParameters := TCommandLineParameterItems.Create;
end;

{ *------------------------------------------------------------------------------
  Destructor for TCLPGroup
  ------------------------------------------------------------------------------- }
destructor TCLPGroup.Destroy;
   // Purpose: Destructor for TCLPGroup.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   FParameters.Free;
   inherited Destroy;
end;

{ *------------------------------------------------------------------------------
  Returns string representing this item.

  @return  Name of item
  ------------------------------------------------------------------------------- }
function TCLPGroup.GetDisplayName: string;
   // Purpose: Returns string representing this item.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   Result := UTF8Encode(FGroupName);
   if Result = '' then begin
      Result := inherited GetDisplayName;
   end;
end;

function TCLPGroup.GetItemText: String;
var
   i: integer;
begin
   for i := 0 to Pred(Parameters.Count) do begin
      if i = 0 then begin
         Result := Parameters[i].Syntax;
      end else begin
         Result := Result + ',' + Parameters[i].Syntax;
      end;
   end;
end;

{ *------------------------------------------------------------------------------
  Sets the GroupName property of TCLPGroup.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCLPGroup.SetGroupName(const Value: String);
// Purpose: Sets the GroupName property of TCLPGroup.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FGroupName <> Value) then begin
      // implement custom actions
   end;
   FGroupName := Value;
end;

{ *------------------------------------------------------------------------------
  Sets the Description property of TCLPGroup.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCLPGroup.SetDescription(const Value: String);
// Purpose: Sets the Description property of TCLPGroup.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FDescription <> Value) then begin
      // implement custom actions
   end;
   FDescription := Value;
end;

{ *------------------------------------------------------------------------------
  Sets the Necessity property of TCLPGroup.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCLPGroup.SetNecessity(const Value: TGroupNecessities);
// Purpose: Sets the Necessity property of TCLPGroup.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FNecessity <> Value) then begin
      // implement custom actions
   end;
   FNecessity := Value;
end;

{ *------------------------------------------------------------------------------
  Sets the Parameters property of TCLPGroup.

  @param   Value  The contents that is to be searched for.
  ------------------------------------------------------------------------------- }
procedure TCLPGroup.SetParameters(const Value: TCommandLineParameterItems);
// Purpose: Sets the Parameters property of TCLPGroup.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   if (FParameters <> Value) then begin
      // implement custom actions
   end;
   FParameters := Value;
end;

{ TCommandLineParameters }

{ *------------------------------------------------------------------------------
  Adds a new item of type TCLPGroup to list TCommandLineParameters.

  @return  Returns the added object of type TCLPGroup.
  ------------------------------------------------------------------------------- }
function TCommandLineParameters.Add: TCLPGroup;
   // Purpose: Adds a new item of type TCLPGroup to list TCommandLineParameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   Result := TCLPGroup(inherited Add);
end;

{ *------------------------------------------------------------------------------
  Constructor for TCommandLineParameters.
  ------------------------------------------------------------------------------- }
constructor TCommandLineParameters.Create;
   // Purpose: Constructor for TCommandLineParameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited Create(TCLPGroup);
end;

{ *------------------------------------------------------------------------------
  Destructor for TCommandLineParameters.
  ------------------------------------------------------------------------------- }
destructor TCommandLineParameters.Destroy;
   // Purpose: Destructor for TCommandLineParameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited;
end;

{ *------------------------------------------------------------------------------
  Returns first TCLPGroup matching search parameters.

  @param   AGroupName  The contents that is to be searched for.
  @return  Found item that matches GroupName, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameters.FindGroupName(const AGroupName: String): TCLPGroup;
   // Purpose: Returns first TCLPGroup matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).GroupName = AGroupName then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns first TCLPGroup matching search parameters.

  @param   ADescription  The contents that is to be searched for.
  @return  Found item that matches Description, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameters.FindDescription(const ADescription: String): TCLPGroup;
   // Purpose: Returns first TCLPGroup matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).Description = ADescription then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns first TCLPGroup matching search parameters.

  @param   ANecessity  The contents that is to be searched for.
  @return  Found item that matches Necessity, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameters.FindNecessity(const ANecessity: TGroupNecessities): TCLPGroup;
   // Purpose: Returns first TCLPGroup matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).Necessity = ANecessity then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns first TCLPGroup matching search parameters.

  @param   AParameters  The contents that is to be searched for.
  @return  Found item that matches Parameters, or nil if nothing found.
  ------------------------------------------------------------------------------- }
function TCommandLineParameters.FindParameters(const AParameters: TCommandLineParameterItems): TCLPGroup;
   // Purpose: Returns first TCLPGroup matching search parameters.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
var
   iItem: integer;
begin
   Result := nil;
   for iItem := 0 to Pred(Count) do
      if GetItem(iItem).Parameters = AParameters then begin
         Result := GetItem(iItem);
         Exit;
      end;
end;

{ *------------------------------------------------------------------------------
  Returns one TCLPGroup by index.

  @param   Index  Index of item to be retrieved.
  @return  Item of type TCLPGroup identified by Index.
  ------------------------------------------------------------------------------- }
function TCommandLineParameters.GetItem(AIndex: integer): TCLPGroup;
   // Purpose: Returns one TCLPGroup by index.
   // Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   Result := TCLPGroup(inherited GetItem(AIndex));
end;

function TCommandLineParameters.Insert(AIndex: integer): TCLPGroup;
begin
   Result := TCLPGroup(inherited Insert(AIndex));
end;

{ *------------------------------------------------------------------------------
  Sets one TCLPGroup by index.

  @param   Index  Index of item to be set.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameters.SetItem(AIndex: integer; AValue: TCLPGroup);
// Purpose: Sets one TCLPGroup by index.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   inherited SetItem(AIndex, AValue);
end;

{$IFDEF CollectionSorting}

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameters by GroupName.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameters.SortGroupName;
// Purpose: Sorts TCommandLineParameters by GroupName.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCLPGroupCompareGroupName);
end;

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameters by Description.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameters.SortDescription;
// Purpose: Sorts TCommandLineParameters by Description.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCLPGroupCompareDescription);
end;

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameters by Necessity.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameters.SortNecessity;
// Purpose: Sorts TCommandLineParameters by Necessity.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCLPGroupCompareNecessity);
end;

{ *------------------------------------------------------------------------------
  Sorts TCommandLineParameters by Parameters.
  ------------------------------------------------------------------------------- }
procedure TCommandLineParameters.SortParameters;
// Purpose: Sorts TCommandLineParameters by Parameters.
// Date:    2011-03-10 (Function created by CollectionTemplater)
begin
   GetCollectionList(Self).Sort(@TCLPGroupCompareParameters);
end;

{$ENDIF CollectionSorting}

procedure TCommandLineParameters.TestWriteLn;
begin
   if FindCmdLineSwitch('help') or FindCmdLineSwitch('?') or FindCmdLineSwitch('man') then begin
      Self.WriteLn;
      Halt(0);
   end;
end;

procedure TCommandLineParameters.ToLines(ALines: TStrings);
const
   LeftColumnWidth = 20;
var
   iGroup, iParameter: integer;
   g: TCLPGroup;

   procedure LineOut(AText: String);
   begin
      {$IFDEF FPC}
      ALines.Add(UTF8Encode(AText));
      {$ELSE FPC}
      ALines.Add(AText);
      {$ENDIF FPC}
   end;

begin
   for iGroup := 0 to Pred(Count) do begin
      g := GetItem(iGroup);
      if g.Parameters.Count > 0 then begin
         LineOut('');
         if Length(g.Description) > 0 then begin
            LineOut(WideFormat('%-20s    - %s', [g.GroupName, g.Description]));
         end else begin
            LineOut(WideFormat('%-20s', [g.GroupName]));
         end;
         for iParameter := 0 to Pred(g.Parameters.Count) do begin
            LineOut(WideFormat('  /%-20s - %s', [g.Parameters[iParameter].Syntax, g.Parameters[iParameter].Description]));
         end;
      end;
   end;
end;

procedure TCommandLineParameters.WriteLn;
var
   sl: TStringList;
   i: integer;
begin
   sl := TStringList.Create;
   try
      ToLines(sl);
      if IsConsole then begin
         for i := 0 to Pred(sl.Count) do begin
            System.WriteLn(sl[i]);
         end;
      end else begin
         // ShowCommandLineHelp;
         // MessageBox(0, PChar(sl.Text), 'Help', MB_OK or MB_ICONINFORMATION);
      end;
   finally
      sl.Free;
   end;
end;


initialization

   CommandLineParametersInt := nil;
   CLPGroupBasic.AddParameter('help', 'shows this page');
   CLPAutomationVerbose := CLPGroupBasic.AddParameter('verbose', 'displays more output');
   CLPAutomationSilent := CLPGroupAuto.AddParameter('silent', 'avoid unnecessary output');

finalization

   CommandLineParametersInt.Free;

end.
