{
@author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Some simple number conversions. !!check ranges: longInttoHex?!!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2016-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-08-30  pk  10m  Fixed UInt64Swap to work on FPC (data type issue)
// 2011-06-14  pk   1h  More updates for Delphi XE and snlBase package.
// 2010-10-01  pk  20m  Updated for Delphi XE (all warnings fixed)
// 2010-09-30  pk  60m  Started splitting into dedicated A/W functions
// 2007-10-13  pk  10m  Added first octal functions
// 2007-06-11  pk   5m  Added Word2KToHex function
// 2007-04-05  pk   1h  Cleaned up names etc. & prepared unit tests
// 2007-01-30  pk  10m  Got rid of SysUtils dependency
// 2006-09-13  pk  10m  Checked for memory leaks
// *****************************************************************************
   )
}

unit Firefly.Numbers;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

interface

uses
   {$IFDEF Unicode}
   Character,
   {$ENDIF Unicode}
   SysUtils;

{
  @name converts an Integer to AnsiString.

  @param(AValue is the number to convert.)
  @returns(String representation.)
}
function snlIntToStr(AValue: integer): ansistring; overload;

{
  @name converts an Integer to AnsiString.

  @param(AValue is the number to convert.)
  @returns(String representation.)
}
function snlIntToStr(AValue: word): ansistring; overload;

{
  @name converts an Integer to AnsiString.

  @param(AValue is the number to convert.)
  @returns(String representation.)
}
function snlIntToStr(AValue: cardinal): ansistring; overload;

{
  @name converts an Integer to AnsiString.

  @param(AValue is the number to convert.)
  @returns(String representation.)
}
function snlIntToStr(AValue: int64): ansistring; overload;

{
  @name converts an Integer to AnsiString.

  @param(AValue is the number to convert.)
  @returns(String representation.)
}
function snlIntToStr(AValue: UInt64): ansistring; overload;

// Functions converting value to HEX

{
  @name converts the lower 4 bits of a byte to a nibble, aka singlehexadecimal char.

  @param(ANibbleValue has the nibble value in the lower 4 bytes.)
  @returns(Hexadecimal representation.)
}
function NibbleToHex(const ANibbleValue: byte): char;

{
  @name converts the lower 4 bits of a byte to a nibble, aka singlehexadecimal char.

  @param(ANibbleValue has the nibble value in the lower 4 bytes.)
  @returns(Hexadecimal representation.)
}
function NibbleToHexA(const ANibbleValue: byte): AnsiChar;

{
  @name converts the lower 4 bits of a byte to a nibble, aka singlehexadecimal char.

  @param(ANibbleValue has the nibble value in the lower 4 bytes.)
  @returns(Hexadecimal representation, upper case.)
}
function NibbleToHexW(const ANibbleValue: byte): widechar;

{
  @name converts a byte to hexadecimal  representation.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function ByteToHex(const AByteValue: byte): string;

{
  @name converts a byte to hexadecimal  representation.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function ByteToHexA(const AByteValue: byte): ansistring;

{
  @name converts a byte to hexadecimal  representation.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function ByteToHexW(const AByteValue: byte): WideString;

{
  @name converts a byte to hexadecimal  representation lower case.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, lower case.)
}
function ByteToHexLA(const AByteValue: byte): ansistring;

{
  @name converts a byte with signs in 2K mode to hexadecimal  representation.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function Byte2KToShortIntHex(const AByteValue: byte): string;

{
  @name converts a byte with signs in 2K mode to hexadecimal  representation.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function Byte2KToShortIntHexA(const AByteValue: byte): ansistring;

{
  @name converts a byte with signs in 2K mode to hexadecimal  representation.

  @param(AByteValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function Byte2KToShortIntHexW(const AByteValue: byte): WideString;

{
  @name converts a word to hexadecimal  representation.

  @param(AWordValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function WordToHex(const AWordValue: word): string;

{
  @name converts a word to hexadecimal  representation.

  @param(AWordValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function WordToHexA(const AWordValue: word): ansistring;

{
  @name converts a word to hexadecimal  representation.

  @param(AWordValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function WordToHexW(const AWordValue: word): WideString;

{
  @name converts a word with signs in 2K mode to hexadecimal  representation.

  @param(AWordValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function Word2KToHex(const AWordValue: word): string;

{
  @name converts a word with signs in 2K mode to hexadecimal  representation.

  @param(AWordValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function Word2KToHexA(const AWordValue: word): ansistring;

{
  @name converts a word with signs in 2K mode to hexadecimal  representation.

  @param(AWordValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation, upper case.)
}
function Word2KToHexW(const AWordValue: word): WideString;

function WordToSwapHex(const AWordValue: word): string;
function WordToSwapHexA(const AWordValue: word): ansistring;
function WordToSwapHexW(const AWordValue: word): WideString;
function LongWordToHex(const ALongWordValue: longword): string;
function LongWordToHexA(const ALongWordValue: longword): ansistring;
function LongWordToHexW(const ALongWordValue: longword): WideString;
function LongWordToSwapHex(const ALongWordValue: longword): string;
function LongWordToSwapHexA(const ALongWordValue: longword): ansistring;
function LongWordToSwapHexW(const ALongWordValue: longword): WideString;
function LongWordSwap(const ALongWordValue: longword): longword;
function LongWord2KToHex(const ALongWordValue: longword): string;
function LongWord2KToHexA(const ALongWordValue: longword): ansistring;
function LongWord2KToHexW(const ALongWordValue: longword): WideString;

{
  @name converts a LongInt to hexadecimal  representation. ! negative values unclear !

  @param(ALongIntValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function LongIntToHex(const ALongIntValue: longint): string;

{
  @name converts a LongInt to hexadecimal  representation.

  @param(ALongIntValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function LongIntToHexA(const ALongIntValue: longint): ansistring;

{
  @name converts a LongInt to hexadecimal  representation.

  @param(ALongIntValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function LongIntToHexW(const ALongIntValue: longint): WideString;

{
  @name converts a Int64 to hexadecimal  representation.

  @param(AInt64Value has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function Int64ToHex(const AInt64Value: int64): string;

{
  @name converts a Int64 to hexadecimal  representation.

  @param(AInt64Value has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function Int64ToHexA(const AInt64Value: int64): ansistring;

{
  @name converts a Int64 to hexadecimal  representation.

  @param(AInt64Value has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function Int64ToHexW(const AInt64Value: int64): WideString;

function CardinalSwap(const AInt64Value: cardinal): cardinal;
function UInt64Swap(const AInt64Value: UInt64): UInt64;

{
  @name converts a Double to hexadecimal  representation. 
  ????
  @param(AValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function DoubleToHex(AValue: double): string;

{
  @name converts a Double to hexadecimal  representation.

  @param(AValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function DoubleToHexA(AValue: double): ansistring;

{
  @name converts a Double to hexadecimal  representation.

  @param(AValue has the value to present in hexadecimal.)
  @returns(Hexadecimal representation)
}
function DoubleToHexW(AValue: double): WideString;

// Functions converting value to BINARY

{
  @name converts a Byte to Bit String.

  @param(AByteValue has the value to present in Bit String.)
  @returns(Bit String representation)
}
function ByteToBits(const AByteValue: byte): string;

{
  @name converts a Byte to Bit String.

  @param(AByteValue has the value to present in Bit String.)
  @returns(Bit String representation)
}
function ByteToBitsA(const AByteValue: byte): ansistring;

{
  @name converts a Byte to Bit String.

  @param(AByteValue has the value to present in Bit String.)
  @returns(Bit String representation)
}
function ByteToBitsW(const AByteValue: byte): WideString;

{
  @name converts a Byte Array to Bit String.

  @param(AByteValues has the value to present in Bit String.)
  @returns(Bit String representation)
}
function BytesToBits(const AByteValues: array of byte): string;

{
  @name converts a Byte Array to Bit String.

  @param(AByteValues has the value to present in Bit String.)
  @returns(Bit String representation)
}
function BytesToBitsA(const AByteValues: array of byte): ansistring;

{
  @name converts a Byte Array to Bit String.

  @param(AByteValues has the value to present in Bit String.)
  @returns(Bit String representation)
}
function BytesToBitsW(const AByteValues: array of byte): WideString;

{
  @name converts a Cardinal to Bit String. ????

  @param(AByteValue has the value to present in Bit String.)
  @returns(Bit String representation)
}
function CardinalToBits(const AValue: cardinal): string;

{
  @name converts a Cardinal to Bit String. ????

  @param(AValue has the value to present in Bit String.)
  @returns(Bit String representation)
}
function CardinalToBitsA(const AValue: cardinal): ansistring;

{
  @name converts a Cardinal to Bit String. ????

  @param(AValue has the value to present in Bit String.)
  @returns(Bit String representation)
}
function CardinalToBitsW(const AValue: cardinal): WideString;

{
  @name converts a Cardinal to Binary String. 

  @param(AByteValue has the value to present in Binary String.)
  @returns(Binary String representation)
}
function CardinalToBinary(const ACardinalValue: cardinal; ATrimLeadingZeros: boolean = True): string;

{
  @name converts a Cardinal to Binary String. 

  @param(AByteValue has the value to present in Binary String.)
  @returns(Binary String representation)
}
function CardinalToBinaryA(const ACardinalValue: cardinal; ATrimLeadingZeros: boolean = True): ansistring;

{
  @name converts a Cardinal to Binary String. 

  @param(AByteValue has the value to present in Binary String.)
  @returns(Binary String representation)
}
function CardinalToBinaryW(const ACardinalValue: cardinal; ATrimLeadingZeros: boolean = True): WideString;

{
  @name converts a LongInt to Binary String. 

  @param(ALongIntValue has the value to present in Binary String.)
  @returns(Binary String representation)
}
function LongIntToBinary(const ALongIntValue: longint): string;

{
  @name converts a LongInt to Binary String. 

  @param(ALongIntValue has the value to present in Binary String.)
  @returns(Binary String representation)
}
function LongIntToBinaryA(const ALongIntValue: longint): ansistring;

{
  @name converts a LongInt to Binary String.

  @param(ALongIntValue has the value to present in Binary String.)
  @returns(Binary String representation)
}
function LongIntToBinaryW(const ALongIntValue: longint): WideString;

{
  @name converts a LongInt to Binary String filling with '0' at front up to aStringLength.

  @param(ALongIntValue has the value to present in Binary String.)
  @param(aStringLength: lenght of String representation)
  @returns(Binary String representation)
}
function LongIntToBinaryFill(const ALongIntValue: longint; const aStringLength: integer): string;

{
  @name converts a LongInt to Binary String filling with '0' at front up to aStringLength.

  @param(ALongIntValue has the value to present in Binary String.)
  @param(aStringLength: lenght of String representation)
  @returns(Binary String representation)
}
function LongIntToBinaryFillA(const ALongIntValue: longint; const aStringLength: integer): ansistring;

{
  @name converts a LongInt to Binary String filling with '0' at front up to aStringLength.

  @param(ALongIntValue has the value to present in Binary String.)
  @param(aStringLength: lenght of String representation)
  @returns(Binary String representation)
}
function LongIntToBinaryFillW(const ALongIntValue: longint; const aStringLength: integer): WideString;

{
  @name converts a Int64 Value to Binary String. 

  @param(AInt64Value has the value to present in Binary String.)
  @returns(Binary String representation)
}
function Int64ToBinary(const AInt64Value: int64; ATrimLeadingZeros: boolean = True): string;

{
  @name converts a Int64 Value to Binary String. 

  @param(AInt64Value has the value to present in Binary String.)
  @returns(Binary String representation)
}
function Int64ToBinaryA(const AInt64Value: int64; ATrimLeadingZeros: boolean = True): ansistring;

{
  @name converts a Int64 Value to Binary String. 

  @param(AInt64Value has the value to present in Binary String.)
  @returns(Binary String representation)
}
function Int64ToBinaryW(const AInt64Value: int64; ATrimLeadingZeros: boolean = True): WideString;

{
  @name converts a String to hexadecimal  Values with empty spaces between.
        '123' -> '31 32 33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToBinary(const ANormalText: ansistring): string; overload;

{
  @name converts a String to hexadecimal  Values with empty spaces between.
        '123' -> '31 32 33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToBinary(const ANormalText: WideString): string; overload;

{
  @name converts a String to hexadecimal  Values with empty spaces between.
        '123' -> '31 32 33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToBinaryA(const ANormalText: ansistring): ansistring;

{
  @name converts a String to hexadecimal  Values with empty spaces between.
        '123' -> '31 32 33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToBinaryW(const ANormalText: ansistring): WideString;

{
  @name converts a String to hexadecimal  Values with commas between.
        '123' -> '31,32,33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToRegBinary(const ANormalText: ansistring): string; overload;

{
  @name converts a String to hexadecimal  Values with commas between.
        '123' -> '31,32,33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToRegBinary(const ANormalText: WideString): string; overload;

{
  @name converts a String to hexadecimal  Values with commas between.
        '123' -> '31,32,33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToRegBinaryA(const ANormalText: ansistring): ansistring;

{
  @name converts a String to hexadecimal  Values with commas between.
        '123' -> '31,32,33'   

  @param(ANormalText has the value to present in Binary String.)
  @returns(Binary String representation)
}
function StringToRegBinaryW(const ANormalText: WideString): WideString;

{
  @name converts a String to hexadecimal  Values with separator between.
        '123' -> '31s32s33' with separator 's'  

  @param(ANormalText has the value to present in Binary String.)
  @param(ASeparatorChar is the separator.)
  @returns(Binary String representation)
}
function StringToBinarySeparator(const ANormalText: ansistring; const ASeparatorChar: char): string;

{
  @name converts a String to hexadecimal  Values with separator between.
        '123' -> '31s32s33' with separator 's'  

  @param(ANormalText has the value to present in Binary String.)
  @param(ASeparatorChar is the separator.)
  @param(ALowerCase decides whether Result is in lowercase)
  @returns(Binary String representation)
}
function StringToBinarySeparatorA(const ANormalText: ansistring; const ASeparatorChar: AnsiChar; ALowerCase: boolean = False): ansistring;

{
  @name converts a String to hexadecimal  Values with separator between.
        '123' -> '31s32s33' with separator 's'  

  @param(ANormalText has the value to present in Binary String.)
  @param(ASeparatorChar is the separator.)
  @returns(Binary String representation)
}
function StringToBinarySeparatorW(const ANormalText: ansistring; const ASeparatorChar: widechar): WideString; overload;

{
  @name converts a String to hexadecimal  Values with separator between.
        '123' -> '31s32s33' with separator 's'  

  @param(ANormalText has the value to present in Binary String.)
  @param(ASeparatorChar is the separator.)
  @returns(Binary String representation)
}
function StringToBinarySeparatorW(const ANormalText: WideString; const ASeparatorChar: widechar): WideString; overload;

{ .$IFNDEF FPC }
function WideStringToBinary(const ANormalText: ansistring): ansistring; overload;
function WideStringToBinary(const ANormalText: WideString): WideString; overload;
function WideStringToBinarySeparator(const ANormalText: ansistring; const ASeparatorChar: AnsiChar): ansistring; overload;
function WideStringToBinarySeparator(const ANormalText: WideString; const ASeparatorChar: widechar): WideString; overload;
{ .$ENDIF }
// Functions convertinghexadecimal to value

{
  @name converts ahexadecimal Char Text to Byte. 

  @param(AHexCharText has thehexadecimal Char to present as Byte.)
  @returns(Byte representation)
}
function HexToNibble(const AHexCharText: AnsiChar): byte; overload;

{
  @name converts ahexadecimal Char Text to Byte. 

  @param(AHexCharText has thehexadecimal Char to present as Byte.)
  @returns(Byte representation)
}
function HexToNibble(const AHexCharText: widechar): byte; overload;

{
  @name converts ahexadecimal String Text of lenght 2 to Byte. 

  @param(AHexCharText has thehexadecimal Char to present as Byte.)
  @returns(Byte representation)
}
function HexToByte(const AHexByteText: ansistring): byte; overload;

{
  @name converts ahexadecimal String Text of lenght 2 to Byte. 

  @param(AHexCharText has thehexadecimal Char to present as Byte.)
  @returns(Byte representation)
}
function HexToByte(const AHexByteText: WideString): byte; overload;

{
  @name converts ahexadecimal String Text to longword. 

  @param(AHexCharText has thehexadecimal Char to present as longword.)
  @returns(longword representation)
}
function HexToLongWord(ALongWordText: ansistring): longword; overload;

{
  @name converts ahexadecimal String Text to longword. 

  @param(AHexCharText has thehexadecimal Char to present as longword.)
  @returns(longword representation)
}
function HexToLongWord(ALongWordText: WideString): longword; overload;

{
  @name converts ahexadecimal String Text to longint. 

  @param(AHexCharText has thehexadecimal Char to present as longint.)
  @returns(longword representation)
}
function HexToLongInt(const ALongIntText: ansistring): longint;

{
  @name converts ahexadecimal String Text to longint. 

  @param(AHexCharText has thehexadecimal Char to present as longint.)
  @returns(longint representation)
}
function HexToInt64(const AInt64Text: ansistring): int64; overload;

{
  @name converts ahexadecimal String Text to longint. 

  @param(AHexCharText has thehexadecimal Char to present as longint.)
  @returns(longint representation)
}
function HexToInt64(const AInt64Text: WideString): int64; overload;

{
  @name converts Octal Digit To Byte. 

  @param(ADigit has the Octal to present as byte.)
  @returns(byte representation)
}
function OctalDigitToByte(ADigit: AnsiChar): byte;

{
  @name converts Octals To Byte. (? check ranges ?)

  @param(AOctal has the Octal to present as byte.)
  @returns(byte representation)
}
function OctalToByte(AOctal: ansistring): byte;

{
  @name converts Octals To Byte. (? check ranges ?)

  @param(AOctal has the Octal to present as Cardinal.)
  @returns(byte representation)
}
function OctalToLongWord(AOctal: ansistring): cardinal;

// Functions converting BINARY to value

{
  @name converts Binary To LongInt

  @param(ALongIntText has the String to present as LongInt.)
  @returns(LongInt representation)
}
function BinaryToLongInt(const ALongIntText: ansistring): longint;

{
  @name converts Binary To Cardinal

  @param(ALongIntText has the String to present as Cardinal.)
  @returns(Cardinal representation)
}
function BinaryToCardinal(const ALongIntText: ansistring): cardinal;

{
  @name converts Binary To Int64

  @param(AInt64Text has the String to present as Int64.)
  @returns(Int64 representation)
}
function BinaryToInt64(AInt64Text: ansistring): int64;

function BinaryToString(const ABinaryText: ansistring; const ASeparatorChar: AnsiChar = #0): string;
function BinaryToStringA(const ABinaryText: ansistring; const ASeparatorChar: AnsiChar = #0): ansistring;
function BinaryToStringW(const ABinaryText: ansistring; const ASeparatorChar: AnsiChar = #0): WideString; overload;
function BinaryToStringW(const ABinaryText: WideString; const ASeparatorChar: widechar = #0): WideString; overload;

// Functions converting values
function Byte2KToShortInt(const AByteValue: byte): shortint;
function ShortIntTo2KByte(const AShortIntValue: shortint): byte;
function LongEndianToggle(const ALongIntValue: longint): longint;

function PreviewChrOrHex(const AByteValue: byte): string;
function PreviewChrOrHexA(const AByteValue: byte): ansistring;
function PreviewChrOrHexW(const AByteValue: byte): WideString;

procedure SwapHexInt64String(var AValue: ansistring); overload;
procedure SwapHexInt64String(var AValue: WideString); overload;

implementation

function LongEndianToggle(const ALongIntValue: longint): longint;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongIntValue);
   b2 := byte(ALongIntValue shr 8);
   b3 := byte(ALongIntValue shr 16);
   b4 := byte(ALongIntValue shr 24);
   Result := b4 + (b3 shl 8) + (b2 shl 16) + (b1 shl 24);
end;

function LongIntToBinaryFill(const ALongIntValue: longint; const aStringLength: integer): string;
begin
   Result := LongIntToBinary(ALongIntValue);
   while Length(Result) < aStringLength do begin
      Result := '0' + Result;
   end;
end;

function LongIntToBinaryFillA(const ALongIntValue: longint; const aStringLength: integer): ansistring;
begin
   Result := LongIntToBinaryA(ALongIntValue);
   while Length(Result) < aStringLength do begin
      Result := '0' + Result;
   end;
end;

function LongIntToBinaryFillW(const ALongIntValue: longint; const aStringLength: integer): WideString;
begin
   Result := LongIntToBinaryW(ALongIntValue);
   while Length(Result) < aStringLength do begin
      Result := '0' + Result;
   end;
end;

function LongIntToBinary(const ALongIntValue: longint): string;
var
   sResult: string;
   cChar: char;
   cValue: cardinal;
begin
   sResult := '';
   cValue := cardinal(ALongIntValue);
   while cValue > 0 do begin
      if (cValue and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      cValue := cValue shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   Result := sResult;
end;

function LongIntToBinaryA(const ALongIntValue: longint): ansistring;
var
   sResult: ansistring;
   cChar: AnsiChar;
   cValue: cardinal;
begin
   sResult := '';
   cValue := cardinal(ALongIntValue);
   while cValue > 0 do begin
      if (cValue and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      cValue := cValue shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   Result := sResult;
end;

function LongIntToBinaryW(const ALongIntValue: longint): WideString;
var
   sResult: WideString;
   cChar: widechar;
   cValue: cardinal;
begin
   sResult := '';
   cValue := cardinal(ALongIntValue);
   while cValue > 0 do begin
      if (cValue and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      cValue := cValue shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   Result := sResult;
end;

function CardinalToBinary(const ACardinalValue: cardinal; ATrimLeadingZeros: boolean): string;
var
   sResult: string;
   cChar: char;
   cValue: cardinal;
begin
   sResult := '';
   cValue := ACardinalValue;
   while cValue > 0 do begin
      if (cValue and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      cValue := cValue shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   if not ATrimLeadingZeros then begin
      while Length(sResult) < 32 do begin
         sResult := '0' + sResult;
      end;
   end;
   Result := sResult;
end;

function CardinalToBinaryA(const ACardinalValue: cardinal; ATrimLeadingZeros: boolean): ansistring;
var
   sResult: ansistring;
   cChar: AnsiChar;
   cValue: cardinal;
begin
   sResult := '';
   cValue := ACardinalValue;
   while cValue > 0 do begin
      if (cValue and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      cValue := cValue shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   if not ATrimLeadingZeros then begin
      while Length(sResult) < 32 do begin
         sResult := '0' + sResult;
      end;
   end;
   Result := sResult;
end;

function CardinalToBinaryW(const ACardinalValue: cardinal; ATrimLeadingZeros: boolean): WideString;
var
   sResult: WideString;
   cChar: widechar;
   cValue: cardinal;
begin
   sResult := '';
   cValue := ACardinalValue;
   while cValue > 0 do begin
      if (cValue and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      cValue := cValue shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   if not ATrimLeadingZeros then begin
      while Length(sResult) < 32 do begin
         sResult := '0' + sResult;
      end;
   end;
   Result := sResult;
end;

function Int64ToBinary(const AInt64Value: int64; ATrimLeadingZeros: boolean): string;
var
   sResult: string;
   cChar: char;
   i64Value: int64;
begin
   sResult := '';
   i64Value := AInt64Value;
   {$IFDEF FPC}
   if i64Value < 0 then begin
      Inc(i64Value, 9223372036854775807);
      Inc(i64Value, 1);
   end;
   {$ELSE FPC}
   if i64Value < 0 then begin
      Inc(i64Value, 9223372036854775808);
   end;
   {$ENDIF FPC}
   while i64Value > 0 do begin
      if (i64Value and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      i64Value := i64Value shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   if not ATrimLeadingZeros then begin
      while Length(sResult) < 64 do begin
         sResult := '0' + sResult;
      end;
   end;
   Result := sResult;
end;

function Int64ToBinaryA(const AInt64Value: int64; ATrimLeadingZeros: boolean): ansistring;
var
   sResult: ansistring;
   cChar: AnsiChar;
   i64Value: int64;
begin
   sResult := '';
   i64Value := AInt64Value;
   {$IFNDEF FPC}
   if i64Value < 0 then begin
      Inc(i64Value, 9223372036854775808);
   end;
   {$ENDIF FPC}
   while i64Value > 0 do begin
      if (i64Value and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      i64Value := i64Value shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   if not ATrimLeadingZeros then begin
      while Length(sResult) < 64 do begin
         sResult := '0' + sResult;
      end;
   end;
   Result := sResult;
end;

function Int64ToBinaryW(const AInt64Value: int64; ATrimLeadingZeros: boolean): WideString;
var
   sResult: WideString;
   cChar: widechar;
   i64Value: int64;
begin
   sResult := '';
   i64Value := AInt64Value;
   {$IFNDEF FPC}
   if i64Value < 0 then begin
      Inc(i64Value, 9223372036854775808);
   end;
   {$ENDIF FPC}
   while i64Value > 0 do begin
      if (i64Value and 1) > 0 then begin
         cChar := '1';
      end else begin
         cChar := '0';
      end;
      sResult := cChar + sResult;
      i64Value := i64Value shr 1;
   end;
   if Length(sResult) = 0 then begin
      sResult := '0';
   end;
   if not ATrimLeadingZeros then begin
      while Length(sResult) < 64 do begin
         sResult := '0' + sResult;
      end;
   end;
   Result := sResult;
end;

function BinaryToInt64(AInt64Text: ansistring): int64;
var
   l: int64;
   cnt: int64;
   c: AnsiChar;
begin
   l := 0;
   cnt := 1;
   while Length(AInt64Text) > 0 do begin
      c := AInt64Text[Length(AInt64Text)];
      if c = '1' then begin
         l := l + cnt;
      end;
      cnt := cnt shl 1;
      SetLength(AInt64Text, Length(AInt64Text) - 1);
   end;
   Result := l;
end;

function BinaryToLongInt(const ALongIntText: ansistring): longint;
var
   iValue, iCount, iChar: longint;
   cChar: AnsiChar;
begin
   iValue := 0;
   iCount := 1;
   for iChar := Length(ALongIntText) downto 1 do begin
      cChar := ALongIntText[iChar];
      if cChar = '1' then begin
         iValue := iValue + iCount;
      end;
      iCount := iCount shl 1;
   end;
   Result := iValue;
end;

function BinaryToCardinal(const ALongIntText: ansistring): cardinal;
var
   iCount, iChar: integer;
   cChar: AnsiChar;
   iValue: cardinal;
begin
   iValue := 0;
   iCount := 1;
   for iChar := Length(ALongIntText) downto 1 do begin
      cChar := ALongIntText[iChar];
      if cChar = '1' then begin
         iValue := iValue + cardinal(iCount);
      end;
      iCount := iCount shl 1;
   end;
   Result := iValue;
end;

function StringToBinary(const ANormalText: ansistring): string;
begin
   Result := StringToBinarySeparator(ANormalText, AnsiChar(' '));
end;

function StringToBinary(const ANormalText: WideString): string;
begin
   Result := String(StringToBinarySeparatorW(ANormalText, widechar(' ')));
end;

function StringToBinaryA(const ANormalText: ansistring): ansistring;
begin
   Result := StringToBinarySeparatorA(ANormalText, ' ');
end;

function StringToBinaryW(const ANormalText: ansistring): WideString;
begin
   Result := StringToBinarySeparatorW(ANormalText, widechar(' '));
end;

function StringToRegBinary(const ANormalText: ansistring): string;
const
   WCommaA: AnsiChar = ',';
begin
   Result := string(StringToBinarySeparatorA(ansistring(ANormalText), WCommaA));
end;

function StringToRegBinary(const ANormalText: WideString): string;
const
   WCommaW: widechar = ',';
begin
   Result := String(StringToBinarySeparatorW(WideString(ANormalText), WCommaW));
end;

function StringToRegBinaryA(const ANormalText: ansistring): ansistring;
begin
   Result := StringToBinarySeparatorA(ANormalText, ',');
end;

function StringToRegBinaryW(const ANormalText: WideString): WideString;
begin
   Result := StringToBinarySeparatorW(ANormalText, widechar(','));
end;

function StringToBinarySeparator(const ANormalText: ansistring; const ASeparatorChar: char): string;
var
   i: integer;
   b: byte;
begin
   Result := '';
   if Length(ANormalText) = 0 then begin
      Exit;
   end;
   if Length(ANormalText) = 1 then begin
      Result := ByteToHex(Ord(ANormalText[1]));
   end else begin
      for i := 1 to Length(ANormalText) do begin
         b := Ord(ANormalText[i]);
         if (not ((b = 0) and (i = Length(ANormalText)))) then begin
            Result := Result + ByteToHex(b);
            if (i < Length(ANormalText)) and (ASeparatorChar <> #0) then begin
               Result := Result + ASeparatorChar;
            end;
         end;
      end;
   end;
end;

function StringToBinarySeparatorA(const ANormalText: ansistring; const ASeparatorChar: AnsiChar; ALowerCase: boolean): ansistring;
var
   i: integer;
   b: byte;
begin
   Result := '';
   if Length(ANormalText) = 0 then begin
      Exit;
   end;
   if Length(ANormalText) = 1 then begin
      Result := ByteToHexA(Ord(ANormalText[1]));
   end else begin
      for i := 1 to Length(ANormalText) do begin
         b := Ord(ANormalText[i]);
         if (not ((b = 0) and (i = Length(ANormalText)))) then begin
            if ALowerCase then begin
               Result := Result + ByteToHexLA(b);
            end else begin
               Result := Result + ByteToHexA(b);
            end;
            if (i < Length(ANormalText)) and (ASeparatorChar <> #0) then begin
               Result := Result + ASeparatorChar;
            end;
         end;
      end;
   end;
end;

function StringToBinarySeparatorW(const ANormalText: ansistring; const ASeparatorChar: widechar): WideString;
var
   i: integer;
   b: byte;
begin
   Result := '';
   if Length(ANormalText) = 0 then begin
      Exit;
   end;
   if Length(ANormalText) = 1 then begin
      Result := ByteToHexW(Ord(ANormalText[1]));
   end else begin
      for i := 1 to Length(ANormalText) do begin
         b := Ord(ANormalText[i]);
         if (not ((b = 0) and (i = Length(ANormalText)))) then begin
            Result := Result + ByteToHexW(b);
            if (i < Length(ANormalText)) and (ASeparatorChar <> #0) then begin
               Result := Result + ASeparatorChar;
            end;
         end;
      end;
   end;
end;

function StringToBinarySeparatorW(const ANormalText: WideString; const ASeparatorChar: widechar): WideString;
var
   i: integer;
   b1, b2: byte;
   c: widechar;
begin
   Result := '';
   if Length(ANormalText) = 0 then begin
      Exit;
   end;
   if Length(ANormalText) = 1 then begin
      Result := ByteToHexW(Ord(ANormalText[1]));
   end else begin
      for i := 1 to Length(ANormalText) do begin
         c := ANormalText[i];
         b1 := word(c) shr 8 and $FF;
         b2 := word(c) and $FF;
         if (not ((c = #0) and (i = Length(ANormalText)))) then begin
            Result := Result + ByteToHexW(b1) + ByteToHexW(b2);
            if (i < Length(ANormalText)) and (ASeparatorChar <> #0) then begin
               Result := Result + ASeparatorChar;
            end;
         end;
      end;
   end;
end;

{ .$IFNDEF FPC }

function WideStringToBinary(const ANormalText: ansistring): ansistring;
begin
   Result := WideStringToBinarySeparator(ANormalText, ' ');
end;

function WideStringToBinary(const ANormalText: WideString): WideString;
begin
   Result := WideStringToBinarySeparator(ANormalText, ' ');
end;

function WideStringToBinarySeparator(const ANormalText: ansistring; const ASeparatorChar: AnsiChar): ansistring;
var
   i: integer;
   w: word;
begin
   Result := '';
   if Length(ANormalText) = 0 then begin
      Exit;
   end;
   if Length(ANormalText) = 1 then begin
      Result := ByteToHexA(Ord(ANormalText[1]));
   end else begin
      for i := 1 to Length(ANormalText) do begin
         w := Ord(ANormalText[i]);
         if (not ((w = 0) and (i = Length(ANormalText)))) then begin
            Result := Result + WordToHexA(w);
            if (i < Length(ANormalText)) and (ASeparatorChar <> #0) then
               Result := Result + ASeparatorChar;
         end;
      end;
   end;
end;

function WideStringToBinarySeparator(const ANormalText: WideString; const ASeparatorChar: widechar): WideString;
var
   i: integer;
   w: word;
begin
   Result := '';
   if Length(ANormalText) = 0 then begin
      Exit;
   end;
   if Length(ANormalText) = 1 then begin
      Result := WideString(WordToHex(Ord(ANormalText[1])));
   end else begin
      for i := 1 to Length(ANormalText) do begin
         w := Ord(ANormalText[i]);
         if (not ((w = 0) and (i = Length(ANormalText)))) then begin
            Result := Result + WordToHexW(w);
            if (i < Length(ANormalText)) and (ASeparatorChar <> #0) then begin
               Result := Result + ASeparatorChar;
            end;
         end;
      end;
   end;
end;

{ .$ENDIF }

function BinaryToStringA(const ABinaryText: ansistring; const ASeparatorChar: AnsiChar): ansistring;
var
   iIncrementor: integer;
   iChar: integer;
   sByte: ansistring;
begin
   Result := '';
   if ASeparatorChar = #0 then begin
      iIncrementor := 2;
   end else begin
      iIncrementor := 3;
   end;
   iChar := 1;
   while (iChar < Length(ABinaryText)) do begin
      sByte := Copy(ABinaryText, iChar, 2);
      Result := Result + AnsiChar(Chr(HexToByte(sByte)));
      Inc(iChar, iIncrementor);
   end;
end;

function BinaryToString(const ABinaryText: ansistring; const ASeparatorChar: AnsiChar): string;
begin
   Result := string(BinaryToStringA(ABinaryText, ASeparatorChar));
end;

function BinaryToStringW(const ABinaryText: ansistring; const ASeparatorChar: AnsiChar): WideString;
begin
   Result := WideString(BinaryToStringA(ABinaryText, ASeparatorChar));
end;

function BinaryToStringW(const ABinaryText: WideString; const ASeparatorChar: widechar = #0): WideString; overload;
begin
   Result := WideString(BinaryToStringA(ansistring(ABinaryText), AnsiChar(ASeparatorChar)));
end;

function NibbleToHex(const ANibbleValue: byte): char;
var
   o1: byte;
const
   chars: array [0 .. 15] of char = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
begin
   o1 := ANibbleValue and $0F;
   Result := chars[o1];
end;

function NibbleToHexA(const ANibbleValue: byte): AnsiChar;
var
   o1: byte;
const
   chars: array [0 .. 15] of AnsiChar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
begin
   o1 := ANibbleValue and $0F;
   Result := chars[o1];
end;

function NibbleToHexW(const ANibbleValue: byte): widechar;
var
   o1: byte;
const
   chars: array [0 .. 15] of widechar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
begin
   o1 := ANibbleValue and $0F;
   Result := chars[o1];
end;

function ByteToHex(const AByteValue: byte): string;
var
   o1, o2: byte;
const
   chars: array [0 .. 15] of char = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
begin
   o1 := byte(AByteValue and $0F);
   o2 := byte(AByteValue and $F0 shr 4);
   Result := chars[o2] + chars[o1];
end;

function ByteToHexA(const AByteValue: byte): ansistring;
var
   o1, o2: byte;
const
   chars: array [0 .. 15] of AnsiChar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
begin
   o1 := byte(AByteValue and $0F);
   o2 := byte(AByteValue and $F0 shr 4);
   Result := chars[o2] + chars[o1];
end;

function ByteToHexLA(const AByteValue: byte): ansistring;
var
   o1, o2: byte;
const
   chars: array [0 .. 15] of AnsiChar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
begin
   o1 := byte(AByteValue and $0F);
   o2 := byte(AByteValue and $F0 shr 4);
   Result := chars[o2] + chars[o1];
end;

function ByteToHexW(const AByteValue: byte): WideString;
var
   o1, o2: byte;
const
   chars: array [0 .. 15] of widechar = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
begin
   o1 := byte(AByteValue and $0F);
   o2 := byte(AByteValue and $F0 shr 4);
   Result := chars[o2] + chars[o1];
end;

function ByteToBits(const AByteValue: byte): string;
var
   i: integer;
begin
   Result := '';
   for i := 7 downto 0 do begin
      if (AByteValue and (1 shl i)) > 0 then begin
         Result := Result + '1';
      end else begin
         Result := Result + '0';
      end;
   end;
end;

function ByteToBitsA(const AByteValue: byte): ansistring;
var
   i: integer;
begin
   Result := '';
   for i := 7 downto 0 do begin
      if (AByteValue and (1 shl i)) > 0 then begin
         Result := Result + '1';
      end else begin
         Result := Result + '0';
      end;
   end;
end;

function ByteToBitsW(const AByteValue: byte): WideString;
var
   i: integer;
begin
   Result := '';
   for i := 7 downto 0 do begin
      if (AByteValue and (1 shl i)) > 0 then begin
         Result := Result + '1';
      end else begin
         Result := Result + '0';
      end;
   end;
end;

function BytesToBits(const AByteValues: array of byte): string;
var
   i: integer;
begin
   Result := '';
   for i := low(AByteValues) to high(AByteValues) do begin
      Result := Result + ByteToBits(AByteValues[i]);
   end;
end;

function BytesToBitsA(const AByteValues: array of byte): ansistring;
var
   i: integer;
begin
   Result := '';
   for i := low(AByteValues) to high(AByteValues) do begin
      Result := Result + ByteToBitsA(AByteValues[i]);
   end;
end;

function BytesToBitsW(const AByteValues: array of byte): WideString;
var
   i: integer;
begin
   Result := '';
   for i := low(AByteValues) to high(AByteValues) do begin
      Result := Result + ByteToBitsW(AByteValues[i]);
   end;
end;

function CardinalToBits(const AValue: cardinal): string;
begin
   Result := ByteToBits((AValue shr 24) and $FF) // left-most byte
      + ByteToBits((AValue shr 16) and $FF) // second byte
      + ByteToBits((AValue shr 8) and $FF) // third byte
      + ByteToBits((AValue shr 0) and $FF);
end;

function CardinalToBitsA(const AValue: cardinal): ansistring;
begin
   Result := ByteToBitsA((AValue shr 24) and $FF) // left-most byte
      + ByteToBitsA((AValue shr 16) and $FF) // second byte
      + ByteToBitsA((AValue shr 8) and $FF) // third byte
      + ByteToBitsA((AValue shr 0) and $FF);
end;

function CardinalToBitsW(const AValue: cardinal): WideString;
begin
   Result := ByteToBitsW((AValue shr 24) and $FF) // left-most byte
      + ByteToBitsW((AValue shr 16) and $FF) // second byte
      + ByteToBitsW((AValue shr 8) and $FF) // third byte
      + ByteToBitsW((AValue shr 0) and $FF);
end;

function Byte2KToShortInt(const AByteValue: byte): shortint;
begin
   if (AByteValue < 129) then begin
      Result := AByteValue;
   end else begin
      Result := shortint(AByteValue - 256);
   end;
end;

function Byte2KToShortIntHex(const AByteValue: byte): string;
begin
   if (AByteValue < 129) then begin
      Result := '+' + ByteToHex(AByteValue);
   end else begin
      Result := '-' + ByteToHex(byte(Abs(AByteValue - 256)));
   end;
end;

function Byte2KToShortIntHexA(const AByteValue: byte): ansistring;
begin
   if (AByteValue < 129) then begin
      Result := '+' + ByteToHexA(AByteValue);
   end else begin
      Result := '-' + ByteToHexA(byte(Abs(AByteValue - 256)));
   end;
end;

function Byte2KToShortIntHexW(const AByteValue: byte): WideString;
begin
   if (AByteValue < 129) then begin
      Result := '+' + ByteToHexW(AByteValue);
   end else begin
      Result := '-' + ByteToHexW(byte(Abs(AByteValue - 256)));
   end;
end;

function ShortIntTo2KByte(const AShortIntValue: shortint): byte;
begin
   Result := byte(AShortIntValue);
end;

function DoubleToHex(AValue: double): string;
var
   b: array [0 .. 7] of byte
{$IFDEF FPC}
   = (0, 0, 0, 0, 0, 0, 0, 0)
{$ENDIF FPC}
   ;
   i: integer;
begin
   Move(AValue, b[0], 8);
   Result := '';
   for i := 0 to 7 do begin
      Result := Result + ByteToHex(b[i]);
   end;
end;

function DoubleToHexA(AValue: double): ansistring;
var
   b: array [0 .. 7] of byte
{$IFDEF FPC}
   = (0, 0, 0, 0, 0, 0, 0, 0)
{$ENDIF FPC}
   ;
   i: integer;
begin
   Move(AValue, b[0], 8);
   Result := '';
   for i := 0 to 7 do begin
      Result := Result + ByteToHexA(b[i]);
   end;
end;

function DoubleToHexW(AValue: double): WideString;
var
   b: array [0 .. 7] of byte
{$IFDEF FPC}
   = (0, 0, 0, 0, 0, 0, 0, 0)
{$ENDIF FPC}
   ;
   i: integer;
begin
   Move(AValue, b[0], 8);
   Result := '';
   for i := 0 to 7 do begin
      Result := Result + ByteToHexW(b[i]);
   end;
end;

function LongWordToHex(const ALongWordValue: longword): string;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := ByteToHex(b4) + ByteToHex(b3) + ByteToHex(b2) + ByteToHex(b1);
end;

function LongWordToHexA(const ALongWordValue: longword): ansistring;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := ByteToHexA(b4) + ByteToHexA(b3) + ByteToHexA(b2) + ByteToHexA(b1);
end;

function LongWordToHexW(const ALongWordValue: longword): WideString;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := ByteToHexW(b4) + ByteToHexW(b3) + ByteToHexW(b2) + ByteToHexW(b1);
end;

function LongIntToHex(const ALongIntValue: longint): string;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongIntValue and ($000000FF));
   b2 := byte(ALongIntValue and ($0000FF00) shr 8);
   b3 := byte(ALongIntValue and ($00FF0000) shr 16);
   b4 := byte(ALongIntValue and ($FF000000) shr 24);
   Result := ByteToHex(b4) + ByteToHex(b3) + ByteToHex(b2) + ByteToHex(b1);
end;

function LongIntToHexA(const ALongIntValue: longint): ansistring;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongIntValue and ($000000FF));
   b2 := byte(ALongIntValue and ($0000FF00) shr 8);
   b3 := byte(ALongIntValue and ($00FF0000) shr 16);
   b4 := byte(ALongIntValue and ($FF000000) shr 24);
   Result := ByteToHexA(b4) + ByteToHexA(b3) + ByteToHexA(b2) + ByteToHexA(b1);
end;

function LongIntToHexW(const ALongIntValue: longint): WideString;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongIntValue and ($000000FF));
   b2 := byte(ALongIntValue and ($0000FF00) shr 8);
   b3 := byte(ALongIntValue and ($00FF0000) shr 16);
   b4 := byte(ALongIntValue and ($FF000000) shr 24);
   Result := ByteToHexW(b4) + ByteToHexW(b3) + ByteToHexW(b2) + ByteToHexW(b1);
end;

function LongWordToSwapHex(const ALongWordValue: longword): string;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := ByteToHex(b1) + ByteToHex(b2) + ByteToHex(b3) + ByteToHex(b4);
end;

function LongWordToSwapHexA(const ALongWordValue: longword): ansistring;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := ByteToHexA(b1) + ByteToHexA(b2) + ByteToHexA(b3) + ByteToHexA(b4);
end;

function LongWordToSwapHexW(const ALongWordValue: longword): WideString;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := ByteToHexW(b1) + ByteToHexW(b2) + ByteToHexW(b3) + ByteToHexW(b4);
end;

function LongWordSwap(const ALongWordValue: longword): longword;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(ALongWordValue and ($000000FF));
   b2 := byte(ALongWordValue and ($0000FF00) shr 8);
   b3 := byte(ALongWordValue and ($00FF0000) shr 16);
   b4 := byte(ALongWordValue and ($FF000000) shr 24);
   Result := (b1 shl 24) + (b2 shl 16) + (b3 shl 8) + b4;
end;

function LongWord2KToHex(const ALongWordValue: longword): string;
var
   li: longint;
begin
   li := (not ALongWordValue + 1);
   if li >= 0 then begin
      Result := '-' + LongWordToHex(li);
   end else begin
      Result := '+' + LongWordToHex(Abs(li));
   end;
end;

function LongWord2KToHexA(const ALongWordValue: longword): ansistring;
var
   li: longint;
begin
   li := (not longint(ALongWordValue) + 1);
   if li >= 0 then begin
      Result := '-' + LongWordToHexA(li);
   end else begin
      Result := '+' + LongWordToHexA(Abs(li));
   end;
end;

function LongWord2KToHexW(const ALongWordValue: longword): WideString;
var
   li: longint;
begin
   li := (not longint(ALongWordValue) + 1);
   if li >= 0 then begin
      Result := '-' + LongWordToHexW(li);
   end else begin
      Result := '+' + LongWordToHexW(Abs(li));
   end;
end;

function WordToHex(const AWordValue: word): string;
var
   b1, b2: byte;
begin
   b1 := byte(AWordValue and ($00FF));
   b2 := byte(AWordValue and ($FF00) shr 8);
   Result := ByteToHex(b2) + ByteToHex(b1);
end;

function WordToHexA(const AWordValue: word): ansistring;
var
   b1, b2: byte;
begin
   b1 := byte(AWordValue and ($00FF));
   b2 := byte(AWordValue and ($FF00) shr 8);
   Result := ByteToHexA(b2) + ByteToHexA(b1);
end;

function WordToHexW(const AWordValue: word): WideString;
var
   b1, b2: byte;
begin
   b1 := byte(AWordValue and ($00FF));
   b2 := byte(AWordValue and ($FF00) shr 8);
   Result := ByteToHexW(b2) + ByteToHexW(b1);
end;

function Word2KToHex(const AWordValue: word): string;
var
   si: smallint;
begin
   si := (not AWordValue + 1);
   if si >= 0 then begin
      Result := '-' + WordToHex(si);
   end else begin
      Result := '+' + WordToHex(Abs(si));
   end;
end;

function Word2KToHexA(const AWordValue: word): ansistring;
var
   si: smallint;
begin
   si := (not AWordValue + 1);
   if si >= 0 then begin
      Result := '-' + WordToHexA(si);
   end else begin
      Result := '+' + WordToHexA(Abs(si));
   end;
end;

function Word2KToHexW(const AWordValue: word): WideString;
var
   si: smallint;
begin
   si := (not AWordValue + 1);
   if si >= 0 then begin
      Result := '-' + WordToHexW(si);
   end else begin
      Result := '+' + WordToHexW(Abs(si));
   end;
end;

function WordToSwapHex(const AWordValue: word): string;
var
   b1, b2: byte;
begin
   b1 := byte(AWordValue and ($00FF));
   b2 := byte(AWordValue and ($FF00) shr 8);
   Result := ByteToHex(b1) + ByteToHex(b2);
end;

function WordToSwapHexA(const AWordValue: word): ansistring;
var
   b1, b2: byte;
begin
   b1 := byte(AWordValue and ($00FF));
   b2 := byte(AWordValue and ($FF00) shr 8);
   Result := ByteToHexA(b1) + ByteToHexA(b2);
end;

function WordToSwapHexW(const AWordValue: word): WideString;
var
   b1, b2: byte;
begin
   b1 := byte(AWordValue and ($00FF));
   b2 := byte(AWordValue and ($FF00) shr 8);
   Result := ByteToHexW(b1) + ByteToHexW(b2);
end;

function HexToNibble(const AHexCharText: AnsiChar): byte;
var
   cChar: AnsiChar;
begin
   cChar := UpCase(AHexCharText);
   if cChar = '0' then begin
      Result := byte(0);
   end else if cChar in ['1' .. '9'] then begin
      Result := Ord(cChar) - 48;
   end else if cChar in ['A' .. 'F'] then begin
      Result := Ord(cChar) - 55;
   end else begin
      Result := byte(0);
   end;
end;

function HexToNibble(const AHexCharText: widechar): byte;
var
   cChar: widechar;
begin
   cChar := WideUpperCase(AHexCharText)[1];
   if cChar = '0' then begin
      Result := byte(0);
      {$IFDEF Unicode}
   end else if TCharacter.IsNumber(cChar) then begin
      /// This might be Delphi XE only!
      Result := Ord(cChar) - 48;
   end else if (cChar = 'A') or (cChar = 'B') or (cChar = 'C') or (cChar = 'D') or (cChar = 'E') or (cChar = 'F') then begin
      Result := Ord(cChar) - 55;
      {$ELSE Unicode}
   end else if cChar in ['0' .. '9'] then begin
      Result := Ord(cChar) - 48;
   end else if cChar in ['A' .. 'F'] then begin
      Result := Ord(cChar) - 55;
      {$ENDIF Unicode}
   end else begin
      Result := byte(0);
   end;
end;

function HexToLongWord(ALongWordText: ansistring): longword;
var
   i: integer;
   lw, tw: longword;
begin
   if Copy(ALongWordText, 1, 2) = '0x' then begin
      Delete(ALongWordText, 1, 2);
   end;
   lw := 0;
   for i := Length(ALongWordText) downto 1 do begin
      tw := HexToNibble(ALongWordText[i]);
      lw := lw + (tw shl ((Length(ALongWordText) - i) * 4));
   end;
   Result := lw;
end;

function HexToLongWord(ALongWordText: WideString): longword;
var
   i: integer;
   lw, tw: longword;
begin
   if Copy(ALongWordText, 1, 2) = '0x' then begin
      Delete(ALongWordText, 1, 2);
   end;
   lw := 0;
   for i := Length(ALongWordText) downto 1 do begin
      tw := HexToNibble(ALongWordText[i]);
      lw := lw + (tw shl ((Length(ALongWordText) - i) * 4));
   end;
   Result := lw;
end;

function HexToLongInt(const ALongIntText: ansistring): longint;
var
   i: integer;
   lw, tw: longint;
begin
   lw := 0;
   for i := Length(ALongIntText) downto 1 do begin
      tw := HexToNibble(ALongIntText[i]);
      lw := lw + (tw shl ((Length(ALongIntText) - i) * 4));
   end;
   Result := lw;
end;

function HexToInt64(const AInt64Text: ansistring): int64;
var
   iChar: integer;
   iResult, iNibble: int64;
begin
   iResult := 0;
   for iChar := Length(AInt64Text) downto 1 do begin
      iNibble := HexToNibble(AInt64Text[iChar]);
      iResult := iResult + (iNibble shl ((Length(AInt64Text) - iChar) * 4));
   end;
   Result := iResult;
end;

function HexToInt64(const AInt64Text: WideString): int64;
var
   iChar: integer;
   iResult, iNibble: int64;
begin
   iResult := 0;
   for iChar := Length(AInt64Text) downto 1 do begin
      iNibble := HexToNibble(AInt64Text[iChar]);
      iResult := iResult + (iNibble shl ((Length(AInt64Text) - iChar) * 4));
   end;
   Result := iResult;
end;

function Int64ToHex(const AInt64Value: int64): string;
var
   loc, hic: longword;
begin
   loc := longword(AInt64Value and ($FFFFFFFF));
   hic := longword((AInt64Value shr 32));
   Result := LongWordToHex(hic) + LongWordToHex(loc);
end;

function Int64ToHexA(const AInt64Value: int64): ansistring;
var
   loc, hic: longword;
begin
   loc := longword(AInt64Value and ($FFFFFFFF));
   hic := longword((AInt64Value shr 32));
   Result := LongWordToHexA(hic) + LongWordToHexA(loc);
end;

function Int64ToHexW(const AInt64Value: int64): WideString;
var
   loc, hic: longword;
begin
   loc := longword(AInt64Value and ($FFFFFFFF));
   hic := longword((AInt64Value shr 32));
   Result := LongWordToHexW(hic) + LongWordToHexW(loc);
end;

function UInt64Swap(const AInt64Value: UInt64): UInt64;
var
   iLowCardinal, iHighCardinal: longword;
   uLowCardinal, uHighCardinal: UInt64;
var
   b1, b2, b3, b4, b5, b6, b7, b8: byte;
begin
   iLowCardinal := longword(AInt64Value and ($FFFFFFFF));
   iHighCardinal := longword((AInt64Value shr 32));
   b1 := byte(iLowCardinal and ($000000FF));
   b2 := byte(iLowCardinal and ($0000FF00) shr 8);
   b3 := byte(iLowCardinal and ($00FF0000) shr 16);
   b4 := byte(iLowCardinal and ($FF000000) shr 24);
   b5 := byte(iHighCardinal and ($000000FF));
   b6 := byte(iHighCardinal and ($0000FF00) shr 8);
   b7 := byte(iHighCardinal and ($00FF0000) shr 16);
   b8 := byte(iHighCardinal and ($FF000000) shr 24);
   uHighCardinal := (b1 shl 24) + (b2 shl 16) + (b3 shl 8) + b4;
   uLowCardinal := (b5 shl 24) + (b6 shl 16) + (b7 shl 8) + b8;
   Result := uLowCardinal or (uHighCardinal shl 32);
end;

function CardinalSwap(const AInt64Value: cardinal): cardinal;
var
   b1, b2, b3, b4: byte;
begin
   b1 := byte(AInt64Value and ($000000FF));
   b2 := byte(AInt64Value and ($0000FF00) shr 8);
   b3 := byte(AInt64Value and ($00FF0000) shr 16);
   b4 := byte(AInt64Value and ($FF000000) shr 24);
   Result := (b1 shl 24) + (b2 shl 16) + (b3 shl 8) + b4;
end;

function HexToByte(const AHexByteText: ansistring): byte;
begin
   if Length(AHexByteText) <> 2 then begin
      Result := 0;
   end else begin
      Result := byte(HexToNibble(AHexByteText[2]) + (HexToNibble(AHexByteText[1]) shl 4));
   end;
end;

function HexToByte(const AHexByteText: WideString): byte;
begin
   if Length(AHexByteText) <> 2 then begin
      Result := 0;
   end else begin
      Result := byte(HexToNibble(AHexByteText[2]) + (HexToNibble(AHexByteText[1]) shl 4));
   end;
end;

function PreviewChrOrHex(const AByteValue: byte): string;
begin
   if ((AByteValue > 31) and (AByteValue < 127)) then begin
      Result := char(Chr(AByteValue));
   end else begin
      Result := '<$' + ByteToHex(AByteValue) + '>';
   end;
end;

function PreviewChrOrHexA(const AByteValue: byte): ansistring;
begin
   if ((AByteValue > 31) and (AByteValue < 127)) then begin
      Result := AnsiChar(Chr(AByteValue));
   end else begin
      Result := '<$' + ByteToHexA(AByteValue) + '>';
   end;
end;

function PreviewChrOrHexW(const AByteValue: byte): WideString;
begin
   if ((AByteValue > 31) and (AByteValue < 127)) then begin
      Result := widechar(Chr(AByteValue));
   end else begin
      Result := '<$' + ByteToHexW(AByteValue) + '>';
   end;
end;

function OctalDigitToByte(ADigit: AnsiChar): byte;
begin
   Result := Ord(ADigit) - 48;
end;

function OctalToByte(AOctal: ansistring): byte;
begin
   // max 3 chars...
   while Length(AOctal) < 3 do begin
      AOctal := '0' + AOctal;
   end;
   if Length(AOctal) > 3 then begin
      Delete(AOctal, 1, Length(AOctal) - 3);
   end;
   Result := (OctalDigitToByte(AOctal[3])) + (OctalDigitToByte(AOctal[2]) shl 3) + (OctalDigitToByte(AOctal[1]) shl 6);
end;

function OctalToLongWord(AOctal: ansistring): cardinal;
var
   i: integer;
   lw, tw: longword;
begin
   lw := 0;
   for i := Length(AOctal) downto 1 do begin
      tw := Ord(AOctal[i]) - 48;
      lw := lw + (tw shl ((Length(AOctal) - i) * 3));
   end;
   Result := lw;
end;

function snlIntToStr(AValue: integer): ansistring;
begin
   Str(AValue, Result);
end;

function snlIntToStr(AValue: word): ansistring;
begin
   Str(AValue, Result);
end;

function snlIntToStr(AValue: cardinal): ansistring;
begin
   Str(AValue, Result);
end;

function snlIntToStr(AValue: int64): ansistring;
begin
   Str(AValue, Result);
end;

function snlIntToStr(AValue: UInt64): ansistring;
begin
   Str(AValue, Result);
end;

procedure SwapHexInt64String(var AValue: ansistring);
var
   sa: array [0 .. 7] of ansistring;
   i: integer;
begin
   if Length(AValue) = 16 then begin
      for i := 0 to 7 do begin
         sa[i] := Copy(AValue, 1 + (i * 2), 2);
      end;
      AValue := '';
      for i := 7 downto 0 do begin
         AValue := AValue + sa[i];
      end;
   end;
end;

procedure SwapHexInt64String(var AValue: WideString);
var
   sa: array [0 .. 7] of WideString;
   i: integer;
begin
   if Length(AValue) = 16 then begin
      for i := 0 to 7 do begin
         sa[i] := Copy(AValue, 1 + (i * 2), 2);
      end;
      AValue := '';
      for i := 7 downto 0 do begin
         AValue := AValue + sa[i];
      end;
   end;
end;

end.
