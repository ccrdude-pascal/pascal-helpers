{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Type to help event properties manage multiple event handlers.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2022-2924 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2022-12-27  pk  ---  Added documentation.
// 2022-12-23  pk  ---  Drafted record.
// *****************************************************************************
   )
}

unit Firefly.Events;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   TypInfo,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Rtti,
   Generics.Collections;

type
   { TEventMultiplexer is a generic class to manage more than one method
     in event properties.

     Events are defined like:

     property OnSome: TEventMultiplexer<TOnSomeEvent> read FOnSome write FOnSome;

     Assignment works as usual, the plus operator can be used to add multiple
     events.
   }
   TEventMultiplexer<TEvent> = record
      Events: array of TEvent;
      procedure Trigger(TheArguments: array of const);
      procedure Trigger2(const TheArguments);
      procedure DirtyTrigger;
      class operator implicit(AnEvent: TEvent): TEventMultiplexer<TEvent>;
      class operator explicit(AMultiplexer: TEventMultiplexer<TEvent>): TEvent;
      class operator add(AMultiplexer: TEventMultiplexer<TEvent>; AnEvent: TEvent): TEventMultiplexer<TEvent>;
      class operator subtract(AMultiplexer: TEventMultiplexer<TEvent>; AnEvent: TEvent): TEventMultiplexer<TEvent>;
   end;

implementation

{ TEventMultiplexer }

procedure TEventMultiplexer<TEvent>.Trigger(TheArguments: array of const);
var
   ev: TEvent;
   a: TValueArray;
   ti: TTypeInfo;
begin
   for ev in Events do begin
      //SetLength(a, 1);
      //a[0] := ev.
      //Invoke(TMethod(ev).Code, a, ccPascal, @ti, false, false);
   end;
end;

procedure TEventMultiplexer<TEvent>.Trigger2(const TheArguments);
begin

end;

procedure TEventMultiplexer<TEvent>.DirtyTrigger;
var
   ev: TEvent;
begin
   for ev in Events do begin
      {$IFDEF MSWindows}
      OutputDebugString('bla');
      {$ENDIF MSWindows}
   end;
end;

class operator TEventMultiplexer<TEvent>.implicit(AnEvent: TEvent): TEventMultiplexer<TEvent>;
begin
   Initialize(Result);
   if Assigned(AnEvent) then begin
      SetLength(Result.Events, 1);
      Result.Events[0] := AnEvent;
   end else begin
      SetLength(Result.Events, 0);
   end;
end;

class operator TEventMultiplexer<TEvent>.explicit(AMultiplexer: TEventMultiplexer<TEvent>): TEvent;
begin
   if Length(AMultiplexer.Events) > 0 then begin
      Result := TEvent(AMultiplexer.Events[0]);
   end else begin
      Result := nil;
   end;
end;

class operator TEventMultiplexer<TEvent>.add(AMultiplexer: TEventMultiplexer<TEvent>; AnEvent: TEvent): TEventMultiplexer<TEvent>;
var
   i: integer;
begin
   Result := AMultiplexer;
   i := Length(Result.Events);
   SetLength(Result.Events, Succ(i));
   Result.Events[i] := AnEvent;
end;

class operator TEventMultiplexer<TEvent>.subtract(AMultiplexer: TEventMultiplexer<TEvent>; AnEvent: TEvent): TEventMultiplexer<TEvent>;
var
   i: integer;
   iFound: integer;
begin
   Result := AMultiplexer;
   iFound := -1;
   for i := 0 to Pred(Length(Result.Events)) do begin
      if @Result.Events[i] = @AnEvent then begin
         iFound := i;
         break;
      end;
   end;
   if iFound > -1 then begin
      Result.Events[iFound] := Result.Events[Pred(Length(Result.Events))];
      SetLength(Result.Events, Pred(Length(Result.Events)));
   end;
end;

end.
