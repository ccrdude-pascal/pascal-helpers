unit Layers.Internet.DNS.Base;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections,
   Layers.Transport.Base,
   Layers.JSON.Base;

type

   { TDNSAnswer }

   TDNSAnswer = class
   private
      FIP: string;
      FName: string;
      FTTL: integer;
      FType: integer;
   public
      procedure LoadFromJSON(AnObject: TLayeredJSONObject);
      property Name: string read FName;
      property AType: integer read FType write FType;
      property TTL: integer read FTTL;
      property IP: string read FIP write FIP;
   end;

   { TDNSAnswerList }

   TDNSAnswerList = class(TObjectList<TDNSAnswer>)
   protected
      procedure ForEachAnswer({%H-}TheElementName: string; TheElement: TLayeredJSONObject; {%H-}AnUserData: pointer; var Continue: boolean);
   public
      procedure LoadFromJSON(AnArray: TLayeredJSONArray);
   end;

   { TLayeredDNSResults }

   TLayeredDNSResults = class
   private
      FAnswers: TDNSAnswerList;
   public
      constructor Create;
      destructor Destroy; override;
      procedure LoadFromJSON(AnObject: TLayeredJSONObject);
      property Answers: TDNSAnswerList read FAnswers;
   end;

   { TLayeredDNSOverHTTPS }

   TLayeredDNSOverHTTPS = class
   private
      FServerURL: string;
      FTransport: TLayeredTransport;
   public
      constructor Create(ATransport: TLayeredTransport);
      function Query(ADomain: string; const TheResults: TLayeredDNSResults; AQueryType: string = 'A'): boolean;
      property ServerURL: string read FServerURL write FServerURL;
      property Transport: TLayeredTransport read FTransport;
   end;

   TLayeredDNS = class;

   TLayeredDNSClass = class of TLayeredDNS;

   TLayeredDNS = class
   private
      FServerIP: string;
   protected
      class var DefaultClass: TLayeredDNSClass;
      class var FPersistentLog: TStringList;
   public
      class constructor Create;
      class destructor Destroy;
      class function CreateInstance: TLayeredDNS;
      class procedure SetDefaultClass(ADefaultClass: TLayeredDNSClass);
      class function PersistentLog: TStringList;
   public
      function Query(ADomain: string; const TheResults: TLayeredDNSResults; AQueryType: string = 'A'): boolean; virtual; abstract;
      property ServerIP: string read FServerIP write FServerIP;
   end;

implementation

{$IFDEF MSWindows}
uses
   Windows;
{$ENDIF MSWindows}

   { TDNSAnswer }

procedure TDNSAnswer.LoadFromJSON(AnObject: TLayeredJSONObject);
begin
   AnObject.FindString('name', FName);
   AnObject.FindString('data', FIP);
   AnObject.FindInteger('TTL', FTTL);
   AnObject.FindInteger('type', FType);
end;

{ TDNSAnswerList }

procedure TDNSAnswerList.ForEachAnswer(TheElementName: string; TheElement: TLayeredJSONObject; AnUserData: pointer; var Continue: boolean);
var
   a: TDNSAnswer;
begin
   a := TDNSAnswer.Create;
   a.LoadFromJSON(TheElement);
   Self.Add(a);
   Continue := True;
end;

procedure TDNSAnswerList.LoadFromJSON(AnArray: TLayeredJSONArray);
begin
   AnArray.ForEachObject(ForEachAnswer, nil);
end;

{ TLayeredDNSResults }

constructor TLayeredDNSResults.Create;
begin
   FAnswers := TDNSAnswerList.Create;
end;

destructor TLayeredDNSResults.Destroy;
begin
   FAnswers.Free;
   inherited Destroy;
end;

procedure TLayeredDNSResults.LoadFromJSON(AnObject: TLayeredJSONObject);
var
   a: TLayeredJSONArray;
begin
   if AnObject.FindArray('Answer', a) then begin
      FAnswers.LoadFromJSON(a);
   end;
end;

{ TLayeredDNSOverHTTPS }

constructor TLayeredDNSOverHTTPS.Create(ATransport: TLayeredTransport);
begin
   FServerURL := 'https://dns.google/resolve';
   FTransport := ATransport;
   FTransport.VerifyCertificate := True;
end;

function TLayeredDNSOverHTTPS.Query(ADomain: string; const TheResults: TLayeredDNSResults; AQueryType: string): boolean;
var
   sURL: string;
   o: TLayeredJSONObject;
begin
   sURL := FServerURL + '?name=' + ADomain + '&type=' + AQueryType;
   o := TLayeredJSONObject.CreateInstance(False);
   try
      FTransport.Headers.Clear;
      FTransport.Headers.Add('Accept=application/dns-json');
      Result := FTransport.GET(sURL, o);
      if Result then begin
         TheResults.LoadFromJSON(o);
      end;
   finally
      o.Free;
   end;
end;

class constructor TLayeredDNS.Create;
begin
   DefaultClass := nil;
   FPersistentLog := TStringList.Create;
end;

class destructor TLayeredDNS.Destroy;
begin
   FPersistentLog.Free;
end;

class function TLayeredDNS.CreateInstance: TLayeredDNS;
begin
   if Assigned(DefaultClass) then begin
      Result := DefaultClass.Create;
   end else begin
      Result := nil;
   end;
end;

class procedure TLayeredDNS.SetDefaultClass(ADefaultClass: TLayeredDNSClass);
begin
   DefaultClass := ADefaultClass;
end;

class function TLayeredDNS.PersistentLog: TStringList;
begin
   Result := FPersistentLog;
end;

end.
