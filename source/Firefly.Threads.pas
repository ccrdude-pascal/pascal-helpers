{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Custom thread routines used e.g. for threads whithin DLLs.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-18  pk   5m  Updated TInterfacedThread.QueryInterface to constref for FPC
// 2016-05-18  pk   5m  Renamed from snlThreads
// 2011-06-14  pk   5m  Processed all compiler warnings.
// 2009-10-12  pk   --  Added this header (from Code Test Browser)
// *****************************************************************************
   )
}

unit Firefly.Threads;

interface

uses
   SysUtils,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Classes,
   Forms,
   SyncObjs;

const
   MS_VC_EXCEPTION = $406D1388;

type
   {$IFDEF MSWINDOWS}
   TThreadNameInfo = record
      FType: longword;     // muss 0x1000 sein
      FName: PAnsiChar;        // Zeiger auf Name (in Anwender-Adress-Bereich)
      FThreadID: longword; // Thread-ID (-1 ist Caller-Thread)
      FFlags: longword;    // reserviert für zukünftige Verwendung, muss 0 sein
   end;
   {$ENDIF}

   TDLLDummyThread = class(TThread)
   private
      FIsPaused: boolean;
      function GetTerminatedEx: boolean;
   protected
      procedure SynchronizeEx(AMethod: TThreadMethod);
   public
      function PauseEx: boolean;
      function ResumeEx: boolean;
      property TerminatedEx: boolean read GetTerminatedEx;
      property IsPaused: boolean read FIsPaused;
   end;

   {
      @abstract TDLLThread is a thread class that uses critical sections for
      synchronization if its successor was compiled with
      @code(@$DEFINE DLLThreads) for use within a DLL.
   }

   TDLLThread = class(TThread)
   private
      FPauseEvent: TEvent;
      FIsPaused: boolean;
      FThreadLock: TRTLCriticalSection;
      function GetTerminatedEx: boolean;
   protected
      procedure SynchronizeEx(AMethod: TThreadMethod);
   public
      constructor Create(CreateSuspended: boolean);
      destructor Destroy; override;
      function PauseEx: boolean;
      function ResumeEx: boolean;
      property TerminatedEx: boolean read GetTerminatedEx;
      property IsPaused: boolean read FIsPaused;
   end;

   { TInterfacedThread }

   {$IFDEF MSWindows}
   TInterfacedThread = class(TThread, IInterface)
   protected
      FRefCount: integer;
      {$IFDEF FPC_HAS_CONSTREF}
      function QueryInterface(constref IID: TGUID; out Obj): HResult; stdcall;
      {$ELSE}
      function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
      {$ENDIF}
      function _AddRef: integer; stdcall;
      function _Release: integer; stdcall;
   public
      procedure AfterConstruction; override;
      procedure BeforeDestruction; override;
      class function NewInstance: TObject; override;
      property RefCount: integer read FRefCount;
   end;
   {$ENDIF MSWindows}

procedure ThreadEnterCritical;
procedure ThreadLeaveCritical;
function IsInThreadCritical(const ATimeOut: cardinal = 5000): boolean;

procedure SetThreadName(AThreadID: DWORD; AThreadName: AnsiString);

implementation

var
   CritialThreadSection: TCriticalSection;
   {$IFDEF MSWindows}
   CritialThreadEvent: TEvent;
   CritialThreadEventExt: TEvent;
   {$ENDIF MSWindows}
   ThreadLock: TRTLCriticalSection;
   ThreadEvent: THandle;

(**
 * This can be used only in Delphi, since named threats are an IDE feature there.
 *)
procedure SetThreadName(AThreadID: DWORD; AThreadName: AnsiString);
{$IFNDEF FPC}
var
   info: TThreadNameInfo;
   {$ENDIF FPC}
begin
   {$IFNDEF FPC}
   // This is an IDE feature and works only in Delphi!
   info.FType := $1000;
   info.FName := PAnsiChar(AThreadName);
   info.FThreadID := AThreadID;
   info.FFlags := 0;
   try
      RaiseException(MS_VC_EXCEPTION, 0, SizeOf(info) div sizeOf(ULONG_PTR), LPDWORD(@info));
   except
   end;
   {$ENDIF FPC}
end;

procedure ThreadEnterCritical;
begin
   {$IFDEF ThreadLockDebug}
   DebugLine('> ThreadEnterCritical-before-cs');
   {$ENDIF}
   {$IFDEF MSWindows}
   CritialThreadEvent.ResetEvent;
   {$ENDIF MSWindows}
   {$IFDEF ThreadLockDebug}
   DebugLine('> ThreadEnterCritical-after-cs');
   {$ENDIF}
   CritialThreadSection.Enter;
end;

procedure ThreadLeaveCritical;
begin
   {$IFDEF ThreadLockDebug}
   DebugLine('< ThreadLeaveCritical-before-cs');
   {$ENDIF}
   CritialThreadSection.Leave;
   {$IFDEF ThreadLockDebug}
   DebugLine('< ThreadLeaveCritical-after-cs');
   {$ENDIF}
   {$IFDEF MSWindows}
   CritialThreadEvent.SetEvent;
   {$ENDIF MSWindows}
end;

function IsInThreadCritical(const ATimeOut: cardinal): boolean;
var
   wr: TWaitResult;
begin
   {$IFDEF ThreadLockDebug}
   DebugLine('? IsInThreadCritical-before-waitfor');
   {$ENDIF}
   {$IFDEF MSWindows}
   wr := CritialThreadEventExt.WaitFor(ATimeOut);
   {$ELSE MSWindows}
   wr := wrTimeout;
   {$ENDIF MSWindows}
   {$IFDEF ThreadLockDebug}
   DebugLine('? IsInThreadCritical-after-waitfor');
   {$ENDIF}
   {$IFDEF ThreadLockDebug}
   case wr of
      wrSignaled: DebugLine('? WaitFor: wrSignaled');
      wrTimeout: DebugLine('? WaitFor: wrTimeout');
      wrAbandoned: DebugLine('? WaitFor: wrAbandoned');
      wrError: DebugLine('? WaitFor: wrError');
   end;
   {$ENDIF}
   Result := (wr = wrTimeout);
end;

{ TDLLThread }

constructor TDLLThread.Create(CreateSuspended: boolean);
begin
   inherited Create(CreateSuspended);
   FPauseEvent := TEvent.Create(nil, True, False, '');
   FPauseEvent.ResetEvent;
   FIsPaused := False;
   {$IFDEF MSWindows}
   InitializeCriticalSection(FThreadLock);
   {$ENDIF MSWindows}
end;

destructor TDLLThread.Destroy;
begin
   {$IFDEF MSWindows}
   DeleteCriticalSection(FThreadLock);
   {$ENDIF MSWindows}
   FPauseEvent.Free;
   inherited;
end;

function TDLLThread.GetTerminatedEx: boolean;
{$IFDEF DLLThreads}
var
   dw: DWord;
   {$ENDIF DLLThreads}
begin
   Result := inherited Terminated;
   {$IFDEF DLLThreads}
   dw := WaitForSingleObjectEx(FPauseEvent.Handle, 0, True);
   if dw = WAIT_OBJECT_0 then begin
      Suspend;
   end;
   {$ENDIF DLLThreads}
end;

function TDLLThread.PauseEx: boolean;
begin
   FIsPaused := True;
   {$IFDEF DLLThreads}
   FPauseEvent.SetEvent;
   Result := True;
   {$ELSE DLLThreads}
   // to remove this deprecated thing, we should switch to using the pause event from DLLThreads.
   Suspend;
   Result := True;
   {$ENDIF DLLThreads}
end;

function TDLLThread.ResumeEx: boolean;
begin
   {$IFDEF DLLThreads}
   FPauseEvent.ResetEvent;
   Resume;
   Result := True;
   {$ELSE DLLThreads}
   Resume;
   Result := True;
   {$ENDIF DLLThreads}
   FIsPaused := False;
end;

procedure TDLLThread.SynchronizeEx(AMethod: TThreadMethod);
begin
   if IsLibrary then begin
      EnterCriticalSection(FThreadLock);
      try
         AMethod;
      finally
         LeaveCriticalSection(FThreadLock);
      end;
   end else begin
      Synchronize(AMethod);
   end;
end;

{ TInterfacedThread }

{$IFDEF MSWindows}
procedure TInterfacedThread.AfterConstruction;
begin
   // Release the constructor's implicit refcount
   InterlockedDecrement(FRefCount);
end;

procedure TInterfacedThread.BeforeDestruction;
begin
   if (RefCount <> 0) then
      System.Error(reInvalidPtr);
end;

class function TInterfacedThread.NewInstance: TObject;
begin
   Result := inherited NewInstance;
   TInterfacedThread(Result).FRefCount := 1;
end;

{$IFDEF FPC_HAS_CONSTREF}
function TInterfacedThread.QueryInterface(constref IID: TGUID; out Obj): HResult; stdcall;
{$ELSE}
function TInterfacedThread.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
{$ENDIF}
begin
   if GetInterface(IID, Obj) then begin
      Result := 0;
   end else begin
      Result := E_NOINTERFACE;
   end;
end;

function TInterfacedThread._AddRef: integer; stdcall;
begin
   Result := InterlockedIncrement(FRefCount);
end;

function TInterfacedThread._Release: integer; stdcall;
begin
   Result := InterlockedDecrement(FRefCount);
   if Result = 0 then begin
      Destroy;
   end;
end;
{$ENDIF MSWindows}

{ TDLLDummyThread }

function TDLLDummyThread.GetTerminatedEx: boolean;
begin
   Result := Terminated;
end;

function TDLLDummyThread.PauseEx: boolean;
begin
   Result := True;
   FIsPaused := True;
   Suspend;
end;

function TDLLDummyThread.ResumeEx: boolean;
begin
   Result := True;
   FIsPaused := False;
   Start;
end;

procedure TDLLDummyThread.SynchronizeEx(AMethod: TThreadMethod);
begin
   AMethod;
   // Synchronize(AMethod);
end;

initialization

   CritialThreadSection := TCriticalSection.Create;
   {$IFDEF MSWindows}
   CritialThreadEvent := TEvent.Create(nil, True, True, 'snlCritialThreadEvent');
   CritialThreadEventExt := TEvent.Create(nil, True, True, 'snlCritialThreadEvent');
   {$ENDIF MSWindows}
   Initialize(@ThreadLock, SizeOf(ThreadLock));
   {$IFDEF MSWindows}
   InitializeCriticalSection(ThreadLock);
   ThreadEvent := CreateEvent(nil, True, True, 'snlCritialThreadEventH');
   {$ENDIF MSWindows}

finalization

   {$IFDEF MSWindows}
   CloseHandle(ThreadEvent);
   DeleteCriticalSection(ThreadLock);
   FreeAndNil(CritialThreadEventExt);
   FreeAndNil(CritialThreadEvent);
   {$ENDIF MSWindows}
   FreeAndNil(CritialThreadSection);

end.
