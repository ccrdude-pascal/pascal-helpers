﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Comparing strings using different methods.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2003-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// ---; BM algo from DelphiPraxis.net by Jens Schuhmann
// ---; Levenshtein algo from DelphiPraxis.net by Volker
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-05-06  pk   5m  Added credits
// 2017-05-17  pk   5m  Renamed from snlAlgoStringCompare
// 2012-01-15  pk  20m  Added new StartingBytes comparison method
// 2010-09-17  ##   --  Unit renamed from pkStringCompare to snlAlgoStringCompare
// 2010-09-15  pk  10m  Added new BooleanCompareStrings2
// 2010-09-15  pk  10m  Added new WildcardCompareStrings2
// 2008-05-26  pk  10m  Copying objects to results of *CompareStrings() now
// 2007-02-20  pk  15m  Fixed mem leak in TCompareRule.FromString
// 2007-02-20  pk  15m  New pkCompareIDString(s) with prefix algo selector!
// 2007-02-20  pk  30m  Added Boyer Moore Algorithm and implemented test class
// 2007-01-31  pk   5m  Made parameters const
// 2007-01-31  pk   5m  Added overloaded WildcardCompareStrings
// 2005-04-08  pk   1h  Fixed WildcardCompareString:MatchStrings to use typecasting instead of StrPCopy
// *****************************************************************************
   )
}

unit Firefly.StringCompare;

{.$DEFINE UsePerlRegExpr}
// Choose one of these:
// *****************************************************************************
// Do not modify the stuff below!
//
// for FreePascal
//
// {$IFDEF FPC}
// {$mode objfpc}{$H+}
// {$IFNDEF LCL}           // RegExp only for LCL projects (?)
// {$ENDIF LCL}
// {$ENDIF FPC}
//

{$IFNDEF Unicode}
{$DEFINE NoAnsiStrings}
{$ELSE Unicode}
{$UNDEF NoAnsiStrings}
{$ENDIF Unicode}

interface

uses
   SysUtils,
   Classes,
   RegExpr,
   {$IFNDEF FPC}
   AnsiStrings,
   JclStrings,
   JclAnsiStrings,
   JclWideStrings,
   {$ENDIF FPC}
   {$IFDEF UsePerlRegExpr} RegularExpressions, {$ENDIF UsePerlRegExpr}
   Firefly.BoyerMoore,
   Firefly.AhoCorasick;

type
   TCompareStyle = (csSubString, csWildcards, csBoolean, csRegExp, csPerlRegExp, csBoyerMoore, csIdentical, csStartingBytes);

const
   SCompareStyles: array [TCompareStyle] of string        = ('Substring', 'Wildcards', 'Boolean', 'Regular Expressions', 'Perl Regular Expressions', 'Boyer Moore', 'Identical', 'Starting Bytes');
   SCompareStylePrefixes: array [TCompareStyle] of string = ('<substr>', '<wc>', '<bool>', '<regexpr>', '<pcre>', '<bm>', '', '<start>');

function pkCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList; overload;
function pkCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList; overload;
function pkCompareStringsLegacy(const ABaseList: TStrings; const ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList; overload;
function pkCompareStringsLegacy(const ABaseList: TStrings; const ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList; overload;
function pkCompareString(const ABaseText, ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean; overload;
function pkCompareString(const ABaseText, ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean; overload;
function pkCompareStringLegacy(const ABaseText, ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean; overload;
function pkCompareStringLegacy(const ABaseText, ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean; overload;

function pkCompareIDStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; AIgnoreCase: boolean): TStringList; overload;
function pkCompareIDStrings(const ABaseList: TStrings; const ACompareRule: WideString; AIgnoreCase: boolean): TStringList; overload;
function pkCompareIDStringsLegacy(const ABaseList: TStrings; const ACompareRule: AnsiString; ignoreCase: boolean): TStringList; overload;
function pkCompareIDStringsLegacy(const ABaseList: TStrings; const ACompareRule: WideString; ignoreCase: boolean): TStringList; overload;
function pkCompareIDString(const ABaseText, ACompareRule: AnsiString; AIgnoreCase: boolean = true; ADefaultStyle: TCompareStyle = csWildcards): boolean; overload;
function pkCompareIDString(const ABaseText, ACompareRule: AnsiString; var AFoundStyle: TCompareStyle; AIgnoreCase: boolean = true; ADefaultStyle: TCompareStyle = csWildcards): boolean; overload;
function pkCompareIDString(const ABaseText, ACompareRule: WideString; AIgnoreCase: boolean = true; ADefaultStyle: TCompareStyle = csWildcards): boolean; overload;
function pkCompareIDStringLegacy(const ABaseText, ACompareRule: AnsiString; AIgnoreCase: boolean = true; ADefaultStyle: TCompareStyle = csWildcards): boolean; overload;
function pkCompareIDStringLegacy(const ABaseText, ACompareRule: WideString; AIgnoreCase: boolean = true; ADefaultStyle: TCompareStyle = csWildcards): boolean; overload;

procedure GetCompareIDStyle(var ACompareRule: AnsiString; var AStyle: TCompareStyle; ADefault: TCompareStyle = csWildcards); overload;
procedure GetCompareIDStyle(var ACompareRule: WideString; var AStyle: TCompareStyle; ADefault: TCompareStyle = csWildcards); overload;

function SupportsRegExp: boolean;
function SupportsPerlRegExp: boolean;

function RegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean = false): TStringList; overload;
function RegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean = false): TStringList; overload;
function RegExpCompareString(const ABaseText: AnsiString; const ACompareRule: AnsiString; const AIgnoreCase: boolean = false): boolean; overload;
function RegExpCompareString(const ABaseText: WideString; const ACompareRule: WideString; const AIgnoreCase: boolean = false): boolean; overload;
{$IFDEF UsePerlRegExpr}
function PerlRegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean = false): TStringList; overload;
function PerlRegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean = false): TStringList; overload;
function PerlRegExpCompareString(const ABaseText: AnsiString; const ACompareRule: AnsiString; const AIgnoreCase: boolean = false): boolean; overload;
function PerlRegExpCompareString(const ABaseText: WideString; const ACompareRule: WideString; const AIgnoreCase: boolean = false): boolean; overload;
{$ENDIF UsePerlRegExpr}
function BooleanCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
function BooleanCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
function BooleanCompareStrings2(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): Integer; overload;
function BooleanCompareStrings2(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): Integer; overload;
function BooleanCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean; overload;
function BooleanCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean; overload;
function WildcardCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
function WildcardCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
function WildcardCompareStrings2(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): Integer; overload;
function WildcardCompareStrings2(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): Integer; overload;
function WildcardCompareStrings(const ABaseText: AnsiString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
function WildcardCompareStrings(const ABaseText: WideString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
function WildcardCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean = true): boolean; overload;
function WildcardCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean = true): boolean; overload;
function SubStringCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
function SubStringCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
function SubStringCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean; overload;
function SubStringCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean; overload;
function IdenticalCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
function IdenticalCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
function IdenticalCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean; overload;
function IdenticalCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean; overload;

function StartingBytesCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
function StartingBytesCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
function StartingBytesCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean; overload;
function StartingBytesCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean; overload;

function BoyerMooreCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
function BoyerMooreCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
function BoyerMooreCompareStrings(const ABaseText: AnsiString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
function BoyerMooreCompareStrings(const ABaseText: WideString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
function BoyerMooreCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean; overload;
function BoyerMooreCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean; overload;

function Levenshtein(const Text1, Text2: AnsiString): Integer;

function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TStrings; Flags: TReplaceFlags; ASeparatedList: boolean = true): AnsiString; overload;
function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TStrings; Flags: TReplaceFlags; Results: TStrings; ASeparatedList: boolean = false): boolean; overload;
{$IFNDEF NoAnsiStrings}
function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TAnsiStrings; Flags: TReplaceFlags; ASeparatedList: boolean = true): AnsiString; overload;
function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TAnsiStringList; Flags: TReplaceFlags; ASeparatedList: boolean = true): AnsiString; overload;
function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TAnsiStrings; Flags: TReplaceFlags; Results: TAnsiStrings; ASeparatedList: boolean = false): boolean; overload;
{$ENDIF NoAnsiStrings}
function AhoCorasickSpybotAFPStringReplaceA(const s: AnsiString; Patterns: TStrings; Flags: TReplaceFlags): AnsiString;

type
   TRegExpResults = class
   private
      FTrueResultsCache: string;
      FResultsCache: string;
      FTrueResultsCacheSuccess: string;
      FResultsCacheSuccess: string;
      FTrueResults: TStringList;
      FResults: TStringList;
      // FReplacer: TAnsiStringList; // might be needed in the future when we want ApplyToString during the update phase
      FExpressionInUpdate: boolean;
      FUpdating: boolean;
      procedure Push;
      procedure Pop;
      procedure PushSuccess;
      procedure PopSuccess;
   public
      class constructor Create;
      class destructor Destroy;
      constructor Create;
      destructor Destroy; override;
      procedure BeginUpdate;
      procedure EndUpdate;
      procedure ResetUpdate;
      procedure SetUpdateSuccess;
      procedure FromRegExp(AResult: boolean; ARegExpr: TRegExpr); overload;
      {$IFDEF UsePerlRegExpr} procedure FromRegExp(AMatch: TMatch); overload; {$ENDIF}
      function ApplyToString(AInput: string): string;
      property LastResults: TStringList read FResults;
      property LastTrueResults: TStringList read FTrueResults;
   end;

var
   RegExpResults: TRegExpResults;

const
   MAXCHAR = 65535;

const
   SPrefixWildcardsA: AnsiString         = '<wc>';
   SPrefixSubStringA: AnsiString         = '<substr>';
   SPrefixBooleanA: AnsiString           = '<bool>';
   SPrefixRegularExpressionA: AnsiString = '<regexpr>';
   SPrefixPCREA: AnsiString              = '<pcre>';
   SPrefixBoyerMooreA: AnsiString        = '<bm>';
   SPrefixStartA: AnsiString             = '<start>';
   SPrefixWildcardsW: WideString         = '<wc>';
   SPrefixSubStringW: WideString         = '<substr>';
   SPrefixBooleanW: WideString           = '<bool>';
   SPrefixRegularExpressionW: WideString = '<regexpr>';
   SPrefixPCREW: WideString              = '<pcre>';
   SPrefixBoyerMooreW: WideString        = '<bm>';
   SPrefixStartW: WideString             = '<start>';

implementation

uses
   {$IFNDEF FPC}
   Masks,
   WideStrings,
   WideStrUtils,
   {$ENDIF FPC}
   Firefly.Credits,
   Firefly.Parser.Boolean;

function DCStringToAnsiStringUTF8(const AText: string): AnsiString; inline;
begin
   {$IFDEF Unicode}
   Result := UTF8Encode(AText);
   {$ELSE Unicode}
   Result := AText;
   {$ENDIF Unicode}
end;

function pkCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList;
begin
   Result := nil;
   case AStyle of
      csSubString: Result := SubStringCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
         {$ELSE UsePerlRegExpr}
         Result := nil;
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csIdentical: Result := IdenticalCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList;
begin
   Result := nil;
   case AStyle of
      csSubString: Result := SubStringCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csIdentical: Result := IdenticalCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareStringsLegacy(const ABaseList: TStrings; const ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList;
begin
   Result := nil;
   case AStyle of
      csIdentical: Result := IdenticalCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csSubString: Result := SubStringCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareStrings(ABaseList, ACompareRule);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareStrings(ABaseList, ACompareRule);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareStringsLegacy(const ABaseList: TStrings; const ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): TStringList;
begin
   Result := nil;
   case AStyle of
      csIdentical: Result := IdenticalCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csSubString: Result := SubStringCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareStrings(ABaseList, ACompareRule);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareStrings(ABaseList, ACompareRule);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareStrings(ABaseList, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareString(const ABaseText, ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean;
begin
   Result := false;
   case AStyle of
      csIdentical: Result := IdenticalCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csSubString: Result := SubStringCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareString(ABaseText, ACompareRule, AIgnoreCase);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareString(ABaseText, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareString(const ABaseText, ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean;
begin
   Result := false;
   case AStyle of
      csIdentical: Result := IdenticalCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csSubString: Result := SubStringCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareString(ABaseText, ACompareRule, AIgnoreCase);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareString(ABaseText, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareStringLegacy(const ABaseText, ACompareRule: AnsiString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean;
begin
   Result := false;
   case AStyle of
      csIdentical: Result := IdenticalCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csSubString: Result := SubStringCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareString(string(ABaseText), string(ACompareRule));
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareString(ABaseText, ACompareRule);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareString(ABaseText, ACompareRule, AIgnoreCase);
   end;
end;

function pkCompareStringLegacy(const ABaseText, ACompareRule: WideString; AStyle: TCompareStyle; AIgnoreCase: boolean): boolean;
begin
   Result := false;
   case AStyle of
      csIdentical: Result := IdenticalCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csSubString: Result := SubStringCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csStartingBytes: Result := StartingBytesCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csWildcards: Result := WildcardCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csBoolean: Result := BooleanCompareString(ABaseText, ACompareRule, AIgnoreCase);
      csRegExp: Result := RegExpCompareString(ABaseText, ACompareRule);
      csPerlRegExp: begin
         {$IFDEF UsePerlRegExpr}
         Result := PerlRegExpCompareString(ABaseText, ACompareRule);
         {$ENDIF UsePerlRegExpr}
      end;
      csBoyerMoore: Result := BoyerMooreCompareString(ABaseText, ACompareRule, AIgnoreCase);
   end;
end;

procedure GetCompareIDStyle(var ACompareRule: AnsiString; var AStyle: TCompareStyle; ADefault: TCompareStyle); overload;
begin
   // trac:sd2:280 - new ADefault
   if AnsiCompareText(Copy(ACompareRule, 1, 4), SPrefixWildcardsA) = 0 then begin
      AStyle := csWildcards;
      System.Delete(ACompareRule, 1, 4);
   end
   else if AnsiCompareText(Copy(ACompareRule, 1, 8), SPrefixSubStringA) = 0 then begin
      AStyle := csSubString;
      System.Delete(ACompareRule, 1, 8);
   end
   else if AnsiCompareText(Copy(ACompareRule, 1, 6), SPrefixBooleanA) = 0 then begin
      AStyle := csBoolean;
      System.Delete(ACompareRule, 1, 6);
   end
   else if AnsiCompareText(Copy(ACompareRule, 1, 9), SPrefixRegularExpressionA) = 0 then begin
      AStyle := csRegExp;
      System.Delete(ACompareRule, 1, 9);
   end
   else if AnsiCompareText(Copy(ACompareRule, 1, 6), SPrefixPCREA) = 0 then begin
      AStyle := csPerlRegExp;
      System.Delete(ACompareRule, 1, 6);
   end
   else if AnsiCompareText(Copy(ACompareRule, 1, 4), SPrefixBoyerMooreA) = 0 then begin
      AStyle := csBoyerMoore;
      System.Delete(ACompareRule, 1, 4);
   end
   else if AnsiCompareText(Copy(ACompareRule, 1, 7), SPrefixStartA) = 0 then begin
      AStyle := csStartingBytes;
      System.Delete(ACompareRule, 1, 7);
   end else begin
      AStyle := ADefault;
   end;
end;

procedure GetCompareIDStyle(var ACompareRule: WideString; var AStyle: TCompareStyle; ADefault: TCompareStyle); overload;
var s4: WideString;
begin
   s4 := Copy(ACompareRule,1,4);
   // trac:sd2:280 - new ADefault
   if WideSameText(s4, SPrefixWildcardsW) then begin
      AStyle := csWildcards;
      System.Delete(ACompareRule, 1, 4);
   end
   else if WideSameText(Copy(ACompareRule, 1, 8), SPrefixSubStringW) then begin
      AStyle := csSubString;
      System.Delete(ACompareRule, 1, 8);
   end
   else if WideSameText(Copy(ACompareRule, 1, 6), SPrefixBooleanW) then begin
      AStyle := csBoolean;
      System.Delete(ACompareRule, 1, 6);
   end
   else if WideSameText(Copy(ACompareRule, 1, 9), SPrefixRegularExpressionW) then begin
      AStyle := csRegExp;
      System.Delete(ACompareRule, 1, 9);
   end
   else if WideSameText(Copy(ACompareRule, 1, 6), SPrefixPCREW) then begin
      AStyle := csPerlRegExp;
      System.Delete(ACompareRule, 1, 6);
   end
   else if WideSameText(s4, SPrefixBoyerMooreW) then begin
      AStyle := csBoyerMoore;
      System.Delete(ACompareRule, 1, 4);
   end
   else if WideSameText(Copy(ACompareRule, 1, 7), SPrefixStartW) then begin
      AStyle := csStartingBytes;
      System.Delete(ACompareRule, 1, 7);
   end else begin
      AStyle := ADefault;
   end;
end;

function pkCompareIDStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; AIgnoreCase: boolean): TStringList;
var
   style: TCompareStyle;
   sCompareRule: AnsiString;
begin
   style := csWildcards;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, csIdentical);
   Result := pkCompareStrings(ABaseList, sCompareRule, style, AIgnoreCase);
end;

function pkCompareIDStrings(const ABaseList: TStrings; const ACompareRule: WideString; AIgnoreCase: boolean): TStringList;
var
   style: TCompareStyle;
   sCompareRule: WideString;
begin
   style := csWildcards;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, csIdentical);
   Result := pkCompareStrings(ABaseList, sCompareRule, style, AIgnoreCase);
end;

function pkCompareIDString(const ABaseText, ACompareRule: AnsiString; AIgnoreCase: boolean; ADefaultStyle: TCompareStyle): boolean;
var
   style: TCompareStyle;
   sCompareRule: AnsiString;
begin
   style := ADefaultStyle;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, ADefaultStyle);
   Result := pkCompareString(ABaseText, sCompareRule, style, AIgnoreCase);
end;

function pkCompareIDString(const ABaseText, ACompareRule: AnsiString; var AFoundStyle: TCompareStyle; AIgnoreCase: boolean = true; ADefaultStyle: TCompareStyle = csWildcards): boolean; overload;
var
   sCompareRule: AnsiString;
begin
   AFoundStyle := ADefaultStyle;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, AFoundStyle, ADefaultStyle);
   Result := pkCompareString(ABaseText, sCompareRule, AFoundStyle, AIgnoreCase);
end;

function pkCompareIDString(const ABaseText, ACompareRule: WideString; AIgnoreCase: boolean; ADefaultStyle: TCompareStyle): boolean;
var
   style: TCompareStyle;
   sCompareRule: WideString;
begin
   style := ADefaultStyle;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, ADefaultStyle);
   Result := pkCompareString(ABaseText, sCompareRule, style, AIgnoreCase);
end;

function pkCompareIDStringLegacy(const ABaseText, ACompareRule: AnsiString; AIgnoreCase: boolean; ADefaultStyle: TCompareStyle): boolean;
var
   style: TCompareStyle;
   sCompareRule: AnsiString;
begin
   style := ADefaultStyle;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, ADefaultStyle);
   Result := pkCompareStringLegacy(ABaseText, sCompareRule, style, AIgnoreCase);
end;

function pkCompareIDStringLegacy(const ABaseText, ACompareRule: WideString; AIgnoreCase: boolean; ADefaultStyle: TCompareStyle): boolean;
var
   style: TCompareStyle;
   sCompareRule: WideString;
begin
   style := ADefaultStyle;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, ADefaultStyle);
   Result := pkCompareStringLegacy(ABaseText, sCompareRule, style, AIgnoreCase);
end;

function pkCompareIDStringsLegacy(const ABaseList: TStrings; const ACompareRule: AnsiString; ignoreCase: boolean): TStringList;
var
   style: TCompareStyle;
   sCompareRule: AnsiString;
begin
   style := csWildcards;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, csIdentical);
   Result := pkCompareStringsLegacy(ABaseList, sCompareRule, style, ignoreCase);
end;

function pkCompareIDStringsLegacy(const ABaseList: TStrings; const ACompareRule: WideString; ignoreCase: boolean): TStringList;
var
   style: TCompareStyle;
   sCompareRule: WideString;
begin
   style := csWildcards;
   sCompareRule := ACompareRule;
   GetCompareIDStyle(sCompareRule, style, csIdentical);
   Result := pkCompareStringsLegacy(ABaseList, sCompareRule, style, ignoreCase);
end;

function RegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to ABaseList.Count - 1 do begin
      if RegExpCompareString(AnsiString(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function RegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to Pred(ABaseList.Count) do begin
      if RegExpCompareString(WideString(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function RegExpCompareString(const ABaseText: AnsiString; const ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   r: TRegExpr;
begin
   if ACompareRule = '' then begin
      Result := false;
   end else begin
      r := TRegExpr.Create;
      try
         r.ModifierI := AIgnoreCase;
         r.Expression := string(ACompareRule);
         Result := r.Exec(string(ABaseText));
         RegExpResults.FromRegExp(Result, r);
      finally
         r.Free;
      end;
   end;
   // Result := ExecRegExpr(ACompareRule, ABaseText);
end;

function RegExpCompareString(const ABaseText: WideString; const ACompareRule: WideString; const AIgnoreCase: boolean): boolean;
var
   r: TRegExpr;
begin
   if ACompareRule = '' then begin
      Result := false;
   end else begin
      r := TRegExpr.Create;
      try
         r.ModifierI := AIgnoreCase;
         r.Expression := UTF8Encode(ACompareRule);
         Result := r.Exec(UTF8Encode(ABaseText));
         RegExpResults.FromRegExp(Result, r);
      finally
         r.Free;
      end;
   end;
   // Result := ExecRegExpr(ACompareRule, ABaseText);
end;

{$IFDEF UsePerlRegExpr}

function PerlRegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to ABaseList.Count - 1 do begin
      if PerlRegExpCompareString(AnsiString(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function PerlRegExpCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean = false): TStringList;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to ABaseList.Count - 1 do begin
      if PerlRegExpCompareString(ABaseList[i], ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function PerlRegExpCompareString(const ABaseText: AnsiString; const ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   o: TRegExOptions;
   m: TMatch;
begin
   if AIgnoreCase then begin
      o := [roIgnoreCase];
   end else begin
      o := [roNone];
   end;
   m := TRegEx.Match(string(ABaseText), string(ACompareRule), o);
   Result := (m.Success);
   RegExpResults.FromRegExp(m);
end;

function PerlRegExpCompareString(const ABaseText: WideString; const ACompareRule: WideString; const AIgnoreCase: boolean = false): boolean; overload;
var
   o: TRegExOptions;
   m: TMatch;
begin
   if AIgnoreCase then begin
      o := [roIgnoreCase];
   end else begin
      o := [roNone];
   end;
   m := TRegEx.Match(ABaseText, ACompareRule, o);
   Result := (m.Success);
   RegExpResults.FromRegExp(m);
end;

{$ENDIF UsePerlRegExpr}

function BooleanCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: AnsiString;
   ruleSet: TCompareRule;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule)
   end else begin
      sCompareRule := ACompareRule;
   end;
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString(sCompareRule);
      for i := 0 to Pred(ABaseList.Count) do begin
         if AIgnoreCase then begin
            sCompareText := DCStringToAnsiStringUTF8(UpperCase(ABaseList[i]));
         end else begin
            sCompareText := DCStringToAnsiStringUTF8(ABaseList[i]);
         end;
         if ruleSet.StringFits(sCompareText) then begin
            Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
         end;
      end;
   finally
      ruleSet.Free;
   end;
end;

function BooleanCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: WideString;
   ruleSet: TCompareRule;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule)
   end else begin
      sCompareRule := ACompareRule;
   end;
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString(sCompareRule);
      for i := 0 to Pred(ABaseList.Count) do begin
         {$IFDEF FPC}
         if AIgnoreCase then begin
            sCompareText := UTF8Decode(UpperCase(ABaseList[i]));
         end else begin
            sCompareText := UTF8Decode(ABaseList[i]);
         end;
         {$ELSE FPC}
         if AIgnoreCase then begin
            sCompareText := UpperCase(ABaseList[i])
         end else begin
            sCompareText := ABaseList[i];
         end;
         {$ENDIF FPC}
         if ruleSet.StringFits(sCompareText) then begin
            Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
         end;
      end;
   finally
      ruleSet.Free;
   end;
end;

function BooleanCompareStrings2(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): Integer; overload;
var
   i: Integer;
   sCompareText, sCompareRule: AnsiString;
   ruleSet: TCompareRule;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString(sCompareRule);
      for i := Pred(ABaseList.Count) downto 0 do begin
         if AIgnoreCase then begin
            sCompareText := DCStringToAnsiStringUTF8(UpperCase(ABaseList[i]));
         end else begin
            sCompareText := DCStringToAnsiStringUTF8(ABaseList[i]);
         end;
         if not ruleSet.StringFits(sCompareText) then begin
            ABaseList.Delete(i);
         end;
      end;
   finally
      Result := ABaseList.Count;
      FreeAndNil(ruleSet);
   end;
end;

function BooleanCompareStrings2(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): Integer; overload;
var
   i: Integer;
   sCompareText, sCompareRule: WideString;
   ruleSet: TCompareRule;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString(sCompareRule);
      for i := Pred(ABaseList.Count) downto 0 do begin
         {$IFDEF FPC}
         if AIgnoreCase then begin
            sCompareText := UTF8Decode(UpperCase(ABaseList[i]));
         end else begin
            sCompareText := UTF8Decode(ABaseList[i]);
         end;
         {$ELSE FPC}
         if AIgnoreCase then begin
            sCompareText := UpperCase(ABaseList[i]);
         end else begin
            sCompareText := ABaseList[i];
         end;
         {$ENDIF FPC}
         if not ruleSet.StringFits(sCompareText) then begin
            ABaseList.Delete(i);
         end;
      end;
   finally
      Result := ABaseList.Count;
      FreeAndNil(ruleSet);
   end;
end;

function BooleanCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   ruleSet: TCompareRule;
   sBaseText, sCompareRule: AnsiString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString(sCompareRule);
      Result := ruleSet.StringFits(sBaseText);
   finally
      FreeAndNil(ruleSet);
   end;
end;

function BooleanCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean;
var
   ruleSet: TCompareRule;
   sBaseText, sCompareRule: WideString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   ruleSet := TCompareRule.Create;
   try
      ruleSet.FromString(sCompareRule);
      Result := ruleSet.StringFits(sBaseText);
   finally
      FreeAndNil(ruleSet);
   end;
end;

function WildcardCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to Pred(ABaseList.Count) do begin
      if WildcardCompareString(DCStringToAnsiStringUTF8(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function WildcardCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to Pred(ABaseList.Count) do begin
      if WildcardCompareString(WideString(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function WildcardCompareStrings2(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): Integer; overload;
var
   i: Integer;
begin
   for i := Pred(ABaseList.Count) downto 0 do begin
      if not WildcardCompareString(DCStringToAnsiStringUTF8(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         ABaseList.Delete(i);
      end;
   end;
   Result := ABaseList.Count;
end;

function WildcardCompareStrings2(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): Integer; overload;
var
   i: Integer;
begin
   for i := Pred(ABaseList.Count) downto 0 do begin
      if not WildcardCompareString(WideString(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         ABaseList.Delete(i);
      end;
   end;
   Result := ABaseList.Count;
end;

function WildcardCompareStrings(const ABaseText: AnsiString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
var
   i: Integer;
begin
   Result := false;
   for i := 0 to ACompareRules.Count - 1 do begin
      if WildcardCompareString(ABaseText, DCStringToAnsiStringUTF8(ACompareRules[i]), AIgnoreCase) then begin
         Result := true;
         Exit;
      end;
   end;
end;

function WildcardCompareStrings(const ABaseText: WideString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
var
   i: Integer;
begin
   Result := false;
   for i := 0 to ACompareRules.Count - 1 do begin
      if WildcardCompareString(ABaseText, WideString(ACompareRules[i]), AIgnoreCase) then begin
         Result := true;
         Exit;
      end;
   end;
end;

function WComp(AWildcardText, ACompareText: string): boolean;
var
   iWildcardText, j, l, p: Byte;
begin
   iWildcardText := 1;
   j := 1;
   while (iWildcardText <= Length(AWildcardText)) do begin
      // if WildS[i]='*' then begin
      if Copy(AWildcardText, iWildcardText, 1) = '*' then begin
         if iWildcardText = Length(AWildcardText) then begin
            Result := true;
            Exit;
         end else begin
            { we need to synchronize }
            l := iWildcardText + 1;
            // while (l < length(WildS)) and (WildS[l+1] <> '*') do inc (l);
            while (l < Length(AWildcardText)) and (Copy(AWildcardText, l + 1, 1) <> '*') do inc(l);
            p := pos(Copy(AWildcardText, iWildcardText + 1, l - iWildcardText), ACompareText);
            if p > 0 then begin
               j := p - 1;
            end else begin
               Result := false;
               Exit;
            end;
         end;
         // end else if (WildS[i]<>'?') and ((length(IstS) < i) or (WildS[i]<>IstS[j])) then begin
      end
      else if (Copy(AWildcardText, iWildcardText, 1) <> '?') and ((Length(ACompareText) < iWildcardText) or (Copy(AWildcardText, iWildcardText, 1) <> Copy(ACompareText, j, 1))) then begin
         Result := false;
         Exit;
      end;
      inc(iWildcardText);
      inc(j);
   end;
   Result := (j > Length(ACompareText));
end;

function MatchStrings(Source, Pattern: AnsiString): boolean; overload;
   function MatchPattern(AElement, APattern: PAnsiChar): boolean;
      function IsPatternWild(APattern: PAnsiChar): boolean;
      begin
         Result := StrScan(APattern, '*') <> nil;
         if not Result then Result := StrScan(APattern, '?') <> nil;
      end;

   begin
      if 0 = StrComp(APattern, '*') then Result := true
      else if (AElement^ = Chr(0)) and (APattern^ <> Chr(0)) then Result := false
      else if AElement^ = Chr(0) then Result := true
      else begin
         case APattern^ of
            '*': if MatchPattern(AElement, @APattern[1]) then Result := true
               else Result := MatchPattern(@AElement[1], APattern);
            '?': Result := MatchPattern(@AElement[1], @APattern[1]);
            else if AElement^ = APattern^ then Result := MatchPattern(@AElement[1], @APattern[1])
               else Result := false;
         end;
      end;
   end;

begin
   Result := MatchPattern(PAnsiChar(Source), PAnsiChar(Pattern));
end;

{$IFDEF FPC}

function StrScan(const aStr: PWideChar; aChar: WideChar): PWideChar;
begin

   Result := aStr;
   while Result^ <> aChar do begin

      if Result^ = #0 then begin
         Result := nil;
         Exit;
      end;

      inc(Result);
   end;

end;
{$ENDIF FPC}

function MatchStrings(Source, Pattern: WideString): boolean; overload;
   function MatchPattern(AElement, APattern: PWideChar): boolean;
      function IsPatternWild(APattern: PWideChar): boolean;
      begin
         {$IFDEF FPC}
         Result := StrScan(APattern, '*') <> nil;
         if not Result then begin
            Result := StrScan(APattern, '?') <> nil;
         end;
         {$ELSE FPC}
         Result := WStrScan(APattern, '*') <> nil;
         if not Result then begin
            Result := WStrScan(APattern, '?') <> nil;
         end;
         {$ENDIF FPC}
      end;

   var
      b: boolean;
   begin
      {$IFDEF FPC}
      b := (0 = StrComp(APattern, '*'));
      {$ELSE FPC}
      b := (0 = WStrComp(APattern, '*'));
      {$ENDIF FPC}
      if b then begin
         Result := true;
      end
      else if (AElement^ = Chr(0)) and (APattern^ <> Chr(0)) then begin
         Result := false;
      end
      else if AElement^ = Chr(0) then begin
         Result := true;
      end else begin
         case APattern^ of
            '*': if MatchPattern(AElement, @APattern[1]) then begin
                  Result := true;
               end else begin
                  Result := MatchPattern(@AElement[1], APattern);
               end;
            '?': Result := MatchPattern(@AElement[1], @APattern[1]);
            else if AElement^ = APattern^ then begin
                  Result := MatchPattern(@AElement[1], @APattern[1]);
               end else begin
                  Result := false;
               end;
         end;
      end;
   end;

begin
   Result := MatchPattern(PWideChar(Source), PWideChar(Pattern));
end;

function WildcardCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
const
   SQuestionMark: AnsiString = '?';
   SMultiply: AnsiString     = '*';
var
   sCompareRule, sBaseText: AnsiString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   if (Copy(sCompareRule, 1, 1) = SMultiply) and (Copy(sCompareRule, Length(sCompareRule), 1) = SMultiply) and (Length(sCompareRule) > 1) then begin
      sCompareRule := Copy(sCompareRule, 2, Length(sCompareRule) - 2);
      if (AnsiPos(SQuestionMark, sCompareRule) > 0) or (AnsiPos(SMultiply, sCompareRule) > 0)
         then Result := MatchStrings(sBaseText, sCompareRule)
      else Result := pos(sCompareRule, sBaseText) > 0;
   end
   else if Length(sCompareRule) = 0 then begin
      Result := Length(sBaseText) = 0;
   end
   else if sCompareRule = '*' then begin
      Result := true;
   end else begin
      Result := MatchStrings(sBaseText, sCompareRule);
   end;
end;

function WildcardCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: WideString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   if (Copy(sCompareRule, 1, 1) = '*') and (Copy(sCompareRule, Length(sCompareRule), 1) = '*') and (Length(sCompareRule) > 1) then begin
      sCompareRule := Copy(sCompareRule, 2, Length(sCompareRule) - 2);
      if (pos('?', sCompareRule) > 0) or (pos('*', sCompareRule) > 0)
         then begin Result := MatchStrings(sBaseText, sCompareRule); end
      else begin
         Result := pos(sCompareRule, sBaseText) > 0;
      end;
   end
   else if Length(sCompareRule) = 0 then begin
      Result := Length(sBaseText) = 0;
   end
   else if sCompareRule = '*' then begin
      Result := true;
   end else begin
      Result := MatchStrings(sBaseText, sCompareRule);
   end;
end;

function SubStringCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: AnsiString;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   for i := 0 to Pred(ABaseList.Count) do begin
      if AIgnoreCase then begin
         sCompareText := DCStringToAnsiStringUTF8(UpperCase(ABaseList[i]));
      end else begin
         sCompareText := DCStringToAnsiStringUTF8(ABaseList[i]);
      end;
      if AnsiStrPos(PAnsiChar(sCompareText), PAnsiChar(sCompareRule)) <> nil then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function SubStringCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: WideString;
   b: boolean;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   for i := 0 to Pred(ABaseList.Count) do begin
      {$IFDEF FPC}
      if AIgnoreCase then begin
         sCompareText := UTF8Decode(UpperCase(ABaseList[i]));
      end else begin
         sCompareText := UTF8Decode(ABaseList[i]);
      end;
      b := (StrPos(PWideChar(sCompareText), PWideChar(sCompareRule)) <> nil);
      {$ELSE FPC}
      if AIgnoreCase then begin
         sCompareText := UpperCase(ABaseList[i]);
      end else begin
         sCompareText := ABaseList[i];
      end;
      b := (WStrPos(PWideChar(sCompareText), PWideChar(sCompareRule)) <> nil);
      {$ENDIF FPC}
      if b then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function SubStringCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: AnsiString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   Result := (AnsiStrPos(PAnsiChar(sBaseText), PAnsiChar(sCompareRule)) <> nil);
end;

function SubStringCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: WideString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   {$IFDEF FPC}
   Result := (StrPos(PWideChar(sBaseText), PWideChar(sCompareRule)) <> nil);
   {$ELSE FPC}
   Result := (WStrPos(PWideChar(sBaseText), PWideChar(sCompareRule)) <> nil);
   {$ENDIF FPC}
end;

function IdenticalCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: AnsiString;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   for i := 0 to Pred(ABaseList.Count) do begin
      if AIgnoreCase then begin
         sCompareText := DCStringToAnsiStringUTF8(UpperCase(ABaseList[i]));
      end else begin
         sCompareText := DCStringToAnsiStringUTF8(ABaseList[i]);
      end;
      if sCompareText = sCompareRule then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function IdenticalCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: WideString;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   for i := 0 to Pred(ABaseList.Count) do begin
      {$IFDEF FPC}
      if AIgnoreCase then begin
         sCompareText := UTF8Decode(UpperCase(ABaseList[i]));
      end else begin
         sCompareText := UTF8Decode(ABaseList[i]);
      end;
      {$ELSE FPC}
      if AIgnoreCase then begin
         sCompareText := UpperCase(ABaseList[i]);
      end else begin
         sCompareText := ABaseList[i];
      end;
      {$ENDIF FPC}
      if sCompareText = sCompareRule then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function IdenticalCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: AnsiString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   Result := sBaseText = sCompareRule;
end;

function IdenticalCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: WideString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   Result := sBaseText = sCompareRule;
end;

function StartingBytesCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: AnsiString;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   for i := 0 to Pred(ABaseList.Count) do begin
      if AIgnoreCase then begin
         sCompareText := DCStringToAnsiStringUTF8(UpperCase(ABaseList[i]));
      end else begin
         sCompareText := DCStringToAnsiStringUTF8(ABaseList[i]);
      end;
      sCompareText := Copy(sCompareText, 1, Length(sCompareRule));
      if sCompareText = sCompareRule then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function StartingBytesCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList;
var
   i: Integer;
   sCompareText, sCompareRule: WideString;
begin
   Result := TStringList.Create;
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
   end else begin
      sCompareRule := ACompareRule;
   end;
   for i := 0 to Pred(ABaseList.Count) do begin
      {$IFDEF FPC}
      if AIgnoreCase then begin
         sCompareText := UTF8Decode(UpperCase(ABaseList[i]));
      end else begin
         sCompareText := UTF8Decode(ABaseList[i]);
      end;
      {$ELSE FPC}
      if AIgnoreCase then begin
         sCompareText := UpperCase(ABaseList[i]);
      end else begin
         sCompareText := ABaseList[i];
      end;
      {$ENDIF FPC}
      sCompareText := Copy(sCompareText, 1, Length(sCompareRule));
      if sCompareText = sCompareRule then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function StartingBytesCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: AnsiString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   Result := Copy(sBaseText, 1, Length(sCompareRule)) = sCompareRule;
end;

function StartingBytesCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: WideString;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   Result := Copy(sBaseText, 1, Length(sCompareRule)) = sCompareRule;
end;

function BoyerMooreCompareStrings(const ABaseList: TStrings; const ACompareRule: AnsiString; const AIgnoreCase: boolean): TStringList; overload;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to Pred(ABaseList.Count) do begin
      if BoyerMooreCompareString(DCStringToAnsiStringUTF8(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function BoyerMooreCompareStrings(const ABaseList: TStrings; const ACompareRule: WideString; const AIgnoreCase: boolean): TStringList; overload;
var
   i: Integer;
begin
   Result := TStringList.Create;
   for i := 0 to Pred(ABaseList.Count) do begin
      if BoyerMooreCompareString(WideString(ABaseList[i]), ACompareRule, AIgnoreCase) then begin
         Result.AddObject(ABaseList[i], ABaseList.Objects[i]);
      end;
   end;
end;

function BoyerMooreCompareStrings(const ABaseText: AnsiString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
var
   i: Integer;
begin
   Result := false;
   for i := 0 to Pred(ACompareRules.Count) do begin
      if BoyerMooreCompareString(ABaseText, DCStringToAnsiStringUTF8(ACompareRules[i]), AIgnoreCase) then begin
         Result := true;
         Exit;
      end;
   end;
end;

function BoyerMooreCompareStrings(const ABaseText: WideString; const ACompareRules: TStrings; const AIgnoreCase: boolean): boolean; overload;
var
   i: Integer;
begin
   Result := false;
   for i := 0 to Pred(ACompareRules.Count) do begin
      if BoyerMooreCompareString(ABaseText, WideString(ACompareRules[i]), AIgnoreCase) then begin
         Result := true;
         Exit;
      end;
   end;
end;

function BoyerMooreCompareString(const ABaseText, ACompareRule: WideString; const AIgnoreCase: boolean): boolean; overload;
begin
   Result := BoyerMooreCompareString(UTF8Encode(ABaseText), UTF8Encode(ACompareRule), AIgnoreCase);
end;

function BoyerMooreCompareString(const ABaseText, ACompareRule: AnsiString; const AIgnoreCase: boolean): boolean;
var
   sCompareRule, sBaseText: AnsiString;
   {$IFDEF FPC}
var
   bmSNL: TBoyerMoorePepiMK;
   ms: TMemoryStream;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := '___' + UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := '___' + ABaseText;
   end;
   Result := false;

   bmSNL := TBoyerMoorePepiMK.Create;
   bmSNL.FindFirstOnly := true;
   ms := TMemoryStream.Create;
   try
      ms.Write(sBaseText[1], Length(sBaseText));
      bmSNL.Search(ms, sCompareRule);
      if bmSNL.LastPosition > 0 then begin
         Result := true;
      end else begin
         Result := false;
      end;
   finally
      FreeAndNil(ms);
      FreeAndNil(bmSNL);
   end;
end;
   {$ELSE FPC}

var
   i, j, k, m, n: Integer;
   aSkipTable: array [0 .. MAXCHAR] of Integer;
begin
   if AIgnoreCase then begin
      sCompareRule := UpperCase(ACompareRule);
      sBaseText := UpperCase(ABaseText);
   end else begin
      sCompareRule := ACompareRule;
      sBaseText := ABaseText;
   end;
   Result := false;
   // search := 0; // Position
   m := Length(ACompareRule);
   if m = 0 then begin
      // search := 1; // position
      Result := true;
   end;
   for k := 0 to MAXCHAR do aSkipTable[k] := m; { *** Preprocessing *** }
   for k := 1 to m - 1 do aSkipTable[ord(ACompareRule[k])] := m - k;
   k := m;
   n := Length(ABaseText); { *** Search *** }
   while (not Result) and (k <= n) do begin
      i := k;
      j := m;
      while (j >= 1) do
         if ABaseText[i] <> ACompareRule[j] then j := -1
         else begin
            j := j - 1;
            i := i - 1;
         end;
      if j = 0 then begin
         // search := i+1; // position
         Result := true;
      end;
      k := k + aSkipTable[ord(ABaseText[k])];
   end;
end;
{$ENDIF FPC}

// returns 0 to 100, 0 means identical
function Levenshtein(const Text1, Text2: AnsiString): Integer;
var
   delta: array of Integer;
   len1, len2: Integer;  // length of str1, str2
   idx1, idx2: Integer;  // index into str1, str2
   clast, cnew: Integer; // last/next cost
begin
   len1 := Length(Text1);
   len2 := Length(Text2);
   if (len1 = 0) and (len2 = 0) then begin
      Result := 0;
   end
   else if (len1 = 0) or (len2 = 0) then begin
      Result := 100;
   end else begin
      SetLength(delta, len2 + 1);
      for idx2 := 0 to len2 do begin
         delta[idx2] := idx2;
      end;
      for idx1 := 1 to len1 do begin
         clast := delta[0];
         delta[0] := idx1;

         for idx2 := 1 to len2 do begin
            cnew := clast + ord(Text1[idx1] <> Text2[idx2]);
            if delta[idx2] + 1 < cnew then // <-- ist noch der alte!
                  cnew := delta[idx2] + 1;
            if delta[idx2 - 1] + 1 < cnew then cnew := delta[idx2 - 1] + 1;
            clast := delta[idx2];
            delta[idx2] := cnew;
         end;
      end;

      Result := delta[len2] * 100 div len2;
      (* Alternativ:
        if len2 > len1 then
        Result := delta[len2] * 100 div len2
        else
        Result := delta[len2] * 100 div len1;
      *)
   end;
end;

function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TStrings; Flags: TReplaceFlags; ASeparatedList: boolean): AnsiString;
var
   acr: TAhoCorasickReplacer;
begin
   acr := TAhoCorasickReplacer.Create;
   try
      acr.CaseSensitive := not(rfIgnoreCase in Flags);
      acr.Keywords.Assign(Patterns);
      acr.BuildTree(ASeparatedList);
      acr.Replace(s);
      Result := acr.Result;
   finally
      FreeAndNil(acr);
   end;
end;

{$IFNDEF NoAnsiStrings}

function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TAnsiStrings; Flags: TReplaceFlags; ASeparatedList: boolean = true): AnsiString; overload;
var
   acr: TAhoCorasickReplacer;
begin
   acr := TAhoCorasickReplacer.Create;
   try
      acr.CaseSensitive := not(rfIgnoreCase in Flags);
      acr.Keywords.Assign(Patterns);
      acr.BuildTree(ASeparatedList);
      acr.Replace(s);
      Result := acr.Result;
   finally
      acr.Free;
   end;
end;
{$ENDIF NoAnsiStrings}
{$IFNDEF NoAnsiStrings}

function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TAnsiStringList; Flags: TReplaceFlags; ASeparatedList: boolean = true): AnsiString;
var
   acr: TAhoCorasickReplacer;
begin
   acr := TAhoCorasickReplacer.Create;
   try
      acr.CaseSensitive := not(rfIgnoreCase in Flags);
      acr.Keywords.Assign(Patterns);
      acr.BuildTree(ASeparatedList);
      acr.Replace(s);
      Result := acr.Result;
   finally
      acr.Free;
   end;
end;
{$ENDIF NoAnsiStrings}

function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TStrings; Flags: TReplaceFlags; Results: TStrings; ASeparatedList: boolean): boolean;
// wants TStringList as objects to Patterns
var
   acr: TAhoCorasickMultiReplacer;
begin
   try
      acr := TAhoCorasickMultiReplacer.Create;
      try
         acr.CaseSensitive := not(rfIgnoreCase in Flags);
         acr.Keywords.Assign(Patterns);
         acr.BuildTree(ASeparatedList);
         acr.Replace(s);
         Results.AddStrings(acr.Results);
         Result := Results.Count > 0;
      finally
         FreeAndNil(acr);
      end;
   except
      Result := false;
   end;
end;

{$IFNDEF NoAnsiStrings}

function AhoCorasickStringReplaceA(const s: AnsiString; Patterns: TAnsiStrings; Flags: TReplaceFlags; Results: TAnsiStrings; ASeparatedList: boolean = false): boolean; overload;
var
   acr: TAhoCorasickMultiReplacer;
   i: Integer;
begin
   try
      acr := TAhoCorasickMultiReplacer.Create;
      try
         acr.CaseSensitive := not(rfIgnoreCase in Flags);
         acr.Keywords.Assign(Patterns);
         acr.BuildTree(ASeparatedList);
         acr.Replace(s);
         for i := 0 to Pred(acr.Results.Count) do begin
            Results.Add(AnsiString(acr.Results[i]));
         end;
         Result := Results.Count > 0;
      finally
         acr.Free;
      end;
   except
      Result := false;
   end;
end;
{$ENDIF NoAnsiStrings}

function AhoCorasickSpybotAFPStringReplaceA(const s: AnsiString; Patterns: TStrings; Flags: TReplaceFlags): AnsiString;
var
   acr: TAhoCorasickReplacer;
   sl: TStringList;
   i: Integer;
begin
   sl := TStringList.Create;
   acr := TAhoCorasickReplacer.Create;
   try
      acr.CaseSensitive := not(rfIgnoreCase in Flags);
      sl.NameValueSeparator := #9;
      (* for i := 0 to Pred(Patterns.Count) do begin
        ro := TAhoCorasickReplacementObject.Create;
        ro.Replacer := '"' + Patterns.ValueFromIndex[i] + '=';
        sl.AddObject('"' + Patterns.Names[i] + '=', ro);
        ro := TAhoCorasickReplacementObject.Create;
        ro.Replacer := ',' + Patterns.ValueFromIndex[i] + '=';
        sl.AddObject(',' + Patterns.Names[i] + '=', ro);
        end; *)
      for i := 0 to Pred(Patterns.Count) do begin
         sl.Add('"' + Patterns.Names[i] + '='#9'"' + Patterns.ValueFromIndex[i] + '=');
         sl.Add(',' + Patterns.Names[i] + '='#9',' + Patterns.ValueFromIndex[i] + '=');
      end;
      acr.Keywords.Assign(sl);
      acr.BuildTree(true);
      acr.Replace(s);
      Result := acr.Result;
   finally
      // for i := 0 to Pred(sl.Count) do begin
      // TAhoCorasickReplacementObject(sl.Objects[i]).Free;
      // end;
      FreeAndNil(sl);
      FreeAndNil(acr);
   end;
end;

function SupportsRegExp: boolean;
begin
   Result := true;
end;

function SupportsPerlRegExp: boolean;
begin
   {$IFDEF UsePerlRegExpr}
   Result := true;
   {$ELSE UsePerlRegExpr}
   Result := false;
   {$ENDIF UsePerlRegExpr}
end;

{ TRegExpResults }

class constructor TRegExpResults.Create;
begin
   RegExpResults := TRegExpResults.Create;
end;

function TRegExpResults.ApplyToString(AInput: string): string;
var
   iRegMatch: Integer;
begin
   Result := AInput;
   for iRegMatch := 0 to 10 do begin
      if iRegMatch < FTrueResults.Count then begin
         Result := StringReplace(Result, '<$REGMATCH' + IntToStr(iRegMatch) + '>', FTrueResults[iRegMatch], [rfReplaceAll]);
      end else begin
         Result := StringReplace(Result, '<$REGMATCH' + IntToStr(iRegMatch) + '>', '', [rfReplaceAll]);
      end;
   end;
end;

procedure TRegExpResults.BeginUpdate;
begin
   Push;
   FTrueResultsCacheSuccess := '';
   FResultsCacheSuccess := '';
   FExpressionInUpdate := false;
   // FReplacer.Text := FTrueResults.Text;
   FUpdating := true;
end;

constructor TRegExpResults.Create;
begin
   inherited;
   FUpdating := false;
   FResults := TStringList.Create;
   FTrueResults := TStringList.Create;
   // FReplacer := TAnsiStringList.Create;
end;

class destructor TRegExpResults.Destroy;
begin
   RegExpResults.Free;
end;

destructor TRegExpResults.Destroy;
begin
   FResults.Free;
   FTrueResults.Free;
   // FReplacer.Free;
   inherited;
end;

procedure TRegExpResults.EndUpdate;
begin
   if FExpressionInUpdate then begin
      PopSuccess;
   end else begin
      Pop;
   end;
   FUpdating := false;
   // FReplacer.Text := FTrueResults.Text;
end;

{$IFDEF UsePerlRegExpr}

procedure TRegExpResults.FromRegExp(AMatch: TMatch);
var
   i: Integer;
begin
   FExpressionInUpdate := true;
   FResults.Clear;
   for i := 0 to Pred(AMatch.Groups.Count) do begin
      FResults.Add(AMatch.Groups[i].Value);
   end;
   if AMatch.Success then begin
      FTrueResults.Clear;
      for i := 0 to Pred(AMatch.Groups.Count) do begin
         FTrueResults.Add(AMatch.Groups[i].Value);
      end;
   end;
   (*
     if not FUpdating then begin
     FReplacer.Text := FTrueResults.Text;
     end;
   *)
end;
{$ENDIF}

procedure TRegExpResults.Pop;
begin
   FTrueResults.Text := FTrueResultsCache;
   FResults.Text := FResultsCache;
end;

procedure TRegExpResults.PopSuccess;
begin
   FTrueResults.Text := FTrueResultsCacheSuccess;
   FResults.Text := FResultsCacheSuccess;
end;

procedure TRegExpResults.Push;
begin
   FTrueResultsCache := FTrueResults.Text;
   FResultsCache := FResults.Text;
end;

procedure TRegExpResults.PushSuccess;
begin
   FTrueResultsCacheSuccess := FTrueResults.Text;
   FResultsCacheSuccess := FResults.Text;
end;

procedure TRegExpResults.ResetUpdate;
begin
   // ResetUpdate *might* be unnecessary since replaces take place before
   // sbi commands are called. We implement it mainly for future enhancements.
   Pop;
end;

procedure TRegExpResults.SetUpdateSuccess;
begin
   PushSuccess;
end;

procedure TRegExpResults.FromRegExp(AResult: boolean; ARegExpr: TRegExpr);
var
   i: Integer;
begin
   FExpressionInUpdate := true;
   FResults.Clear;
   for i := 0 to ARegExpr.SubExprMatchCount do begin
      FResults.Add(ARegExpr.Match[i]);
   end;
   if AResult then begin
      FTrueResults.Clear;
      for i := 0 to ARegExpr.SubExprMatchCount do begin
         FTrueResults.Add(ARegExpr.Match[i]);
      end;
   end;
   (*
     if not FUpdating then begin
     FReplacer.Text := FTrueResults.Text;
     end;
   *)
end;

begin
   RegisterLicense('Levensthein Implementation', 'Volker (DelphiPraxis.net)', 'https://www.delphipraxis.net/399584-post33.html', ltPublicDomain, vEndUser);
end.
