﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Constants for file paths.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2016-2021 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-0829  pk   5m  Renamed snlFilesPathsConsts to Firefly.FilePaths.WinConsts
// *****************************************************************************
   )
}

unit Firefly.FilePaths.WinConsts;

interface

   {$IFDEF MSWindows}
   uses
   Windows,
   SHFolder,
   ShlObj;
   {$ENDIF MSWindows}

type
   TPathSolveConfig = record
      ID: integer;
      TemplateName: WideString;
      TemplateShortcut: WideString;
   end;

const
   {$EXTERNALSYM CSIDL_FAVORITES}
   CSIDL_FAVORITES = $0006;
   {$EXTERNALSYM CSIDL_MYDOCUMENTS} // logical "My Documents" desktop icon
   CSIDL_MYDOCUMENTS = $000C;
   {$EXTERNALSYM CSIDL_MYMUSIC} // "My Music" folder
   CSIDL_MYMUSIC = $000D;
   {$EXTERNALSYM CSIDL_MYVIDEO} // "My Videos" folder
   CSIDL_MYVIDEO = $000E;
   {$EXTERNALSYM CSIDL_LOCAL_APPDATA}
   CSIDL_LOCAL_APPDATA = $001C;
   {$EXTERNALSYM CSIDL_COMMON_APPDATA}
   CSIDL_COMMON_APPDATA = $0023;
   {$EXTERNALSYM CSIDL_WINDOWS}
   CSIDL_WINDOWS = $0024;
   {$EXTERNALSYM CSIDL_SYSTEM}
   CSIDL_SYSTEM = $0025;
   {$EXTERNALSYM CSIDL_PROGRAM_FILES}
   CSIDL_PROGRAM_FILES = $0026;
   {$EXTERNALSYM CSIDL_PROFILE}
   CSIDL_PROFILE = $0028;
   {$EXTERNALSYM CSIDL_PROGRAM_FILES_COMMON}
   CSIDL_PROGRAM_FILES_COMMON = $002B;
   {$EXTERNALSYM CSIDL_COMMON_TEMPLATES}
   CSIDL_COMMON_TEMPLATES = $002D;
   {$EXTERNALSYM CSIDL_COMMON_DOCUMENTS}
   CSIDL_COMMON_DOCUMENTS = $002E;
   {$EXTERNALSYM CSIDL_PROFILES}
   CSIDL_PROFILES = $003E;

   {$IFDEF FPC}
   {$EXTERNALSYM CSIDL_DESKTOP} // <desktop>
   CSIDL_DESKTOP = $0000;
   {$EXTERNALSYM CSIDL_INTERNET} // Internet Explorer (icon on desktop)
   CSIDL_INTERNET = $0001;
   {$EXTERNALSYM CSIDL_PROGRAMS} // Start Menu\Programs
   CSIDL_PROGRAMS = $0002;
   {$EXTERNALSYM CSIDL_CONTROLS} // My Computer\Control Panel
   CSIDL_CONTROLS = $003;
   {$EXTERNALSYM CSIDL_PRINTERS} // My Computer\Printers
   CSIDL_PRINTERS = $004;
   {$EXTERNALSYM CSIDL_PERSONAL} // My Documents
   CSIDL_PERSONAL = $0005;
   {$EXTERNALSYM CSIDL_STARTUP} // Start Menu\Programs\Startup
   CSIDL_STARTUP = $0007;
   {$EXTERNALSYM CSIDL_RECENT} // <user name>\Recent
   CSIDL_RECENT = $0008;
   {$EXTERNALSYM CSIDL_SENDTO} // <user name>\SendTo
   CSIDL_SENDTO = $0009;
   {$EXTERNALSYM CSIDL_BITBUCKET} // <desktop>\Recycle Bin
   CSIDL_BITBUCKET = $000A;
   {$EXTERNALSYM CSIDL_STARTMENU} // <user name>\Start Menu
   CSIDL_STARTMENU = $000B;
   {$EXTERNALSYM CSIDL_DESKTOPDIRECTORY} // <user name>\Desktop
   CSIDL_DESKTOPDIRECTORY = $0010;
   {$EXTERNALSYM CSIDL_DRIVES} // My Computer
   CSIDL_DRIVES = $0011;
   {$EXTERNALSYM CSIDL_NETWORK} // Network Neighborhood (My Network Places)
   CSIDL_NETWORK = $0012;
   {$EXTERNALSYM CSIDL_NETHOOD} // <user name>\nethood
   CSIDL_NETHOOD = $00013;
   {$EXTERNALSYM CSIDL_FONTS} // windows\fonts
   CSIDL_FONTS = $0014;
   {$EXTERNALSYM CSIDL_TEMPLATES}
   CSIDL_TEMPLATES = $0015;
   {$EXTERNALSYM CSIDL_COMMON_STARTMENU} // All Users\Start Menu
   CSIDL_COMMON_STARTMENU = $0016;
   {$EXTERNALSYM CSIDL_COMMON_PROGRAMS} // All Users\Start Menu\Programs
   CSIDL_COMMON_PROGRAMS = $0017;
   {$EXTERNALSYM CSIDL_COMMON_STARTUP} // All Users\Startup
   CSIDL_COMMON_STARTUP = $0018;
   {$EXTERNALSYM CSIDL_COMMON_DESKTOPDIRECTORY} // All Users\Desktop
   CSIDL_COMMON_DESKTOPDIRECTORY = $0019;
   {$EXTERNALSYM CSIDL_APPDATA} // <user name>\Application Data
   CSIDL_APPDATA = $001A;
   {$EXTERNALSYM CSIDL_PRINTHOOD}  // <user name>\PrintHood
   CSIDL_PRINTHOOD = $001B;

   {$EXTERNALSYM CSIDL_ALTSTARTUP} // non localized startup
   CSIDL_ALTSTARTUP = $001D;
   {$EXTERNALSYM CSIDL_COMMON_ALTSTARTUP} // non localized common startup
   CSIDL_COMMON_ALTSTARTUP = $001E;
   {$EXTERNALSYM CSIDL_COMMON_FAVORITES}
   CSIDL_COMMON_FAVORITES = $001F;

   {$EXTERNALSYM CSIDL_COMMON_MUSIC} // All Users\My Music
   CSIDL_COMMON_MUSIC = $0035;
   {$EXTERNALSYM CSIDL_COMMON_PICTURES} // All Users\My Pictures
   CSIDL_COMMON_PICTURES = $0036;
   {$EXTERNALSYM CSIDL_COMMON_VIDEO} // All Users\My Video
   CSIDL_COMMON_VIDEO = $0037;
   {$ENDIF FPC}


const
   {$IFDEF MSWindows}
   PathSolveVarCount                                                        = 52;
   {$ELSE MSWindows}
   PathSolveVarCount                                                        = 49;
   {$ENDIF MSWindows}
   PathSolveConfigs: array [0 .. PathSolveVarCount - 1] of TPathSolveConfig = (
      // These entries here are not useful for our purposes.
      // (ID: CSIDL_INTERNET;                TemplateName: 'INTERNET'; ),
      // (ID: CSIDL_PRINTERS;                TemplateName: 'PRINTERS'; ),
      // (ID: CSIDL_BITBUCKET;               TemplateName: 'BITBUCKET'; ),
      // (ID: CSIDL_DRIVES;                  TemplateName: 'DRIVES'; ),
      // (ID: CSIDL_NETWORK;                 TemplateName: 'NETWORK'; ),

      (ID: - 1; TemplateName: 'FIXEDDRIVES'; TemplateShortcut: 'AA';),                  //
      (ID: - 1; TemplateName: 'SYSDRIVE'; TemplateShortcut: 'AB';),                     //
      (ID: CSIDL_WINDOWS; TemplateName: 'WINDIR'; TemplateShortcut: 'AC';),             //
      (ID: - 1; TemplateName: 'WINDIR2'; TemplateShortcut: 'AD';),                      //
      (ID: CSIDL_SYSTEM; TemplateName: 'SYSDIR'; TemplateShortcut: 'AE';),              //
      (ID: - 1; TemplateName: 'SYSDIR2'; TemplateShortcut: 'AF';),                      //
      (ID: CSIDL_FONTS; TemplateName: 'FONTS'; TemplateShortcut: 'AG';),                //
      (ID: CSIDL_CONTROLS; TemplateName: 'CONTROLS'; TemplateShortcut: 'AH';),          //
      (ID: - 1; TemplateName: 'SYSCPLDIR'; TemplateShortcut: 'AI';),                    //
      (ID: - 1; TemplateName: 'PATH'; TemplateShortcut: 'AJ';),                         //
      (ID: CSIDL_PROGRAM_FILES; TemplateName: 'PROGRAMFILES'; TemplateShortcut: 'AK';), //
      (ID: CSIDL_PROGRAM_FILES_COMMON; TemplateName: 'COMMONPROGRAMFILES'; TemplateShortcut: 'AL';), //
      (ID: CSIDL_PROFILES; TemplateName: 'PROFILES'; TemplateShortcut: 'AM';),                     //
      (ID: CSIDL_PROFILE; TemplateName: 'PROFILE'; TemplateShortcut: 'AN';),                       //
      (ID: CSIDL_APPDATA; TemplateName: 'APPDATA'; TemplateShortcut: 'AO';),                       //
      (ID: - 1; TemplateName: 'QUICKLAUNCH'; TemplateShortcut: 'AP';),                             //
      (ID: CSIDL_DESKTOP; TemplateName: 'ADESKTOP'; TemplateShortcut: 'AQ';),                      //
      (ID: CSIDL_DESKTOPDIRECTORY; TemplateName: 'DESKTOP'; TemplateShortcut: 'AR';),              //
      (ID: CSIDL_FAVORITES; TemplateName: 'FAVORITES'; TemplateShortcut: 'AS';),                   //
      (ID: - 1; TemplateName: 'LOCALSETTINGS'; TemplateShortcut: 'AT';),                           //
      (ID: CSIDL_LOCAL_APPDATA; TemplateName: 'LOCALAPPDATA'; TemplateShortcut: 'AU';),            //
      (ID: CSIDL_PERSONAL; TemplateName: 'PERSONAL'; TemplateShortcut: 'AV';),                     //
      (ID: CSIDL_MYDOCUMENTS; TemplateName: 'MYDOCUMENTS'; TemplateShortcut: 'AW';),               //
      (ID: CSIDL_MYMUSIC; TemplateName: 'MYMUSIC'; TemplateShortcut: 'AX';),                       //
      (ID: CSIDL_MYVIDEO; TemplateName: 'MYVIDEO'; TemplateShortcut: 'AY';),                       //
      (ID: CSIDL_NETHOOD; TemplateName: 'NETHOOD'; TemplateShortcut: 'AZ';),                       //
      (ID: CSIDL_PRINTHOOD; TemplateName: 'PRINTHOOD'; TemplateShortcut: 'BA';),                   //
      (ID: CSIDL_RECENT; TemplateName: 'RECENT'; TemplateShortcut: 'BB';),                         //
      (ID: CSIDL_SENDTO; TemplateName: 'SENDTO'; TemplateShortcut: 'BC';),                         //
      (ID: CSIDL_STARTMENU; TemplateName: 'STARTMENU'; TemplateShortcut: 'BD';),                   //
      (ID: CSIDL_PROGRAMS; TemplateName: 'PROGRAMS'; TemplateShortcut: 'BE';),                     //
      (ID: CSIDL_ALTSTARTUP; TemplateName: 'ALTSTARTUP'; TemplateShortcut: 'BF';),                 //
      (ID: CSIDL_STARTUP; TemplateName: 'STARTUP'; TemplateShortcut: 'BG';),                       //
      (ID: CSIDL_TEMPLATES; TemplateName: 'TEMPLATES'; TemplateShortcut: 'BH';),                   //
      (ID: CSIDL_COMMON_APPDATA; TemplateName: 'COMMONAPPDATA'; TemplateShortcut: 'BI';),          //
      (ID: - 1; TemplateName: 'COMMONQUICKLAUNCH'; TemplateShortcut: 'BJ';),                       //
      (ID: CSIDL_COMMON_DESKTOPDIRECTORY; TemplateName: 'COMMONDESKTOP'; TemplateShortcut: 'BK';), //
      (ID: CSIDL_COMMON_DOCUMENTS; TemplateName: 'COMMONDOCUMENTS'; TemplateShortcut: 'BL';),      //
      (ID: CSIDL_COMMON_DOCUMENTS; TemplateName: 'COMMONDOCUMENTS'; TemplateShortcut: 'BM';),      //
      (ID: CSIDL_COMMON_MUSIC; TemplateName: 'COMMONMUSIC'; TemplateShortcut: 'BN';),              //
      (ID: CSIDL_COMMON_VIDEO; TemplateName: 'COMMONVIDEO'; TemplateShortcut: 'BO';),              //
      (ID: CSIDL_COMMON_FAVORITES; TemplateName: 'COMMONFAVORITES'; TemplateShortcut: 'BP';),      //
      (ID: CSIDL_COMMON_STARTMENU; TemplateName: 'COMMONSTARTMENU'; TemplateShortcut: 'BQ';),      //
      (ID: CSIDL_COMMON_PROGRAMS; TemplateName: 'COMMONPROGRAMS'; TemplateShortcut: 'BR';),        //
      (ID: CSIDL_COMMON_ALTSTARTUP; TemplateName: 'COMMONALTSTARTUP'; TemplateShortcut: 'BS';),    //
      (ID: CSIDL_COMMON_STARTUP; TemplateName: 'COMMONSTARTUP'; TemplateShortcut: 'BT';),          //
      (ID: CSIDL_COMMON_TEMPLATES; TemplateName: 'COMMONTEMPLATES'; TemplateShortcut: 'BU';),      //
      (ID: - 1; TemplateName: 'TEMP'; TemplateShortcut: 'BV';),                                    //
      {$IFDEF MSWindows}
      (ID: CSIDL_INTERNET_CACHE; TemplateName: 'INTERNETCACHE'; TemplateShortcut: 'IC';),          //
      (ID: CSIDL_COOKIES; TemplateName: 'COOKIES'; TemplateShortcut: 'IO';),                       //
      (ID: CSIDL_HISTORY; TemplateName: 'HISTORY'; TemplateShortcut: 'IH';),                       //
      {$ENDIF MSWindows}
      (ID: - 1; TemplateName: 'MOZILLAPROFILE'; TemplateShortcut: 'BW';)                           //
      );

implementation

end.
