{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Converts strings to a form that's compatible with regular expressions.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-04  pk  10m  Moved from snlStrings
// *****************************************************************************
   )
}

unit Firefly.Strings.RegularExpressions;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$endif FPC}

interface

uses
   SysUtils;

{
  Converts string to be regular expression compatible.

  @param(AText is a text to convert.)
  @returns(Text that is compatible with regular expressions.)
}
function StringToRegExpr(const AText: AnsiString): AnsiString; overload;

{
  Converts string to be regular expression compatible.

  @param(AText is a text to convert.)
  @returns(Text that is compatible with regular expressions.)
}
function StringToRegExpr(const AText: WideString): WideString; overload;

implementation

{$IFDEF Unicode}
uses
   AnsiStrings; // In Delphi, this makes StringReplace in Ansi available
{$ENDIF Unicode}

function StringToRegExpr(const AText: AnsiString): AnsiString;
const
   SBackslash: AnsiString           = '\';
   SBackslashEscaped: AnsiString    = '\\';
   SBracketOpen: AnsiString         = '(';
   SBracketOpenEscaped: AnsiString  = '\(';
   SBracketClose: AnsiString        = ')';
   SBracketCloseEscaped: AnsiString = '\)';
   SDot: AnsiString                 = '.';
   SDotEscaped: AnsiString          = '\.';
begin
   Result := StringReplace(AText, SBackslash, SBackslashEscaped, [rfReplaceAll]);
   Result := StringReplace(Result, SBracketOpen, SBracketOpenEscaped, [rfReplaceAll]);
   Result := StringReplace(Result, SBracketClose, SBracketCloseEscaped, [rfReplaceAll]);
   Result := StringReplace(Result, SDot, SDotEscaped, [rfReplaceAll]);
end;

function StringToRegExpr(const AText: WideString): WideString;
type
  {$IFDEF FPC}
  TestString = UTF8String;
  {$ELSE FPC}
  TestString = WideString;
  {$ENDIF FPC}
const
   SBackslash: TestString           = '\';
   SBackslashEscaped: TestString    = '\\';
   SBracketOpen: TestString         = '(';
   SBracketOpenEscaped: TestString  = '\(';
   SBracketClose: TestString        = ')';
   SBracketCloseEscaped: TestString = '\)';
   SDot: TestString                 = '.';
   SDotEscaped: TestString          = '\.';
   {$IFDEF FPC}
   var s: UTF8String;
   {$ENDIF FPC}
begin
   {$IFDEF FPC}
   s := UTF8Encode(AText);
   s := StringReplace(s, SBackslash, SBackslashEscaped, [rfReplaceAll]);
   s := StringReplace(s, SBracketOpen, SBracketOpenEscaped, [rfReplaceAll]);
   s := StringReplace(s, SBracketClose, SBracketCloseEscaped, [rfReplaceAll]);
   s := StringReplace(s, SDot, SDotEscaped, [rfReplaceAll]);
   Result := UTF8Decode(s);
   {$ELSE FPC}
   Result := StringReplace(AText, SBackslash, SBackslashEscaped, [rfReplaceAll]);
   Result := StringReplace(Result, SBracketOpen, SBracketOpenEscaped, [rfReplaceAll]);
   Result := StringReplace(Result, SBracketClose, SBracketCloseEscaped, [rfReplaceAll]);
   Result := StringReplace(Result, SDot, SDotEscaped, [rfReplaceAll]);
   {$ENDIF FPC}
end;

end.
