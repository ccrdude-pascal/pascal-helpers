﻿{
  @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
  @abstract(TODO : please fill in abstract here!)

  @preformatted(
  // *****************************************************************************
  // Copyright: © 2017-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
  // License: BSD 3-Clause Revised License
  // *****************************************************************************
  // Redistribution and use in source and binary forms, with or without
  // modification, are permitted provided that the following conditions are met:
  //      * Redistributions of source code must retain the above copyright
  //        notice, this list of conditions and the following disclaimer.
  //      * Redistributions in binary form must reproduce the above copyright
  //        notice, this list of conditions and the following disclaimer in the
  //        documentation and/or other materials provided with the distribution.
  //      * Neither the name of the <organization> nor the
  //        names of its contributors may be used to endorse or promote products
  //        derived from this software without specific prior written permission.
  //
  //  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
  //  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  //  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  //  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
  //  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  //  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  //  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  //  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  //  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  //  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  // *****************************************************************************
  // Changelog (new entries first):
  // ---------------------------------------
  // 2017-07-24  pk  ---  [CCD] Updated unit header.
  // *****************************************************************************
  )
}

unit Firefly.FilePaths.FillersBase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$modeswitch advancedrecords+}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fgl,
   {$ELSE FPC}
   Generics.Collections,
   {$ENDIF FPC}
   SysUtils;

type
   TPathSource = (psCLSID, psWinAPI, psEnvironment, psCopy, psCustom);
   TPathSources = set of TPathSource;

   { TPathCacheEntry }

   TPathCacheEntry = record
      Template: WideString;
      Path: WideString;
      Source: TPathSource;
      {$IFDEF FPC}
      class operator = (a, b: TPathCacheEntry): boolean;
      {$ENDIF FPC}
   end;

   {$IFDEF FPC}

   TPathCache = specialize TFPGList<TPathCacheEntry>;
   {$ELSE FPC}
   TPathCache = TList<TPathCacheEntry>;
   {$ENDIF FPC}
   TOnFoundPathEvent = procedure(ATemplate, APath: WideString; ASource: TPathSource) of object;

   { TCustomPathTemplatesFiller }

   TCustomPathTemplatesFiller = class abstract
   private
      FOnPathFound: TOnFoundPathEvent;
   protected
      procedure DoPathFound(ATemplate, APath: WideString; ASource: TPathSource); virtual;
   public
      procedure Fill; virtual; abstract;
      property OnPathFound: TOnFoundPathEvent read FOnPathFound write FOnPathFound;
   end;

   TCustomPathTemplatesFillerClass = TCustomPathTemplatesFiller;

   {$IFDEF FPC}
   TCustomPathTemplateFillersBase = specialize TFPGList<TCustomPathTemplatesFiller>;
   {$ELSE FPC}
   TCustomPathTemplateFillersBase = TList<TCustomPathTemplatesFiller>;
   {$ENDIF FPC}
   { TCustomPathTemplateFillers }

   TCustomPathTemplateFillers = class(TCustomPathTemplateFillersBase)
   private
      class var FInstance: TCustomPathTemplateFillers;
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TCustomPathTemplateFillers;
   public
      procedure RemoveAll;
   end;

implementation

{ TPathCacheEntry }

{$IFDEF FPC}

class operator TPathCacheEntry.= (a, b: TPathCacheEntry): boolean;
begin
   Result := WideSameStr(a.Path, b.Path) and WideSameStr(a.Template, b.Template) and (a.Source = b.Source);
end;
{$ENDIF FPC}
{ TCustomPathTemplatesFiller }

procedure TCustomPathTemplatesFiller.DoPathFound(ATemplate, APath: WideString; ASource: TPathSource);
begin
   if Assigned(FOnPathFound) then begin
      FOnPathFound(ATemplate, APath, ASource);
   end;
end;

{ TCustomPathTemplateFillers }

class constructor TCustomPathTemplateFillers.Create;
begin
   FInstance := TCustomPathTemplateFillers.Create;
end;

class destructor TCustomPathTemplateFillers.Destroy;
begin
   FInstance.Free;
end;

class function TCustomPathTemplateFillers.Instance: TCustomPathTemplateFillers;
begin
   Result := FInstance;
end;

procedure TCustomPathTemplateFillers.RemoveAll;
var
   i: integer;
begin
   for i := Pred(FInstance.Count) downto 0 do begin
      FInstance[i].Free;
      FInstance.Delete(i);
   end;
end;

initialization

finalization

TCustomPathTemplateFillers.Instance.RemoveAll;

end.
