{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Reading the version resource from Windows PE files.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2003-2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-06-09  jh   --  Moved to new PepiMKBase package
// 2010-09-24  pk  10m  Added GetVersionFileVersion functions
// 2010-04-09  pk   5m  Added functions for accessing Copyright field
// 2008-05-15  pk  10m  Fixed additional field vs. md5[version] bug
// 2006-09-13  pk  10m  Fixed memory leaks
// *****************************************************************************
   )
}

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}
unit Firefly.FileVersion;

interface

uses
   SysUtils, // for exceptions
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Classes;

type
   TVersionResourceField = (vrProductVersion, vrFileVersion);

const
   SVersionResourceFieldNames: array [TVersionResourceField] of WideString = ('ProductVersion', 'FileVersion');

{
  Gets Size of pVersionBlock '\\VarFileInfo\\Translation' from PE
   
  @param(AFilename: File to get Info about)
  @returns(Size of pVersionBlock / SizeOf(TLangAndCodepage))
}
function GetVersionResCountA(const AFilename: ansistring): integer;

{
  Gets Size of pVersionBlock '\\VarFileInfo\\Translation' from PE
   
  @param(AFilename: File to get Info about)
  @returns(Size of pVersionBlock / SizeOf(TLangAndCodepage))
}
function GetVersionResCount(const AFilename: WideString): integer;


function GetVersionResA(const AFilename: ansistring; const AIndex: cardinal; const AList: TStringList): boolean; overload;
function GetVersionRes(const AFilename: WideString; const AIndex: cardinal; const AList: TStringList): boolean; overload;
// function GetVersionRes(const AFilename: string; const AIndex: integer): TStringList; overload;
function GetVersionDetailsA(const AFilename: ansistring; const AIndex: cardinal; var ALanguage, ACodePage: word; var ALanguageName: ansistring): boolean;
function GetVersionDetails(const AFilename: WideString; const AIndex: cardinal; var ALanguage, ACodePage: word; var ALanguageName: WideString): boolean;
function GetVersionField(const AFilename, AField: WideString; out Data: WideString): boolean; overload;
function GetVersionField(const AFilename: WideString; const AField: TVersionResourceField; var Data: WideString): boolean; overload;
function GetVersionField(const AFilename: WideString; const AField: TVersionResourceField): WideString; overload;


{
  Gets Company Info from PE
 
  @param(ACompanyName: Company Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionCompany(const AFilename: WideString; out ACompanyName: WideString): boolean; overload;

{
  Gets Company Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(Company Info)
}
function GetVersionCompany(const AFilename: WideString): WideString; overload;

{
  Gets Product Info from PE
 
  @param(ACopyright: Product Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionProduct(const AFilename: WideString; out AProductName: WideString): boolean; overload;

{
  Gets Product Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(Product Info)
}
function GetVersionProduct(const AFilename: WideString): WideString; overload;


{
  Gets Copyright Info from PE
 
  @param(ACopyright: Copyright Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionCopyright(const AFilename: WideString; out ACopyright: WideString): boolean; overload;

{
  Gets Copyright Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(Copyright Info)
}
function GetVersionCopyright(const AFilename: WideString): WideString; overload;

{
  Gets VersionComments Info from PE
 
  @param(AComments: Comments Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionComments(const AFilename: WideString; out AComments: WideString): boolean; overload;

{
  Gets VersionComments Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(VersionComments Info)
}
function GetVersionComments(const AFilename: WideString): WideString; overload;

{
  Gets Description Info from PE
 
  @param(ADescription: Description Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionDescription(const AFilename: WideString; out ADescription: WideString): boolean; overload;

{
  Gets Description Info from PE
   
  @param(AFilename: File to get Info about)
  @returns(Description Info)
}
function GetVersionDescription(const AFilename: WideString): WideString; overload;

{
  Gets CustomBuild Info from PE
 
  @param(ACustomBuild: CustomBuild Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionCustomBuild(const AFilename: WideString; out ACustomBuild: WideString): boolean; overload;

{
  Gets CustomBuild Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(CustomBuild Info)
}
function GetVersionCustomBuild(const AFilename: WideString): WideString; overload;

{
  Gets ProductVersion Info from PE
 
  @param(AProductVersion: ProductVersion Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionProductVersion(const AFilename: WideString; out AProductVersion: WideString): boolean; overload;

{
  Gets ProductVersion Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(ProductVersion Info)
}
function GetVersionProductVersion(const AFilename: WideString): WideString; overload;

{
  Gets FileVersion Info from PE
 
  @param(AFileVersion: FileVersion Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetVersionFileVersion(const AFilename: WideString; out AFileVersion: WideString): boolean; overload;

{
  Gets FileVersion Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(FileVersion Info)
}
function GetVersionFileVersion(const AFilename: WideString): WideString; overload;

{
  Gets AppVersion Info from PE
 
  @param(AVersion: AppVersion Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetAppVersion(const AFilename: ansistring; out AVersion: int64): ansistring; overload;

{
  Gets AppVersion Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(AppVersion Info)
}
function GetAppVersion(const AFilename: ansistring): ansistring; overload;

{
  Gets AppVersion Info from PE
 
  @param(AVersion: AppVersion Info)
  @param(AFilename: File to get Info about)
  @returns(Success)
}
function GetAppVersion(const AFilename: WideString; out AVersion: int64): WideString; overload;

{
  Gets AppVersion Info from PE
 
  @param(AFilename: File to get Info about)
  @returns(AppVersion Info)
}
function GetAppVersion(const AFilename: WideString): WideString; overload;


function GetAppVersionWords(const AFilename: string; out AMajor, AMinor, ARelease, ABuild: word): boolean;

{
  Gets AppVersion Number from PE
 
  @param(AVersion: AVersion to get Info about)
  @returns(AppVersion Number)
}
function GetAppVersionNumber(const AVersion: string): int64;

{
  Gets AppVersion String from PE
 
  @param(AVersion: AVersion to get Info about)
  @returns(AppVersion String)
}
function GetAppVersionStr(const AVersion: int64): string; overload;

{
  Gets AppVersion String from PE
 
  @param(AVersion: AVersion to get Info about)
  @returns(AppVersion String)
}
{$IFDEF MSWindows}
function GetAppVersionStr(const AVersion: TULargeInteger): string; overload;
{$ENDIF MSWindows}

implementation

uses
   //DebugLog,
   //DebugLog.Consts,
   Firefly.Numbers;

type
   TLangAndCodepage = packed record
      wLanguage, wCodepage: word;
   end;

   PLangAndCodepage = ^TLangAndCodepage;

function IntToStrA(const AValue: integer): ansistring;
   /// Mini implementation to avoid SysUtils
begin
   Str(AValue, Result);
end;

function IntToStrW(const AValue: integer): WideString;
   /// Mini implementation to avoid SysUtils
var
   s: shortstring;
begin
   Str(AValue, s);
   Result := WideString(s);
end;

function StrToIntDef(const AText: string; const default: integer): integer;
   /// Mini implementation to avoid SysUtils
var
   iEC: integer
   {$IFDEF FPC}
   = 0
   {$ENDIF FPC}
   ;
begin
   Val(AText, Result, iEC);
   if iEC > 0 then begin
      Result := default;
   end;
end;

function GetVersionResCountA(const AFilename: ansistring): integer;
var
   dwVersionSize: DWord;
   hVersion: cardinal;
   cSize: cardinal;
   pVersionBlock, pVersionData: Pointer;
begin
   Result := 0;
   {$IFDEF MSWindows}
   dwVersionSize := GetFileVersionInfoSizeA(pansichar(AFilename), hVersion);
   if dwVersionSize > 0 then begin
      GetMem(pVersionBlock, dwVersionSize);
      try
         GetFileVersionInfoA(pansichar(AFilename), hVersion, dwVersionSize, pVersionBlock);
         VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
         Result := (cSize div SizeOf(TLangAndCodepage));
      finally
         FreeMem(pVersionBlock, dwVersionSize);
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionResCount(const AFilename: WideString): integer;
var
   dwVersionSize: DWord;
   hVersion: cardinal;
   cSize: cardinal;
   pVersionBlock, pVersionData: Pointer;
begin
   Result := 0;
   {$IFDEF MSWindows}
   dwVersionSize := GetFileVersionInfoSizeW(pwidechar(AFilename), hVersion);
   if dwVersionSize > 0 then begin
      GetMem(pVersionBlock, dwVersionSize);
      try
         GetFileVersionInfoW(pwidechar(AFilename), hVersion, dwVersionSize, pVersionBlock);
         VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
         Result := (cSize div SizeOf(TLangAndCodepage));
      finally
         FreeMem(pVersionBlock, dwVersionSize);
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionRes(const AFilename: string; const AIndex: cardinal): TStringList; overload;
begin
   Result := TStringList.Create;
   {$IFDEF MSWindows}
   GetVersionRes(WideString(AFilename), AIndex, Result);
   {$ENDIF MSWindows}
end;

function GetVersionResA(const AFilename: ansistring; const AIndex: cardinal; const AList: TStringList): boolean; overload;
   {$IFDEF MSWindows}
var
   pVersionBlock: Pointer;
   lc: TLangAndCodepage;

   function GetVerString(Name: ansistring): ansistring;
   var
      ps: Pointer;
      pc: pansichar;
      size: cardinal;
      sSubBlock: ansistring;
   begin
      Result := '';
      ps := nil;
      sSubBlock := '\\StringFileInfo\\' + WordToHexA(lc.wLanguage) + WordToHexA(lc.wCodepage) + '\\' + Name;
      VerQueryValueA(pVersionBlock, pansichar(sSubBlock), ps, size);
      try
         pc := ps;
         Result := Name + '=' + pc;
         // Dispose(pc);
      except
         on E: Exception do begin
            Result := Name + '=';
            //DebugLogger.LogException(E, 'unexpected situation');
         end;
      end;
   end;

const
   numFields = 12;
var
   dwVersionSize: DWord;
   hVersion: cardinal;
   pVersionData: Pointer;
   cSize: cardinal;
   pLanguageCodeFirst, pLanguageCodeSelected: PLangAndCodepage;
   s: ansistring;
   {$IFDEF FPC}
   cPointer: cardinal;
   {$ENDIF FPC}
   {$ENDIF MSWindows}
begin
   Result := False;
   {$IFDEF MSWindows}
   dwVersionSize := GetFileVersionInfoSizeA(pansichar(AFilename), hVersion);
   if dwVersionSize > 0 then begin
      try
         GetMem(pVersionBlock, dwVersionSize);
         try
            GetFileVersionInfoA(pansichar(AFilename), hVersion, dwVersionSize, pVersionBlock);
            VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
            pLanguageCodeFirst := pVersionData;
            {$IFDEF FPC}
            cPointer := cardinal(pLanguageCodeFirst);
            Inc(cPointer, (AIndex * (AIndex * SizeOf(TLangAndCodepage))));
            pLanguageCodeSelected := PLangAndCodepage(cPointer);
            {$ELSE FPC}
            pLanguageCodeSelected := Pointer(cardinal(pLanguageCodeFirst) + (AIndex * SizeOf(TLangAndCodepage)));
            {$ENDIF FPC}
            lc := pLanguageCodeSelected^;
            AList.Add(string(GetVerString('FileVersion')));
            AList.Add(string(GetVerString('CompanyName')));
            AList.Add(string(GetVerString('InternalName')));
            AList.Add(string(GetVerString('Comments')));
            AList.Add(string(GetVerString('LegalCopyright')));
            AList.Add(string(GetVerString('LegalTrademarks')));
            AList.Add(string(GetVerString('OriginalFilename')));
            AList.Add(string(GetVerString('ProductName')));
            AList.Add(string(GetVerString('ProductVersion')));
            AList.Add(string(GetVerString('FileDescription')));
            AList.Add(string(GetVerString('PrivateBuild')));
            AList.Add(string(GetVerString('SpecialBuild')));
            // Additional fields need to be handled with extra care, since the
            // original list is the base of the md5[version] sbsd hash.
            s := GetVerString('Translators');
            if Copy(s, Length(s), 1) <> '=' then begin
               AList.Add(string(s));
            end;
            s := GetVerString('Build');
            if Copy(s, Length(s), 1) <> '=' then begin
               AList.Add(string(s));
            end;
            s := GetVerString('Instruction');
            if Copy(s, Length(s), 1) <> '=' then begin
               AList.Add(string(s));
            end;
            Result := True;
         finally
            FreeMem(pVersionBlock, dwVersionSize);
         end;
      except
         Result := False;
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionRes(const AFilename: WideString; const AIndex: cardinal; const AList: TStringList): boolean; overload;
   {$IFDEF MSWindows}
var
   pVersionBlock: Pointer;
   lc: TLangAndCodepage;

   function GetVerString(Name: WideString): WideString;
   var
      ps: Pointer;
      pc: pwidechar;
      size: cardinal;
      sSubBlock: WideString;
   begin
      Result := '';
      ps := nil;
      sSubBlock := '\\StringFileInfo\\' + WordToHexW(lc.wLanguage) + WordToHexW(lc.wCodepage) + '\\' + Name;
      VerQueryValueW(pVersionBlock, pwidechar(sSubBlock), ps, size);
      try
         pc := ps;
         Result := Name + '=' + pc;
         // Dispose(pc);
      except
         on E: Exception do begin
            Result := Name + '=';
            //DebugLogger.LogException(E, 'unexpected situation');
         end;
      end;
   end;

var
   dwVersionSize: DWord;
   hVersion: cardinal;
   pVersionData: Pointer;
   cSize: cardinal;
   pLanguageCodeFirst, pLanguageCodeSelected: PLangAndCodepage;
   s: WideString;
   {$ENDIF MSWindows}
begin
   Result := False;
   {$IFDEF MSWindows}
   dwVersionSize := GetFileVersionInfoSizeW(pwidechar(AFilename), hVersion);
   if dwVersionSize > 0 then begin
      try
         GetMem(pVersionBlock, dwVersionSize);
         try
            GetFileVersionInfoW(pwidechar(AFilename), hVersion, dwVersionSize, pVersionBlock);
            VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
            pLanguageCodeFirst := pVersionData;
            pLanguageCodeSelected := Pointer(cardinal(pLanguageCodeFirst) + (AIndex * SizeOf(TLangAndCodepage)));
            lc := pLanguageCodeSelected^;
            AList.Add(String(GetVerString('FileVersion')));
            AList.Add(String(GetVerString('CompanyName')));
            AList.Add(String(GetVerString('InternalName')));
            AList.Add(String(GetVerString('Comments')));
            AList.Add(String(GetVerString('LegalCopyright')));
            AList.Add(String(GetVerString('LegalTrademarks')));
            AList.Add(String(GetVerString('OriginalFilename')));
            AList.Add(String(GetVerString('ProductName')));
            AList.Add(String(GetVerString('ProductVersion')));
            AList.Add(String(GetVerString('FileDescription')));
            AList.Add(String(GetVerString('PrivateBuild')));
            AList.Add(String(GetVerString('SpecialBuild')));
            // Additional fields need to be handled with extra care, since the
            // original list is the base of the md5[version] sbsd hash.
            s := GetVerString('Translators');
            if Copy(s, Length(s), 1) <> '=' then begin
               AList.Add(String(s));
            end;
            s := GetVerString('Build');
            if Copy(s, Length(s), 1) <> '=' then begin
               AList.Add(String(s));
            end;
            s := GetVerString('Instruction');
            if Copy(s, Length(s), 1) <> '=' then begin
               AList.Add(String(s));
            end;
            Result := True;
         finally
            FreeMem(pVersionBlock, dwVersionSize);
         end;
      except
         Result := False;
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionField(const AFilename, AField: WideString; out Data: WideString): boolean;
   {$IFDEF MSWindows}
var
   dwVersionSize: DWord;
   hVersion: cardinal;
   pVersionBlock, pVersionData: Pointer;
   plc, plc2: PLangAndCodepage;
   lc: TLangAndCodepage;
   i: cardinal;
   psVersionInfo: Pointer;
   pcVersionInfo: pwidechar;
   cSize: cardinal;
   {$ENDIF MSWindows}
begin
   Result := False;
   {$IFDEF MSWindows}
   hVersion := 0;
   try
      dwVersionSize := GetFileVersionInfoSizeW(pwidechar(AFilename), hVersion);
      if dwVersionSize > 0 then begin
         GetMem(pVersionBlock, dwVersionSize);
         try
            if GetFileVersionInfoW(pwidechar(AFilename), hVersion, dwVersionSize, pVersionBlock) then begin
               cSize := 0;
               pVersionData := nil;
               VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
               plc := pVersionData;
               for i := 0 to (cSize div SizeOf(TLangAndCodepage)) - 1 do begin
                  plc2 := Pointer(cardinal(plc) + (i * SizeOf(TLangAndCodepage)));
                  lc := plc2^;
                  psVersionInfo := nil;
                  if VerQueryValueW(pVersionBlock, pwidechar(WideString('\\StringFileInfo\\' + WordToHex(lc.wLanguage) + WordToHex(lc.wCodepage) + '\\') + AField), psVersionInfo, cSize) then begin
                     pcVersionInfo := psVersionInfo;
                     if Length(pcVersionInfo) > 0 then begin
                        Data := pcVersionInfo;
                        Result := True;
                        Break;
                     end;
                  end;
               end;
            end else begin
               //DebugLogger.LogError(GetLastError, 'PepiMK.FileFormats.PE.Versions.GetVersionField.GetFileVersionInfoW');
            end;
         finally
            FreeMem(pVersionBlock);
         end;
      end else begin
         //DebugLogger.LogError(GetLastError, 'PepiMK.FileFormats.PE.Versions.GetVersionField.GetFileVersionInfoSizeW');
      end;
   except
      on E: Exception do begin
         Result := False;
         //DebugLogger.LogException(E, 'unexpected situation?');
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionField(const AFilename: WideString; const AField: TVersionResourceField; var Data: WideString): boolean;
begin
   Result := GetVersionField(AFilename, SVersionResourceFieldNames[AField], Data);
end;

function GetVersionField(const AFilename: WideString; const AField: TVersionResourceField): WideString;
begin
   Result := '';
   if not GetVersionField(AFilename, AField, Result) then begin
      Result := '';
   end;
end;

function GetVersionDetailsA(const AFilename: ansistring; const AIndex: cardinal; var ALanguage, ACodePage: word; var ALanguageName: ansistring): boolean;
   {$IFDEF MSWindows}
var
   dwVersionSize: DWord;
   hVersion: cardinal;
   pVersionBlock, pVersionData: Pointer;
   cSize: cardinal;
   pLanguageCodeFirst, pLanguageCodeSelected: PLangAndCodepage;
   lc: TLangAndCodepage;
   pcLanguageName: pansichar;
   {$IFDEF FPC}
   cPointer: cardinal;
   {$ENDIF FPC}
   {$ENDIF MSWindows}
begin
   Result := False;
   {$IFDEF MSWindows}
   hVersion := 0;
   dwVersionSize := GetFileVersionInfoSizeA(pansichar(AFilename), hVersion);
   if dwVersionSize > 0 then begin
      try
         GetMem(pVersionBlock, dwVersionSize);
         try
            GetFileVersionInfoA(pansichar(AFilename), hVersion, dwVersionSize, pVersionBlock);
            cSize := 0;
            pVersionData := nil;
            VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
            pLanguageCodeFirst := pVersionData;
            {$IFDEF FPC}
            cPointer := cardinal(pLanguageCodeFirst);
            Inc(cPointer, (AIndex * (AIndex * SizeOf(TLangAndCodepage))));
            pLanguageCodeSelected := PLangAndCodepage(cPointer);
            {$ELSE FPC}
            pLanguageCodeSelected := Pointer(cardinal(pLanguageCodeFirst) + (AIndex * SizeOf(TLangAndCodepage)));
            {$ENDIF FPC}
            lc := pLanguageCodeSelected^;
            ALanguage := lc.wLanguage;
            ACodePage := lc.wCodepage;
            GetMem(pcLanguageName, 1024);
            try
               VerLanguageNameA(lc.wLanguage, pcLanguageName, 1023);
               ALanguageName := pcLanguageName;
               Result := True;
            finally
               FreeMem(pcLanguageName, 1024);
            end;
         finally
            FreeMem(pVersionBlock, dwVersionSize);
         end;
      except
         on E: Exception do begin
            Result := False;
            //DebugLogger.LogException(E, 'unexpected situation');
         end;
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionDetails(const AFilename: WideString; const AIndex: cardinal; var ALanguage, ACodePage: word; var ALanguageName: WideString): boolean;
   {$IFDEF MSWindows}
var
   dwVersionSize: DWord;
   hVersion: cardinal;
   pVersionBlock, pVersionData: Pointer;
   cSize: cardinal;
   pLanguageCodeFirst, pLanguageCodeSelected: PLangAndCodepage;
   lc: TLangAndCodepage;
   pcLanguageName: pwidechar;
   {$IFDEF FPC}
   cPointer: cardinal;
   {$ENDIF FPC}
   {$ENDIF MSWindows}
begin
   Result := False;
   {$IFDEF MSWindows}
   hVersion := 0;
   dwVersionSize := GetFileVersionInfoSizeW(pwidechar(AFilename), hVersion);
   if dwVersionSize > 0 then begin
      try
         GetMem(pVersionBlock, dwVersionSize);
         try
            GetFileVersionInfoW(pwidechar(AFilename), hVersion, dwVersionSize, pVersionBlock);
            cSize := 0;
            pVersionData := nil;
            VerQueryValue(pVersionBlock, '\\VarFileInfo\\Translation', pVersionData, cSize);
            pLanguageCodeFirst := pVersionData;
            {$IFDEF FPC}
            cPointer := cardinal(pLanguageCodeFirst);
            Inc(cPointer, (AIndex * (AIndex * SizeOf(TLangAndCodepage))));
            pLanguageCodeSelected := PLangAndCodepage(cPointer);
            {$ELSE FPC}
            pLanguageCodeSelected := Pointer(cardinal(pLanguageCodeFirst) + (AIndex * SizeOf(TLangAndCodepage)));
            {$ENDIF FPC}
            lc := pLanguageCodeSelected^;
            ALanguage := lc.wLanguage;
            ACodePage := lc.wCodepage;
            GetMem(pcLanguageName, 1024);
            try
               VerLanguageNameW(lc.wLanguage, pcLanguageName, 1023);
               ALanguageName := pcLanguageName;
               Result := True;
            finally
               FreeMem(pcLanguageName, 1024);
            end;
         finally
            FreeMem(pVersionBlock, dwVersionSize);
         end;
      except
         on E: Exception do begin
            Result := False;
            //DebugLogger.LogException(E, 'unexpected situation');
         end;
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetVersionCompany(const AFilename: WideString; out ACompanyName: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'CompanyName', ACompanyName);
end;

function GetVersionCompany(const AFilename: WideString): WideString; overload;
begin
   if not GetVersionCompany(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionProduct(const AFilename: WideString; out AProductName: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'ProductName', AProductName);
end;

function GetVersionProduct(const AFilename: WideString): WideString; overload;
begin
   if not GetVersionProduct(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionCopyright(const AFilename: WideString; out ACopyright: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'LegalCopyright', ACopyright);
end;

function GetVersionCopyright(const AFilename: WideString): WideString; overload;
begin
   if not GetVersionCopyright(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionComments(const AFilename: WideString; out AComments: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'Comments', AComments);
end;

function GetVersionComments(const AFilename: WideString): WideString; overload;
begin
   if not GetVersionComments(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionDescription(const AFilename: WideString; out ADescription: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'FileDescription', ADescription);
end;

function GetVersionDescription(const AFilename: WideString): WideString;
begin
   if not GetVersionDescription(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionProductVersion(const AFilename: WideString; out AProductVersion: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'ProductVersion', AProductVersion);
end;

function GetVersionProductVersion(const AFilename: WideString): WideString;
begin
   if not GetVersionProductVersion(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionFileVersion(const AFilename: WideString; out AFileVersion: WideString): boolean; overload;
begin
   Result := GetVersionField(AFilename, 'FileVersion', AFileVersion);
end;

function GetVersionFileVersion(const AFilename: WideString): WideString; overload;
begin
   if not GetVersionFileVersion(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetVersionCustomBuild(const AFilename: WideString; out ACustomBuild: WideString): boolean;
begin
   Result := GetVersionField(AFilename, 'Build', ACustomBuild);
end;

function GetVersionCustomBuild(const AFilename: WideString): WideString; overload;
begin
   if not GetVersionCustomBuild(AFilename, Result) then begin
      Result := '';
   end;
end;

function GetAppVersion(const AFilename: ansistring; out AVersion: int64): ansistring;
var
   dwSize1: DWord;
   dwSize2: DWord;
   pBlock: Pointer;
   pBuffer: Pointer;
begin
   Result := '';
   AVersion := 0;
   {$IFDEF MSWindows}
   dwSize2 := 0;
   dwSize1 := GetFileVersionInfoSizeA(pansichar(AFilename), dwSize2);
   if dwSize1 > 0 then begin
      GetMem(pBlock, dwSize1);
      try
         GetFileVersionInfoA(pansichar(AFilename), 0, dwSize1, pBlock);
         pBuffer := nil;
         VerQueryValue(pBlock, '\', pBuffer, dwSize2);
         with TVSFixedFileInfo(pBuffer^) do begin
            Result := IntToStrA(HiWord(dwFileVersionMS)) + '.' + IntToStrA(LoWord(dwFileVersionMS)) + '.' + IntToStrA(HiWord(dwFileVersionLS)) + '.' + IntToStrA(LoWord(dwFileVersionLS));
            AVersion := int64(dwFileVersionMS) shl 32 + int64(dwFileVersionLS);
         end;
      finally
         FreeMem(pBlock);
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetAppVersion(const AFilename: ansistring): ansistring; overload;
var
   i: int64;
begin
   i := 0;
   Result := GetAppVersion(AFilename, i);
end;

function GetAppVersion(const AFilename: WideString; out AVersion: int64): WideString;
var
   dwSize1: DWord;
   dwSize2: DWord;
   pBlock: Pointer;
   pBuffer: Pointer;
begin
   Result := '';
   AVersion := 0;
   {$IFDEF MSWindows}
   dwSize2 := 0;
   dwSize1 := GetFileVersionInfoSizeW(pwidechar(AFilename), dwSize2);
   if dwSize1 > 0 then begin
      GetMem(pBlock, dwSize1);
      try
         GetFileVersionInfoW(pwidechar(AFilename), 0, dwSize1, pBlock);
         pBuffer := nil;
         VerQueryValue(pBlock, '\', pBuffer, dwSize2);
         with TVSFixedFileInfo(pBuffer^) do begin
            Result := IntToStrW(HiWord(dwFileVersionMS)) + '.' + IntToStrW(LoWord(dwFileVersionMS)) + '.' + IntToStrW(HiWord(dwFileVersionLS)) + '.' + IntToStrW(LoWord(dwFileVersionLS));
            AVersion := int64(dwFileVersionMS) shl 32 + int64(dwFileVersionLS);
         end;
      finally
         FreeMem(pBlock);
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetAppVersion(const AFilename: WideString): WideString; overload;
var
   i: int64;
begin
   i := 0;
   Result := GetAppVersion(AFilename, i);
end;

function GetAppVersionWords(const AFilename: string; out AMajor, AMinor, ARelease, ABuild: word): boolean;
var
   dwSize1: DWord
   {$IFDEF FPC}
   = 0
   {$ENDIF FPC}
   ;
   dwSize2: DWord
   {$IFDEF FPC}
   = 0
   {$ENDIF FPC}
   ;
   pBlock: Pointer
   {$IFDEF FPC}
   = nil
   {$ENDIF FPC}
   ;
   pBuffer: Pointer
   {$IFDEF FPC}
   = nil
   {$ENDIF FPC}
   ;
begin
   Result := False;
   {$IFDEF MSWindows}
   {$IFDEF Unicode}
   dwSize1 := GetFileVersionInfoSizeW(pwidechar(WideString(AFilename)), dwSize2);
   {$ELSE Unicode}
   dwSize1 := GetFileVersionInfoSizeA(pansichar(ansistring(AFilename)), dwSize2);
   {$ENDIF Unicode}
   if dwSize1 > 0 then begin
      GetMem(pBlock, dwSize1);
      try
         {$IFDEF Unicode}
         GetFileVersionInfoW(pwidechar(WideString(AFilename)), 0, dwSize1, pBlock);
         {$ELSE Unicode}
         GetFileVersionInfoA(pansichar(ansistring(AFilename)), 0, dwSize1, pBlock);
         {$ENDIF Unicode}
         VerQueryValue(pBlock, '\', pBuffer, dwSize2);
         with TVSFixedFileInfo(pBuffer^) do begin
            AMajor := HiWord(dwFileVersionMS);
            AMinor := LoWord(dwFileVersionMS);
            ARelease := HiWord(dwFileVersionLS);
            ABuild := LoWord(dwFileVersionLS);
            Result := True;
         end;
      finally
         FreeMem(pBlock);
      end;
   end;
   {$ENDIF MSWindows}
end;

function GetAppVersionNumber(const AVersion: string): int64;
var
   aiVersionPart: array [0 .. 3] of int64;
   iPos: integer;
   sVersion: string;
begin
   sVersion := AVersion;
   iPos := Pos(',', sVersion);
   while iPos > 0 do begin
      sVersion[iPos] := '.';
      iPos := Pos(',', sVersion);
   end;
   iPos := Pos(' ', sVersion);
   while iPos > 0 do begin
      System.Delete(sVersion, iPos, 1);
      iPos := Pos(' ', sVersion);
   end;
   aiVersionPart[0] := 0;
   aiVersionPart[1] := 0;
   aiVersionPart[2] := 0;
   aiVersionPart[3] := 0;
   iPos := Pos('.', sVersion);
   if iPos > 0 then begin // digit 0, major
      aiVersionPart[0] := StrToIntDef(Copy(sVersion, 1, iPos - 1), 0);
      System.Delete(sVersion, 1, iPos);
      iPos := Pos('.', sVersion);
      if iPos > 0 then begin // digit 1, minor
         aiVersionPart[1] := StrToIntDef(Copy(sVersion, 1, iPos - 1), 0);
         System.Delete(sVersion, 1, iPos);
         iPos := Pos('.', sVersion);
         if iPos > 0 then begin // digit 2, more minor
            aiVersionPart[2] := StrToIntDef(Copy(sVersion, 1, iPos - 1), 0);
            System.Delete(sVersion, 1, iPos);
            aiVersionPart[3] := StrToIntDef(sVersion, 0);
         end;
      end;
   end;
   Result := (aiVersionPart[0] shl 48) or (aiVersionPart[1] shl 32) or (aiVersionPart[2] shl 16) or (aiVersionPart[3]);
end;

function GetAppVersionStr(const AVersion: int64): string;
begin
   {$IFDEF MSWindows}
   Result := IntToStr(HiWord(cardinal(AVersion shr 32))) + '.' + IntToStr(LoWord(cardinal(AVersion shr 32))) + '.' + IntToStr(HiWord(cardinal(AVersion))) + '.' + IntToStr(LoWord(cardinal(AVersion)));
   {$ELSE MSWindows}
   Result := '';
   {$ENDIF MSWindows}
end;

{$IFDEF MSWindows}
function GetAppVersionStr(const AVersion: TULargeInteger): string;
begin
   {$IFDEF FPC}
   Result := IntToStr(ULARGE_INTEGER(AVersion).HighPart shr 16) + '.' + IntToStr(ULARGE_INTEGER(AVersion).HighPart and $FFFF) + '.' +
      IntToStr(ULARGE_INTEGER(AVersion).LowPart shr 16) + '.' + IntToStr(ULARGE_INTEGER(AVersion).LowPart and $FFFF);
   {$ELSE FPC}
   Result := IntToStr(AVersion.HighPart shr 16) + '.' + IntToStr(AVersion.HighPart and $FFFF) + '.' + IntToStr(AVersion.LowPart shr 16) + '.' + IntToStr(AVersion.LowPart and $FFFF);
   {$ENDIF FPC}
end;

{$ENDIF MSWindows}

end.
