{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Handling of streams and stream reading/writing.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-04-03  pk   5m  Added ReadUInt64
// 2016-08-31  pk  10m  Removed compiler warnings in Lazarus 1.6 / FPC 3.0.0
// 2016-05-04  pk  20m  Fixed access rights in FileCreate
// 2016-05-04  pk  10m  Renamed TpkFileStream to TFileHandleStream
// 2010-10-18  ##   --  Beautified code (using Code Beautifier Wizard)
// 2010-04-29  pk  10m  Added ByteLen string reader
// 2009-09-25  pk   --  Added this header (from Code Test Browser)
// *****************************************************************************
   )
}

unit Firefly.Streams;

{$ifdef FPC}
{$mode objfpc}{$H+}
{$endif FPC}

interface

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Classes;

type
   {
     @abstract TFileHandleStream allows to use a file stream on a file you have an existing handle for.
   }

   TFileHandleStream = class(THandleStream)
   strict private
      FFileName: string; //< Internal storage to access name of file the handle is for.
   public
      {
        Constructor.

        @param(AFilename is the name of file to access.)
        @param(AMode defines the mode to use for opening file.)
      }
      constructor Create(const AFileName: string; AMode: Word); overload;
      {
        Constructor.

        @param(AFilename  is the name of file to access.)
        @param(AMode      defines the ode to use for opening file.)
        @param(ARights    defines sharing rights.)
      }
      constructor Create(const AFileName: string; AMode: Word; ARights: Cardinal); overload;
      destructor Destroy; override; //< Destructor
      property FileName: string read FFileName; //< Property exposes filename specified while creating the class.
   end;

   {
     @abstract TFileStreamReadWriteTypesHelper enhances streams with functions to simplify
     reading and writing of standard situations like strings, or values with a
     prefixed ID.
   }

   { TFileStreamReadWriteTypesHelper }

   TFileStreamReadWriteTypesHelper = class helper for TStream
   private
   public
      {
        Writes a Word to a stream.

        @param(AData takes the value to write to the stream.)
     }
      procedure WriteWord(AData: Word);
      {
        Reads a dword from the stream.

        @returns(Value read from stream.)
      }
      function ReadDWord: DWord;
      function ReadUInt64: UInt64;
      {
        Reads a longint from the stream.

        @returns(Value read from stream.)
      }
      function ReadLongInt: LongInt;
      {
        Writes a LongInt to the stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteLongInt(AData: LongInt);
      {
        Writes a DWord to the stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteDWord(AData: DWord);
      {
        Writes a DWord including a datatype to the stream.

        @param(AData takes the value to write to the stream.)
        @param(ADataType is a value defining the type of the data.)
      }
      procedure WriteTypedDWord(AData: DWord; ADataType: Word); overload;
      {
        Writes a DWord including a datatype to the stream.

        @param(AData takes the value to write to the stream.)
        @param(ADataType is a value defining the type of the data.)
      }
      procedure WriteTypedDWord(AData, ADataType: DWord); overload;
      {
        Reads a zero terminated ansistring from the stream.

        @returns(Value read from stream.)
      }
      function ReadZeroTerminatedAnsiString: AnsiString;
      {
        Reads a zero terminated widestring from the stream.

        @returns(Value read from stream.)
      }
      function ReadZeroTerminatedWideString: WideString;
      {
        Reads a UTF-8 string with dword-size length from the stream.

        @returns(Value read from stream.)
      }
      function ReadUTF8WideString: WideString;
      {
        Reads a UTF-8 string with dword-size swapped length from the stream.

        @returns(Value read from stream.)
      }
      function ReadUTF8WideStringLongWordLenSwapped: WideString;
      {
        Reads a widestring with dword-size length from the stream.

        @returns(Value read from stream.)
      }
      function ReadWideString: WideString;
      {
        Reads a widestring with word-size length from the stream.

        @returns(Value read from stream.)
      }
      function ReadWideStringWordLen: WideString;
      {
        Reads a UTF-8 string with word-size length from the stream.

        @returns(Value read from stream.)
      }
      function ReadUTF8WideStringWordLen: WideString;
      {
        Reads a UTF-8 string with word-size swapped length from the stream.

        @returns(Value read from stream.)
      }
      function ReadUTF8WideStringWordLenSwapped: WideString;
      {
        Reads a ansistring with word-sized length from the stream.

        @returns(Value read from stream.)
      }
      function ReadAnsiStringWordLen: AnsiString;
      {
        Reads a ansistring with byte-sized length from the stream.

        @returns(Value read from stream.)
      }
      function ReadAnsiStringByteLen: AnsiString;
      {
        Writes a UTF-8 string to a stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteUTF8String(const AData: UTF8String);
      {
        Writes a UTF-8 string including a datatype to the stream.

        @param(AData takes the value to write to the stream.)
        @param(ADataType is a value defining the type of the data.)
      }
      procedure WriteTypedUTF8String(const AData: UTF8String; ADataType: Word); overload;
      {
        Writes a UTF-8 string including a datatype to the stream.

        @param(AData takes the value to write to the stream.)
        @param(ADataType is a value defining the type of the data.)
      }
      procedure WriteTypedUTF8String(const AData: UTF8String; ADataType: DWord); overload;
      {
        Writes a raw byte string to the stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteRawByteString(const AData: RawByteString);
      {
        Writes an AnsiSring with dword-length size to the stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteAnsiString(const AData: AnsiString);
      {
        Writes an AnsiSring with word-length size to the stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteAnsiStringWordLen(const AData: AnsiString);
      {
        Writes a WideString to a stream

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteWideString(const AData: WideString);
      {
        Reads all content into an ansistring.

        @param  AText  Variable to read data into
      }
      procedure ReadAllToAnsiString(var AText: AnsiString);
      {
        Reads a ansistring stringlist from the stream.

        @return  Value
      }
      procedure ReadAnsiToStringList(const AList: TStrings);
      {
        Reads a TDateTime from the stream.

        @returns(Value read from stream.)
      }
      function ReadDateTime: TDateTime;
      {
        Writes a TDateTime to the stream.

        @param(AData takes the value to write to the stream.)
      }
      procedure WriteDateTime(ADate: TDateTime);
      {
        Writes a TDateTime including a datatype to the stream.

        @param(AData takes the value to write to the stream.)
        @param(ADataType is a value defining the type of the data.)
      }
      procedure WriteTypedDateTime(ADate: TDateTime; ADataType: Word); overload;
      {
        Writes a TDateTime including a datatype to the stream.

        @param(AData takes the value to write to the stream.)
        @param(ADataType is a value defining the type of the data.)
      }
      procedure WriteTypedDateTime(ADate: TDateTime; ADataType: DWord); overload;
   end;

{
  Opens a file honoring the FILE_SHARE_DELETE attribute.

  @param(AFilename is the name of file to open.)
  @param(AMode defines file access and share modes.)
  @returns(File Handle.)
}
function FileOpen(const AFileName: string; AMode: LongWord): Integer;

{
FileCreate creates a new file.

  @param(AFilename is the name of the new file.)
  @returns(File Handle.)
}
function FileCreate(const AFileName: string): Integer; overload;

{
  FileCreate creates a new file.

  @param(AFilename is the name of the new file.)
  @param(ARights defines the rights to use for accessing file.)
  @returns(File Handle.)
}
function FileCreate(const AFileName: string; {%H-}ARights: Integer): Integer; overload;

{
  Copies data from a file handle to a stream.

  @param(AFileHandle is a handle of file to read from.)
  @param(AStream is the stream to write to.)
  @param(APosition is the position to start reading from.)
  @param(ACount is the number of bytes to read.)
  @returns(True if successful.)
}
function CopyDataFromHandleToStream(AFileHandle: THandle; AStream: TStream; APosition, ACount: Integer): boolean;

{
  Writes a boolean to a stream.

  @param(AStream is the stream to write to.)
  @param(AToggle is the value to write.)
}
procedure pkWriteBooleanDataToStream(AStream: TStream; AToggle: boolean);

{
  Writes a byte to a stream.

  @param(AStream is the stream to write to.)
  @param(AByte is the value to write.)
}
procedure pkWriteByteDataToStream(AStream: TStream; AByte: Byte);

{
  Writes a dword-sized integer to a stream.

  @param(AStream is the stream to read from.)
  @param(AValue is the value to write.)
}
procedure pkWriteIntegerDataToStream(AStream: TStream; AValue: Integer);

{
  Writes a dword-sized length string to a stream.

  @param(AStream is the stream to write to.)
  @param(AText is the value to write.)
  @seealso(TFileStreamReadWriteTypesHelper.WriteRawByteString)
}
procedure pkWriteStringDataToStream(AStream: TStream; AText: RawByteString);

{
  Reads a boolean from the stream.

  @param(AStream is the stream to read from.)
}
function pkReadBooleanDataFromStream(AStream: TStream): boolean;

{
  Reads a byte from the stream.

  @param(AStream is thetream to read from.)
  @returns(Data.)
}
function pkReadByteDataFromStream(AStream: TStream): Byte;

{
  Loads sized length integer from stream.

  @param(AStream is the stream to read from.)
  @returns(Data.)
}
function pkReadIntegerDataFromStream(AStream: TStream): Integer;

{
  Loads dword-sized length ansistring from stream.

  @param(AStream is the stream to read from.)
  @return(Data.)
}
function pkReadStringDataFromStream(AStream: TStream): RawByteString;

implementation

uses
   RTLConsts,
   Firefly.Numbers,
   SysUtils;

{$IFNDEF MSWindows}
const
    INVALID_HANDLE_VALUE = THandle(-1);
{$ENDIF MSWindows}

    {
  Tests whether the system is NT-based.

  @returns(True if NT system.)
}
function NTBased: boolean; inline;
begin
   {$IFDEF MSWindows}
   Result := (Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion >= 4)
   {$ELSE MSWindows}
   Result := false;
   {$ENDIF MSWindows}
end;

function FileOpen(const AFileName: string; AMode: LongWord): Integer;
{$IFDEF MSWindows}
const
   AccessMode: array [0 .. 2] of LongWord = (GENERIC_READ, GENERIC_WRITE, GENERIC_READ or GENERIC_WRITE);
   ShareMode: array [0 .. 4] of LongWord  = (0, 0, FILE_SHARE_READ, FILE_SHARE_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE or FILE_SHARE_DELETE);
{$ENDIF MSWindows}
var
   lwShareMode: LongWord;
begin
   (*
     fmOpenRead       = $0000;
     fmOpenWrite      = $0001;
     fmOpenReadWrite  = $0002;

     fmShareCompat    = $0000 platform; // DOS compatibility mode is not portable
     fmShareExclusive = $0010;
     fmShareDenyWrite = $0020;
     fmShareDenyRead  = $0030 platform; // write-only not supported on all platforms
     fmShareDenyNone  = $0040;
   *)
   {$IFDEF MSWindows}
   Result := -1;
   if ((AMode and 3) <= fmOpenReadWrite) and ((AMode and $F0) <= fmShareDenyNone) then begin
      lwShareMode := ShareMode[(AMode and $F0) shr 4];
      if (not NTBased) and ((lwShareMode and FILE_SHARE_DELETE) > 0) then begin
         lwShareMode := lwShareMode and (not FILE_SHARE_DELETE);
      end;
      Result := Integer(CreateFile(PChar(AFileName), // lpFileName
            AccessMode[AMode and 3],                 // dwDesiredAccess
            lwShareMode,                            // dwShareMode
            nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0));
   end;
   {$ELSE MSWindows}
   Result := SysUtils.FileOpen(AFilename, AMode);
   {$ENDIF MSWindows}
end;

function FileCreate(const AFileName: string): Integer;
begin
   {$IFDEF MSWindows}
   Result := Integer(CreateFile(PChar(AFileName), GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE or FILE_SHARE_DELETE, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0));
   {$ELSE MSWindows}
   // Result := PepiMK.Files.Streams.FileCreate(AFileName, FileAccessRights);
   Result := SysUtils.FileCreate(AFilename);
   {$ENDIF MSWindows}
end;

function FileCreate(const AFileName: string; ARights: Integer): Integer;
begin
   {$IFDEF MSWindows}
   Result := Firefly.Streams.FileCreate(AFileName);
   {$ELSE MSWindows}
   //Result := Integer(open(PChar(AFileName), O_RDWR or O_CREAT or O_TRUNC, ARights));
   Result := SysUtils.FileCreate(AFilename, ARights);
   {$ENDIF MSWindows}
end;

{ TFileHandleStream }

constructor TFileHandleStream.Create(const AFileName: string; AMode: Word);
begin
   {$IFDEF MSWindows}
   Create(AFileName, AMode, 0);
   {$ELSE MSWindows}
   Create(AFileName, AMode, AMode);
   {$ENDIF MSWindows}
end;

constructor TFileHandleStream.Create(const AFileName: string; AMode: Word; ARights: Cardinal);
begin
   if (fmCreate and AMode) = fmCreate then begin
      inherited Create(Firefly.Streams.FileCreate(AFileName, ARights));
      if Handle = INVALID_HANDLE_VALUE then begin
         {$IFDEF MSWindows}
         raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [ExpandFileName(AFileName), SysErrorMessage(GetLastError)]);
         {$ELSE MSWindows}
         raise EFCreateError.CreateResFmt(@SFCreateErrorEx, [ExpandFileName(AFileName), '']);
         {$ENDIF MSWindows}
      end;
   end else begin
      inherited Create(Firefly.Streams.FileOpen(AFileName, AMode));
      if Handle = INVALID_HANDLE_VALUE then begin
         {$IFDEF MSWindows}
         raise EFOpenError.CreateResFmt(@SFOpenErrorEx, [ExpandFileName(AFileName), SysErrorMessage(GetLastError)]);
         {$ELSE MSWindows}
         raise EFOpenError.CreateResFmt(@SFOpenErrorEx, [ExpandFileName(AFileName), '']);
         {$ENDIF MSWindows}
      end;
   end;
   FFileName := AFileName;
end;

destructor TFileHandleStream.Destroy;
begin
   if Handle <> INVALID_HANDLE_VALUE then begin
      FileClose(Handle);
   end;
   inherited Destroy;
end;

function CopyDataFromHandleToStream(AFileHandle: THandle; AStream: TStream; APosition, ACount: Integer): boolean;
const
   blockSize = 4096;
var
   readCount, blockCount: Integer;
   buf: array [0 .. blockSize - 1] of Byte;
begin
   try
      FileSeek(AFileHandle, APosition, soFromBeginning);
      readCount := 0;
      blockCount := blockSize;
      while (readCount < ACount) and (blockCount > 0) do begin
         blockCount := blockSize;
         if ACount - readCount < blockCount then begin
            blockCount := ACount - readCount;
         end;
         blockCount := FileRead(AFileHandle, buf, blockCount);
         AStream.Write(buf[0], blockCount);
         Inc(readCount, blockCount);
      end;
      Result := true;
   except
      Result := false;
   end;
end;

procedure pkWriteBooleanDataToStream(AStream: TStream; AToggle: boolean);
begin
   AStream.Write(Byte(AToggle), 1);
end;

procedure pkWriteByteDataToStream(AStream: TStream; AByte: Byte);
begin
   AStream.Write(AByte, 1);
end;

procedure pkWriteIntegerDataToStream(AStream: TStream; AValue: Integer);
var
   c: Cardinal;
begin
   c := SizeOf(Integer);
   AStream.Write(c, 4);
   AStream.Write(AValue, c);
end;

procedure pkWriteStringDataToStream(AStream: TStream; AText: RawByteString);
var
   c: Cardinal;
begin
   c := Length(AText);
   AStream.WriteBuffer(c, 4);
   AStream.WriteBuffer(AText[1], c);
end;

function pkReadBooleanDataFromStream(AStream: TStream): boolean;
begin
   Result := false;
   AStream.Read(Result, 1);
end;

function pkReadByteDataFromStream(AStream: TStream): Byte;
begin
   Result := 0;
   AStream.Read(Result, 1);
end;

function pkReadIntegerDataFromStream(AStream: TStream): Integer;
var
   c: Cardinal;
begin
   c := 0;
   Result := 0;
   AStream.Read(c, 4);
   AStream.Read(Result, c);
end;

function pkReadStringDataFromStream(AStream: TStream): RawByteString;
var
   c: Cardinal;
begin
   c := 0;
   AStream.Read(c, 4);
   SetLength(Result, c);
   AStream.Read(Result[1], c);
end;

{ TFileStreamReadWriteTypesHelper }

procedure TFileStreamReadWriteTypesHelper.ReadAllToAnsiString(var AText: AnsiString);
var
   iSize: Integer;
begin
   iSize := Self.Size - Self.Position;
   // SetLength(Text, iSize);
   // Self.Read(Text[1], iSize);
   System.SetString(AText, nil, iSize);
   Self.Read(PAnsiChar(AText)^, iSize);
end;

function TFileStreamReadWriteTypesHelper.ReadAnsiStringByteLen: AnsiString;
var
   bLen: Byte;
begin
   bLen := 0;
   Self.Read(bLen, 1);
   SetLength(Result, bLen);
   Self.Read(Result[1], bLen);
end;

function TFileStreamReadWriteTypesHelper.ReadAnsiStringWordLen: AnsiString;
var
   wLen: Word;
begin
   wLen := 0;
   Self.Read(wLen, 2);
   SetLength(Result, wLen);
   Self.Read(Result[1], wLen);
end;

procedure TFileStreamReadWriteTypesHelper.ReadAnsiToStringList(const AList: TStrings);
var
   c: AnsiChar;
   sCache: AnsiString;
   iCurrentPos: Integer;
   iCacheLen: Integer;
begin
   // go through whole stream...
   iCacheLen := 100;
   c := #0;
   SetLength(sCache, iCacheLen);
   iCurrentPos := 0;
   while Self.Position < Self.Size do begin
      Self.Read(c, 1);
      if c = #13 then begin
         // on every #13, start new entry
         AList.Add(string(Copy(sCache, 1, iCurrentPos)));
         iCurrentPos := 0;
      end
      else if c = #10 then begin
         // skip first #10 after a #13
         (*
           if Length(sCache)>0 then begin
           sCache := sCache + c;
           end;
         *)
      end else begin
         // sCache := sCache + c;
         Inc(iCurrentPos);
         if iCurrentPos >= iCacheLen then begin
            Inc(iCacheLen, 100);
            SetLength(sCache, iCacheLen);
         end;
         sCache[iCurrentPos] := c;
      end;
   end;
   if iCurrentPos > 0 then begin
      // add final item
      AList.Add(string(Copy(sCache, 1, iCurrentPos)));
   end;
end;

function TFileStreamReadWriteTypesHelper.ReadDateTime: TDateTime;
begin
   Result := 0;
   Self.Read(Result, SizeOf(Result));
end;

function TFileStreamReadWriteTypesHelper.ReadDWord: DWord;
begin
   Result := 0;
   Self.Read(Result, 4);
end;

function TFileStreamReadWriteTypesHelper.ReadUInt64: UInt64;
begin
   Result := 8;
   Self.Read(Result, 8);
end;

function TFileStreamReadWriteTypesHelper.ReadLongInt: LongInt;
begin
   Result := 0;
   Self.Read(Result, 4);
end;

function TFileStreamReadWriteTypesHelper.ReadUTF8WideString: WideString;
var
   cLen: Cardinal;
   {$IFDEF VER220}
   S: RawByteString;
   {$ELSE}
   S: string;
   {$ENDIF VER220}
begin
   cLen := 0;
   Self.Read(cLen, 4);
   SetLength(S, cLen);
   Self.Read(S[1], cLen);
   Result := UTF8Decode(S);
end;

function TFileStreamReadWriteTypesHelper.ReadUTF8WideStringLongWordLenSwapped: WideString;
var
   cLen: Cardinal;
   {$IFDEF VER220}
   S: RawByteString;
   {$ELSE}
   S: string;
   {$ENDIF VER220}
begin
   cLen := 0;
   Self.Read(cLen, 4);
   cLen := CardinalSwap(cLen);
   SetLength(S, cLen);
   Self.Read(S[1], cLen);
   Result := UTF8Decode(S);
end;

function TFileStreamReadWriteTypesHelper.ReadUTF8WideStringWordLen: WideString;
var
   wLen: Word;
   S: RawByteString;
begin
   wLen := 0;
   Self.Read(wLen, 2);
   SetLength(S, wLen);
   Self.Read(S[1], wLen);
   Result := UTF8Decode(S);
end;

function TFileStreamReadWriteTypesHelper.ReadUTF8WideStringWordLenSwapped: WideString;
var
   wLen: Word;
   S: RawByteString;
begin
   wLen := 0;
   if Self.Read(wLen, 2) <> 0 then begin // Fixed EOF bug
      wLen := Swap(wLen);
      if wLen = $FFFF then begin
         Result := '';
      end else begin
         SetLength(S, wLen);
         Self.Read(S[1], wLen);
         Result := UTF8Decode(S);
      end;
   end else begin
      SetLength(Result, 0);
   end;
end;

function TFileStreamReadWriteTypesHelper.ReadWideString: WideString;
var
   cLen: Cardinal;
begin
   cLen := 0;
   Result := '';
   if Self.Read(cLen, 4) <> 0 then begin // Fixed EOF bug
      SetLength(Result, cLen);
      Self.Read(Result[1], cLen * 2);
   end else begin
      SetLength(Result, 0);
   end;
end;

function TFileStreamReadWriteTypesHelper.ReadWideStringWordLen: WideString;
var
   wLen: Word;
begin
   wLen := 0;
   Result := '';
   if Self.Read(wLen, 2) <> 0 then begin // Fixed EOF bug
      SetLength(Result, wLen);
      Self.Read(Result[1], wLen * 2);
   end else begin
      SetLength(Result, 0);
   end;
end;

function TFileStreamReadWriteTypesHelper.ReadZeroTerminatedAnsiString: AnsiString;
var
   c: AnsiChar;
begin
   Result := '';
   c := #0;
   if Self.Position < Self.Size then begin
      repeat
         Self.Read(c, 1);
         if c <> #0 then begin
            Result := Result + c;
         end;
      until (c = #0) or (Self.Position >= Self.Size);
   end;
end;

function TFileStreamReadWriteTypesHelper.ReadZeroTerminatedWideString: WideString;
var
   c: WideChar;
begin
   Result := '';
   c := #0;
   if Self.Position < Self.Size then begin
      repeat
         Self.Read(c, 2);
         if c <> #0 then begin
            Result := Result + c;
         end;
      until (c = #0) or (Self.Position >= Self.Size);
   end;
end;

procedure TFileStreamReadWriteTypesHelper.WriteAnsiString(const AData: AnsiString);
var
   dwLen: DWord;
begin
   dwLen := Length(AData);
   Self.WriteDWord(dwLen);
   Self.Write(AData[1], dwLen);
end;

procedure TFileStreamReadWriteTypesHelper.WriteAnsiStringWordLen(const AData: AnsiString);
var
   wLen: Word;
begin
   wLen := Length(AData);
   Self.Write(wLen, 2);
   Self.Write(AData[1], wLen);
end;

procedure TFileStreamReadWriteTypesHelper.WriteDateTime(ADate: TDateTime);
begin
   Self.Write(ADate, SizeOf(ADate));
end;

procedure TFileStreamReadWriteTypesHelper.WriteDWord(AData: DWord);
begin
   Self.Write(AData, 4);
end;

procedure TFileStreamReadWriteTypesHelper.WriteLongInt(AData: LongInt);
begin
   Self.Write(AData, 4);
end;

procedure TFileStreamReadWriteTypesHelper.WriteRawByteString(const AData: RawByteString);
var
   dwLen: DWord;
begin
   dwLen := Length(AData);
   Self.WriteDWord(dwLen);
   Self.Write(AData[1], dwLen);
end;

procedure TFileStreamReadWriteTypesHelper.WriteTypedDWord(AData: DWord; ADataType: Word);
begin
   WriteWord(ADataType);
   WriteDWord(AData);
end;

procedure TFileStreamReadWriteTypesHelper.WriteTypedDateTime(ADate: TDateTime; ADataType: Word);
begin
   WriteWord(ADataType);
   WriteDateTime(ADate);
end;

procedure TFileStreamReadWriteTypesHelper.WriteTypedDateTime(ADate: TDateTime; ADataType: DWord);
begin
   WriteDWord(ADataType);
   WriteDateTime(ADate);
end;

procedure TFileStreamReadWriteTypesHelper.WriteTypedDWord(AData, ADataType: DWord);
begin
   WriteDWord(ADataType);
   WriteDWord(AData);
end;

procedure TFileStreamReadWriteTypesHelper.WriteTypedUTF8String(const AData: UTF8String; ADataType: Word);
begin
   WriteWord(ADataType);
   WriteUTF8String(AData);
end;

procedure TFileStreamReadWriteTypesHelper.WriteTypedUTF8String(const AData: UTF8String; ADataType: DWord);
begin
   WriteDWord(ADataType);
   WriteUTF8String(AData);
end;

procedure TFileStreamReadWriteTypesHelper.WriteUTF8String(const AData: UTF8String);
begin
   WriteRawByteString(RawByteString(AData));
end;

procedure TFileStreamReadWriteTypesHelper.WriteWideString(const AData: WideString);
var
   cLen: Cardinal;
   pData: PWideChar;
begin
   // Fixed memory related freeze
   cLen := Length(AData);
   pData := PWideChar(AData);
   Self.Write(cLen, 4);
   Self.Write(pData^, cLen * 2);
end;

procedure TFileStreamReadWriteTypesHelper.WriteWord(AData: Word);
begin
   Self.Write(AData, 2);
end;

initialization
   // http://bugs.freepascal.org/view.php?id=8898
   {$IFDEF FPC}
   {$IFDEF MSWindows}
   if (Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion >=4) then begin
      fmShareDenyNoneFlags := FILE_SHARE_READ or FILE_SHARE_WRITE or FILE_SHARE_DELETE;
   end;
   {$ENDIF MSWindows}
   {$ENDIF FPC}
end.
