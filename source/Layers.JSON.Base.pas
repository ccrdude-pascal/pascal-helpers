{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Defines abstract JSON access.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Wrote interfaces necessary to read threatLists
// *****************************************************************************
   )
}

unit Layers.JSON.Base;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch prefixedattributes}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   TypInfo;

type

   { AJSONObjectProperty }

   AJSONObjectProperty = class(TCustomAttribute)
   private
      FKeyName: string;
   public
      constructor Create(AKeyName: string);
      property KeyName: string read FKeyName;
   end;

   LayeredJSONParsingException = class(Exception);

   TLayeredJSONObject = class;
   TLayeredJSONObjectClass = class of TLayeredJSONObject;
   TLayeredJSONArray = class;
   TLayeredJSONArrayClass = class of TLayeredJSONArray;

   TLayeredJSONForEachObjectEvent = procedure(TheElementName: string; TheElement: TLayeredJSONObject; AnUserData: pointer; var Continue: boolean) of object;

   { TLayeredJSONArray }

   TLayeredJSONArray = class
   protected
      function GetCount: integer; virtual; abstract;
      function GetObjects(Index: integer): TLayeredJSONObject; virtual; abstract;
   protected
      class var DefaultClass: TLayeredJSONArrayClass;
   public
      class constructor Create;
      class function CreateInstance(ACreateObject: boolean): TLayeredJSONArray;
      class procedure SetDefaultClass(ADefaultClass: TLayeredJSONArrayClass);
   public
      constructor Create(ACreateObject: boolean); virtual; abstract;
      procedure FromStream(const AStream: TMemoryStream); virtual; abstract;
      procedure AddString(AValue: string); virtual; abstract;
      procedure AddObject(AnObject: TLayeredJSONObject); virtual; abstract;
      procedure ForEachObject(ACallback: TLayeredJSONForEachObjectEvent; AnUserData: pointer); virtual; abstract;
      procedure GetStrings(AList: TStrings); virtual; abstract;
      property Count: integer read GetCount;
      property Objects[Index: integer]: TLayeredJSONObject read GetObjects;
   end;

   { TLayeredJSONObject }

   TLayeredJSONObject = class
   protected
      FOwnsObject: boolean;
   protected
      class var DefaultClass: TLayeredJSONObjectClass;
   public
      class constructor Create;
      class function CreateInstance(ACreateObject: boolean): TLayeredJSONObject;
      class procedure SetDefaultClass(ADefaultClass: TLayeredJSONObjectClass);
   public
      constructor Create(ACreateObject: boolean); virtual; abstract;
      procedure FreeObject; virtual; abstract;
      procedure ToStream(const AStream: TMemoryStream; ACompact: boolean); virtual; abstract;
      procedure FromStream(const AStream: TMemoryStream); virtual; abstract;
      function FindArray(AKey: string; out AnArray: TLayeredJSONArray): boolean; virtual; abstract;
      function FindBoolean(AKey: string; out AValue: boolean): boolean; virtual; abstract;
      function FindInt64(AKey: string; out AValue: int64): boolean; virtual; abstract;
      function FindInteger(AKey: string; out AValue: integer): boolean; virtual; abstract;
      function FindObject(AKey: string; out AnArray: TLayeredJSONObject): boolean; virtual; abstract;
      function FindString(AKey: string; out AString: string): boolean; virtual; abstract;
      procedure AddArray(AKey: string; AnArray: TLayeredJSONArray); virtual; abstract;
      procedure AddBoolean(AKey: string; AValue: boolean); virtual; abstract;
      procedure AddInt64(AKey: string; AValue: Int64); virtual; abstract;
      procedure AddObject(AKey: string; AnObject: TLayeredJSONObject); virtual; abstract;
      procedure AddString(AKey, AValue: string); virtual; abstract;
      function ToObject(const AnObject: TObject): boolean;
   end;

implementation

{ TLayeredJSONArray }

class constructor TLayeredJSONArray.Create;
begin
   DefaultClass := nil;
end;

class function TLayeredJSONArray.CreateInstance(ACreateObject: boolean): TLayeredJSONArray;
begin
   if Assigned(DefaultClass) then begin
      Result := DefaultClass.Create(ACreateObject);
   end else begin
      Result := nil;
   end;
end;

class procedure TLayeredJSONArray.SetDefaultClass(ADefaultClass: TLayeredJSONArrayClass);
begin
   DefaultClass := ADefaultClass;
end;

{ AJSONObjectProperty }

constructor AJSONObjectProperty.Create(AKeyName: string);
begin
   Self.FKeyName := AKeyName;
end;

{ TLayeredJSONObject }

class constructor TLayeredJSONObject.Create;
begin
   DefaultClass := nil;
end;

class function TLayeredJSONObject.CreateInstance(ACreateObject: boolean): TLayeredJSONObject;
begin
   if Assigned(DefaultClass) then begin
      Result := DefaultClass.Create(ACreateObject);
   end else begin
      Result := nil;
   end;
end;

class procedure TLayeredJSONObject.SetDefaultClass(ADefaultClass: TLayeredJSONObjectClass);
begin
   DefaultClass := ADefaultClass;
end;

function TLayeredJSONObject.ToObject(const AnObject: TObject): boolean;
var
   iCount: integer;
   objectPropertyList: TPropList;
   objectPropertyInfo: PPropInfo;
   iProperty: integer;
   iAttribute: integer;
   attributeEntry: TAttributeEntry;
   attribute: TCustomAttribute;
   sKeyName: string;
   sData: string;
   iData: integer;
   i64Data: int64;
begin
   Result := true;
   // for each published, look up attribute, check according paramter
   iCount := GetPropList(AnObject.ClassInfo, tkAny, @objectPropertyList, False);
   for iProperty := 0 to Pred(iCount) do begin
      try
         objectPropertyInfo := objectPropertyList[iProperty];
         if Assigned(objectPropertyInfo^.AttributeTable) then begin
            for iAttribute := 0 to Pred(objectPropertyInfo^.AttributeTable.AttributeCount) do begin
               attributeEntry := objectPropertyInfo^.AttributeTable.AttributesList[iAttribute];
               attribute := attributeEntry.AttrProc;
               if attribute is AJSONObjectProperty then begin
                  sKeyName := (attribute as AJSONObjectProperty).KeyName;
                  case objectPropertyInfo^.PropType^.Kind of
                     tkAString: begin
                        if Self.FindString(sKeyName, sData) then begin
                           SetStrProp(AnObject, objectPropertyInfo, sData);
                        end;
                     end;
                     tkWString: begin
                        if Self.FindString(sKeyName, sData) then begin
                           SetWideStrProp(AnObject, objectPropertyInfo, sData);
                        end;
                     end;
                     tkInteger, tkInt64: begin
                        if Self.FindInteger(sKeyName, iData) then begin
                           SetInt64Prop(AnObject, objectPropertyInfo, iData);
                        end else if Self.FindInt64(sKeyName, i64Data) then begin
                           SetInt64Prop(AnObject, objectPropertyInfo, i64Data);
                        end;
                     end;
                  end;
               end;
            end;
         end;
      except
         on E: Exception do begin
            Result := false;
            //DebugLogger.LogException(E, Format('TLayeredJSONObject.ToObject(x, %s)', [AnObject.ClassName]));
         end;
      end;
   end;
end;

end.
