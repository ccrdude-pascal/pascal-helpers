{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Functions helping with collection handling.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2005-2034 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2007-06-01  pk   5m  Added types suggested by Elvis at Delphi-PRAXiS
// 2007-04-29  pk   5m  Prepared for CollectionCreator inclusion
// *****************************************************************************
   )
}

unit Firefly.Collections;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   SysUtils,
   Classes;

type
   TPropertyChangedHandler = procedure(Sender: TObject; PropertyName: string) of object;
   TCollectionChangedHandler = procedure(Sender: TObject; index: integer; ChangeType: TCollectionNotification) of object;

   INotifyTCollectionPropertyChanged = interface(IInterface)
      ['{37BDED92-FF4C-4B24-B348-194EF999A4CE}']
      function GetPropertyChanged: TPropertyChangedHandler;
      property PropertyChanged: TPropertyChangedHandler read GetPropertyChanged;
   end;

   INotifyCollectionChanged = interface
      ['{97829CE7-2C68-4151-AC52-9B559D109373}']
      function GetCollectionChanged: TCollectionChangedHandler;
      property CollectionChanged: TCollectionChangedHandler read GetCollectionChanged;
   end;

   { TInterfacedCollectionItem }

   TInterfacedCollectionItem = class(TCollectionItem)
   private
      FOwnerInterface: IInterface;
   protected
      { IInterface }
      function _AddRef: integer; stdcall;
      function _Release: integer; stdcall;
   public
      function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
   end;

   { TInterfacedCollection }

   TInterfacedCollection = class(TCollection)
   private
      FOwnerInterface: IInterface;
   protected
      { IInterface }
   public
      function _AddRef: integer; stdcall;
      function _Release: integer; stdcall;
      function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
   end;

{
  Returns the list belonging to a collection to allow sorting.

  @param(ACollection is the collection to sort.)
  @returns(A TList that can be sorted.)
}
function GetCollectionList(const ACollection: TCollection): TList;
{
  Makes a text a bit more xml compliant by escaping ampersands. Probably not
  complete, since other characters have to be replaced as well.

  @param(AText is the input text.)
  @returns(Text that can be used in XML.)
}
function XMLify(AText: AnsiString): AnsiString; overload;
function XMLify(AText: WideString): WideString; overload;

implementation

function GetCollectionList(const ACollection: TCollection): TList;
var
   mMethod: TMethod;
   lMethod: TList absolute mMethod;
   pItemClass: Pointer;
begin
   // Not sure what problems were there under Linux, will retest asap.
   // {$IF CompilerVersion >= 20}
   pItemClass := @ACollection.ItemClass;
   Inc(NativeInt(pItemClass), SizeOf(TCollectionItemClass));
   mMethod := TMethod(pItemClass^);
   Result := lMethod;
   // {$IFEND}
end;

function XMLify(AText: AnsiString): AnsiString; overload;
begin
   AText := StringReplace(AText, '&', '&amp;', [rfReplaceAll]);
   Result := AText;
end;

function XMLify(AText: WideString): WideString; overload;
begin
   AText := WideStringReplace(AText, '&', '&amp;', [rfReplaceAll]);
   Result := AText;
end;

{ TInterfacedCollectionItem }

function TInterfacedCollectionItem.QueryInterface(const IID: TGUID; out Obj): HResult;
const
   E_NOINTERFACE = HResult($80004002);
begin
   if GetInterface(IID, Obj) then begin
      Result := 0;
   end else begin
      Result := E_NOINTERFACE;
   end;
end;

function TInterfacedCollectionItem._AddRef: integer;
begin
   if FOwnerInterface <> nil then begin
      Result := FOwnerInterface._AddRef;
   end else begin
      Result := -1;
   end;
end;

function TInterfacedCollectionItem._Release: integer;
begin
   if FOwnerInterface <> nil then begin
      Result := FOwnerInterface._Release;
   end else begin
      Result := -1;
   end;
end;

{ TInterfacedCollection }

function TInterfacedCollection.QueryInterface(const IID: TGUID; out Obj): HResult;
const
   E_NOINTERFACE = HResult($80004002);
begin
   if GetInterface(IID, Obj) then begin
      Result := 0;
   end else begin
      Result := E_NOINTERFACE;
   end;
end;

function TInterfacedCollection._AddRef: integer;
begin
   if FOwnerInterface <> nil then begin
      Result := FOwnerInterface._AddRef;
   end else begin
      Result := -1;
   end;
end;

function TInterfacedCollection._Release: integer;
begin
   if FOwnerInterface <> nil then begin
      Result := FOwnerInterface._Release;
   end else begin
      Result := -1;
   end;
end;

end.
