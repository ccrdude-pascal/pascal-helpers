{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Date handling and string conversions.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-17  pk   5m  Renamed from snlParserBoolean
// *****************************************************************************
   )
}


unit Firefly.Parser.Boolean;

interface

uses
   Classes;

type
   TCompareAction = (caAnd, caOr);

   TCompareRuleItem = class
   public
      TextA: AnsiString;
      TextW: WideString;
      Negator: boolean;
      Action: TCompareAction;
      constructor Create;
   end;

   TCompareRule = class(TList)
   private
      function GetItem(AIndex: Integer): TCompareRuleItem;
   public
      constructor Create;
      destructor Destroy; override;
      property Items[AIndex: Integer]: TCompareRuleItem read GetItem; default;
      function Add(AItem: TCompareRuleItem): Integer;
      procedure Delete(AIndex: Integer);
      procedure FromString(ARuleText: AnsiString); overload;
      procedure FromString(ARuleText: WideString); overload;
      function AsString: WideString;
      function StringFits(AText: AnsiString): boolean; overload;
      function StringFits(AText: WideString): boolean; overload;
   end;

implementation

uses
   SysUtils;

{ TCompareRule }

function TCompareRule.Add(AItem: TCompareRuleItem): Integer;
begin
   Result := inherited Add(AItem);
end;

function TCompareRule.AsString: WideString;
const
   actString: array [TCompareAction] of WideString = ('AND', 'OR');
var
   iItem: Integer;
begin
   Result := '';
   for iItem := 0 to Pred(Count) do begin
      if (iItem > 0) then begin
         Result := Result + actString[Items[iItem].Action] + ' ';
      end;
      if Items[iItem].Negator then begin
         Result := Result + 'NOT ';
      end;
      Result := Result + Items[iItem].TextW + ' ';
   end;
end;

constructor TCompareRule.Create;
begin
   inherited;
end;

procedure TCompareRule.Delete(AIndex: Integer);
begin
   TCompareRuleItem(GetItem(AIndex)).Free;
   inherited Delete(AIndex);
end;

destructor TCompareRule.Destroy;
var
   i: Integer;
begin
   if Count > 0 then begin // 2007-02-20  pk  Added to fix memleak
      for i := Pred(Count) downto 0 do begin
         Delete(i);
      end;
   end;
   inherited;
end;

procedure TCompareRule.FromString(ARuleText: AnsiString);
begin
   FromString(WideString(ARuleText));
end;

procedure TCompareRule.FromString(ARuleText: WideString);
var
   iCharacter: Integer;
   sTemp: WideString;
   slTerms: TStringList;
   bWithinQuote: boolean;
   ri: TCompareRuleItem;
begin
   { TODO -oPatrick : Ignore \" for quote comparison }
   Clear;
   slTerms := TStringList.Create;
   try
      bWithinQuote := false;
      sTemp := '';
      for iCharacter := 1 to Length(ARuleText) do begin
         if bWithinQuote then begin
            if ARuleText[iCharacter] = '"' then begin
               bWithinQuote := false;
               if Length(sTemp) > 0 then begin
                  {$IFDEF FPC}
                  slTerms.Add(UTF8Encode(sTemp));
                  {$ELSE FPC}
                  slTerms.Add(sTemp);
                  {$ENDIF FPC}
               end;
               sTemp := '';
            end
            else sTemp := sTemp + ARuleText[iCharacter];
         end else begin
            if ARuleText[iCharacter] = '"' then begin
               bWithinQuote := true;
            end
            else if ARuleText[iCharacter] = ' ' then begin
               if Length(sTemp) > 0 then begin
                  {$IFDEF FPC}
                  slTerms.Add(UTF8Encode(sTemp));
                  {$ELSE FPC}
                  slTerms.Add(sTemp);
                  {$ENDIF FPC}
               end;
               sTemp := '';
            end
            else sTemp := sTemp + ARuleText[iCharacter];
         end;
      end;
      if Length(sTemp) > 0 then begin
         {$IFDEF FPC}
         slTerms.Add(UTF8Encode(sTemp));
         {$ELSE FPC}
         slTerms.Add(sTemp);
         {$ENDIF FPC}
      end;
      for iCharacter := 0 to Pred(slTerms.Count) do begin
         ri := TCompareRuleItem.Create;
         {$IFDEF Unicode}
         ri.TextA := UTF8Encode(slTerms[iCharacter]);
         ri.TextW := slTerms[iCharacter];
         {$ELSE Unicode}
         ri.TextA := slTerms[iCharacter];
         ri.TextW := UTF8Decode(slTerms[iCharacter]);
         {$ENDIF Unicode}
         Add(ri);
      end;
      // Now move all NOTs to flags
      if Count > 0 then
         if UpperCase(Items[Count - 1].TextA) = 'NOT' then begin
            Delete(Count - 1);
         end;
      for iCharacter := Count - 2 downto 0 do begin
         if (UpperCase(Items[iCharacter].TextA) = 'NOT') then begin
            Items[iCharacter + 1].Negator := true;
            Delete(iCharacter);
         end;
      end;
      if Count > 0 then begin
         Items[0].Action := caOr;
      end;
      if Count > 0 then begin
         if UpperCase(Items[Count - 1].TextA) = 'OR' then begin
            Delete(Count - 1);
         end;
      end;
      if Count > 0 then begin
         if UpperCase(Items[Count - 1].TextA) = 'AND' then begin
            Delete(Count - 1);
         end;
      end;
      // Evaluate ANDs and ORs
      for iCharacter := Count - 2 downto 1 do begin
         if UpperCase(Items[iCharacter].TextA) = 'AND' then begin
            Items[iCharacter + 1].Action := caAnd;
            Delete(iCharacter);
         end
         else if UpperCase(Items[iCharacter].TextA) = 'OR' then begin
            Items[iCharacter + 1].Action := caOr;
            Delete(iCharacter);
         end;
      end;
   finally
      slTerms.Free;
   end;
end;

function TCompareRule.GetItem(AIndex: Integer): TCompareRuleItem;
begin
   Result := TCompareRuleItem( inherited Items[AIndex]);
end;

function TCompareRule.StringFits(AText: AnsiString): boolean;
var
   iItem: Integer;
   bFound: boolean;
begin
   Result := false;
   for iItem := 0 to Pred(Count) do begin
      bFound := (Pos(Items[iItem].TextA, AText) > 0) xor Items[iItem].Negator;
      case Items[iItem].Action of
         caOr: Result := Result or bFound;
         caAnd: Result := Result and bFound;
      end;
   end;
end;

function TCompareRule.StringFits(AText: WideString): boolean;
var
   iItem: Integer;
   bFound: boolean;
begin
   Result := false;
   for iItem := 0 to Pred(Count) do begin
      bFound := (Pos(Items[iItem].TextW, AText) > 0) xor Items[iItem].Negator;
      case Items[iItem].Action of
         caOr: Result := Result or bFound;
         caAnd: Result := Result and bFound;
      end;
   end;
end;

{ TCompareRuleItem }

constructor TCompareRuleItem.Create;
begin
   TextA := '';
   TextW := '';
   Negator := false;
   Action := caOr;
end;

end.
