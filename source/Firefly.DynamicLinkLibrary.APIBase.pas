{
   @abstract(Abstract class for API units interfacing DLLs.)
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2018-11-18  pk  10m  Changed THandle to TLibHandle to support MacOS
// 2017-02-21  pk   5m  Renamed from snlAPIBase
// 2016-09-28  pk   5m  Changed base to TMultiReadExclusiveWriteSynchronizer to allow multiple threads accessing the same library
// 2016-09-26  pk   5m  Fixed all FreePascal 3.0 compiler hints
// 2016-09-26  pk   5m  Added IsLoaded property
// 2013-07-25  pk   5m  Added this header
// *****************************************************************************
   )
}

unit Firefly.DynamicLinkLibrary.APIBase;

interface

uses
   SysUtils,
   Classes,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   {$IFDEF SupportInMemory}
   BTMemoryModule,
   {$ENDIF SupportInMemory}
   contnrs;

type
   TLibraryAPIOption = (lapioUseInMemoryLibrary, lapioLoadFromResource);
   TLibraryAPIOptions = set of TLibraryAPIOption;
   {$IFDEF FPC}
   TLibraryAPIHandle = TLibHandle;
   {$ELSE FPC}
   TLibraryAPIHandle = THandle;
   {$ENDIF FPC}

   { TLibraryAPI }

   TLibraryAPI = class(TMultiReadExclusiveWriteSynchronizer)
   private
      function GetIsLoaded: boolean;
   protected
      FHandle: TLibraryAPIHandle;
      {$IFDEF SupportInMemory}
      FMemHandle: PBTMemoryModule;
      {$ENDIF SupportInMemory}
      FMemoryStream: TMemoryStream;
      FAPIOptions: TLibraryAPIOptions;
      FFilename: String;
      procedure AssignFunctions; virtual; abstract;
      function GetAreFunctionsAssigned: boolean; virtual;
      class var FInstances: TObjectList;
   public
      class procedure ClassCreate;
      class procedure ClassDestroy;
      constructor Create(AFilename: String; AAPIOptions: TLibraryAPIOptions = []); overload;
      destructor Destroy; override;
      function GetAPIProcAddress(AProcName: string): {$ifdef cpui8086}FarPointer{$else}Pointer{$endif};
      property IsLoaded: boolean read GetIsLoaded;
      property AreFunctionsAssigned: boolean read GetAreFunctionsAssigned;
      procedure HandleRequest({%H-}APath, {%H-}AFilename: string; {%H-}AParameters, {%H-}AHeadersIn, {%H-}AHeadersOut, {%H-}ADataOut: TStringList); virtual;
   end;

implementation

{ TLibraryAPI }

function TLibraryAPI.GetIsLoaded: boolean;
begin
   Result := (FHandle > 0);
end;

function TLibraryAPI.GetAreFunctionsAssigned: boolean;
begin
   Result := False;
end;

class procedure TLibraryAPI.ClassCreate;
begin
   FInstances := TObjectList.Create(False);
end;

class procedure TLibraryAPI.ClassDestroy;
begin
   FInstances.Free;
end;

constructor TLibraryAPI.Create(AFilename: String; AAPIOptions: TLibraryAPIOptions);
var
   rs: TResourceStream;
begin
   inherited Create;
   FHandle := 0;
   {$IFDEF SupportInMemory}
   FMemHandle := nil;
   {$ENDIF SupportInMemory}
   FMemoryStream := TMemoryStream.Create;
   FAPIOptions := AAPIOptions;
   {$IFDEF DEBUG}
   {$IFDEF MSWindows}
   OutputDebugStringW(PWideChar(WideString(Format('%s.Create(''%s'')', [Self.ClassName, AFilename]))));
   {$ENDIF MSWindows}
   {$ENDIF DEBUG}
   FFilename := AFilename;
   if not (lapioLoadFromResource in AAPIOptions) then begin
      if (not FileExists(FFilename)) then begin
         //DebugLogger.LogError(0, Format('Unable to find library %s for class %s.', [AFilename, Self.ClassName]));
         FHandle := 0;
         Exit;
      end;
   end;
   if (lapioUseInMemoryLibrary in FAPIOptions) then begin
      if (lapioLoadFromResource in FAPIOptions) then begin
         rs := TResourceStream.Create(HINSTANCE, FFilename, RT_RCDATA);
         try
            rs.Seek(0, soFromBeginning);
            FMemoryStream.CopyFrom(rs, rs.Size);
         finally
            rs.Free;
         end;
      end else begin
         FMemoryStream.LoadFromFile(FFilename);
         {$IFDEF SupportInMemory}
         FMemHandle := BTMemoryLoadLibary(FMemoryStream.Memory, FMemoryStream.Size);
         {$ENDIF SupportInMemory}
      end;
   end else begin
      {$IFDEF MSWindows}
      FHandle := LoadLibraryW(PWideChar(WideString(FFilename)));
      {$ELSE MSWindows}
      FHandle := LoadLibrary(FFilename);
      {$ENDIF MSWindows}
   end;
   if (FHandle <> 0) {$IFDEF SupportInMemory} or (FMemHandle <> nil) {$ENDIF} then begin
      try
         AssignFunctions;
      except
         FHandle := 0;
         {$IFDEF SupportInMemory}
         FMemHandle := nil;
         {$ENDIF SupportInMemory}
      end;
   end else begin
      //DebugLogger.LogError(0, Format('Unable to load library %s for class %s (LoadLibrary failed).', [AFilename, Self.ClassName]));
   end;
   FInstances.Add(Self);
end;

destructor TLibraryAPI.Destroy;
begin
   {$IFDEF DEBUG}
   {$IFDEF MSWindows}
   OutputDebugStringW(PWideChar(WideString(Format('%s.Destroy(''%s'')', [Self.ClassName, FFilename]))));
   {$ENDIF MSWindows}
   {$ENDIF DEBUG}
   FMemoryStream.Free;
   FInstances.Remove(Self);
   {$IFDEF SupportInMemory}
   if Assigned(FMemHandle) then begin
      BTMemoryFreeLibrary(FMemHandle);
   end;
   {$ENDIF SupportInMemory}
   if (FHandle <> 0) then begin
      FreeLibrary(FHandle);
   end;
   inherited Destroy;
end;

function TLibraryAPI.GetAPIProcAddress(AProcName: string): Pointer;
begin
   if (lapioUseInMemoryLibrary in FAPIOptions) then begin
      {$IFDEF SupportInMemory}
      Result := BTMemoryGetProcAddress(FMemHandle, PChar(AProcName));
      {$ELSE SupportInMemory}
      Result := nil;
      {$ENDIF SupportInMemory}
   end else begin
      Result := GetProcAddress(FHandle, PChar(AProcName));
   end;
end;

procedure TLibraryAPI.HandleRequest(APath, AFilename: string; AParameters, AHeadersIn, AHeadersOut, ADataOut: TStringList);
begin

end;

initialization
   TLibraryAPI.ClassCreate;

finalization
   TLibraryAPI.ClassDestroy;
end.

