{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Handling of version strings.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-12-05  pk  ---  Moved to Firefly codebase
// *****************************************************************************
   )
}
unit Firefly.Strings.Versions;

{$mode objfpc}{$H+}

interface

uses
  Classes,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   {$IFNDEF FPC}
   JclStrings,
   JclAnsiStrings,
   JclWideStrings,
   {$ENDIF FPC}

  SysUtils;

function CompareVersions(AVersionAvailable, AVersionCurrent: AnsiString): integer; overload;
function CompareVersions(AVersionAvailable, AVersionCurrent: WideString): integer; overload;

implementation

function CompareVersions(AVersionAvailable, AVersionCurrent: AnsiString): integer;
var
   sl1, sl2: TStringList;
begin
   Result := 0;
   sl1 := TStringList.Create;
   sl2 := TStringList.Create;
   try
      if Pos(' ', AVersionAvailable) > 0 then begin
         SetLength(AVersionAvailable, Pred(Pos(' ', AVersionAvailable)));
      end;
      if Pos(' ', AVersionCurrent) > 0 then begin
         SetLength(AVersionCurrent, Pred(Pos(' ', AVersionCurrent)));
      end;
      sl1.Delimiter := '.';
      sl1.DelimitedText := AVersionAvailable;
      sl2.Delimiter := '.';
      sl2.DelimitedText := AVersionCurrent;
      while (sl1.Count > 0) and (sl2.Count > 0) and (Result = 0) do begin
         Result := StrToIntDef(string(sl1[0]), 0) - StrToIntDef(string(sl2[0]), 0);
         sl1.Delete(0);
         sl2.Delete(0);
         if Result <> 0 then begin
            Break;
         end;
      end;
   finally
      sl1.Free;
      sl2.Free;
   end;
end;

function CompareVersions(AVersionAvailable, AVersionCurrent: WideString): integer;
var
   sl1, sl2: TStringList;
begin
   Result := 0;
   sl1 := TStringList.Create;
   sl2 := TStringList.Create;
   try
      if Pos(' ', AVersionAvailable) > 0 then begin
         SetLength(AVersionAvailable, Pred(Pos(' ', AVersionAvailable)));
      end;
      if Pos(' ', AVersionCurrent) > 0 then begin
         SetLength(AVersionCurrent, Pred(Pos(' ', AVersionCurrent)));
      end;
      sl1.Delimiter := '.';
      sl1.DelimitedText := AVersionAvailable;
      sl2.Delimiter := '.';
      sl2.DelimitedText := AVersionCurrent;
      while (sl1.Count > 0) and (sl2.Count > 0) and (Result = 0) do begin
         Result := StrToIntDef(string(sl1[0]), 0) - StrToIntDef(string(sl2[0]), 0);
         sl1.Delete(0);
         sl2.Delete(0);
         if Result <> 0 then begin
            Break;
         end;
      end;
   finally
      FreeAndNil(sl1);
      FreeAndNil(sl2);
   end;
end;


end.

