{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Defines abstract access methods to the Google Safe Browsing endpoints.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-16  pk  ---  Updated code to be independent of specific JSON implementation.
// 2023-03-16  pk  ---  Refactored transport into this interfacing class.
// *****************************************************************************
// TODO : visual Internet connection debugger
// *****************************************************************************
   )
}

unit Layers.Transport.Base;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$modeswitch TypeHelpers}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Layers.JSON.Base;

type

   { TLayeredTransportProxy }

   TLayeredTransportProxy = class(TPersistent)
   private
     FHost: string;
     FPassword: string;
     FPort: string;
     FUse: boolean;
     FUsername: string;
   protected
      procedure AssignTo(Dest: TPersistent); override;
   public
      constructor Create;
      procedure LoadSystemProxy;
      property Use: boolean read FUse write FUse;
      property Host: string read FHost write FHost;
      property Port: string read FPort write FPort;
      property Username: string read FUsername write FUsername;
      property Password: string read FPassword write FPassword;
   end;

   { TLayeredTransportCertificateInformation }

   TLayeredTransportCertificateInformation = class
   private
      FAvailable: boolean;
      FCertInfo: string;
      FCipherName: string;
      FPeerFingerprint: ansistring;
      FPeerIssuer: string;
      FPeerName: string;
      FPeerSubject: string;
      FSerialNumber: string;
      FSSLVersion: string;
   public
      property Available: boolean read FAvailable write FAvailable;
      property SSLVersion: string read FSSLVersion write FSSLVersion;
      property CipherName: string read FCipherName write FCipherName;
      property SerialNumber: string read FSerialNumber write FSerialNumber;
      property PeerFingerprint: ansistring read FPeerFingerprint write FPeerFingerprint;
      property PeerIssuer: string read FPeerIssuer write FPeerIssuer;
      property PeerName: string read FPeerName write FPeerName;
      property PeerSubject: string read FPeerSubject write FPeerSubject;
      property CertInfo: string read FCertInfo write FCertInfo;
   end;

   TLayeredTransport = class;

   TLayeredTransportClass = class of TLayeredTransport;

   { TLayeredTransport }

   TLayeredTransport = class
   private
      FCertificateInformation: TLayeredTransportCertificateInformation;
      FProxy: TLayeredTransportProxy;
      FVerifyCertificate: boolean;
   protected
      class var DefaultClass: TLayeredTransportClass;
      class var FPersistentLog: TStringList;
   protected
      FHeaders: TStringList;
      FUserAgent: string;
      FContentType: string;
      FResultCode: Integer;
      class procedure LogResult(AMethod: string; AStatusCode: integer; AnURL: string);
      class procedure LogSSL(ACertificateInformation: TLayeredTransportCertificateInformation);
   public
      class var PersistentNon200Count: integer;
      class constructor Create;
      class destructor Destroy;
      class function CreateInstance: TLayeredTransport;
      class procedure SetDefaultClass(ADefaultClass: TLayeredTransportClass);
      class function PersistentLog: TStringList;
      class procedure DumpPersistentLogToEventLog;
      class function DownloadFile(AnURL: string; AFilename: string): boolean;
   public
      constructor Create; virtual;
      destructor Destroy; override;
      function GET(AURL: string; const AResponse: TMemoryStream): boolean; virtual; abstract; overload;
      function GET(AURL: string; const AResponse: TLayeredJSONObject): boolean; virtual; overload;
      function GET(AURL: string; const AResponse: TLayeredJSONArray): boolean; virtual; overload;
      function POST(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean; virtual; abstract; overload;
      function POST(AURL: string; const AInput: TLayeredJSONObject; const AResponse: TLayeredJSONObject): boolean; virtual; overload;
      function POST(AURL: string): boolean; overload;
      function PATCH(AURL: string; const AInput: TMemoryStream; const AResponse: TMemoryStream): boolean; virtual; abstract; overload;
      function PATCH(AURL: string): boolean; overload;
      property Proxy: TLayeredTransportProxy read FProxy;
      property ContentType: string read FContentType write FContentType;
      property UserAgent: string read FUserAgent write FUserAgent;
      property Headers: TStringList read FHeaders;
      property VerifyCertificate: boolean read FVerifyCertificate write FVerifyCertificate;
      property CertificateInformation: TLayeredTransportCertificateInformation read FCertificateInformation;
      property ResultCode: Integer read FResultCode;
   end;

implementation

uses
   {$IFDEF MSWindows}
   Windows,
   WinINet,
   {$ENDIF MSWindows}
   Layers.Transport.Debugger.List;

   { TLayeredTransportProxy }

procedure TLayeredTransportProxy.AssignTo(Dest: TPersistent);
begin
   if Dest is TLayeredTransportProxy then begin
      TLayeredTransportProxy(Dest).Use := Use;
      TLayeredTransportProxy(Dest).Host := Host;
      TLayeredTransportProxy(Dest).Port := Port;
      TLayeredTransportProxy(Dest).Username := Username;
      TLayeredTransportProxy(Dest).Password := Password;
   end;
end;

constructor TLayeredTransportProxy.Create;
begin
   FUse := False;
   FHost := '';
   FPort := '';
   FUsername := '';
   FPassword := '';
end;

procedure TLayeredTransportProxy.LoadSystemProxy;
{$IFDEF MSWindows}
   procedure LoadWindowsProxy;
   var
      pc: pansichar;
      dw: DWord;
      pipi: PINTERNET_PROXY_INFO;
      sl: TStringList;
      iHttps: integer;
      iSeparator: integer;
   begin
      Self.FUse := False;
      dw := 1024;
      GetMem(pc, dw);
      GetMem(pipi, dw);
      try
         if InternetQueryOptionA(nil, INTERNET_OPTION_PROXY, pipi, dw) then begin
            case pipi^.dwAccessType of
               INTERNET_OPEN_TYPE_PROXY:
               begin
                  Self.FUse := True;
                  sl := TStringList.Create;
                  try
                     sl.StrictDelimiter := True;
                     sl.Delimiter := ' ';
                     sl.DelimitedText := pipi^.lpszProxy;
                     iHttps := sl.IndexOfName('https');
                     OutputDebugStringA(PAnsiChar('TLayeredTransportProxy.LoadSystemProxy: ' + sl.DelimitedText));
                     if iHttps > -1 then begin
                        Self.FHost := sl.ValueFromIndex[iHttps];
                        iSeparator := Pos(':', Self.FHost);
                        if iSeparator > 0 then begin
                           Self.FPort := Copy(Host, iSeparator + 1, Length(Self.FHost) - iSeparator);
                           SetLength(Self.FHost, iSeparator - 1);
                        end else begin
                           Self.FPort := '8888';
                        end;
                        ZeroMemory(pc, dw);
                        if InternetQueryOptionA(nil, INTERNET_OPTION_PROXY_USERNAME, pc, dw) then begin
                           Self.FUsername := string(ansistring(pc));
                        end;
                        ZeroMemory(pc, dw);
                        if InternetQueryOptionA(nil, INTERNET_OPTION_PROXY_PASSWORD, pc, dw) then begin
                           Self.FPassword := string(ansistring(pc));
                        end;
                     end;
                  finally
                     sl.Free;
                  end;
               end;
            end;
         end else begin
            OutputDebugString(PChar(SysErrorMessage(GetLastError)));
         end;
      finally
         FreeMem(pc);
         FreeMem(pipi);
      end;
   end;

   {$ENDIF MSWindows}
begin
   Use := False;
   {$IF DEFINED(MSWindows)}
   LoadWindowsProxy;
   {$IFEND}
end;

{ TLayeredTransport }

class procedure TLayeredTransport.LogResult(AMethod: string; AStatusCode: integer; AnURL: string);
begin
   FPersistentLog.Add(Format('%6s [%3d] %s', [AMethod, AStatusCode, AnURL]));
end;

class procedure TLayeredTransport.LogSSL(ACertificateInformation: TLayeredTransportCertificateInformation);

   function GetFingerprintString(s: ansistring): string;
   var
      i: integer;
   begin
      Result := '';
      for i := 1 to Length(s) do begin
         Result += IntToHex(byte(s[i]));
         if (i < Length(s)) then begin
            Result += ':';
         end;
      end;
   end;

begin
   if ACertificateInformation.Available then begin
      FPersistentLog.Add(' - SSL Version: ' + ACertificateInformation.SSLVersion);
      FPersistentLog.Add(' - Cipher Name: ' + ACertificateInformation.CipherName);
      FPersistentLog.Add(' - Serial Number: ' + ACertificateInformation.SerialNumber);
      //FPersistentLog.Add(' - Peer Fingerprint: ' + GetFingerprintString(ACertificateInformation.PeerFingerprint));
      FPersistentLog.Add(' - Peer Issuer:      ' + ACertificateInformation.PeerIssuer);
      FPersistentLog.Add(' - Peer Name:        ' + ACertificateInformation.PeerName);
      FPersistentLog.Add(' - Peer Subject:     ' + ACertificateInformation.PeerSubject);
      FPersistentLog.Add(' - Certification Information: ' + ACertificateInformation.CertInfo);
   end;
end;

class constructor TLayeredTransport.Create;
begin
   DefaultClass := nil;
   FPersistentLog := TStringList.Create;
   PersistentNon200Count := 0;
end;

class destructor TLayeredTransport.Destroy;
begin
   FPersistentLog.Free;
end;

class function TLayeredTransport.CreateInstance: TLayeredTransport;
begin
   if Assigned(DefaultClass) then begin
      Result := DefaultClass.Create;
   end else begin
      Result := nil;
   end;
end;

class procedure TLayeredTransport.SetDefaultClass(ADefaultClass: TLayeredTransportClass);
begin
   DefaultClass := ADefaultClass;
end;

class function TLayeredTransport.PersistentLog: TStringList;
begin
   Result := FPersistentLog;
end;

class procedure TLayeredTransport.DumpPersistentLogToEventLog;
var
   i: integer;
begin
   {$IFDEF MSWindows}
   for i := 0 to Pred(FPersistentLog.Count) do begin
      OutputDebugString(PChar(FPersistentLog[i]));
   end;
   OutputDebugString('--------------------------------');
   {$ENDIF MSWindows}
end;

class function TLayeredTransport.DownloadFile(AnURL: string; AFilename: string): boolean;
var
   instance: TLayeredTransport;
   ms: TMemoryStream;
begin
   instance := TLayeredTransport.CreateInstance;
   try
      try
         ms := TMemoryStream.Create;
         try
            Result := instance.GET(AnURL, ms);
            ms.SaveToFile(AFilename);
         finally
            ms.Free;
         end;
      except
         Result := False;
      end;
   finally
      instance.Free;
   end;
end;

constructor TLayeredTransport.Create;
begin
   FVerifyCertificate := False;
   FProxy := TLayeredTransportProxy.Create;
   Self.FUserAgent := 'Layers.Transport/0.2';
   Self.FHeaders := TStringList.Create;
   Self.FCertificateInformation := TLayeredTransportCertificateInformation.Create;
end;

destructor TLayeredTransport.Destroy;
begin
   FProxy.Free;
   Self.FHeaders.Free;
   Self.FCertificateInformation.Free;
   inherited Destroy;
end;

function TLayeredTransport.GET(AURL: string; const AResponse: TLayeredJSONObject): boolean;
var
   ms: TMemoryStream;
begin
   ms := TMemoryStream.Create;
   try
      Result := Self.GET(AURL, ms);
      ms.Seek(0, soFromBeginning);
      AResponse.FromStream(ms);
   finally
      ms.Free;
   end;
end;

function TLayeredTransport.GET(AURL: string; const AResponse: TLayeredJSONArray): boolean;
var
   ms: TMemoryStream;
begin
   ms := TMemoryStream.Create;
   try
      Result := Self.GET(AURL, ms);
      ms.Seek(0, soFromBeginning);
      AResponse.FromStream(ms);
   finally
      ms.Free;
   end;
end;

function TLayeredTransport.POST(AURL: string; const AInput: TLayeredJSONObject; const AResponse: TLayeredJSONObject): boolean;
var
   msInput: TMemoryStream;
   msResponse: TMemoryStream;
begin
   msInput := TMemoryStream.Create;
   try
      AInput.ToStream(msInput, True);
      msInput.Seek(0, soFromBeginning);
      msResponse := TMemoryStream.Create;
      try
         Result := Self.POST(AURL, msInput, msResponse);
         msResponse.Seek(0, soFromBeginning);
         AResponse.FromStream(msResponse);
      finally
         msResponse.Free;
      end;
   finally
      msInput.Free;
   end;
end;

function TLayeredTransport.POST(AURL: string): boolean;
var
   msInput: TMemoryStream;
   msResponse: TMemoryStream;
begin
   msInput := TMemoryStream.Create;
   try
      msResponse := TMemoryStream.Create;
      try
         Result := Self.POST(AURL, msInput, msResponse);
      finally
         msResponse.Free;
      end;
   finally
      msInput.Free;
   end;
end;

function TLayeredTransport.PATCH(AURL: string): boolean;
var
   msInput: TMemoryStream;
   msResponse: TMemoryStream;
begin
   msInput := TMemoryStream.Create;
   try
      msResponse := TMemoryStream.Create;
      try
         Result := Self.PATCH(AURL, msInput, msResponse);
      finally
         msResponse.Free;
      end;
   finally
      msInput.Free;
   end;
end;

end.
