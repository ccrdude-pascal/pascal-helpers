{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Serializes objects to and from JSON or XML.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2022-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2024-04-25  pk   5m  Added header.
// *****************************************************************************
   )
}
unit Firefly.Process;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Process,
   Generics.Collections;

type
   TProcessExecutionSummaryList = class;

   { TProcessExecutionSummary }

   TProcessExecutionSummary = class
   private
      FExecutable: string;
      FExitStatus: integer;
      FList: TProcessExecutionSummaryList;
      FOutput: TStringList;
      FErrors: TStringList;
      FParameters: TStringList;
      FSuccess: boolean;
   public
      constructor Create;
      destructor Destroy; override;
      property Parameters: TStringList read FParameters;
   published
      property Executable: string read FExecutable write FExecutable;
      property ExitStatus: integer read FExitStatus write FExitStatus;
      property Output: TStringList read FOutput;
      property Errors: TStringList read FErrors;
      property Success: boolean read FSuccess;
   end;

   { TProcessExecutionSummaryList }

   TProcessExecutionSummaryList = class(TObjectList<TProcessExecutionSummary>)
   private
   class var FInstance: TProcessExecutionSummaryList;
   public
      function Add(): TProcessExecutionSummary;
      function AddFromProcess(AProcess: TProcess; TheOutput, TheErrors: string; AnExitStatus: integer): TProcessExecutionSummary;
      function AddFromRun(TheResult: boolean; AnExecutable: TProcessString; TheParameters: array of TProcessString;
         TheOutput: string; Options: TProcessOptions = []; SWOptions: TShowWindowOptions = swoNone): TProcessExecutionSummary;
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TProcessExecutionSummaryList;
   end;

function RunLoggedCommand(const AnExecutable: TProcessString; const TheParameters: array of TProcessString; out TheOutput: string;
   Options: TProcessOptions = []; SWOptions: TShowWindowOptions = swoNone): boolean;

implementation

function RunLoggedCommand(const AnExecutable: TProcessString; const TheParameters: array of TProcessString; out TheOutput: string;
   Options: TProcessOptions; SWOptions: TShowWindowOptions): boolean;
begin
   Result := Process.RunCommand(AnExecutable, TheParameters, TheOutput, Options, SWOptions);
   TProcessExecutionSummaryList.Instance.AddFromRun(Result, AnExecutable, TheParameters, TheOutput, Options, SWOptions);
end;

{ TProcessExecutionSummary }

constructor TProcessExecutionSummary.Create;
begin
   FParameters := TStringList.Create;
   FOutput := TStringList.Create;
   FErrors := TStringList.Create;
end;

destructor TProcessExecutionSummary.Destroy;
begin
   FParameters.Free;
   FOutput.Free;
   FErrors.Free;
   inherited Destroy;
end;

{ TProcessExecutionSummaryList }

class constructor TProcessExecutionSummaryList.Create;
begin
   FInstance := nil;
end;

class destructor TProcessExecutionSummaryList.Destroy;
begin
   FInstance.Free;
end;

class function TProcessExecutionSummaryList.Instance: TProcessExecutionSummaryList;
begin
   if not Assigned(FInstance) then begin
      FInstance := TProcessExecutionSummaryList.Create;
   end;
   Result := FInstance;
end;

function TProcessExecutionSummaryList.Add(): TProcessExecutionSummary;
begin
   Result := TProcessExecutionSummary.Create;
   Result.FList := Self;
   inherited Add(Result);
end;

function TProcessExecutionSummaryList.AddFromProcess(AProcess: TProcess; TheOutput, TheErrors: string; AnExitStatus: integer): TProcessExecutionSummary;
begin
   Result := Add();
   Result.Executable := AProcess.Executable;
   Result.Parameters.Assign(AProcess.Parameters);
   Result.ExitStatus := AnExitStatus;
   Result.Output.Text := TheOutput;
   Result.Errors.Text := TheErrors;
   Result.FSuccess := (0 = AnExitStatus);
end;

function TProcessExecutionSummaryList.AddFromRun(TheResult: boolean; AnExecutable: TProcessString; TheParameters: array of TProcessString;
   TheOutput: string; Options: TProcessOptions; SWOptions: TShowWindowOptions): TProcessExecutionSummary;
var
   s: string;
begin
   Result := Add();
   Result.Executable := AnExecutable;
   for s in TheParameters do begin
      Result.Parameters.Add(s);
   end;
   Result.ExitStatus := 0;
   Result.Output.Text := TheOutput;
   Result.Errors.Text := '';
   Result.FSuccess := TheResult;
end;

end.
