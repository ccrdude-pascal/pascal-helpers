﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Defines a few consts for code tests.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-04  pk  10m  Added Status(...) which is missing in fpcunit
// *****************************************************************************
  )
}

unit Firefly.Tests;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$codepage utf8}
{$ENDIF FPC}

interface

uses
   Classes,
   {$IFDEF FPC}
   fpcunit,
   // testutils,
   testregistry,
   {$ELSE FPC}
   TestFramework,
   {$ENDIF FPC}
   SysUtils;

type

   {
     TTestCaseHelper is a class helper that allows access to our specific
     test environment.

     On FreePascals FPCUnit, it also makes the Status() function available
     that is known in DUnit.
   }

   { TFireflyTestCase }

   TFireflyTestCase = class(TTestCase)
   private
   public
      function CreateDummyFile(const Filename: string; const Size: int64): boolean; overload;
      function CreateSimpleDummyFile(const Filename: string): boolean;
      { @name returns the base folder that contains test files. }
      function TestFileFolder: string;
      { @name returns a system-specific test folder. }
      function TempTestFolder: string; overload;
      {
        @name returns a system-specific test folder dedicated to one domain.
        @param(ADomain identifies the domain for the test.)
      }
      function TempTestFolder(ADomain: string): string; overload;
      procedure CheckTextEquals(expected, actual: ansistring; msg: string = ''); overload;
      procedure CheckTextEquals(expected, actual: unicodestring; msg: string = ''); overload;
      {$IFDEF FPC}
      class procedure CheckEquals(expected, actual: cardinal; msg: string = ''); overload;
      class procedure CheckEquals(expected, actual: int64; msg: string = ''); overload;
      {$ENDIF FPC}
      procedure Status(AText: ansistring); overload;
      procedure Status(AText: WideString); overload;
      procedure Status(ATexts: TStrings); overload;
   end;

implementation

{$IFDEF MSWindows}
uses
   Windows;

{$ENDIF MSWindows}

{ TFireflyTestCase }

function TFireflyTestCase.TestFileFolder: string;
begin
   if DirectoryExists(ExtractFilePath(ParamStr(0)) + '..\testfiles\') then begin
      Result := IncludeTrailingPathDelimiter(ExtractFilePath(ExcludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))))) + 'testfiles\';
   end else if DirectoryExists('C:\QA\CodeTest\') then begin
      Result := 'C:\QA\CodeTest\';
   end else if DirectoryExists('Z:\QA\CodeTest\') then begin
      Result := 'Z:\QA\CodeTest\';
   end else begin
      Result := 'C:\QA\CodeTest\';
   end;
end;

procedure TFireflyTestCase.Status(ATexts: TStrings);
var
   i: integer;
begin
   for i := 0 to Pred(ATexts.Count) do begin
      {$IFDEF FPC}
      Status(ATexts[i]);
      {$ELSE FPC}
      inherited Status(ATexts[i]);
      {$ENDIF FPC}
   end;
end;

procedure TFireflyTestCase.Status(AText: ansistring);
begin
   {$IFDEF FPC}
   // if not IsConsole then begin
   // AllocConsole;
   // end;
   // WriteLn(AText);
   {$IFDEF MSWindows}
   OutputDebugStringA(pansichar(AText));
   {$ENDIF MSWindows}
   {$ELSE FPC}
   inherited Status(AText);
   {$ENDIF FPC}
end;

procedure TFireflyTestCase.Status(AText: WideString);
begin
   {$IFDEF FPC}
   // if not IsConsole then begin
   // AllocConsole;
   // end;
   // WriteLn(AText);
   {$IFDEF MSWindows}
   OutputDebugStringW(pwidechar(AText));
   {$ENDIF MSWindows}
   {$ELSE FPC}
   inherited Status(AText);
   {$ENDIF FPC}
end;

procedure TFireflyTestCase.CheckTextEquals(expected, actual: ansistring; msg: string);
begin
   CheckEquals(LowerCase(string(expected)), LowerCase(string(actual)), msg);
end;

procedure TFireflyTestCase.CheckTextEquals(expected, actual: unicodestring; msg: string);
begin
   CheckEquals(LowerCase(expected), LowerCase(actual), msg);
end;

{$IFDEF FPC}

class procedure TFireflyTestCase.CheckEquals(expected, actual: cardinal; msg: string);
begin
   AssertEquals(msg, integer(expected), integer(actual));
end;

{$ENDIF FPC}
{$IFDEF FPC}

class procedure TFireflyTestCase.CheckEquals(expected, actual: int64; msg: string);
begin
   AssertEquals(msg, expected, actual);
end;

{$ENDIF FPC}

function TFireflyTestCase.TempTestFolder: string;
begin
   Result := TempTestFolder(Self.ClassName);
end;

function TFireflyTestCase.TempTestFolder(ADomain: string): string;
begin
   Result := ExtractFilePath(ParamStr(0));
   {$IFDEF Win32}
   Result := 'C:\Temp\';
   if Length(ADomain) > 0 then begin
      Result := Result + ADomain + '\';
   end;
   {$ENDIF Win32}
   {$IFDEF Linux}
   Result := '/tmp/';
   if Length(ADomain) > 0 then begin
      Result := Result + ADomain + '/';
   end;
   {$ENDIF Linux}
   if Length(Result) > 0 then begin
      if not ForceDirectories(Result) then begin
         Result := ExtractFilePath(ParamStr(0));
      end;
   end;
end;

function TFireflyTestCase.CreateSimpleDummyFile(const Filename: string): boolean;
var
   sl: TStringList;
begin
   Result := True;
   try
      try
         sl := TStringList.Create;
         sl.Add('Hallo Welt');
         sl.Add('Hello World');
         sl.SaveToFile(Filename);
      finally
         sl.Free;
      end;
   except
      Result := False;
   end;
end;

function TFireflyTestCase.CreateDummyFile(const Filename: string; const Size: int64): boolean;
var
   fs: TFileStream;
   ms: TMemoryStream;
   b: byte;
   i: integer;
begin
   Result := True;
   try
      Randomize;
      ms := TMemoryStream.Create;
      try
         for i := 0 to Pred(4096 * 1024) do begin
            b := Random(256);
            ms.Write(b, 1);
         end;
         fs := TFileStream.Create(Filename, fmCreate);
         try
            while fs.Size < Size do begin
               ms.Seek(0, soFromBeginning);
               fs.CopyFrom(ms, ms.Size);
            end;
            if fs.Size > Size then begin
               fs.Size := Size;
            end;
         finally
            fs.Free;
         end;
      finally
         ms.Free;
      end;
   except
      Result := False;
   end;
end;

end.
