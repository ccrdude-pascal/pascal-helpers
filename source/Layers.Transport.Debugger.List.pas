{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Storage for Internet communication debug information.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-03-17  pk  ---  Created as part of connection debugger.
// *****************************************************************************
   )
}
unit Layers.Transport.Debugger.List;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Generics.Collections;

type

   { TLayeredTransportConnectionData }

   TLayeredTransportConnectionData = class
   private
      FData: TMemoryStream;
      FHeaders: TStringList;
      FParsedHeaders: TStringList;
      function GetContentType: string;
      function GetParsedHeaders: TStringList;
   public
      constructor Create;
      destructor Destroy; override;
      property Data: TMemoryStream read FData;
      property Headers: TStringList read FHeaders;
      property ParsedHeaders: TStringList read GetParsedHeaders;
      property ContentType: string read GetContentType;
   end;

   TLayeredTransportConnection = class
   private
      FMethod: string;
      FRequest: TLayeredTransportConnectionData;
      FResponse: TLayeredTransportConnectionData;
      FReturnCode: integer;
      FURL: string;
   public
      constructor Create;
      destructor Destroy; override;
      property Request: TLayeredTransportConnectionData read FRequest;
      property Response: TLayeredTransportConnectionData read FResponse;
   published
      property Code: integer read FReturnCode write FReturnCode;
      property Method: string read FMethod write FMethod;
      property URL: string read FURL write FURL;
   end;

   { TLayeredTransportConnectionList }

   TLayeredTransportConnectionList = class(TObjectList<TLayeredTransportConnection>)
   private
   var
      FListeners: TList<TNotifyEvent>;
   private
   class var FInstance: TLayeredTransportConnectionList;
   protected
      procedure NotifyListeners;
   public
      class constructor Create;
      class destructor Destroy;
      class function Instance: TLayeredTransportConnectionList;
   public
      constructor Create;
      destructor Destroy; override;
      function Add(const AValue: TLayeredTransportConnection): SizeInt; override;
      procedure Insert(AIndex: SizeInt; const AValue: TLayeredTransportConnection); override;
      procedure RegisterListener(AnEvent: TNotifyEvent);
   end;

implementation

procedure TLayeredTransportConnectionList.NotifyListeners;
var e: TNotifyEvent;
begin
   for e in FListeners do begin
      e(Self);
   end;
end;

class constructor TLayeredTransportConnectionList.Create;
begin
   FInstance := nil;
end;

class destructor TLayeredTransportConnectionList.Destroy;
begin
   FInstance.Free;
end;

class function TLayeredTransportConnectionList.Instance: TLayeredTransportConnectionList;
begin
   if not Assigned(FInstance) then begin
      FInstance := TLayeredTransportConnectionList.Create;
   end;
   Result := FInstance;
end;

constructor TLayeredTransportConnectionList.Create;
begin
   inherited Create(True);
   FListeners := TList<TNotifyEvent>.Create;
end;

destructor TLayeredTransportConnectionList.Destroy;
begin
   FListeners.Free;
   inherited Destroy;
end;

procedure TLayeredTransportConnectionList.RegisterListener(AnEvent: TNotifyEvent);
begin
   FListeners.Add(AnEvent);
end;

function TLayeredTransportConnectionList.Add(const AValue: TLayeredTransportConnection): SizeInt;
begin
   Result := inherited Add(AValue);
   NotifyListeners;
end;

procedure TLayeredTransportConnectionList.Insert(AIndex: SizeInt; const AValue: TLayeredTransportConnection);
begin
   inherited Insert(AIndex, AValue);
   NotifyListeners;
end;

{ TLayeredTransportConnection }

constructor TLayeredTransportConnection.Create;
begin
   FRequest := TLayeredTransportConnectionData.Create;
   FResponse := TLayeredTransportConnectionData.Create;
end;

destructor TLayeredTransportConnection.Destroy;
begin
   FRequest.Free;
   FResponse.Free;
   inherited Destroy;
end;

{ TLayeredTransportConnectionData }

function TLayeredTransportConnectionData.GetParsedHeaders: TStringList;
var
   i: integer;
   iPos: integer;
   sKey: string;
   sValue: string;
begin
   if Self.FParsedHeaders.Count <> Self.FHeaders.Count then begin
      Self.FParsedHeaders.Clear;
      for i := 0 to Pred(Self.FHeaders.Count) do begin
         sKey := Self.Headers[i];
         iPos := Pos(': ', sKey);
         if iPos > 0 then begin
            SetLength(sKey, iPos - 1);
            sValue := Self.Headers[i];
            Delete(sValue, 1, iPos + 1);
            Self.FParsedHeaders.Values[Trim(sKey)] := Trim(sValue);
         end;
      end;
   end;
   Result := Self.FParsedHeaders;
end;

function TLayeredTransportConnectionData.GetContentType: string;
var
   i: integer;
   iPos: integer;
begin
   Result := '';
   i := Self.ParsedHeaders.IndexOfName('Content-Type');
   if i < 0 then begin
      Exit;
   end;
   Result := Self.ParsedHeaders.ValueFromIndex[i];
   iPos := Pos(';', Result);
   if (iPos > 0) then begin
      SetLength(Result, Pred(iPos));
   end;
end;

constructor TLayeredTransportConnectionData.Create;
begin
   Self.FData := TMemoryStream.Create;
   Self.FHeaders := TStringList.Create;
   Self.FParsedHeaders := TStringList.Create;
end;

destructor TLayeredTransportConnectionData.Destroy;
begin
   Self.FData.Free;
   Self.FHeaders.Free;
   Self.FParsedHeaders.Free;
   inherited Destroy;
end;

end.
