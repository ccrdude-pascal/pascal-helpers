{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Registration and text display of licenses used by application.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2010-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-10-02  pk  10m  Moved to new open sourced FireFly package
// 2016-05-04  pk  10m  Moved to new package as Firefly.Credits.pas
// 2011-03-18  ##   --  Beautified code (using Code Beautifier Wizard)
// 2011-03-18  ##   --  Added this header (from Code Beautifier Wizard)
// *****************************************************************************
// Examples: see pre-filled Register* procedures
// *****************************************************************************
   )
}

unit Firefly.Credits;

interface

uses
   Classes;

type
   TVisibility = (vEndUser, vInternal);
   TLicenseType = (ltCustom, ltMPL, ltLGPL, ltBSD, ltBSDModified, ltFreeware, ltCCBY30, ltCCByNC30, slTrademark, ltSILOFL11, ltPublicDomain);

   TLicenseEntry = record
      Identifier: string;
      Visibility: TVisibility;
      LicenseType: TLicenseType;
      LicenseText: string;
      Link: string;
   end;

{
  Now default procedures. These are available here since we can't add this
  to external libraries or packages, and want it to appear uniform across
  our applications where we add it with the simple procedure call.
}

{
  Registers use of the package DelphiZip.
}
procedure RegisterDelZipLicense;

{
  Registers use of the package LKJSON.
}
procedure RegisterLKJSONLicense;

{
  Registers use of the MD5 algorithm.
}
procedure RegisterMD5License;

{
  Registers use of OpenSSL libraries.
}
procedure RegisterOpenSSLLicense;

{
  Registers use of the PCRE libraries.
}
procedure RegisterPCRELicense;

{
  Registers use of the package Synapse for Internet connectivity.
}
procedure RegisterSynapseLicense;

{
  Registers use of the UnRAR library.
}
procedure RegisterUnRARLicense;

{
  Registers use of the term iPhone.
}
procedure RegisterAppleiPhoneTrademark;

{
  Registers use of the term iPod touch.
}
procedure RegisterAppleiPodTouchTrademark;

{
  Registers use of the term iPad.
}
procedure RegisterAppleiPadTrademark;

{
  Registers use of the term iTunes.
}
procedure RegisterAppleiTunesTrademark;

{
  Registers a license.

  @param(AIdentifier   is a short, internal name of the licensed object.)
  @param(ALicenseText  is a text describing the license type.)
  @param(ALink         is a link pointing to licensed object.)
  @param(ALicenseType  allows to set one of a few widely spread standard licenses.)
  @param(AVisibility   determines the license is required to be shown to the end user.)
}
procedure RegisterLicense(AIdentifier, ALicenseText, ALink: string; ALicenseType: TLicenseType = ltCustom; AVisibility: TVisibility = vEndUser);

{
  Shows license text to the end user.
  @returns(License text for all licenses that should be visible to the end user, CRLF separated.)
}
function GetLicenseEndUserText(AnIncludeIdentifier: boolean = false): string;

{
  Shows license text to the end user as lines.

  @param(ALines  is a TStringlist that gets filled with licence texts.)
}
procedure ListLicenseEndUserText(const ALines: TStrings);

implementation

var
   GLicenseEntries: array of TLicenseEntry;

procedure RegisterAppleiPhoneTrademark;
begin
   RegisterLicense('iPhone', 'iPhone is trademark of Apple Inc., registered in the U.S. and other countries.', 'http://www.apple.com/legal/trademark/appletmlist.html', slTrademark, vEndUser);
end;

procedure RegisterAppleiPodTouchTrademark;
begin
   RegisterLicense('iPod touch', 'iPod touch is a trademark of Apple Inc., registered in the U.S. and other countries.', 'http://www.apple.com/legal/trademark/appletmlist.html',
      slTrademark, vEndUser);
end;

procedure RegisterAppleiPadTrademark;
begin
   RegisterLicense('iPad', 'iPad is a trademark of Apple Inc., registered in the U.S. and other countries.', 'http://www.apple.com/legal/trademark/appletmlist.html', slTrademark, vEndUser);
end;

procedure RegisterAppleiTunesTrademark;
begin
   RegisterLicense('iTunes', 'iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.', 'http://www.apple.com/legal/trademark/appletmlist.html', slTrademark, vEndUser);
end;

procedure RegisterDelZipLicense;
begin
   RegisterLicense('DelZip', 'Copyright (C) 2009, 2010 by Russell J. Peters, Roger Aelbrecht, Eric W. Engler and Chris Vleghert.', 'http://www.delphizip.org/zipmaster-190/license', ltLGPL, vEndUser);
end;

procedure RegisterLKJSONLicense;
begin
   RegisterLicense('uLkJSON', 'Copyright (c) 2006,2007,2008,2009 Leonid Koninin', 'http://sourceforge.net/projects/lkjson/', ltCustom, vEndUser);
end;

procedure RegisterMD5License;
begin
   RegisterLicense('MD5', 'This application uses the RSA Data Security, Inc. MD5 Message-Digest Algorithm.', 'http://www.ietf.org/rfc/rfc1321.txt', ltCustom, vEndUser);
end;

procedure RegisterOpenSSLLicense;
begin
   RegisterLicense('OpenSSL', 'Copyright (c) 1998-2005 The OpenSSL project. This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit (http://www.openssl.org/).',
      'http://www.openssl.org/related/binaries.html', ltCustom, vEndUser);
end;

procedure RegisterPCRELicense;
begin
   RegisterLicense('PCRE', 'Copyright (c) 1997-2010  University of Cambridge, (c) 2007-2010 Google Inc.', 'http://www.pcre.org/licence.txt', ltBSD, vEndUser);
end;

procedure RegisterSynapseLicense;
begin
   RegisterLicense('Synapse', 'Copyright (c) 1999-2010 Lukas Gebauer.', 'http://www.ararat.cz/synapse/doku.php/license', ltBSDModified, vEndUser);
end;

procedure RegisterUnRARLicense;
begin
   RegisterLicense('UnRAR', 'The unrar.dll library is freeware, copyright Alexander L. Roshal.', 'http://www.rarlab.com/rar_add.htm', ltFreeware, vEndUser);
end;

procedure RegisterLicense(AIdentifier, ALicenseText, ALink: string; ALicenseType: TLicenseType; AVisibility: TVisibility);
var
   iLicenseEntry, iFound: integer;
begin
   iFound := -1;
   for iLicenseEntry := 0 to Pred(Length(GLicenseEntries)) do begin
      if GLicenseEntries[iLicenseEntry].Identifier = AIdentifier then begin
         iFound := iLicenseEntry;
         break;
      end;
   end;
   if iFound < 0 then begin
      iLicenseEntry := Length(GLicenseEntries);
      SetLength(GLicenseEntries, Succ(iLicenseEntry));
      GLicenseEntries[iLicenseEntry].Identifier := AIdentifier;
      GLicenseEntries[iLicenseEntry].Visibility := AVisibility;
      GLicenseEntries[iLicenseEntry].LicenseType := ALicenseType;
      GLicenseEntries[iLicenseEntry].LicenseText := ALicenseText;
      GLicenseEntries[iLicenseEntry].Link := ALink;
   end;
end;

function GetLicenseEndUserText(AnIncludeIdentifier: boolean = false): string;
var
   i: integer;
begin
   Result := '';
   for i := 0 to Pred(Length(GLicenseEntries)) do begin
      if (GLicenseEntries[i].Visibility = vEndUser) then begin
         if AnIncludeIdentifier then begin
            Result += (GLicenseEntries[i].Identifier + ': ');
         end;
         Result += GLicenseEntries[i].LicenseText + #13#10;
      end;
   end;
end;

procedure ListLicenseEndUserText(const ALines: TStrings);
var
   i: integer;
begin
   for i := 0 to Pred(Length(GLicenseEntries)) do begin
      if (GLicenseEntries[i].Visibility = vEndUser) then begin
         ALines.Add(GLicenseEntries[i].Identifier + ': ' + GLicenseEntries[i].LicenseText);
      end;
   end;
end;

initialization

   SetLength(GLicenseEntries, 0);

finalization

   SetLength(GLicenseEntries, 0);

end.
