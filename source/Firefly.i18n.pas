﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Interface for localization of tools (gnugettext))

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-06-09  jh   --  Moved to new PepiMKBase package
// 2013-07-25  pk   5m  Added this header
// *****************************************************************************
   )
}

unit Firefly.i18n;

interface

uses
   Classes;

{$DEFINE UseGnuGetText}
{$IFDEF FPC}
{$UNDEF UseGnuGetText}
{$ENDIF FPC}

function _(const AMessageId: UnicodeString): UnicodeString; overload;
function _(const AMessageId: WideString): WideString; overload;
function _(const AMessageId: AnsiString): AnsiString; overload;

{
        Translates a Component

        @param(AObject: Component to translate)
        @param(ATextDomain:) 
}
procedure TranslateComponent(AObject: TComponent; const ATextDomain: string = '');

{
         ReTranslates a Component

        @param(nObject: Component to retranslate)
        @param(ATextDomain:) 
}
procedure ReTranslateComponent(nObject: TComponent; const ATextDomain: string = '');

{
        Sets Language to use

        @param(ALanguageCode: Language to use)        
}
procedure UseLanguage(ALanguageCode: string);

{
        Sets Language to use from registry
}
procedure UseLanguageFromRegistry;

{
        Gets current Language
        @returns(current Language)
}
function GetCurrentLanguage: string;


{
        Formats Local DateTime

        @param(Format: Format string)
        @param(DateTime: DateTime to format)
        @returns(formated DateTime)
}
function FormatLocalDateTime(const Format: string; DateTime: TDateTime): string;

{
        Adds a StringReplacer

        @param(AOriginal: String to replace)
        @param(ANew: replacer) 
}
procedure AddStringReplacer(AOriginal, ANew: WideString);

implementation

uses
   {$IF DEFINED(UseGnuGetText)}
   gnugettext,
   {$ELSEIF DEFINED(FPC)}
   //DefaultTranslator, // FPC fails on SDECon64
   LCLTranslator,
   LResources,
   //Translations,
   {$IFEND}
   {$IFDEF MSWindows}
   Registry,
   Windows, // for HKEY_ on non-FPC
   Controls,
   ComCtrls,
   CommCtrl,
   {$ENDIF MSWindows}
   SysUtils;

const
   SRegLocalesPath = '\Software\Safer Networking Limited\Localization\';

var
  FLocale: string;

type
   TSpecialTranslators = class
   private
   public
      class constructor Create;
      class destructor Destroy;
      {$IFDEF UseGnuGetText}
      procedure TranslateTListGroup(obj: TObject);
      procedure TranslateTListView(obj: TObject);
      {$ENDIF UseGnuGetText}
   end;

   { TReplacers }

   TReplacers = class
   private
      FList: TStringList;
   public
      class constructor Create;
      class destructor Destroy;
      constructor Create;
      destructor Destroy; override;
      function Replace(AText: WideString): WideString;
      procedure AddStringReplacer(AOriginal, ANew: WideString);
   end;

var
   SpecialTranslators: TSpecialTranslators;
   Replacers: TReplacers;

procedure AddStringReplacer(AOriginal, ANew: WideString);
begin
   Replacers.AddStringReplacer(AOriginal, ANew);
end;

function GetLocalizationBiDiMode: TBiDiMode;
{$IFNDEF FPC}
const
   MysticalUnicodeRTLBit = $08000000;
var
   // lcid: Cardinal;
   lsig: LOCALESIGNATURE;
{$ENDIF FPC}
begin
   Result := bdLeftToRight;
   try
      {$IFNDEF FPC}
      // Exit;
      // lcid := $040D;
      if GetLocaleInfoEx(PWideChar(WideString(GetCurrentLanguage)), LOCALE_FONTSIGNATURE, PWideChar(@lsig), SizeOf(lsig) div SizeOf(WideChar)) > 0 then begin
         // if GetLocaleInfoW(lcid, LOCALE_FONTSIGNATURE, PWideChar(@lsig), SizeOf(lsig) div SizeOf(WideChar)) > 0 then begin
         if (lsig.lsUsb[3] and MysticalUnicodeRTLBit) = MysticalUnicodeRTLBit then begin
            Result := bdRightToLeft;
         end;
      end;
      {$ENDIF FPC}
   except
      Result := bdLeftToRight;
   end;
end;

function StoreLocaleSetting(ALanguageCode: string): boolean;
{$IFDEF MSWindows}
var
   reg: TRegistry;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   try
      reg := TRegistry.Create;
      reg.RootKey := HKEY_CURRENT_USER;
      try
         if reg.OpenKey(SRegLocalesPath, True) then begin
            try
               reg.WriteString(ExtractFilePath(ParamStr(0)), ALanguageCode);
            finally
               Result := True;
               reg.CloseKey;
            end;
         end else begin
            Result := false;
         end;
      finally
         reg.Free;
      end;
   except
      Result := false;
   end;
   // trac:sd2:2642  pk  2013-07-23  write this to services account as well
   try
      reg := TRegistry.Create;
      reg.RootKey := HKEY_USERS;
      try
         if reg.OpenKey('\S-1-5-18' + SRegLocalesPath, True) then begin
            try
               reg.WriteString(ExtractFilePath(ParamStr(0)), ALanguageCode);
            finally
               Result := True;
               reg.CloseKey;
            end;
         end else begin
            Result := false;
         end;
      finally
         reg.Free;
      end;
   except
      // this can be ignerod if a non-admin changes language!
   end;
   {$ENDIF MSWindows}
end;

function GetSystemLocaleDefault: string; // trac:sd2:2642
{$IFNDEF FPC}
var
   pc: PWideChar;
   s: string;
   iSize, iStatus: integer;
   {$ENDIF FPC}
begin
   Result := 'en_IE';
   {$IFNDEF FPC}
   pc := nil;
   iSize := GetLocaleInfoW(LOCALE_USER_DEFAULT, LOCALE_SISO639LANGNAME, pc, 0);
   GetMem(pc, Succ(iSize) * SizeOf(WideChar));
   try
      iStatus := GetLocaleInfoW(LOCALE_USER_DEFAULT, LOCALE_SISO639LANGNAME, pc, iSize);
      if iStatus > 0 then begin
         s := pc;
         if DirectoryExists(ExtractFilePath(ParamStr(0)) + 'locale\' + s) then begin
            Result := s;
         end;
      end;
   finally
      FreeMem(pc);
   end;
   {$ENDIF FPC}
end;

function GetLocaleSetting(ADefault: string = 'en_US'): string;
{$IFDEF MSWindows}
var
   reg: TRegistry;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   Result := GetSystemLocaleDefault; // trac:sd2:2642
   try
      reg := TRegistry.Create;
      reg.RootKey := HKEY_CURRENT_USER;
      try
         if reg.OpenKeyReadOnly(SRegLocalesPath) then begin
            try
               Result := reg.ReadString(ExtractFilePath(ParamStr(0)));
            finally
               reg.CloseKey;
            end;
         end;
      finally
         reg.Free;
      end;
   except
   end;
   {$ELSE MSWindows}
   Result := ADefault;
   {$ENDIF MSWindows}
end;

function _(const AMessageId: UnicodeString): UnicodeString; overload;
begin
   {$IF DEFINED(UseGnuGetText)}
   Result := gnugettext._(AMessageId);
   {$ELSEIF DEFINED(FPC)}
   Result := AMessageId;
   {$IFEND}
   Result := Replacers.Replace(Result);
end;

function _(const AMessageId: WideString): WideString; overload;
begin
   {$IF DEFINED(UseGnuGetText)}
   Result := gnugettext._(AMessageId);
   {$ELSEIF DEFINED(FPC)}
   Result := AMessageId;
   {$IFEND}
   Result := Replacers.Replace(Result);
end;

function _(const AMessageId: AnsiString): AnsiString;
begin
   {$IF DEFINED(UseGnuGetText)}
   Result := gnugettext._(AMessageId);
   {$ELSEIF DEFINED(FPC)}
   Result := AMessageId;

   {$IFEND}
   Result := UTF8Encode(Replacers.Replace(UTF8Decode(Result)));
end;

procedure TranslateComponent(AObject: TComponent; const ATextDomain: string = '');
begin
   {$IFDEF UseGnuGetText}
   if AObject is TControl then begin
      TControl(AObject).BiDiMode := GetLocalizationBiDiMode;
   end;
   gnugettext.TranslateComponent(AObject, ATextDomain);
   {$ENDIF}
end;

procedure ReTranslateComponent(nObject: TComponent; const ATextDomain: string = '');
begin
   {$IFDEF UseGnuGetText}
   if nObject is TControl then begin
      TControl(nObject).BiDiMode := GetLocalizationBiDiMode;
   end;
   gnugettext.ReTranslateComponent(nObject, ATextDomain);
   {$ENDIF}
end;

function GetCurrentLanguage: string;
begin
   {$IF DEFINED(UseGnuGetText)}
   Result := gnugettext.GetCurrentLanguage;
   {$ELSEIF DEFINED(FPC)}
   Result := FLocale;
   {$IFEND}
end;

procedure UseLanguage(ALanguageCode: string);
begin
   {$IF DEFINED(UseGnuGetText)}
   gnugettext.UseLanguage(ALanguageCode);
   {$ELSEIF DEFINED(FPC)}
   LCLTranslator.SetDefaultLang(ALanguageCode);
   {$IFEND}
   StoreLocaleSetting(ALanguageCode);
   FLocale := ALanguageCode;
end;

procedure UseLanguageFromRegistry;
begin
   UseLanguage(GetLocaleSetting);
end;

{ TSpecialTranslators }

class constructor TSpecialTranslators.Create;
begin
   SpecialTranslators := TSpecialTranslators.Create;
end;

class destructor TSpecialTranslators.Destroy;
begin
   SpecialTranslators.Free;
   inherited;
end;

{$IFDEF UseGnuGetText}

procedure TSpecialTranslators.TranslateTListGroup(obj: TObject);
var
   lg: TListGroup;
   lgs: TListGroups;
   lv: TCustomListView;
   s: string;
begin
   // trac:sd2:1669  pk  Update for TListGroup localization!
   if obj is TListGroup then begin
      lg := TListGroup(obj);
      if not(lg.Collection is TListGroups) then begin
         Exit;
      end;
      lgs := TListGroups(lg.Collection);
      lv := lgs.Owner;
      lv.HandleNeeded;
      // s := _(lg.Header);
      s := DefaultInstance.dgettext(getcurrenttextdomain, lg.Header);
      lg.Header := s;
   end;
end;

procedure TSpecialTranslators.TranslateTListView(obj: TObject);
var
   iGroup: integer;
   lg: TListGroup;
   lgs: TListGroups;
   lv: TListView;
   s: string;
begin
   if obj is TListView then begin
      lv := TListView(obj);
      for iGroup := 0 to Pred(lv.Groups.Count) do begin
         lg := lv.Groups[iGroup];
         lgs := TListGroups(lg.Collection);
         lv := TListView(lgs.Owner);
         lv.HandleNeeded;
         // s := _(lg.Header);
         s := DefaultInstance.dgettext(getcurrenttextdomain, lg.Header);
         lg.Header := s;
      end;
   end;
end;

{$ENDIF UseGnuGetText}

var
   GCurrentFormatDateTimeLanguage: string;
   GCurrentFormatSettings: TFormatSettings;

function FormatLocalDateTime(const Format: string; DateTime: TDateTime): string;
var
   sCurrent: string;
begin
   sCurrent := GetCurrentLanguage;
   if Length(sCurrent) > 2 then begin
      sCurrent := Copy(sCurrent, 1, 2);
   end;
   if sCurrent <> GCurrentFormatDateTimeLanguage then begin
      GCurrentFormatDateTimeLanguage := sCurrent;
      {$IFNDEF FPC}
      GCurrentFormatSettings := TFormatSettings.Create(GCurrentFormatDateTimeLanguage);
      // trac:sd2:2041 - adjustment for russian
      {$ENDIF FPC}
      if (sCurrent = 'ru') or (sCurrent = 'ru_RU') then begin
         GCurrentFormatSettings.LongDateFormat := GCurrentFormatSettings.ShortDateFormat;
         // the following would adjust the character, but might be the wrong case [grammar]
         // GCurrentFormatSettings.LongDateFormat := StringReplace(GCurrentFormatSettings.LongDateFormat, '/', '.', [rfReplaceAll]);
      end;
   end;
   Result := FormatDateTime(Format, DateTime, GCurrentFormatSettings);
end;

{ TReplacers }

class constructor TReplacers.Create;
begin
   Replacers := TReplacers.Create;
end;

procedure TReplacers.AddStringReplacer(AOriginal, ANew: WideString);
begin
   {$IFDEF FPC}
   FList.Add(UTF8Encode(AOriginal) + '=' + UTF8Encode(ANew));
   {$ELSE FPC}
   FList.Add(AOriginal + '=' + ANew);
   {$ENDIF FPC}
end;

constructor TReplacers.Create;
begin
   FList := TStringList.Create;
end;

class destructor TReplacers.Destroy;
begin
   Replacers.Free;
   Replacers := nil;
end;

destructor TReplacers.Destroy;
begin
   FList.Free;
end;

function TReplacers.Replace(AText: WideString): WideString;
var
   i: integer;
begin
   Result := AText;
   for i := 0 to Pred(FList.Count) do begin
      {$IFDEF FPC}
      Result := WideStringReplace(Result, UTF8Decode(FList.Names[i]), UTF8Decode(FList.ValueFromIndex[i]), [rfReplaceAll]);
      {$ELSE FPC}
      Result := StringReplace(Result, FList.Names[i], FList.ValueFromIndex[i], [rfReplaceAll]);
      {$ENDIF FPC}
   end;
end;

initialization

   GCurrentFormatDateTimeLanguage := '';
   {$IFDEF UseGnuGetText}
   TP_GlobalHandleClass(TListGroup, SpecialTranslators.TranslateTListGroup);
   // TP_GlobalHandleClass(TListView, SpecialTranslators.TranslateTListView);
   {$ENDIF UseGnuGetText}
   UseLanguageFromRegistry;
   FLocale := GetCurrentLanguage;

end.
