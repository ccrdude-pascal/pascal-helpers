﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Solve string templates into system-specific paths.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2010-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-02-22  pk   5m  [CCD] Header update
// 2017-02-21  pk   5m  [CCD] Renamed from snlFilesPaths
// 2013-07-25  pk   5m  Added this header
// *****************************************************************************
// TODO : support for wrong drive letters, aka UpdateLocationTemplate in snlFilesPathsSolve.pas
// intended replacement for snlFilesPathsSolve.pas, which has grown more than we like.
// *****************************************************************************
   )
}

unit Firefly.FilePaths;

{ .$DEFINE AddFromUserTokens }
{$IFDEF SDPRE}
{$UNDEF AddFromUserTokens}
{$ENDIF SDPRE}
{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   SysUtils,
   Classes,
   Contnrs,
   {$IFDEF FPC}
   fgl,
   {$ELSE FPC}
   WideStrings,
   Generics.Collections,
   {$ENDIF FPC}
   {$IFDEF MSWindows}
   Windows,
   //Firefly.FilePaths.FillersWindows,
   {$ENDIF MSWindows}
   Firefly.FilePaths.FillersBase;

const
   SPathSource: array [TPathSource] of WideString = ('CLSID', 'WinAPI', 'Environment', 'Adjusted Copy', 'Custom');

type
   { TPathItem }

   TPathItem = class
   private
      FPath: WideString;
      FSources: TPathSources;
      function GetSourcesText: WideString;
   public
      property Path: WideString read FPath;
      property Sources: TPathSources read FSources write FSources;
      property SourcesText: WideString read GetSourcesText;
   end;

   { TPathList }

   TPathList = class(TObjectList)
   private
      FTemplateName: WideString;
      FShortcutName: WideString;
      function GetPath(AIndex: integer): TPathItem;
      procedure SetTemplateName(const Value: WideString);
      function GetBracketedTemplateName: WideString;
   public
      constructor Create; overload;
      function UpdatePath(APath: WideString; ASource: TPathSource): TPathItem;
      function SetPath(APath: WideString; ASource: TPathSource): TPathItem;
      property Path[AIndex: integer]: TPathItem read GetPath; default;
      property TemplateName: WideString read FTemplateName write SetTemplateName;
      property BracketedTemplateName: WideString read GetBracketedTemplateName;
      property ShortcutName: WideString read FShortcutName;
   end;

   TPathTemplates = class;

   {$IFDEF FPC}
   TPathTemplatesData = specialize TFPGMap<WideString, TPathList>;
   {$ELSE FPC}
   TPathTemplatesData = TDictionary<WideString, TPathList>;
   {$ENDIF FPC}

   { TPathTemplates }

   TPathTemplates = class
   private
      FData: TPathTemplatesData;
      function GetCount: integer;
      function GetPaths(ATemplateName: WideString): TPathList;
      function GetPathByIndex(AIndex: integer): TPathList;
      procedure RefreshDates;
      class var FInstance: TPathTemplates;
   protected
      procedure DoPathFound(ATemplate, APath: WideString; ASource: TPathSource); virtual;
   public
      class constructor Create;
      class destructor Destroy;
      constructor Create;
      destructor Destroy; override;
      procedure Clear;
      function Solve(ATemplatedPath: WideString; AIgnoreCase: boolean = True): WideString; overload;
      procedure Solve(ATemplatedPath: WideString; AList: TStrings; AIgnoreCase: boolean = True; AClearList: boolean = True); overload;
      function SolveEnvironment(APath: WideString): WideString;
      property Paths[ATemplateName: WideString]: TPathList read GetPaths;
      property PathByIndex[AIndex: integer]: TPathList read GetPathByIndex;
      procedure AddFinalize;
      procedure Add;
      property Count: integer read GetCount;
   end;


   function ExtractDriveFromRegKey(ATemplate: WideString): string; overload;
   function ExtractDriveFromRegKey(ATemplate: WideString; var ADrive: WideString): boolean; overload;
   function ExtractDriveFromTemplate(ATemplate: WideString; var ADrive: WideString): boolean; overload;
   function ExtractDriveFromTemplate(ATemplate: ansistring; var ADrive: ansistring): boolean; overload;

function PathTemplates: TPathTemplates;

implementation

uses
   Firefly.FilePaths.WinConsts; // just RTL

function ExtractDriveFromTemplate(ATemplate: WideString; var ADrive: WideString): boolean;
begin
   Result := Copy(ATemplate, 1, 3) = 'PE_';
   if Result then begin
      ADrive := Copy(ATemplate, 4, 1);
   end;
end;

function ExtractDriveFromTemplate(ATemplate: ansistring; var ADrive: ansistring): boolean; overload;
begin
   Result := Copy(ATemplate, 1, 3) = 'PE_';
   if Result then begin
      ADrive := Copy(ATemplate, 4, 1);
   end;
end;

function PathTemplates: TPathTemplates;
begin
  if not Assigned(TPathTemplates.FInstance) then begin
                                     TPathTemplates.FInstance := TPathTemplates.Create;
                                     TPathTemplates.FInstance.Add;
  end;
  Result := TPathTemplates.FInstance;
end;

function ExtractDriveFromRegKey(ATemplate: WideString): string;
begin
   if Copy(ATemplate, 1, 4) = '\PE_' then begin
      Result := Copy(String(ATemplate), 5, 1);
   end else begin
      Result := '';
   end;
end;

function ExtractDriveFromRegKey(ATemplate: WideString; var ADrive: WideString): boolean;
begin
   Result := Copy(ATemplate, 1, 4) = '\PE_';
   if Result then begin
      ADrive := Copy(ATemplate, 5, 1);
   end;
end;

{ TPathItem }

function TPathItem.GetSourcesText: WideString;
var
   ps: TPathSource;
begin
   Result := '';
   for ps := low(TPathSource) to high(TPathSource) do begin
      if ps in FSources then begin
         if Length(Result) > 0 then begin
            Result := Result + ', ';
         end;
         Result := Result + SPathSource[ps];
      end;
   end;
end;

{ TPathTemplates }

procedure TPathTemplates.AddFinalize;
const
   SQuicklaunchSuffix = '\Microsoft\Internet Explorer\Quick Launch';
var
   pl, plOut: TPathList;
   i: integer;
   //slProfiles: TStringList;
begin
   pl := Paths['APPDATA'];
   plOut := Paths['QUICKLAUNCH'];
   for i := 0 to Pred(pl.Count) do begin
      plOut.UpdatePath(ExcludeTrailingPathDelimiter(pl[i].Path) + SQuicklaunchSuffix, psCopy);
   end;
   (*
   plOut := Paths['MOZILLAPROFILE']; // trac:sd2:1618  -  add special templates for Mozilla profiles
   slProfiles := TStringList.Create;
   try
      for i := 0 to Pred(pl.Count) do begin
         DetectMozillaProfilePaths(pl[i].Path, slProfiles);
      end;
      for i := 0 to Pred(slProfiles.Count) do begin
         plOut.UpdatePath(ExcludeTrailingPathDelimiter(UTF8Decode(slProfiles[i])), psCopy);
      end;
   finally
      slProfiles.Free;
   end;
   *)
   pl := Paths['COMMONAPPDATA'];
   plOut := Paths['COMMONQUICKLAUNCH'];
   for i := 0 to Pred(pl.Count) do begin
      plOut.UpdatePath(ExcludeTrailingPathDelimiter(pl[i].Path) + SQuicklaunchSuffix, psCopy);
   end;
end;

procedure TPathTemplates.Add;
var
   cf: TCustomPathTemplatesFiller;
begin
   RefreshDates;
   for cf in TCustomPathTemplateFillers.Instance do begin
      cf.OnPathFound := {$IFDEF FPC}@{$ENDIF}DoPathFound;
      cf.Fill;
   end;
   AddFinalize;
end;

procedure TPathTemplates.Clear;
var
   i: integer;
   {$IFNDEF FPC}
   s: WideString;
   {$ENDIF FPC}
begin
   {$IFDEF FPC}
   for i := Pred(FData.Count) downto 0 do begin
      FData.Data[i].Free;
      FData.Data[i] := nil;
      FData.Delete(i);
   end;
   {$ELSE FPC}
   for s in FData.Keys do begin
      FData[s].Free;
   end;
   FData.Clear;
   {$ENDIF FPC}
end;

constructor TPathTemplates.Create;
begin
   FData := TPathTemplatesData.Create;
   {$IFDEF FPC}
   FData.Sorted := True;
   {$ENDIF FPC}
end;

class constructor TPathTemplates.Create;
begin
   FInstance := nil;
end;

class destructor TPathTemplates.Destroy;
begin
   FInstance.Free;
end;

destructor TPathTemplates.Destroy;
begin
   Clear;
   FData.Free;
end;

function TPathTemplates.GetPathByIndex(AIndex: integer): TPathList;
begin
   {$IFDEF FPC}
   Result := FData.Data[AIndex];
   {$ELSE FPC}
   Result := FData.Values.ToArray[AIndex];
   {$ENDIF FPC}
end;

function TPathTemplates.GetPaths(ATemplateName: WideString): TPathList;
var
   i: integer;
begin
   {$IFDEF FPC}
   if FData.Find(ATemplateName, i) then begin
      Result := FData.Data[i];
   end else begin
      Result := TPathList.Create;
      Result.TemplateName := ATemplateName;
      FData.Add(UpperCase(ATemplateName), Result);
   end;
   {$ELSE FPC}
   try
      Result := FData.Items[ATemplateName];
   except
      Result := nil;
   end;
   if not Assigned(Result) then begin
      Result := TPathList.Create;
      Result.TemplateName := ATemplateName;
      FData.Add(UpperCase(ATemplateName), Result);
   end;
   {$ENDIF FPC}
end;

function TPathTemplates.GetCount: integer;
begin
  Result := FData.Count;
end;

procedure TPathTemplates.RefreshDates;
begin
   Paths['DATE'].SetPath(WideString(FormatDateTime('yy-mm-dd', Now)), psCustom);
   Paths['TIME'].SetPath(WideString(FormatDateTime('hh-nn-ss', Now)), psCustom);
   Paths['DATETIME'].SetPath(WideString(FormatDateTime('yy-mm-dd_hh-nn-ss', Now)), psCustom);
end;

procedure TPathTemplates.DoPathFound(ATemplate, APath: WideString; ASource: TPathSource);
begin
   with Paths[UpperCase(ATemplate)] do begin
      UpdatePath(APath, ASource);
   end;
end;

procedure TPathTemplates.Solve(ATemplatedPath: WideString; AList: TStrings;
   AIgnoreCase: boolean; AClearList: boolean);
var
   i, iPath, iList: integer;
   pl: TPathList;
   rf: TReplaceFlags;
   sLine: WideString;
   {$IFNDEF FPC}
   s: WideString;
   {$ENDIF FPC}
begin
   RefreshDates;
   if AClearList then begin
      AList.Clear;
   end;
   {$IFDEF Unicode}
   AList.Add(ATemplatedPath);
   {$ELSE Unicode}
   AList.Add(UTF8Encode(ATemplatedPath));
   {$ENDIF Unicode}
   {$IFDEF FPC}
   for i := 0 to Pred(FData.Count) do begin
      pl := FData.Data[i];
   {$ELSE FPC}
   for s in FData.Keys do begin
      pl := FData.Items[s];
   {$ENDIF FPC}
      if pl.Count > 0 then begin
         rf := [rfReplaceAll];
         if AIgnoreCase then begin
            rf := rf + [rfIgnoreCase];
         end;
         for iList := Pred(AList.Count) downto 0 do begin
            {$IFDEF Unicode}
            sLine := AList[iList];
            {$ELSE Unicode}
            sLine := UTF8Decode(AList[iList]);
            {$ENDIF Unicode}
            if Pos(pl.BracketedTemplateName, sLine) > 0 then begin
               AList.Delete(iList);
               for iPath := 0 to Pred(pl.Count) do begin
                  {$IFDEF Unicode}
                  {$IFDEF FPC}
                  AList.Add(WideStringReplace(sLine, pl.BracketedTemplateName, pl[iPath].Path, rf));
                  {$ELSE FPC}
                  AList.Add(StringReplace(sLine, pl.BracketedTemplateName, pl[iPath].Path, rf));
                  {$ENDIF FPC}
                  {$ELSE Unicode}
                  AList.Add(UTF8Encode(WideStringReplace(sLine, pl.BracketedTemplateName, pl[iPath].Path, rf)));
                  {$ENDIF Unicode}
               end;
            end;
         end;
      end;
   end;
end;

function TPathTemplates.Solve(ATemplatedPath: WideString; AIgnoreCase: boolean): WideString;
var
   i: integer;
   pl: TPathList;
   rf: TReplaceFlags;
   {$IFNDEF FPC}
   s: WideString;
   {$ENDIF FPC}
begin
   RefreshDates;
   Result := ATemplatedPath;
   {$IFDEF FPC}
   for i := 0 to Pred(FData.Count) do begin
      pl := FData.Data[i];
      if pl.Count > 0 then begin
         rf := [rfReplaceAll];
         if AIgnoreCase then begin
            rf := rf + [rfIgnoreCase];
         end;
         Result := WideStringReplace(Result, pl.BracketedTemplateName, pl[0].Path, rf);
      end;
   end;
   {$ELSE FPC}
   for s in FData.Keys do begin
      pl := FData.Items[s];
      if pl.Count > 0 then begin
         rf := [rfReplaceAll];
         if AIgnoreCase then begin
            rf := rf + [rfIgnoreCase];
         end;
         Result := StringReplace(Result, pl.BracketedTemplateName, pl[0].Path, rf);
      end;
   end;
   {$ENDIF FPC}
end;

function TPathTemplates.SolveEnvironment(APath: WideString): WideString;

   function ExpandEnvVars(const AText: WideString): WideString;
      // http://delphidabbler.com/articles?article=6#expandenvvars
   var
      iBufferSize: integer;
   begin
      {$IFDEF MSWindows}
      iBufferSize := ExpandEnvironmentStringsW(PWideChar(AText), nil, 0);
      if iBufferSize > 0 then begin
         SetLength(Result, 2 * Pred(iBufferSize));
         ExpandEnvironmentStringsW(PWideChar(AText), PWideChar(Result), iBufferSize);
      end else begin
         Result := AText;
      end;
      {$ELSE MSWindows}
      Result := AText;
      {$ENDIF MSWindows}
   end;

begin
   Result := ExpandEnvVars(APath);
end;

{ TPathList }

constructor TPathList.Create;
begin
   inherited Create(True);
end;

function TPathList.GetBracketedTemplateName: WideString;
begin
   Result := '<$' + FTemplateName + '>';
end;

function TPathList.GetPath(AIndex: integer): TPathItem;
begin
   Result := TPathItem(Self.Items[AIndex]);
end;

function TPathList.SetPath(APath: WideString; ASource: TPathSource): TPathItem;
begin
   if Length(APath) = 0 then begin
      Result := nil;
      Exit;
   end;
   APath := ExcludeTrailingPathDelimiter(APath);
   if Self.Count < 1 then begin
      Result := TPathItem.Create;
      Result.FPath := APath;
      Result.FSources := [ASource];
      Self.Add(Result);
   end;
   Result := Self[0];
   Result.FPath := APath;
   Result.FSources := [ASource];
end;

procedure TPathList.SetTemplateName(const Value: WideString);
var
   i: integer;
begin
   FTemplateName := Value;
   {$IFDEF MSWindows}
   for i := low(PathSolveConfigs) to high(PathSolveConfigs) do begin
      if WideCompareText(PathSolveConfigs[i].TemplateName, Value) = 0 then begin
         FShortcutName := PathSolveConfigs[i].TemplateShortcut;
      end;
   end;
   {$ENDIF MSWindows}
end;

function TPathList.UpdatePath(APath: WideString; ASource: TPathSource): TPathItem;
var
   i: integer;
begin
   if Length(APath) = 0 then begin
      Result := nil;
      Exit;
   end;
   APath := ExcludeTrailingPathDelimiter(APath);
   for i := 0 to Pred(Self.Count) do begin
      if WideCompareText(Self.Path[i].Path, APath) = 0 then begin
         Self.Path[i].Sources := Self.Path[i].Sources + [ASource];
         Result := Self.Path[i];
         Exit;
      end;
   end;
   Result := TPathItem.Create;
   Result.FPath := APath;
   Result.FSources := [ASource];
   Self.Add(Result);
end;

end.
