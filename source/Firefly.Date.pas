﻿{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Date handling and string conversions.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-04  pk  30m  Documented and sorted
// 2011-06-17  pk  10m  Code Audit
// 2009-09-29  pk   --  Added this header (from Code Test Browser)
// 2007-04-05  pk  10m  FileTimeToDateTime for FPC
// 2007-01-30  pk  10m  Moved SchaltJahr, DayOfJahr, DaysOfYear, DaysSinceYear from snlNumbers
// *****************************************************************************
   )
}

unit Firefly.Date;

{$ifdef FPC}
{$mode Delphi}{$H+}
{$modeswitch TypeHelpers}
{$codepage utf8}
{$endif FPC}

interface

uses
   SysUtils,
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   Types,
   Classes;

type
   { TDateTimeHelper }

   TDateTimeHelper = type helper for TDateTime
      procedure FromUnixTime(AUnixTime: int64);
   end;

// RFC 822
{
  Converts a string following rfc 822 to a TDateTime.

  @param  ADateTime  String representing date/time.
  @return  TDateTime.
}
function RFC2822ToDateTime(ADateText: string): TDateTime;
{
  Converts a DateTime to a string following rfc 822

  @param  ADateTime  Date/time to convert.
  @return  rfc 822 String
}
function DateTimeToRFC822(ADateTime: TDateTime): string;
// .
// RFC 5545 (ISO 8601/2004)
{
  Converts a TDateTime according to a rfc 5545 (ISO 8601-2004) string.

  @param  ADateTime  Date/time to convert.
  @return  Text representation.
}
function ISO_8601_2004_DateTime(ADateTime: TDateTime; AUTC: boolean = false): string; overload;
{
  Converts a string following rfc 5545 (ISO 8601-2004) to a TDateTime.

  @param  ADateTime  String representing date/time.
  @return  TDateTime.
}
function ISO_8601_2004_DateTime(ADateText: string; AUTC: boolean = false): TDateTime; overload;

// Trac
{
  Converts a TDateTime to a format as used by Trac.

  @param  ADateTime  Date/time to convert.
  @return  Text representation.
}
function Trac_DateTime(ADateTime: TDateTime; AUTC: boolean = false): string;

// ARJ
{
  Converts ARJ date/time format cardinal to TDateTime

  @param AARJTime  Date/time parameter from ARJ file header.
  @return A regular TDateTime.
}
function ConvertARJTimeToDateTime(AARJTime: cardinal): TDateTime;

// Linux
{
  Converts a TDateTime to Linux representation.

  @param  ADateTime  Date/time to convert.
  @return  Linux representation.
}
function ConvertDateTimeToUNIXTimeRaw(PT: TDateTime): cardinal;
{
  Converts a Linux representation of a date/time to a TDateTime.

  @param  ADateTime  Linux representation of date/time.
  @return  TDateTime.
}
function ConvertUNIXTimeToDateTimeRaw(ADateTime: Double): TDateTime;
{$IFDEF MSWindows}
{
  Converts a Linux representation of a date/time to a TDateTime, respecting daylight saving time and timezone.

  @param  ADateTime  Linux representation of date/time.
  @return  TDateTime.
}
function ConvertUNIXTimeToDateTime(ADateTime: Double): TDateTime;
{
  Converts a TDateTime to Linux representation, respecting daylight saving time and timezone.

  @param  ADateTime  Date/time to convert.
  @return  Linux representation.
}
function ConvertDateTimeToUNIXTime(ADateTime: TDateTime): cardinal;
{$ENDIF MSWindows}
// .
// Apple
{
  Converts a Apple representation of a date/time to a TDateTime.

  @param  ADateTime  Apple representation of date/time.
  @return  TDateTime.
}
function ConvertAppleTimeToDateTimeRaw(ADateTime: Double): TDateTime;

// Windows
{
  Converts a Windows file time to TDateTime

  @param  AFileTime  File time from file system.
  @return  TDateTime.
}
function FileTimeToDateTime(AFileTime: uint64): TDateTime; overload;
function FileTimeToDateTime(AFileTime: int64): TDateTime; overload;
{
  Converts a Windows file time to TDateTime

  @param  AFileTime  File time from file system.
  @return  TDateTime.
}
function FileTimeToDateTime(AFileTime: TFileTime): TDateTime; overload;
{
  Converts a TDateTime to a Windows filesystem file time.

  @param  AFileTime  Date/time.
  @return  TFileTime representation.
}
function DateTimeToFileTime(ADateTime: TDateTime): TFileTime;
{$IFDEF MSWindows}
function GetWindowsInstallationDate(out ADate: TDateTime; ATry64Bit: boolean = false): boolean;
{
  Tries to find out the days the Windows installation is old.

  @param  ADays  Number of days since Windows was installed.
  @return  True if successful.
}
function GetWindowsInstallationDateDays(var ADays: cardinal): boolean;
{$ENDIF MSWindows}
{
  Converts a Kernel date/time to standard TDateTime

  @param  AKernelDateTime      Date/time as used by the kernel.
  @return  Standard TDateTime
}
function KernelDateTimeToDateTime(const AKernelDateTime: int64): TDateTime;
{
  Tries to find out the installation date of Windows.

  @param  ADate      The date and time, if found.
  @param  ATry64Bit  If set, this is tested for 64 bit only.
  @return  True if successful.
}
// .
// DOS
{$IFDEF MSWindows}
{
  Converts a DOS file age to standard TDateTime

  @param  ADate      Date as used by DOS.
  @param  ATime      Time as used by DOS.
  @return  Standard TDateTime
}
function DosFileAgeToDateTime(const ADate, ATime: word): TDateTime;
{$ENDIF MSWindows}
// SNL
{
  Converts a date/time to a standard SNL format (yyyy-mm-dd).

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function SNLDateOrNA(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a standard SNL format (yyyy-mm-dd), or n/a as default.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function SNLDateTimeOrNA(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a standard SNL format (yyyy-mm-dd), or zeros as default.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function SNLDateOrZeroes(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a standard SNL format (yyyy-mm-dd hh:nn:ss), or zeros as default.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function SNLDateTimeOrZeroes(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a standard SNL format (yyyy-mm-dd hh:nn), or n/a as default.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function SNLDateTimeShortOrNA(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a format used in filenames (yyyy-mm-dd_hh-nn-ss).

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function SNLDateTimeForFilename(const ATimestamp: TDateTime): string;

{
  Returns the first day of the week, according to the Windows locale.
}
function pkGetFirstDayOfWeek: integer;
{
  Checks whether a certain year is a leap year.

  @param  AYear  Year to check.
  @return  Days more than 365.
}
function SchaltJahr(const AYear: integer): integer;
{
  Tests how many days have passed since the first day of the year.

  @return  Day of the year.
}
function DayOfYear: integer;
{
  Tests how many days a year has.

  @param  AYear  Year to test.
  @return  Number of days.
}
function DaysOfYear(AYear: word): integer;
{
  Tests how many days have passed since a certain year started.

  @param  AYear  Year to test.
  @return  Number of days.
}
function DaysSinceYear(AYear: word): longint;
{
  Converts text in international format (yyyy?mm?dd hh?nn?ss) to TDateTime.

  @param  ADateText  Text representation of date/time.
  @return A regular TDateTime.
}
function IntlDateToDateTime(ADateText: string): TDateTime;
{
  Converts text in international format (yyyymmdd) to TDate.

  @param  ADateText  Text representation of date/time.
  @return A regular TDateTime.
}
function EightLetterDateToDateTime(ADateText: string): TDate;
{
  Converts text in international format (yyyymmdd-hhnnss) to TDateTime.

  @param  ADateText  Text representation of date/time.
  @return A regular TDateTime.
}
function EightLetterDateTimeToDateTime(ADateText: string): TDateTime;
{
  Converts a date/time to a time, if today, or a date.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function DateOrTodaysTime(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a different text based on its age.

  @param  APattern  USed for adding in date/time representation and unit name.
  @returns(Text representation.)
}
function DurationInHoursMinutesOrSeconds(APattern: string; const ATimestamp: TDateTime): string;
{
  Converts a date/time to a locale long standard format (yyyy-mm-dd), or n/a as default.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function DateTimeOrNA(const ATimestamp: TDateTime): string;
{
  Converts a date/time to a number of days plus time.

  @param  ATimestamp  A regular TDateTime.
  @returns(Text representation.)
}
function DaysAndTime(const ATimestamp: TDateTime): string;

implementation

{$IFDEF MSWindows}

var
   DaylightSavings: boolean;
   TZInfo: TTimeZoneInformation;
   {$ENDIF MSWindows}

function RFC2822ToDateTime(ADateText: string): TDateTime;
begin
   Result := 0; // TODO : implement RFC 822 date conversion
end;


function DateTimeToRFC822(ADateTime: TDateTime): string;
   function IntTo0Str(i, cnt: integer): string;
   begin
      Result := IntToStr(i);
      while Length(Result) < cnt do begin
         Result := '0' + Result;
      end;
   end;
   {$IFDEF MSWindows}
   function BiasToString(tzi: TTimeZoneInformation): string;
   var
      minutes, hs, ms: longint;
   begin
      (* case ret of
        TIME_ZONE_ID_STANDARD: Result := tzi.StandardName;
        TIME_ZONE_ID_DAYLIGHT: Result := tzi.DaylightName;
        TIME_ZONE_ID_UNKNOWN: begin *)
      minutes := tzi.Bias;
      if minutes = 0 then begin
         Result := 'GMT'
      end else begin
         if minutes < 0 then begin
            Result := '+';
         end else begin
            Result := '-';
         end;
         minutes := Abs(minutes);
         hs := minutes div 60;
         ms := minutes - (hs * 60);
         Result := Result + IntTo0Str(hs, 2) + IntTo0Str(ms, 2);
      end;
      (* end;
        end; *)
   end;
   {$ENDIF MSWindows}

const
   enMonthShort: array [1 .. 12] of string  = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
   enWeekdayShort: array [1 .. 7] of string = ('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
var
   {$IFDEF MSWindows}
   tzi: TTimeZoneInformation;
   {$ENDIF MSWindows}
   Year, month, day: word;
   hour, min, sec, msec: word;
begin
   DecodeDate(ADateTime, Year, month, day);
   DecodeTime(ADateTime, hour, min, sec, msec);
   {$IFDEF MSWindows}
   ZeroMemory(@tzi, SizeOf(tzi));
   {$IFDEF FPCWinCE}
   GetTimeZoneInformation(@tzi);
   {$ELSE FPCWinCE}
   GetTimeZoneInformation(tzi);
   {$ENDIF FPCWinCE}
   {$ENDIF MSWindows}
   Result := enWeekdayShort[DayOfWeek(ADateTime)] + ', ' + IntTo0Str(day, 2) + ' ' + enMonthShort[month] + ' ' + IntTo0Str(Year, 4) + ' ' + IntTo0Str(hour, 2) + ':' + IntTo0Str(min, 2) + ':' + IntTo0Str(sec, 2) + ' ';
   {$IFDEF MSWindows}
   Result := Result + BiasToString(tzi);
   {$ENDIF MSWindows}
end;

function ISO_8601_2004_DateTime(ADateTime: TDateTime; AUTC: boolean = false): string;
begin
   // http://tools.ietf.org/html/rfc5545#section-3.3.5
   {$IFDEF MSWindows}
   if AUTC then begin
      // TZInfo.Bias are minutes
      ADateTime := ADateTime + TZInfo.Bias / 60 / 24;
   end;
   {$ENDIF MSWindows}
   Result := FormatDateTime('yyyymmdd', ADateTime) + 'T' + FormatDateTime('hhnnss', ADateTime);
   if AUTC then begin
      Result := Result + 'Z';
   end;
end;

function ISO_8601_2004_DateTime(ADateText: string; AUTC: boolean = false): TDateTime; overload;
var
   dwYear, dwMonth, dwDay: word;
   dwHour, dwMinute, dwSecond: word;
begin
   // 1    6  8
   // 2013-06-18T09:34:45
   dwYear := StrToIntDef(Copy(ADateText, 1, 4), 1899);
   dwMonth := StrToIntDef(Copy(ADateText, 6, 2), 1);
   dwDay := StrToIntDef(Copy(ADateText, 9, 2), 1);
   Delete(ADateText, 1, 11);
   if Length(ADateText) > 1 then begin
      dwHour := StrToIntDef(Copy(ADateText, 1, 2), 0);
      Delete(ADateText, 1, 3);
   end
   else dwHour := 0;
   if Length(ADateText) > 1 then begin
      dwMinute := StrToIntDef(Copy(ADateText, 1, 2), 0);
      Delete(ADateText, 1, 3);
   end
   else dwMinute := 0;
   if Length(ADateText) > 1 then begin
      dwSecond := StrToIntDef(Copy(ADateText, 1, 2), 0);
   end
   else dwSecond := 0;
   Result := EncodeDate(dwYear, dwMonth, dwDay) + EncodeTime(dwHour, dwMinute, dwSecond, 0);
end;

function Trac_DateTime(ADateTime: TDateTime; AUTC: boolean = false): string;
begin
   {$IFDEF MSWindows}
   if AUTC then begin
      // TZInfo.Bias are minutes
      ADateTime := ADateTime + TZInfo.Bias / 60 / 24;
   end;
   {$ENDIF MSWindows}
   Result := FormatDateTime('yyyy-mm-dd', ADateTime) + 'T' + FormatDateTime('hh:nn:ss', ADateTime);
   if AUTC then begin
      Result := Result + 'Z';
   end;
end;

function ConvertARJTimeToDateTime(AARJTime: cardinal): TDateTime;
var
   wHour, wMinute, wSec, wHSec: word;
   wYear, wMonth, wDay: word;
begin
   (*
     Time stamp format:
     31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16
     |<---- year-1980 --->|<- month ->|<--- day ---->|
     15 14 13 12 11 10  9  8  7  6  5  4  3	2  1  0
     |<--- hour --->|<---- minute --->|<- second/2 ->|
   *)
   wYear := ((AARJTime shr 25) and 127) + 1980;
   wMonth := (AARJTime shr 21) and 15;
   wDay := (AARJTime shr 16) and 31;
   wHour := (AARJTime shr 11) and 31;
   wMinute := (AARJTime shr 5) and 63;
   wSec := (AARJTime and 31) div 2;
   wHSec := 0;
   if ((AARJTime and 31) mod 2) > 0 then begin
      wHSec := 500;
   end;
   Result := EncodeDate(wYear, wMonth, wDay) + EncodeTime(wHour, wMinute, wSec, wHSec);
end;

function ConvertDateTimeToUNIXTimeRaw(PT: TDateTime): cardinal;
begin
   Result := Round((PT - 25569) * 86400);
end;

function ConvertUNIXTimeToDateTimeRaw(ADateTime: Double): TDateTime;
begin
   Result := ADateTime / 86400 + 25569;
end;

{$IFDEF MSWindows}
function ConvertUNIXTimeToDateTime(ADateTime: Double): TDateTime;
begin
   if DaylightSavings then begin
      Result := ADateTime / 86400 + 25569 - ((TZInfo.Bias + TZInfo.DaylightBias) / 1440)
   end else begin
      Result := ADateTime / 86400 + 25569 - (TZInfo.Bias / 1440)
   end;
end;
{$ENDIF MSWindows}

{$IFDEF MSWindows}
function ConvertDateTimeToUNIXTime(ADateTime: TDateTime): cardinal;
begin
   if DaylightSavings then begin
      Result := Round((ADateTime - 25569 + ((TZInfo.Bias + TZInfo.DaylightBias) / 1440)) * 86400)
   end else begin
      Result := Round((ADateTime - 25569 + ((TZInfo.Bias) / 1440)) * 86400);
   end;
end;
{$ENDIF MSWindows}

function ConvertAppleTimeToDateTimeRaw(ADateTime: Double): TDateTime;
begin
   Result := ConvertUNIXTimeToDateTimeRaw(ADateTime + (31 * 365.25 * 24 * 60 * 60));
end;

function FileTimeToDateTime(AFileTime: TFileTime): TDateTime;
var
   LocalFileTime: TFileTime;
   NewTimeVar: int64;
   st: TSystemTime;
begin
   if (AFileTime.dwLowDateTime = 0) and (AFileTime.dwHighDateTime = 0) then begin
      Result := 0;
      Exit;
   end;
   try
      {$IFDEF MSWindows}
      {$IFDEF FPCWinCE}
      FileTimeToLocalFileTime(@ft, @LocalFileTime);
      {$ELSE FPCWinCE}
      FileTimeToLocalFileTime(AFileTime, LocalFileTime);
      {$ENDIF FPCWinCE}
      {$IFNDEF FPCWinCE}
      if not FileTimeToDosDateTime(LocalFileTime, LongRec(NewTimeVar).Hi, LongRec(NewTimeVar).Lo) then begin
         NewTimeVar := 0;
      end;
      {$ELSE FPCWinCE}
      NewTimeVar := 0;
      {$ENDIF FPCWinCE}
      {$ELSE MSWindows}
      NewTimeVar := AFiletime.dwLowDateTime + (AFiletime.dwHighDateTime shl 32);
      {$ENDIF MSWindows}
      Result := FileDateToDateTime(NewTimeVar);
   except
      Result := 0;
   end;
end;

function FileTimeToDateTime(AFileTime: uint64): TDateTime; overload;
var
   ft: TFileTime;
begin
   ft.dwLowDateTime := AFileTime and $FFFFFFFFFFFFFFFF;
   ft.dwHighDateTime := AFileTime shr 32;
   Result := FileTimeToDateTime(ft);
end;

function FileTimeToDateTime(AFileTime: int64): TDateTime; overload;
var
   ft: TFileTime;
begin
   ft.dwLowDateTime := AFileTime and $FFFFFFFFFFFFFFFF;
   ft.dwHighDateTime := AFileTime shr 32;
   Result := FileTimeToDateTime(ft);
end;

function DateTimeToFileTime(ADateTime: TDateTime): TFileTime;
var
   wDate, wTime: word;
   iDT: int64;
begin
   iDT := DateTimeToFileDate(ADateTime);
   {$IFDEF MSWindows}
   wDate := HiWord(iDT);
   wTime := LoWord(iDT);
   DosDateTimeToFileTime(wDate, wTime, Result);
   {$ELSE MSWindows}
   Result.dwLowDateTime := iDT and $FFFFFFFFFFFFFFFF;
   Result.dwHighDateTime := iDT shr 32;
   {$ENDIF MSWindows}
end;

{$IFDEF MSWindows}

function GetWindowsInstallationDate(out ADate: TDateTime; ATry64Bit: boolean): boolean;
const
   KEY_WOW64_64KEY = $0100;
var
   h: HKEY;
   dwType, cOptions: cardinal;
   dwData, dwDataSize: DWord;
   p: PByte;
   iResult: integer;
begin
   // trac:sd2:822 created for SDPrepPos to create allow list only on new systems
   cOptions := KEY_READ;
   if ATry64Bit then begin
      cOptions := cOptions or KEY_WOW64_64KEY;
   end;
   h := 0;
   iResult := RegOpenKeyEx(HKEY_LOCAL_MACHINE, 'SOFTWARE\Microsoft\Windows NT\CurrentVersion\', 0, cOptions, h);
   Result := (iResult = ERROR_SUCCESS);
   if Result then begin
      dwType := REG_DWORD;
      dwDataSize := 4;
      p := @dwData;
      iResult := RegQueryValueEx(h, 'InstallDate', nil, @dwType, p, @dwDataSize);
      Result := (iResult = ERROR_SUCCESS);
      if Result then begin
         ADate := ConvertUNIXTimeToDateTimeRaw(dwData);
      end;
      RegCloseKey(h);
   end;
   if ((not Result) or (dwData = 0)) and (not ATry64Bit) then begin
      Result := GetWindowsInstallationDate(ADate, true);
   end;
end;

function GetWindowsInstallationDateDays(var ADays: cardinal): boolean;
var
   dt: TDateTime;
begin
   // trac:sd2:822 created for SDPrepPos to create allow list only on new systems
   Result := GetWindowsInstallationDate(dt);
   if Result then begin
      ADays := Round(Now - dt);
   end;
end;

{$ENDIF MSWindows}

function KernelDateTimeToDateTime(const AKernelDateTime: int64): TDateTime;
begin
   Result := FileTimeToDateTime(TFileTime(AKernelDateTime));
end;

function pkGetFirstDayOfWeek: integer;
{$IFDEF MSWindows}
var
   dwSize, ec: DWord;
   pc: PAnsiChar;
   {$ENDIF MSWindows}
begin
   Result := 0;
   {$IFDEF MSWindows}
   dwSize := 0;
   pc := nil;
   dwSize := GetLocaleInfoA(LOCALE_USER_DEFAULT, LOCALE_IFIRSTDAYOFWEEK, pc, dwSize);
   GetMem(pc, dwSize);
   try
      ec := GetLocaleInfoA(LOCALE_USER_DEFAULT, LOCALE_IFIRSTDAYOFWEEK, pc, dwSize);
      if (ec = 2) then begin
         if pc = '0' then Result := 0
         else if pc = '1' then Result := 1
         else if pc = '2' then Result := 2
         else if pc = '3' then Result := 3
         else if pc = '4' then Result := 4
         else if pc = '5' then Result := 5
         else if pc = '6' then Result := 6;
      end;
   finally
      FreeMem(pc);
   end;
   {$ENDIF MSWindows}
end;

function DosFileAgeToDateTime(const ADate, ATime: word): TDateTime;
var
   wYear, wMonth, wDay, wHour, wMinute: word;
begin
   // this is kind of an emulation of the good old PackTime stuff...
   // YYYYYYY MMMM DDDDD
   // 7   +  4 +  5   = 16
   // hhhhh mmmmmm bbbbb
   // 5  +  6   +  5   = 16
   wYear := ADate shr 9 and $007F + 1980;
   wMonth := ADate shr 5 and $000F;
   wDay := ADate and $001F;
   wHour := ATime shr 11 and $003F;
   wMinute := ATime shr 5 and $003F;
   Result := EncodeDate(wYear, wMonth, wDay) + EncodeTime(wHour, wMinute, 0, 0);
end;

function SchaltJahr(const AYear: integer): integer;
begin
   if AYear mod 4 = 0 then begin
      if AYear mod 100 = 0 then begin
         if AYear mod 400 = 0 then begin
            Result := 1;
         end else begin
            Result := 0;
         end;
      end else begin
         Result := 1;
      end;
   end else begin
      Result := 0;
   end;
end;

function DayOfYear: integer;
var
   wYear, wMonth, wDay: word;
const
   iDayRemainings: array [1 .. 12] of integer = (1, -1, 0, 0, 1, 1, 2, 3, 3, 4, 4, 5);
begin
   DecodeDate(Now, wYear, wMonth, wDay);
   Result := wDay + (wMonth - 1) * 30 + iDayRemainings[wMonth - 1] + SchaltJahr(wYear);
end;

function DaysOfYear(AYear: word): integer;
begin
   Result := 365 + SchaltJahr(AYear);
end;

function DaysSinceYear(AYear: word): longint;
var
   wYear, wMonth, wDay: word;
   iYear: integer;
   iResult: longint;
begin
   DecodeDate(Now, wYear, wMonth, wDay);
   iResult := 0;
   if wYear > AYear then begin
      for iYear := AYear to wYear - 1 do begin
         Inc(iResult, DaysOfYear(iYear));
      end;
   end;
   Inc(iResult, DayOfYear);
   Result := iResult;
end;

function IntlDateToDateTime(ADateText: string): TDateTime;
var
   dwYear, dwMonth, dwDay: word;
   dwHour, dwMinute, dwSecond: word;
begin
   dwYear := StrToIntDef(Copy(ADateText, 1, 4), 1899);
   dwMonth := StrToIntDef(Copy(ADateText, 6, 2), 1);
   dwDay := StrToIntDef(Copy(ADateText, 9, 2), 1);
   Delete(ADateText, 1, 11);
   if Length(ADateText) > 1 then begin
      dwHour := StrToIntDef(Copy(ADateText, 1, 2), 0);
      Delete(ADateText, 1, 3);
   end else begin
      dwHour := 0;
   end;
   if Length(ADateText) > 1 then begin
      dwMinute := StrToIntDef(Copy(ADateText, 1, 2), 0);
      Delete(ADateText, 1, 3);
   end else begin
      dwMinute := 0;
   end;
   if Length(ADateText) > 1 then begin
      dwSecond := StrToIntDef(Copy(ADateText, 1, 2), 0);
   end else begin
      dwSecond := 0;
   end;
   Result := EncodeDate(dwYear, dwMonth, dwDay) + EncodeTime(dwHour, dwMinute, dwSecond, 0);
end;

function EightLetterDateToDateTime(ADateText: string): TDate;
var
   dwYear, dwMonth, dwDay: word;
begin
   dwYear := StrToIntDef(Copy(ADateText, 1, 4), 1899);
   dwMonth := StrToIntDef(Copy(ADateText, 5, 2), 1);
   dwDay := StrToIntDef(Copy(ADateText, 7, 2), 1);
   Result := EncodeDate(dwYear, dwMonth, dwDay);
end;

function EightLetterDateTimeToDateTime(ADateText: string): TDateTime;
var
   dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond: word;
begin
   dwYear := StrToIntDef(Copy(ADateText, 1, 4), 1899);
   dwMonth := StrToIntDef(Copy(ADateText, 5, 2), 1);
   dwDay := StrToIntDef(Copy(ADateText, 7, 2), 1);
   dwHour := StrToIntDef(Copy(ADateText, 10, 2), 0);
   dwMinute := StrToIntDef(Copy(ADateText, 12, 2), 0);
   dwSecond := StrToIntDef(Copy(ADateText, 14, 2), 0);
   Result := EncodeDate(dwYear, dwMonth, dwDay) + EncodeTime(dwHour, dwMinute, dwSecond, 0);
end;

function SNLDateOrNA(const ATimestamp: TDateTime): string;
begin
   if ATimestamp = 0 then begin
      Result := 'n/a';
   end else begin
      Result := FormatDateTime('yyyy-mm-dd', ATimestamp);
   end;
end;

function SNLDateTimeOrNA(const ATimestamp: TDateTime): string;
begin
   if ATimestamp = 0 then begin
      Result := 'n/a';
   end else begin
      Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', ATimestamp);
   end;
end;

function DateTimeOrNA(const ATimestamp: TDateTime): string;
begin
   if ATimestamp = 0 then begin
      Result := 'n/a';
   end else begin
      Result := FormatDateTime('c', ATimestamp);
   end;
end;

function DaysAndTime(const ATimestamp: TDateTime): string;
begin
   Result := IntToStr(Trunc(ATimestamp)) + ' days, ' + FormatDateTime('hh:nn:ss', ATimestamp);
end;

function SNLDateTimeForFilename(const ATimestamp: TDateTime): string;
begin
   Result := FormatDateTime('yyyy-mm-dd_hh-nn-ss', ATimestamp);
end;

function SNLDateOrZeroes(const ATimestamp: TDateTime): string;
begin
   if ATimestamp = 0 then begin
      Result := '0000-00-00';
   end else begin
      Result := FormatDateTime('yyyy-mm-dd', ATimestamp);
   end;
end;

function SNLDateTimeOrZeroes(const ATimestamp: TDateTime): string;
begin
   if ATimestamp = 0 then begin
      Result := '0000-00-99 00:00:00';
   end else begin
      Result := FormatDateTime('yyyy-mm-dd hh:nn:ss', ATimestamp);
   end;
end;

function SNLDateTimeShortOrNA(const ATimestamp: TDateTime): string;
begin
   if ATimestamp = 0 then begin
      Result := 'n/a';
   end else begin
      Result := FormatDateTime('yyyy-mm-dd hh:nn', ATimestamp);
   end;
end;

function DateOrTodaysTime(const ATimestamp: TDateTime): string;
begin
   if Trunc(Now) = Trunc(ATimestamp) then begin
      Result := FormatDateTime('h:nn', ATimestamp);
   end else begin
      Result := FormatDateTime('ddddd', ATimestamp);
   end;
end;

function DurationInHoursMinutesOrSeconds(APattern: string; const ATimestamp: TDateTime): string;
// TODO : add localization
begin
   if (ATimestamp > 1 / 24) then begin
      Result := Format(APattern, [FormatDateTime('hh:nn:ss', ATimestamp), ('hours')]);
   end
   else if (ATimestamp < 1 / 24 / 60) then begin
      Result := Format(APattern, [FormatDateTime('ss.zzz', ATimestamp), ('seconds')]);
   end else begin
      Result := Format(APattern, [FormatDateTime('nn:ss', ATimestamp), ('minutes')]);
   end;
end;

{ TDateTimeHelper }

procedure TDateTimeHelper.FromUnixTime(AUnixTime: int64);
begin
   {$IFDEF MSWindows}
   Self := ConvertUNIXTimeToDateTime(AUnixTime);
   {$ELSE MSWindows}
   Self := ConvertUNIXTimeToDateTimeRaw(AUnixTime);
   {$ENDIF MSWindows}
end;

initialization

{$IFDEF Windows}
   ZeroMemory(@TZInfo, SizeOf(TZInfo));
{$IFDEF FPCWinCE}
DaylightSavings := (GetTimeZoneInformation(@TZInfo) = TIME_ZONE_ID_DAYLIGHT);
{$ELSE FPCWinCE}
DaylightSavings := (GetTimeZoneInformation(TZInfo) = TIME_ZONE_ID_DAYLIGHT);
{$ENDIF FPCWinCE}
{$ENDIF Windows}

end.
