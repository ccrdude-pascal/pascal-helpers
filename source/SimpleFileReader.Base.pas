{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Base code for custom file format handlers.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2000-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-02-28  --  ---  Renamed from snlFFBase to PepiMK.FileFormats.Base
// 2010-09-24  ##   --  Beautified code (using CodeBeautifier)
// 2010-09-24  ##   --  Added this header (from CodeBeautifier)
// *****************************************************************************
   )
}

unit SimpleFileReader.Base;

interface

uses
   SysUtils,
   Classes;

type

   { TCustomFileFormat }

   TCustomFileFormat = class
   private
      FFilename: WideString;
      FEvents: boolean;
      FRaiseExceptions: boolean;
   protected
      FValid: boolean;
      function GetValid: boolean;
   public
      constructor Create; virtual;
      procedure Clear; virtual;
      procedure LoadFromFile(AFilename: WideString; ACacheInMemory: boolean = false); virtual;
      procedure LoadFromStream({%H-}AStream: TStream); virtual;
      procedure SaveToFile(AFilename: WideString); virtual;
      procedure SaveToStream({%H-}AStream: TStream); virtual;
      property Valid: boolean read GetValid;
      property Filename: WideString read FFilename;
      property Events: boolean read FEvents write FEvents;
      property RaiseExceptions: boolean read FRaiseExceptions write FRaiseExceptions;
   end;

implementation

{ TCustomFileFormat }

procedure TCustomFileFormat.Clear;
begin
   FValid := false;
end;

constructor TCustomFileFormat.Create;
begin
   FValid := false;
   FEvents := true;
   FRaiseExceptions := false;
end;

function TCustomFileFormat.GetValid: boolean;
begin
   Result := FValid;
end;

procedure TCustomFileFormat.LoadFromFile(AFilename: WideString; ACacheInMemory: boolean = false);
var
   fs: TFileStream;
   ms: TMemoryStream;
begin
   if not FileExists(AFilename) then begin
      raise Exception.Create('File not found!');
   end;
   FFilename := AFilename;
   fs := TFileStream.Create(String(AFilename), fmOpenRead or fmShareDenyWrite);
   try
      fs.Seek(0, soFromBeginning);
      if ACacheInMemory then begin
         ms := TMemoryStream.Create;
         try
            ms.CopyFrom(fs, fs.Size);
            ms.Seek(0, soFromBeginning);
            LoadFromStream(ms);
         finally
            ms.Free;
         end;
      end else begin
         LoadFromStream(fs);
      end;
   finally
      fs.Free;
   end;
end;

procedure TCustomFileFormat.LoadFromStream(AStream: TStream);
begin
   raise Exception.Create('No stream load handler for this file format exists!');
end;

procedure TCustomFileFormat.SaveToFile(AFilename: WideString);
var
   fs: TFileStream;
begin
   FFilename := AFilename;
   fs := TFileStream.Create(String(AFilename), fmCreate or fmShareDenyWrite);
   try
      fs.Seek(0, soFromBeginning);
      fs.Size := 0;
      SaveToStream(fs);
   finally
      fs.Free;
   end;
end;

procedure TCustomFileFormat.SaveToStream(AStream: TStream);
begin
   raise Exception.Create('No stream save handler for this file format exists!');
end;

end.
