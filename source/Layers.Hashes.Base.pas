﻿{
@author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Abstract base class for hashing algorithm class framework.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-05-17  pk   5m  Moved from snlHashesBase
// *****************************************************************************
   )
}

unit Layers.Hashes.Base;

interface

uses
   SysUtils, // exceptions
   {$IFDEF MSWindows}
   Windows, // for GetRawByteFileMappingHash
   {$ENDIF MSWindows}
   Classes;  // TStream

type
   TCustomHashAlgorithm = class
   public
      function Init: boolean; virtual; abstract;
      function Finalize: boolean; virtual; abstract;
      function Update(const Buffer; Size: Cardinal): boolean; virtual; abstract;
      function GetHash: RawByteString; virtual; abstract;
   end;

   TCustomHashAlgorithmClass   = class of TCustomHashAlgorithm;
   TCustomHashAlgorithmClasses = array of TCustomHashAlgorithmClass;

const
   HashBlockSize = $00100000;

{
  Calculates a hash value for stream data.

  @param(AStream is the stream to process.)
  @param(AnAlgorithm is the hashing algorithm class.)
  @param(AHash will return the calculated hash.)
  @param(AErrorCode will return details about possible errors.)
  @returns(True if calculation was successful.)
}
function GetRawByteStreamHash(const AStream: TStream; AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out {%H-}AErrorCode: LongWord): boolean;

{
  Calculates a hash value for a text.

  @param(AText is a raw byte stream.)
  @param(AnAlgorithm is the hashing algorithm class.)
  @param(AHash will return the calculated hash.)
  @param(AErrorCode will return details about possible errors.)
  @returns(True if calculation was successful.)
}
function GetRawByteStringHash(const AText: RawByteString; AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out {%H-}AErrorCode: LongWord): boolean;

{
  Calculates a hash value for a file, using file mapping.

  @param(AFilename is the name of the file to hash.)
  @param(AnAlgorithm is the hashing algorithm class.)
  @param(AHash will return the calculated hash.)
  @param(AErrorCode will return details about possible errors.)
  @returns(True if calculation was successful.)
}
function GetRawByteFileMappingHash(const AFilename: WideString; AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out {%H-}AErrorCode: LongWord): boolean;

{
  Calculates a hash value for a file, using a file stream.

  @param(AFilename is the name of the file to hash.)
  @param(AnAlgorithm is the hashing algorithm class.)
  @param(AHash will return the calculated hash.)
  @param(AErrorCode will return details about possible errors.)
  @returns(True if calculation was successful.)
}
function GetRawByteFileStreamHash(const AFilename: WideString; AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out {%H-}AErrorCode: LongWord): boolean;

{
  Calculates a hash value for a part of a file, using a file stream.

  @param(AFilename is the name of the file to hash.)
  @param(AOFfset is the position where to start hashing.)
  @param(ACount is the number of bytes to hash.)
  @param(AnAlgorithm is the hashing algorithm class.)
  @param(AHash will return the calculated hash.)
  @param(AErrorCode will return details about possible errors.)
  @returns(True if calculation was successful.)
}
function GetRawByteFileStreamPartHash(const AFilename: WideString; AOffset, ACount: UInt64; Algorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out {%H-}AErrorCode: LongWord): boolean;

implementation

function GetRawByteStreamHash(const AStream: TStream;
   AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out
   AErrorCode: LongWord): boolean;
var
   h: TCustomHashAlgorithm;
   iRead: integer;
   buf: Pointer;
   dwBufSize: Cardinal;
begin
   Result := false;
   //DebugLogger.LogEnterMethod(nil, 'GetRawByteStreamHash');
   try
      try
         h := AnAlgorithm.Create;
         try
            Result := h.Init;
            if Result then begin
               dwBufSize := HashBlockSize;
               if dwBufSize > AStream.Size then begin
                  dwBufSize := AStream.Size;
               end;
               GetMem(buf, dwBufSize);
               try
                  if dwBufSize > 0 then begin
                     repeat
                        iRead := AStream.Read(buf^, dwBufSize);
                        h.Update(buf^, iRead);
                     until dword(iRead) <> dwBufSize;
                  end;
                  Result := h.Finalize;
                  if Result then begin
                     AHash := h.GetHash;
                  end;
               finally
                  FreeMem(buf);
               end;
            end;
         finally
            h.Free;
         end;
      except
         on E: Exception do begin
            //DebugLogger.LogException(E, 'GetRawByteStreamHash');
         end;
      end;
   finally
      //DebugLogger.LogLeaveMethod(nil, 'GetRawByteStreamHash');
   end;
end;

function GetRawByteStringHash(const AText: RawByteString;
   AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out
   AErrorCode: LongWord): boolean;
var
   a: TCustomHashAlgorithm;
begin
   a := AnAlgorithm.Create;
   try
      Result := a.Init;
      if Result then begin
         Result := a.Update(AText[1], Length(AText));
         if Result then begin
            Result := a.Finalize;
            if Result then begin
               AHash := a.GetHash;
            end;
         end;
      end;
   finally
      a.Free;
   end;
end;

function GetRawByteFileMappingHash(const AFilename: WideString;
   AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out
   AErrorCode: LongWord): boolean;
{$IFDEf MSWindows}
var
   hFile: THandle;
   hMap: THandle;
   pView: Pointer;
   h: TCustomHashAlgorithm;
   dwFileMode: LongWord;
   iSizeHigh, iBlock, iBlockCount: integer;
   dwSizeLow, dwSizeHigh: dword;
   dwBlock, dwSubBlock, dwOffset: dword;
{$ENDIF MSWindows}
begin
   {$IFDEF MSWindows}
   h := AnAlgorithm.Create;
   try
      h.Init;
      dwFileMode := FILE_SHARE_READ or FILE_SHARE_WRITE;
      if (Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion >= 4) then begin
         dwFileMode := dwFileMode or FILE_SHARE_DELETE;
      end;
      hFile := CreateFile(PChar(AFilename), GENERIC_READ, dwFileMode, nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL or FILE_FLAG_SEQUENTIAL_SCAN, 0);
      Result := (hFile <> INVALID_HANDLE_VALUE);
      if Result then begin
         try
            hMap := CreateFileMapping(hFile, nil, PAGE_READONLY, 0, 0, nil);
            Result := (hMap <> 0);
            if Result then begin
               try
                  dwSizeLow := GetFileSize(hFile, @dwSizeHigh);
                  for iSizeHigh := 0 to dwSizeHigh do begin
                     if dword(iSizeHigh) = dwSizeHigh then begin
                        dwBlock := dwSizeLow;
                     end else begin
                        dwBlock := MAXDWORD;
                     end;
                     iBlockCount := dwBlock div HashBlockSize;
                     if (dwBlock mod HashBlockSize) > 0 then begin
                        inc(iBlockCount);
                     end;
                     for iBlock := 0 to Pred(iBlockCount) do begin
                        if iBlock = Pred(iBlockCount) then begin
                           dwSubBlock := dwBlock mod HashBlockSize;
                        end else begin
                           dwSubBlock := HashBlockSize;
                        end;
                        dwOffset := iBlock * HashBlockSize;
                        pView := MapViewOfFile(hMap, FILE_MAP_READ, iSizeHigh, dwOffset, dwSubBlock);
                        Result := (pView <> nil);
                        if Result then begin
                           try
                              h.Update(pView^, dwSubBlock);
                           finally
                              UnmapViewOfFile(pView);
                           end;
                        end else begin
                           AErrorCode := GetLastError;
                           //DebugLogger.LogError(AErrorCode, 'MapViewOfFile');
                        end;
                     end;
                  end;
               finally
                  CloseHandle(hMap);
               end;
            end else begin
               AErrorCode := GetLastError;
               //DebugLogger.LogError(AErrorCode, 'CreateFileMapping');
            end;
         finally
            CloseHandle(hFile);
         end;
      end;
   finally
      Result := h.Finalize;
      AHash := h.GetHash;
      h.Free;
   end;
   {$ELSE MSWindows}
   Result := GetRawByteFileStreamHash(AFilename, AnAlgorithm, AHash, AErrorCode);
   {$ENDIF MSWindows}
end;

function GetRawByteFileStreamHash(const AFilename: WideString;
   AnAlgorithm: TCustomHashAlgorithmClass; out AHash: RawByteString; out
   AErrorCode: LongWord): boolean;
var
   h: TCustomHashAlgorithm;
   fs: TFileStream;
   iRead: integer;
   buf: Pointer;
   dwBufSize: Cardinal;
begin
   //DebugLogger.LogEnterMethod(nil, 'GetRawByteFileStreamHash', [AFilename, AnAlgorithm.ClassName]);
   Result := false;
   try
      try
         h := AnAlgorithm.Create;
         try
            Result := h.Init;
            if Result then begin
               {$IFDEF FPC}
               fs := TFileStream.Create(UTF8Encode(AFilename), fmOpenRead or fmShareDenyNone);
               {$ELSE FPC}
               fs := TFileStream.Create(AFilename, fmOpenRead or fmShareDenyNone);
               {$ENDIF FPC}
               dwBufSize := HashBlockSize;
               if dwBufSize > fs.Size then begin
                  dwBufSize := fs.Size;
               end;
               GetMem(buf, dwBufSize);
               try
                  if dwBufSize > 0 then begin
                     repeat
                        iRead := fs.Read(buf^, dwBufSize);
                        h.Update(buf^, iRead);
                     until dword(iRead) <> dwBufSize;
                  end;
                  Result := h.Finalize;
                  if Result then begin
                     AHash := h.GetHash;
                  end else begin
                     // TODO : error handling
                     // MessageBox(0, 'GetRawByteFileStreamHash.fail:Finalize', 'GetRawByteFileStreamHash.fail:Finalize', 0);
                     //DebugLogger.LogError('GetRawByteFileStreamHash.fail:Finalize');
                  end;
               finally
                  FreeMem(buf);
                  fs.Free;
               end;
            end else begin
               // TODO : error handling
               // MessageBox(0, 'GetRawByteFileStreamHash.fail:Init', 'GetRawByteFileStreamHash.fail:Init', 0);
               //DebugLogger.LogError('GetRawByteFileStreamHash.fail:Init');
            end;
         finally
            h.Free;
         end;
      except
         on E: Exception do begin
            //DebugLogger.LogException(E, 'GetRawByteFileStreamHash');
         end;
      end;
   finally
      //DebugLogger.LogLeaveMethod(nil, 'GetRawByteFileStreamHash', [AHash]);
   end;
end;

function GetRawByteFileStreamPartHash(const AFilename: WideString; AOffset,
   ACount: UInt64; Algorithm: TCustomHashAlgorithmClass; out
   AHash: RawByteString; out AErrorCode: LongWord): boolean;
var
   h: TCustomHashAlgorithm;
   fs: TFileStream;
   iRead: integer;
   buf: Pointer;
   dwBufSize: Cardinal;
   iBytesToRead: Int64;
begin
   //DebugLogger.LogEnterMethod(nil, 'GetRawByteFileStreamHash', [Algorithm.ClassName]);
   Result := false;
   try
      try
         h := Algorithm.Create;
         try
            Result := h.Init;
            if Result then begin
               {$IFDEF FPC}
               fs := TFileStream.Create(UTF8Encode(AFilename), fmOpenRead or fmShareDenyNone);
               {$ELSE FPC}
               fs := TFileStream.Create(AFilename, fmOpenRead or fmShareDenyNone);
               {$ENDIF FPC}
               dwBufSize := HashBlockSize;
               if dwBufSize > fs.Size then begin
                  dwBufSize := fs.Size;
               end;
               GetMem(buf, dwBufSize);
               try
                  if AOffset < fs.Size then begin
                     fs.Seek(AOffset, soFromBeginning);
                     iBytesToRead := ACount;
                     if dwBufSize > 0 then begin
                        repeat
                           iRead := fs.Read(buf^, dwBufSize);
                           if iRead > iBytesToRead then begin
                              iRead := iBytesToRead;
                           end;
                           h.Update(buf^, iRead);
                           Dec(iBytesToRead, iRead);
                        until (dword(iRead) <> dwBufSize);
                     end;
                  end;
                  Result := h.Finalize;
                  if Result then begin
                     AHash := h.GetHash;
                  end;
               finally
                  FreeMem(buf);
                  fs.free;
               end;
            end;
         finally
            h.Free;
         end;
      except
         on E: Exception do begin
            //DebugLogger.LogException(E, 'GetRawByteFileStreamHash');
         end;
      end;
   finally
      //DebugLogger.LogLeaveMethod(nil, 'GetRawByteFileStreamHash', [AHash]);
   end;
end;

end.
