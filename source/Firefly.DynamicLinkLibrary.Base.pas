﻿{
@author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
  @abstract(Base class for library interfaces.)

  @preformatted(
  // *****************************************************************************
  // Copyright: © 2011-2013 Patrick Michael Kolla-ten Venne. All rights reserved.
  // License: BSD 3-Clause Revised License
  // *****************************************************************************
  // Redistribution and use in source and binary forms, with or without
  // modification, are permitted provided that the following conditions are met:
  //      * Redistributions of source code must retain the above copyright
  //        notice, this list of conditions and the following disclaimer.
  //      * Redistributions in binary form must reproduce the above copyright
  //        notice, this list of conditions and the following disclaimer in the
  //        documentation and/or other materials provided with the distribution.
  //      * Neither the name of the <organization> nor the
  //        names of its contributors may be used to endorse or promote products
  //        derived from this software without specific prior written permission.
  //
  //  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
  //  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  //  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  //  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
  //  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  //  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  //  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  //  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  //  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  //  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  // *****************************************************************************
  // Changelog (new entries first):
  // ---------------------------------------
  // 2023-09-18  pk  10m  Moved to Firefly open source pakacge
  // 2016-08-24  pk   5m  Renamed from snlDynamicLinkLibrary to PepiMK.DynamicLinkLibrary.Base
  // 2011-03-23  ##   --  Beautified code (using Code Beautifier Wizard)
  // 2011-03-23  ##   --  Added this header (from Code Beautifier Wizard)
  // *****************************************************************************
  // Dynamic-Link Library Search Order: http://msdn.microsoft.com/en-us/library/ms682586%28v=vs.85%29.aspx
  // SetSearchPathMode: http://msdn.microsoft.com/en-us/library/dd266735%28v=vs.85%29.aspx
  // *****************************************************************************
  )
}

unit Firefly.DynamicLinkLibrary.Base;

{$IFDEF FPC}
{$MODE Delphi}
{$MODESWITCH AdvancedRecords}
{$ENDIF FPC}

interface

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   SysUtils;

type
   LibraryFilenameArray = array of WideString;

   { TDynamicLinkLibrary }

   TDynamicLinkLibrary = class
   private
      FLoadCounter: cardinal;
      FFilename: string;
      FLibraryInitialized: boolean;
      function GetLibraryFilename: string;
   protected
      class function GetBaseFilename: string; virtual; abstract;
      class function GetBaseFilenames: LibraryFilenameArray; virtual;
   protected
      FHandle: THandle;
      procedure ReportMissingCall(AFunctionName: string);
      procedure ReportMissingLoadedLibrary(AFunctionName: string);
      function IsLibraryLoaded: boolean;
      function LoadLibrary: boolean; virtual; // used e.g. in PepiMK.API.UnRAR
      function UnloadLibrary: boolean;
      function GetLibraryInitialized: boolean; virtual;
      function InitializeImports: boolean; virtual;
      function InitializeInPath(APath: string; AFilename: WideString = ''): boolean;
   public
      constructor Create; overload; virtual;
      constructor Create(AFilePath: string); overload; virtual;
      constructor Create(AFilePath, AFilename: string); overload;
      destructor Destroy; override;
      function Initialize(AFilename: string): boolean;
      function LibraryExists: boolean;
      property LibraryInitialized: boolean read GetLibraryInitialized;
      property LibraryFilename: string read GetLibraryFilename;
      property Filename: string read FFilename;
      property Handle: THandle read FHandle;
   end;

implementation

 //uses
 //   DebugLog;

procedure TDynamicLinkLibrary.ReportMissingCall(AFunctionName: string);
begin
   //DebugLogger.LogError(Format('Missing import %s from %s', [AFunctionName, ExtractFileName(FFilename)]));
end;

procedure TDynamicLinkLibrary.ReportMissingLoadedLibrary(AFunctionName: string);
begin
   //DebugLogger.LogError(Format('Expected %s to be loaded for %s, but it isn''t!', [ExtractFileName(FFilename), AFunctionName]));
end;

constructor TDynamicLinkLibrary.Create;
var
   ModuleFileName: array [0 .. MAX_PATH] of widechar;
begin
   inherited Create;
   FHandle := 0;
   if not InitializeInPath(WideString(ExtractFilePath(ParamStr(0)))) then begin
      {$IFDEF MSWindows}
      if GetModuleFileNameW(HInstance, ModuleFileName, SizeOf(ModuleFileName)) > 0 then begin
         InitializeInPath(ExtractFilePath(ModuleFileName));
      end;
      {$ENDIF MSWindows}
   end;
end;

constructor TDynamicLinkLibrary.Create(AFilePath: string);
begin
   inherited Create;
   FHandle := 0;
   InitializeInPath(AFilePath);
end;

constructor TDynamicLinkLibrary.Create(AFilePath, AFilename: string);
begin
   inherited Create;
   FHandle := 0;
   InitializeInPath(AFilePath, AFilename);
end;

destructor TDynamicLinkLibrary.Destroy;
begin
   // UnloadLibrary;
   if FHandle > 0 then begin
      FreeLibrary(FHandle);
   end;
   inherited;
end;

class function TDynamicLinkLibrary.GetBaseFilenames: LibraryFilenameArray;
begin
   Result := [];
   //SetLength(Result, 0);
end;

function TDynamicLinkLibrary.GetLibraryFilename: string;
begin
   Result := ExtractFileName(FFilename);
end;

function TDynamicLinkLibrary.GetLibraryInitialized: boolean;
begin
   Result := FLibraryInitialized;
end;

function TDynamicLinkLibrary.Initialize(AFilename: string): boolean;
begin
   FFilename := AFilename;
   if FHandle > 0 then begin
      FreeLibrary(FHandle);
   end;
   // FLoadCounter := 0;
   // LoadLibrary;
   {$IFDEF MSWindows}
   if FileExists(AFilename) then begin
      FHandle := Windows.LoadLibraryW(pwidechar(AFilename));
      if (FHandle = 0) then begin
         OutputDebugString(PChar(SysErrorMessage(GetLastOSError)));
      end;
   end else begin
      FHandle := Windows.LoadLibraryW(pwidechar(AFilename));
   end;
   {$ELSE MSWindows}
   FHandle := System.LoadLibrary(AFilename);
   {$ENDIF MSWindows}
   // FLoadCounter := 1;
   FLibraryInitialized := (FHandle > 0);
   if FLibraryInitialized then begin
      FLibraryInitialized := InitializeImports;
   end;
   Result := FLibraryInitialized;
end;

function TDynamicLinkLibrary.InitializeImports: boolean;
begin
   Result := LibraryInitialized;
end;

function TDynamicLinkLibrary.InitializeInPath(APath: string; AFilename: WideString): boolean;
var
   s: WideString;
begin
   if Length(APath) > 0 then begin
      APath := IncludeTrailingPathDelimiter(APath);
   end;
   if Length(AFilename) = 0 then begin
      AFilename := GetBaseFilename;
   end;
   FLibraryInitialized := Initialize(APath + AFilename);
   if not FLibraryInitialized then begin
      for s in GetBaseFilenames do begin
         FLibraryInitialized := Initialize(APath + s);
         if FLibraryInitialized then begin
            Result := True;
            Exit;
         end;
      end;
   end;
   Result := FLibraryInitialized;
end;

function TDynamicLinkLibrary.LibraryExists: boolean;
begin
   Result := FileExists(FFilename);
end;

function TDynamicLinkLibrary.LoadLibrary: boolean;
begin
   if (FHandle = 0) and (Length(FFilename) > 0) then begin
      {$IFDEF MSWindows}
      FHandle := Windows.LoadLibraryW(pwidechar(FFilename));
      {$ELSE MSWindows}
      FHandle := System.LoadLibrary(FFilename);
      {$ENDIF MSWindows}
   end;
   Result := (FHandle > 0);
   if Result then begin
      Inc(FLoadCounter);
   end else begin
      //DebugLogger.LogError('Error loading ' + ExtractFileName(FFilename));
   end;
end;

function TDynamicLinkLibrary.UnloadLibrary: boolean;
begin
   if FLoadCounter > 1 then begin
      Dec(FLoadCounter);
      Result := True;
      Exit;
   end;
   if (FHandle > 0) then begin
      if FreeLibrary(FHandle) then begin
         FHandle := 0;
      end;
   end;
   Result := (FHandle = 0);
   if Result then begin
      // Dec(FLoadCounter);
      FLoadCounter := 0;
   end else begin
      {$IFDEF FPC}
      //DebugLogger.LogError('Error unloading ' + UTF8Encode(ExtractFileName(FFilename)));
      {$ELSE FPC}
      //DebugLogger.LogError('Error unloading ' + ExtractFileName(FFilename));
      {$ENDIF FPC}
   end;
end;

function TDynamicLinkLibrary.IsLibraryLoaded: boolean;
begin
   Result := (FHandle > 0);
end;

const
   BASE_SEARCH_PATH_ENABLE_SAFE_SEARCHMODE = $00000001;
   BASE_SEARCH_PATH_PERMANENT = $00008000;
   PROCESS_DEP_ENABLE = $00000001;

   {$IFDEF MSWindows}

type
   TSetDllDirectoryFunc = function(lpPathName: pwidechar): BOOL; stdcall;
   TSetSearchPathModeFunc = function(Flags: DWORD): BOOL; stdcall;


var
   hKernel: HMODULE;
   SetDllDirectoryFunc: TSetDllDirectoryFunc;
   SetSearchPathModeFunc: TSetSearchPathModeFunc;
   // SetProcessDEPPolicyFunc: function(dwFlags: DWORD): BOOL; stdcall;
   {$ENDIF MSWindows}

initialization

   //DebugLogger.LogEnterUnitInitialization('PepiMK.DynamicLinkLibrary.Base');
try
   {$IFDEF MSWindows}
   hKernel := GetModuleHandle(kernel32);

   SetDllDirectoryFunc := TSetDllDirectoryFunc(GetProcAddress(hKernel, pansichar('SetDllDirectoryW')));
   if Assigned(SetDllDirectoryFunc) then begin
      SetDllDirectoryFunc('');
   end;

   SetSearchPathModeFunc := TSetSearchPathModeFunc(GetProcAddress(hKernel, pansichar('SetSearchPathMode')));
   if Assigned(SetSearchPathModeFunc) then begin
      SetSearchPathModeFunc(BASE_SEARCH_PATH_ENABLE_SAFE_SEARCHMODE or BASE_SEARCH_PATH_PERMANENT);
   end;
   (*
     SetProcessDEPPolicyFunc := GetProcAddress(hKernel, PAnsiChar('SetProcessDEPPolicy'));
     if Assigned(SetProcessDEPPolicyFunc) then begin
     SetProcessDEPPolicyFunc(PROCESS_DEP_ENABLE);
     end;
   *)
   {$ENDIF MSWindows}
finally
   //DebugLogger.LogLeaveUnitInitialization('PepiMK.DynamicLinkLibrary.Base');
end;

end.
