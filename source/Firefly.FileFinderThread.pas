{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(File system functions, like finding file.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2003-2024 Patrick Michael Kolla-ten Venne All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-01-30  pk   5m  Fixed GetDiskSize usage problem (string type) in pkGet*
// 2016-05-17  pk   5m  Renamed unit from snlFilesListBasic
// 2010-09-21  ##   --  Updated unit name usage from pkFileSys to snlFilesListBasic
// 2010-09-21  ##   --  Unit renamed from pkFileSys to snlFilesListBasic
// 2007-02-01  pk  10m  Changed ListFiles param TStringList->TStrings
// 2006-12-28  pk  10m  Removed SetName warnings
// 2006-03-28  pk   1h  Changed class to work in a separate thread
// 2005-12-29  pk   1h  Implementation of TFileFinder
// *****************************************************************************
   )
}

unit Firefly.FileFinderThread;

{$ifdef FPC}
{$mode Delphi}{$H+}
{$endif FPC}

interface

uses
   SysUtils,
   Classes,
   {$IFDEF MSWINDOWS} Windows, {$ENDIF}
   Firefly.Threads,
   Layers.Hashes.Base;

type
   TFileOrFolderFoundEvent = procedure(Path: String; Info: TSearchRec) of object;
   TFileWithHashFoundEvent = procedure(Path: String; MD5: ansistring; Info: TSearchRec) of object;
   TFolderProgressEvent = procedure(Path: String) of object;
   TFinishedEvent = procedure(FilesFound, FoldersFound: int64; TimeElapsed: TDateTime) of object;

   { TFileFinder }

   TFileFinder = class(TDLLThread)
   private
      FRecurseFolders: boolean;
      FReportFullPath: boolean;
      FFolderStack: TStringList;
      FScanning: boolean;
      FStop: boolean;
      FFilesFound: int64;
      FFoldersFound: int64;
      FTimeStarted: TDateTime;
      FTimeFinished: TDateTime;
      FStartFolders: String;
      FCurrentBasePath: String;
      FSyncPath: String;
      FSyncInfo: TSearchRec;
      FSyncMD5: ansistring;
      // Event vars
      FOnFileFound: TFileOrFolderFoundEvent;
      FOnFinished: TFinishedEvent;
      FOnFolderFound: TFileOrFolderFoundEvent;
      FOnFolderProgress: TFolderProgressEvent;
      FReportRelativePath: boolean;
      FThreadRunning: boolean;
      FOnFileWithHashFound: TFileWithHashFoundEvent;
      FCreateHashes: boolean;
      FHashClass: TCustomHashAlgorithmClass;
      FFollowSymLinks: boolean;
      function GetTimeElapsed: TDateTime;
      procedure SetRecurseFolders(const AValue: boolean);
      procedure SetReportFullPath(const AValue: boolean);
      procedure SetName;
      procedure SetReportRelativePath(const Value: boolean);
      procedure SetOnFileWithHashFound(const Value: TFileWithHashFoundEvent);
   protected
      procedure ScanFolder(const APath: String);
      // Sync handlers
      procedure SyncFireFileFoundEvent;
      procedure SyncFireFileWithHashFoundEvent;
      procedure SyncFireFolderFoundEvent;
      procedure SyncFireFolderProgressEvent;
      procedure SyncFireFinishedEvent;
      procedure DoFileFound(APath: String; AInfo: TSearchRec); virtual;
      procedure DoFolderFound(APath: String; AInfo: TSearchRec); virtual;
      procedure FireFolderProgressEvent(APath: String);
      procedure FireFinishedEvent;
      procedure Execute; override;
   public
      procedure StartScan;
      constructor Create;
      destructor Destroy; override;
      procedure Scan(const APath: String; AThreaded: boolean = True);
      procedure Stop;
      procedure AddPathToStack(const APath: String);
      property RecurseFolders: boolean read FRecurseFolders write SetRecurseFolders;
      property ReportFullPath: boolean read FReportFullPath write SetReportFullPath;
      property ReportRelativePath: boolean read FReportRelativePath write SetReportRelativePath;
      property Scanning: boolean read FScanning;
      property FollowSymLinks: boolean read FFollowSymLinks write FFollowSymLinks;
      property FilesFound: int64 read FFilesFound;
      property FoldersFound: int64 read FFoldersFound;
      property TimeStarted: TDateTime read FTimeStarted;
      property TimeFinished: TDateTime read FTimeFinished;
      property TimeElapsed: TDateTime read GetTimeElapsed;
      property StartFolders: String read FStartFolders write FStartFolders;
      property OnFileFound: TFileOrFolderFoundEvent read FOnFileFound write FOnFileFound;
      property OnFileWithHashFound: TFileWithHashFoundEvent read FOnFileWithHashFound write SetOnFileWithHashFound;
      property OnFolderFound: TFileOrFolderFoundEvent read FOnFolderFound write FOnFolderFound;
      property OnFolderProgress: TFolderProgressEvent read FOnFolderProgress write FOnFolderProgress;
      property OnFinished: TFinishedEvent read FOnFinished write FOnFinished;
      property HashClass: TCustomHashAlgorithmClass read FHashClass;
   end;

   TFileFinderThread = class(TFileFinder)
   private
   public
   end;

   TFileListOptions = (floShowFiles, floShowFolders, floRecursive);

   TConstDriveType = (dctFixed, dctRemote, dctRemovable, dctOptical, dctRAMDisk);
   TConstDriveTypes = set of TConstDriveType;

procedure ListFiles(const APath: String; Results: TStrings; const ShowFiles: boolean = True; const ShowFolders: boolean = False; const Fullpath: boolean = True;
   const Recursive: boolean = False; const RelativePath: boolean = False); overload;
procedure ListFiles(APaths, Results: TStrings; const ShowFiles: boolean = True; const ShowFolders: boolean = False; const Fullpath: boolean = True;
   const Recursive: boolean = False; const RelativePath: boolean = False); overload;

function FindFileList(const AFileMask: String; const AShowFiles, AShowFolders: boolean): TStringList; overload;
procedure FindFileList(const AFileMask: String; FileList: TStrings; const AShowFiles, AShowFolders: boolean); overload;
function FindFileList(const AFileMask: String; const AShowFiles, AShowFolders, AFullPath: boolean): TStringList; overload;
procedure FindFileList(const AFileMask: String; FileList: TStrings; const AShowFiles, AShowFolders, AFullPath: boolean); overload;
function FindFileList(const AFileMask: String; const AShowFiles, AShowFolders, AFullPath, ARecursive: boolean): TStringList; overload;
procedure FindFileList(const AFileMask: String; FileList: TStrings; const AShowFiles, AShowFolders, AFullPath, ARecursive: boolean); overload;
procedure FindFileListFilterNaviDirs(const ADirectoryList: TStringList);

{$IFDEF MSWindows}
procedure ListAvailableDrives(AList: TStrings; const AddToLetter: String = ':'; const DriveTypes: TConstDriveTypes = [dctFixed]);
procedure ListAllDrives(AList: TStrings; const AddToLetter: String = ':');
function pkGetSectorsPerCluster(ARootPath: String): cardinal;
function pkGetBytesPerSector(ARootPath: String): cardinal;
function pkGetBytesPerCluster(ARootPath: String): cardinal;
{$ENDIF MSWindows}
function CompareFolders(const RealPath: String; const PathMask: String): boolean; overload;
function CompareFolders(RealPaths: TStrings; const PathMask: String): boolean; overload;

{
  @name checks whether File Exists 

  @param(Filename is the name of the file to check.)
  @returns(Success)
}
function pkFileExists(const Filename: String): boolean;

type

   { TFileFinderCollector }

   TFileFinderCollector = class
   public
      FileList, FolderList: TStringList;
      constructor Create;
      destructor Destroy; override;
      procedure FileFound(Path: String; Info: TSearchRec);
      procedure FolderFound(Path: String; Info: TSearchRec);
   end;

{
  @name adds ASlash at the end of FilePath if not exists

  @param(AFilePath is the name of the FilePath to add slash.)
  @param(ASlash is the char to add)
  @returns(FilePath with slash ending)
}
function SolveEnv(const AFilenameInEnvironment: String): String;
function RemoveQuotes(const AQuotedFilename: String): String;
function CommandLineToFilename(const CommandLine: ansistring): ansistring;
function ExpandComplicatedFilename(const AFilename: String): String;
function ExtractFilenameFromCommandLine(const AFilename: String): String;

{
  @name Extracts the FilePath

  @param(AFilename is the name of the file to extract path for.)
  @returns(FilePath)
}
function ExtractFilePathW(const Filename: String): String;

{
  @name Gets FileSize in String Format

  @param(AFilename is the name of the file to check.)
  @returns(FileSize)
}
function GetFileSizeString(const AFilename: String): String;

{
  @name Gets FileSize in Int64 Format

  @param(AFilename is the name of the file to check.)
  @returns(FileSize)
}
function GetFileSizeInt64(const AFilename: String): int64;

function GetFileSizeFromInt(iLow, iHigh: integer): int64;
function MyFileSizeToStringShort(AFilesize: int64): string;

{
  @name Gets File Date 

  @param(AFilename is the name of the file to get datestring.)
  @returns(File Date String representation)
}
function GetFileDateString(const AFilename: String): String;

{
  @name checks whether Filename Is Reserved

  @param(Filename is the name of the file to check.)
  @returns(Success)
}
function FilenameIsReserved(const Filename: String): boolean;

// function ShellCopyFileList(const PathFrom, PathTo: String; const RelativeFilenames: TStrings; const Move: boolean = false): boolean;

implementation

uses
   Firefly.StringCompare;

   {$IFDEF MSWINDOWS}

type
   TThreadNameInfo = record
      FType: longword;     // muss 0x1000 sein
      FName: PChar;        // Zeiger auf Name (in Anwender-Adress-Bereich)
      FThreadID: longword; // Thread-ID (-1 ist Caller-Thread)
      FFlags: longword;    // reserviert für zukünftige Verwendung, muss 0 sein
   end;
   {$ENDIF}
   {$IFDEF MSWINDOWS}

const
   PathSeparator: char          = '\';
   PathAllFilesMask: String = '*.*';
   {$ELSE MSWINDOWS}

const
   PathSeparator: char          = '/';
   PathAllFilesMask: String = '*';

   {$ENDIF MSWINDOWS}

function FilenameIsReserved(const Filename: String): boolean;
var
   sFilename: String;
begin
   sFilename := LowerCase(Filename);
   if Pos('.', sFilename) > 0 then begin
      sFilename := Copy(sFilename, 1, Pred(Pos('.', sFilename)));
   end;
   Result := (sFilename = 'aux') or (sFilename = 'com1') or (sFilename = 'com2') or (sFilename = 'com3') or (sFilename = 'com4') or (sFilename = 'com5') or
      (sFilename = 'com6') or (sFilename = 'com7') or (sFilename = 'com8') or (sFilename = 'com9') or (sFilename = 'con') or (sFilename = 'lpt1') or
      (sFilename = 'lpt2') or (sFilename = 'lpt3') or (sFilename = 'lpt4') or (sFilename = 'lpt5') or (sFilename = 'lpt6') or (sFilename = 'lpt7') or
      (sFilename = 'lpt8') or (sFilename = 'lpt9') or (sFilename = 'nul') or (sFilename = 'prn');
end;

procedure ListFiles(const APath: String; Results: TStrings; const ShowFiles: boolean; const ShowFolders: boolean; const Fullpath: boolean; const Recursive: boolean; const RelativePath: boolean);
var
   ff: TFileFinder;
   collector: TFileFinderCollector;
begin
   //DebugLogger.LogEnterMethod(nil, 'ListFiles', [APath, '()', ShowFiles, ShowFolders, Fullpath, Recursive, RelativePath]);
   try
      collector := TFileFinderCollector.Create;
      ff := TFileFinder.Create;
      try
         ff.RecurseFolders := Recursive;
         ff.ReportFullPath := Fullpath;
         ff.ReportRelativePath := RelativePath;
         { DONE : set OnFileFound and OnFolderFound properties if corresponding boolean is true }
         if ShowFiles then begin
            ff.OnFileFound := collector.FileFound;
         end;
         if ShowFolders then begin
            ff.OnFolderFound := collector.FolderFound;
         end;
         ff.StartFolders := APath;
         ff.StartScan;
         Results.AddStrings(collector.FileList);
         Results.AddStrings(collector.FolderList);
      finally
         ff.Free;
         collector.Free;
      end;
   finally
      //DebugLogger.LogLeaveMethod(nil, 'ListFiles');
   end;
end;

procedure ListFiles(APaths, Results: TStrings; const ShowFiles: boolean = True; const ShowFolders: boolean = False; const Fullpath: boolean = True;
   const Recursive: boolean = False; const RelativePath: boolean = False); overload;
var
   ff: TFileFinder;
   collector: TFileFinderCollector;
begin
   //DebugLogger.LogEnterMethod(nil, 'ListFiles', [APaths.Text, '()', ShowFiles, ShowFolders, Fullpath, Recursive, RelativePath]);
   try
      collector := TFileFinderCollector.Create;
      ff := TFileFinder.Create;
      try
         ff.RecurseFolders := Recursive;
         ff.ReportFullPath := Fullpath;
         ff.ReportRelativePath := RelativePath;
         if ShowFiles then begin
            ff.OnFileFound := collector.FileFound;
         end;
         if ShowFolders then begin
            ff.OnFolderFound := collector.FolderFound;
         end;
         APaths.StrictDelimiter := True;
         APaths.Delimiter := ';';
         ff.StartFolders := String(APaths.DelimitedText);
         ff.StartScan;
         Results.AddStrings(collector.FileList);
         Results.AddStrings(collector.FolderList);
      finally
         ff.Free;
         collector.Free;
      end;
   finally
      //DebugLogger.LogLeaveMethod(nil, 'ListFiles');
   end;
end;

function FindFileList(const AFileMask: String; const AShowFiles, AShowFolders: boolean): TStringList;
begin
   Result := FindFileList(AFileMask, AShowFiles, AShowFolders, False, False);
end;

procedure FindFileList(const AFileMask: String; FileList: TStrings; const AShowFiles, AShowFolders: boolean);
begin
   FindFileList(AFileMask, FileList, AShowFiles, AShowFolders, False, False);
end;

function FindFileList(const AFileMask: String; const AShowFiles, AShowFolders, AFullPath: boolean): TStringList;
begin
   Result := TStringList.Create;
   FindFileList(AFileMask, Result, AShowFiles, AShowFolders, AFullPath);
end;

procedure FindFileList(const AFileMask: String; FileList: TStrings; const AShowFiles, AShowFolders, AFullPath: boolean); overload;
const
   DoubleBackslashW: String = '\\';
   SingleBackslashW: String = '\';
   StarDotStarW: String     = '*.*';
   RegExrPrefixW: String    = '<regexpr>';
   SBracketOpenW: String    = '<';
   SBracketCloseW: String   = '>';
   /// 2007-05-16  pk  Algo-Prefix implementation
var
   ec: integer;
   sr: TSearchRec;
   bShow, bAlgoPrefix: boolean;
   pZu { , iAdd }: integer;
   sPath, sFilename, sFilePattern, sSearchMask: String;
begin
   { DONE -oPatrick : Algo-Prefix-handling on < }
   bAlgoPrefix := Copy(AFileMask, 1, 1) = SBracketOpenW;
   if bAlgoPrefix then begin
      pZu := Pos(SBracketCloseW, AFileMask);
      if Copy(AFileMask, 1, 9) = RegExrPrefixW then begin
         { DONE -oPatrick : Double backslashes here! }
         sFilePattern := AFileMask;
         while Pos(DoubleBackslashW, sFilePattern) > 0 do begin
            System.Delete(sFilePattern, 1, Pos(DoubleBackslashW, sFilePattern) + 1);
         end;
         sPath := Copy(AFileMask, 1, Length(AFileMask) - Length(sFilePattern));
         sPath := StringReplace(sPath, DoubleBackslashW, SingleBackslashW, [rfReplaceAll]);
         System.Delete(sPath, 1, 9);
         sSearchMask := sPath + StarDotStarW;
      end else begin
         sSearchMask := ExtractFilePath(Copy(AFileMask, pZu + 1, Length(AFileMask) - pZu)) + StarDotStarW;
         sPath := ExtractFilePath(sSearchMask);
      end;
   end else begin
      sPath := ExtractFilePath(AFileMask);
      sSearchMask := AFileMask;
   end;
   ec := FindFirst(rawbytestring(sSearchMask), faAnyFile, sr);
   try
      if ec = 0 then begin
         repeat
            bShow := True;
            if ((sr.Attr and faDirectory) > 0) and (not AShowFolders) then begin
               bShow := False;
            end;
            if ((sr.Attr and faDirectory) = 0) and (not AShowFiles) then begin
               bShow := False;
            end;
            if ((sr.Attr and faDirectory) > 0) and ((sr.Attr and {%H-}faSymLink) > 0) then begin
               bShow := False;
            end else begin
               sFilename := String(Trim(sr.Name));
               if bAlgoPrefix and bShow then begin
                  bShow := pkCompareIDString(sPath + sFilename, AFileMask, True);
               end;
            end;
            if bShow then begin
               case AFullPath of
                  False: { iAdd := } FileList.Add(string(sFilename));
                  True: { iAdd := } FileList.Add(string(sPath + sFilename));
               end;
               (*
                 // add regular results here!
                 if FStoreRegMatchObjects then begin
                 slRegMatches := TStringList.Create;
                 slRegMatches.AddStrings(RegExpLastTrueResultList);
                 Result.Objects[iAdd] := slRegMatches;
                 end;
               *)
            end;
         until (FindNext(sr) <> 0);
      end;
   finally
      SysUtils.FindClose(sr);
   end;
end;

function FindFileList(const AFileMask: String; const AShowFiles, AShowFolders, AFullPath, ARecursive: boolean): TStringList;
begin
   Result := TStringList.Create;
   FindFileList(AFileMask, Result, AShowFiles, AShowFolders, AFullPath, ARecursive);
end;

procedure FindFileList(const AFileMask: String; FileList: TStrings; const AShowFiles, AShowFolders, AFullPath, ARecursive: boolean);
 // Author:  Patrick M. Kolla
 // Purpose: List all files
 // Date:    01.12.2003 (Optimized recursive, showFiles, showFolders)
 /// 2007-05-16  pk  Algo-Prefix implementation
const
   DoubleBackslashW: String = '\\';
   SingleBackslashW: String = '\';
   StarDotStarW: String     = '*.*';
   RegExrPrefixW: String    = '<regexpr>';
   SBracketOpenW: String    = '<';
   SBracketCloseW: String   = '>';
var
   slTemp: TStringList;
   slFiles: TStringList;
   slFolders: TStringList;
   sAlgoPrefix: String;
   sFilePattern: String;
   sPath: String;
   sPrefix: String;
   pZu, i: integer;
   bRegExpr: boolean;

   function GetNextPath: String;
   begin
      if bRegExpr then begin
         Result := sAlgoPrefix + StringReplace(ExtractFilePath(slFolders[0]), '\', '\\', [rfReplaceAll]) + sFilePattern;
      end else begin
         Result := sAlgoPrefix + ExtractFilePath(slFolders[0]) + sFilePattern;
      end;
   end;

begin
   sAlgoPrefix := '';
   bRegExpr := False;
   if Copy(AFileMask, 1, 1) <> SBracketOpenW then begin
      sFilePattern := AFileMask;
      while Pos(SingleBackslashW, sFilePattern) > 0 do begin
         System.Delete(sFilePattern, 1, Pos(SingleBackslashW, sFilePattern));
      end;
      sPath := ExtractFilePath(AFileMask);
   end else begin
      pZu := Pos(SBracketCloseW, AFileMask);
      sAlgoPrefix := Copy(AFileMask, 1, pZu);
      bRegExpr := Copy(AFileMask, 1, 9) = RegExrPrefixW;
      if bRegExpr then begin
         { DONE -oPatrick : Double backslashes here! }
         sAlgoPrefix := RegExrPrefixW;
         sPath := AFileMask;
         sFilePattern := AFileMask;
         while Pos(DoubleBackslashW, sFilePattern) > 0 do begin
            System.Delete(sFilePattern, 1, Pos(DoubleBackslashW, sFilePattern) + 1);
         end;
         sPath := Copy(AFileMask, 1, Length(AFileMask) - Length(sFilePattern));
         sPath := StringReplace(sPath, DoubleBackslashW, SingleBackslashW, [rfReplaceAll]);
      end else begin
         sFilePattern := AFileMask;
         sPath := ExtractFilePath(AFileMask);
         Delete(sFilePattern, 1, Length(sPath));
      end;
      System.Delete(sPath, 1, pZu);
   end;

   slFolders := TStringList.Create;
   try
      slFolders.Add(string(sPath + StarDotStarW));
      while slFolders.Count > 0 do begin
         if AShowFiles then begin
            slFiles := FindFileList(GetNextPath, True, False, True);
            try
               if AFullPath then begin
                  FileList.AddStrings(slFiles);
               end else begin
                  sPrefix := String(ExtractFilePath(slFolders[0]));
                  System.Delete(sPrefix, 1, Length(sPath));
                  for i := 0 to Pred(slFiles.Count) do begin
                     FileList.Add(string(sPrefix) + ExtractFilename(slFiles[i]));
                  end;
               end;
            finally
               slFiles.Free;
            end;
         end;
         if AShowFolders then begin
            slTemp := FindFileList(GetNextPath, False, True, True);
            try
               FindFileListFilterNaviDirs(slTemp);
               if AFullPath then begin
                  FileList.AddStrings(slTemp);
               end else begin
                  sPrefix := ExtractFilePath(slFolders[0]);
                  System.Delete(sPrefix, 1, Length(sPath));
                  for i := 0 to Pred(slTemp.Count) do begin
                     FileList.Add(sPrefix + ExtractFilename(slTemp[i]));
                  end;
               end;
            finally
               slTemp.Free;
            end;
         end;
         if ARecursive then begin
            slTemp := FindFileList(sAlgoPrefix + ExtractFilePath(slFolders[0]) + StarDotStarW, False, True, True);
            try
               FindFileListFilterNaviDirs(slTemp);
               if ARecursive then begin
                  for i := 0 to Pred(slTemp.Count) do begin
                     slFolders.Add(IncludeTrailingPathDelimiter(slTemp[i]));
                  end;
               end;
            finally
               slTemp.Free;
            end;
         end;
         slFolders.Delete(0);
      end;
   finally
      slFolders.Free;
   end;
end;

procedure FindFileListFilterNaviDirs(const ADirectoryList: TStringList);
var
   i: integer;
begin
   if Assigned(ADirectoryList) then
      for i := ADirectoryList.Count - 1 downto 0 do begin
         if (ADirectoryList[i] = '.') or (ADirectoryList[i] = '..') then ADirectoryList.Delete(i);
         if (Copy(ADirectoryList[i], Length(ADirectoryList[i]) - 1, 2) = '\.') or (Copy(ADirectoryList[i], Length(ADirectoryList[i]) - 2, 3) = '\..') then ADirectoryList.Delete(i);
      end;
end;

{$IFDEF MSWindows}

procedure ListAvailableDrives(AList: TStrings; const AddToLetter: String; const DriveTypes: TConstDriveTypes);
var
   DriveBits: set of 0 .. 25;
   DriveNum, DriveType: integer;
   {$IFDEF FPC}
   pc: PChar;
   {$ENDIF FPC}
   dt: TConstDriveType;
   b: boolean;
begin
   AList.Clear;
   integer(DriveBits) := GetLogicalDrives;
   for DriveNum := 2 to 22 do
      if (DriveNum in DriveBits) then begin
         {$IFDEF FPC}
         GetMem(pc, 3);
         try
            StrPCopy(pc, Chr(DriveNum + 65) + ':\');
            DriveType := GetDriveType(pc);
         finally
            FreeMem(pc, 3);
         end;
         {$ELSE FPC}
         DriveType := GetDriveType(PChar(Chr(DriveNum + 65) + ':\'));
         {$ENDIF FPC}
         b := False;
         for dt := low(TConstDriveType) to high(TConstDriveType) do begin
            if dt in DriveTypes then begin
               case dt of
                  dctFixed: if DriveType = DRIVE_FIXED then b := True;
                  dctRemote: if DriveType = DRIVE_REMOTE then b := True;
                  dctRemovable: if DriveType = DRIVE_REMOVABLE then b := True;
                  dctOptical: if DriveType = DRIVE_CDROM then b := True;
                  dctRAMDisk: if DriveType = DRIVE_RAMDISK then b := True;
               end;
            end;
         end;
         // if (DriveType = DRIVE_FIXED)
         // and ((DriveType and DRIVE_REMOTE)=0) removed, since this is no bitmask!
         if b then AList.Add(Chr(DriveNum + 65) + AddToLetter);
      end;
end;

procedure ListAllDrives(AList: TStrings; const AddToLetter: String);
var
   DriveBits: set of 0 .. 25;
   DriveNum: integer;
begin
   AList.Clear;
   integer(DriveBits) := GetLogicalDrives;
   for DriveNum := 0 to 25 do
      if (DriveNum in DriveBits) then AList.Add(Chr(DriveNum + 65) + AddToLetter);
end;

function pkGetBytesPerSector(ARootPath: String): cardinal;
var
   cSectorsPerCluster, cBytesPerSector, cNumberOfFreeClusters, cTotalNumberOfClusters: cardinal;
begin
   Result := 0;
   if GetDiskFreeSpaceW(pwidechar(ARootPath), cSectorsPerCluster, cBytesPerSector, cNumberOfFreeClusters, cTotalNumberOfClusters) then Result := cBytesPerSector;
end;

function pkGetSectorsPerCluster(ARootPath: String): cardinal;
var
   cSectorsPerCluster, cBytesPerSector, cNumberOfFreeClusters, cTotalNumberOfClusters: cardinal;
begin
   Result := 0;
   if GetDiskFreeSpaceW(pwidechar(ARootPath), cSectorsPerCluster, cBytesPerSector, cNumberOfFreeClusters, cTotalNumberOfClusters) then Result := cSectorsPerCluster;
end;

function pkGetBytesPerCluster(ARootPath: String): cardinal;
var
   cSectorsPerCluster, cBytesPerSector, cNumberOfFreeClusters, cTotalNumberOfClusters: cardinal;
begin
   Result := 0;
   if GetDiskFreeSpaceW(pwidechar(ARootPath), cSectorsPerCluster, cBytesPerSector, cNumberOfFreeClusters, cTotalNumberOfClusters) then begin
      Result := cBytesPerSector * cSectorsPerCluster;
      //end else begin
      //   OutputDebugString(PChar('GetDiskFreeSpace failed: ' + SysErrorMessage(GetLastError)));
   end;
end;

{$ENDIF MSWindows}

function CompareFolders(const RealPath: String; const PathMask: String): boolean;
begin
   { TODO -oPatrick : Compare two folders }
   // * We already get solved paths
   // * We have wildcards ?, * and ** , ** being greedy
   Result := True;
end;

function CompareFolders(RealPaths: TStrings; const PathMask: String): boolean;
   // Purpose: Returns true if any of the folders is a match?
var
   i: integer;
begin
   try
      for i := Pred(RealPaths.Count) downto 0 do begin
         if not CompareFolders(RealPaths[i], PathMask) then RealPaths.Delete(i);
      end;
      Result := RealPaths.Count > 0;
   except
      Result := False;
   end;
end;

function pkFileExists(const Filename: String): boolean;
var
   ec: integer;
   sr: TSearchRec;
begin
   ec := FindFirst(Filename, faAnyFile, sr);
   Result := (ec = 0);
   if Result then begin
      if (sr.Attr and faDirectory) > 0 then begin
         Result := False;
      end;
   end;
end;

{ TFileFinder }

procedure TFileFinder.SetRecurseFolders(const AValue: boolean);
begin
   if FRecurseFolders = AValue then Exit;
   FRecurseFolders := AValue;
end;

procedure TFileFinder.SetReportFullPath(const AValue: boolean);
begin
   if FReportFullPath = AValue then begin
      Exit;
   end;
   FReportFullPath := AValue;
end;

procedure TFileFinder.SetReportRelativePath(const Value: boolean);
begin
   if FReportRelativePath = Value then begin
      Exit;
   end;
   FReportRelativePath := Value;
end;

procedure TFileFinder.SyncFireFileFoundEvent;
begin
   if Assigned(FOnFileFound) then begin
      FOnFileFound(FSyncPath, FSyncInfo);
   end;
end;

procedure TFileFinder.SyncFireFileWithHashFoundEvent;
begin
   if Assigned(FOnFileWithHashFound) then begin
      FOnFileWithHashFound(FSyncPath, FSyncMD5, FSyncInfo);
   end;
end;

procedure TFileFinder.SyncFireFolderFoundEvent;
begin
   if Assigned(FOnFolderFound) then begin
      FOnFolderFound(FSyncPath, FSyncInfo);
   end;
end;

procedure TFileFinder.SyncFireFolderProgressEvent;
begin
   if Assigned(FOnFolderProgress) then begin
      FOnFolderProgress(FSyncPath);
   end;
end;

procedure TFileFinder.SyncFireFinishedEvent;
begin
   if Assigned(FOnFinished) then begin
      FOnFinished(FFilesFound, FFoldersFound, TimeElapsed);
   end;
end;

procedure TFileFinder.Stop;
begin
   FStop := True;
end;

procedure TFileFinder.AddPathToStack(const APath: String);
begin
   FFolderStack.Add(APath);
end;

procedure TFileFinder.ScanFolder(const APath: String);
var
   sr: TSearchRec;
   ec: integer;
   sFilename: String;
   bStarDotStar: boolean;
   iAttr: longint;
begin
   //DebugLogger.LogEnterMethod(Self, 'ScanFolder', [APath], dlvDebug);
   try
      Initialize(sr);
      bStarDotStar := (ExtractFilename(APath) = '*.*');
      DoFolderFound(ExcludeTrailingPathDelimiter(ExtractFilePath(APath)), sr);
      // files first
      // //DebugLogger.LogMessage('TFileFinder.ScanFolder(' + APath + '): file scan started');
      ec := FindFirst(APath, faAnyFile, sr);
      while (ec = 0) and (not Terminated) do begin
         // //DebugLogger.LogMessage('TFileFinder.ScanFolder(' + APath + '): file scan found ' + sr.Name);
         if FStop then begin
            FScanning := False;
            Exit;
         end;
         if (sr.Name <> '.') and (sr.Name <> '..') then begin
            sFilename := ExtractFilePath(APath) + sr.Name;
            if (sr.Attr and faDirectory) = 0 then begin
               if WildcardCompareString(ExtractFilename(sr.Name), ExtractFilename(APath)) or bStarDotStar then begin // trac:sd2:1491  2012-06-14  pk
                  DoFileFound(sFilename, sr);
               end;
            end else begin
               // FireFolderFoundEvent(sFilename, sr);
            end;
         end;
         ec := FindNext(sr);
      end;
      SysUtils.FindClose(sr);
      // trac:sd2:1193 - fixed ShowFolders without Recursive scanning
      iAttr := faDirectory or faReadOnly or faArchive;
      {$IFDEF MSWindows}
      iAttr := iAttr or {%H-}faHidden or {%H-}faSysFile;
      {$ENDIF MSWindows}
      ec := FindFirst(ExtractFilePath(APath) + '*.*', iAttr, sr);
      try
         while (ec = 0) and (not Terminated) do begin
            if FStop then begin
               FScanning := False;
               Exit;
            end;
            sFilename := ExtractFilePath(APath) + sr.Name;
            if (sr.Name <> '.') and (sr.Name <> '..') and ((sr.Attr and faDirectory) > 0) {$IFDEF MSWindows} and (((sr.Attr and {%H-}faSymLink) = 0) or (FFollowSymLinks)) {$ENDIF}
            // trac:sd2:2794
            then begin
               if FRecurseFolders then begin
                  FFolderStack.Add(sFilename + PathSeparator + ExtractFilename(APath));
               end else begin
                  DoFolderFound(sFilename, sr);
               end;
            end;
            ec := FindNext(sr);
         end;
      finally
         SysUtils.FindClose(sr);
      end;
   finally
      //DebugLogger.LogLeaveMethod(Self, 'ScanFolder', dlvDebug);
   end;
end;

procedure TFileFinder.SetName;
{$IFDEF MSWINDOWS}
{$IFNDEF FPC}
var
   ThreadNameInfo: TThreadNameInfo;
   {$ENDIF FPC}
   {$ENDIF MSWINDOWS}
begin
   {$IFDEF MSWINDOWS}
   {$IFNDEF FPC}
   ThreadNameInfo.FType := $1000;
   ThreadNameInfo.FName := 'FileFinder_X';
   ThreadNameInfo.FThreadID := $FFFFFFFF;
   ThreadNameInfo.FFlags := 0;
   try
      RaiseException($406D1388, 0, SizeOf(ThreadNameInfo) div SizeOf(longword), @ThreadNameInfo);
   except
   end;
   {$ENDIF FPC}
   {$ENDIF MSWINDOWS}
end;

procedure TFileFinder.SetOnFileWithHashFound(const Value: TFileWithHashFoundEvent);
begin
   FOnFileWithHashFound := Value;
   FCreateHashes := Assigned(FOnFileWithHashFound);
end;

procedure TFileFinder.StartScan;
var
   sPath, sFolder: String;
   iFolder: integer;

   procedure ConvertPathToStack;
   begin
      while Pos(';', sPath) > 0 do begin
         sFolder := Copy(sPath, 1, Pos(';', sPath) - 1);
         AddPathToStack(sFolder);
         System.Delete(sPath, 1, Pos(';', sPath));
      end;
   end;

begin
   //DebugLogger.LogEnterMethod(Self, 'StartScan', [FStartFolders]);
   try
      FScanning := True;
      FTimeStarted := Now;
      FFilesFound := 0;
      FFoldersFound := 0;
      FStop := False;
      sPath := FStartFolders;
      ConvertPathToStack;
      AddPathToStack(sPath);
      for iFolder := 0 to Pred(FFolderStack.Count) do begin
         if ((Length(FFolderStack[iFolder]) = 2) and (Copy(FFolderStack[iFolder], 2, 1) = ':')) then begin
            FFolderStack[iFolder] := FFolderStack[iFolder] + PathSeparator;
         end;
         if (Copy(FFolderStack[iFolder], Length(FFolderStack[iFolder]), 1) = PathSeparator) then begin
            FFolderStack[iFolder] := FFolderStack[iFolder] + PathAllFilesMask;
         end;
         //DebugLogger.LogVarString('FFolderStack[iFolder]', FFolderStack[iFolder]);
      end;
      if FFolderStack.Count = 1 then begin
         FCurrentBasePath := ExtractFilePath(FFolderStack[0]);
      end else begin
         FCurrentBasePath := '';
      end;
      //DebugLogger.LogVarString('FCurrentBasePath', FCurrentBasePath);
      while (FFolderStack.Count > 0) and (not FStop) and ((not FThreadRunning) or (not Terminated)) do begin
         FireFolderProgressEvent(FFolderStack[0]);
         ScanFolder(FFolderStack[0]);
         FFolderStack.Delete(0);
      end;
      FTimeFinished := Now;
      FScanning := False;
      FStop := False;
      FireFinishedEvent;
   finally
      //DebugLogger.LogLeaveMethod(Self, 'StartScan');
   end;
end;

procedure TFileFinder.DoFileFound(APath: String; AInfo: TSearchRec);
var
   dwErrorCode: longword;
   sHash: rawbytestring;
begin
   if not FReportFullPath then begin
      if FReportRelativePath and (Length(FCurrentBasePath) > 0) then begin
         APath := ExtractRelativePath(FCurrentBasePath, APath);
      end else begin
         APath := ExtractFilename(APath);
      end;
   end;
   { TODO -oPatrick : Relative path here! }
   Inc(FFilesFound);
   FSyncPath := APath;
   FSyncInfo := AInfo;
   if FThreadRunning then begin
      SynchronizeEx(SyncFireFileFoundEvent);
   end else begin
      SyncFireFileFoundEvent;
   end;
   if FCreateHashes and Assigned(FHashClass) then begin
      // FSyncMD5 := MD5PrintA(MD5File(APath));
      // if GetRawByteFileStreamHash(APath, THashAlgorithmDECMD5, sHash, dwErrorCode) then begin
      if GetRawByteFileStreamHash(WideString(APath), FHashClass, sHash, dwErrorCode) then begin
         FSyncMD5 := sHash;
         // FSyncMD5 := FastestMD5File(APath);
         if FThreadRunning then begin
            SynchronizeEx(SyncFireFileWithHashFoundEvent);
         end else begin
            SyncFireFileWithHashFoundEvent;
         end;
      end else begin
         // TODO : error handling
         // ShowMessage('fail:GetRawByteFileStreamHash: ' + SysErrorMessage(dwErrorCode));
      end;
   end;
end;

procedure TFileFinder.DoFolderFound(APath: String; AInfo: TSearchRec);
begin
   if AnsiCompareText(IncludeTrailingPathDelimiter(APath), FCurrentBasePath) = 0 then begin
      Exit;
   end;
   if not FReportFullPath then begin
      APath := ExtractFilename(APath);
   end;
   Inc(FFoldersFound);
   FSyncPath := APath;
   FSyncInfo := AInfo;
   if FThreadRunning then begin
      SynchronizeEx(SyncFireFolderFoundEvent);
   end else begin
      SyncFireFolderFoundEvent;
   end;
end;

procedure TFileFinder.FireFolderProgressEvent(APath: String);
begin
   FSyncPath := APath;
   if FThreadRunning then begin
      SynchronizeEx(SyncFireFolderProgressEvent);
   end else begin
      SyncFireFolderProgressEvent;
   end;
end;

function TFileFinder.GetTimeElapsed: TDateTime;
begin
   Result := FTimeFinished - FTimeStarted;
end;

procedure TFileFinder.FireFinishedEvent;
begin
   if FThreadRunning then begin
      SynchronizeEx(SyncFireFinishedEvent);
   end else begin
      SyncFireFinishedEvent;
   end;
end;

constructor TFileFinder.Create;
begin
   FreeOnTerminate := False;
   FThreadRunning := False;
   inherited Create(True);
   FFollowSymLinks := True;
   FFilesFound := 0;
   FFoldersFound := 0;
   FTimeStarted := 0;
   FTimeFinished := 0;
   FCreateHashes := False;
   FReportFullPath := True;
   FRecurseFolders := True;
   FFolderStack := TStringList.Create;
   FScanning := False;
   FStop := False;
   FHashClass := nil;
end;

destructor TFileFinder.Destroy;
begin
   FFolderStack.Free;
   inherited;
end;

procedure TFileFinder.Execute;
begin
   FThreadRunning := True;
   {$IFDEF UseSetName}
   SetName;
   {$ENDIF UseSetName}
   StartScan;
   FThreadRunning := False;
end;

procedure TFileFinder.Scan(const APath: String; AThreaded: boolean);
begin
   //DebugLogger.LogEnterMethod(Self, 'Scan', [APath]);
   try
      FStartFolders := APath;
      if AThreaded then begin
         Start;
      end else begin
         StartScan;
      end;
   finally
      //DebugLogger.LogLeaveMethod(Self, 'Scan');
   end;
end;

{ TFileFinderCollector }

constructor TFileFinderCollector.Create;
begin
   FileList := TStringList.Create;
   FolderList := TStringList.Create;
end;

destructor TFileFinderCollector.Destroy;
begin
   FolderList.Free;
   FileList.Free;
end;

procedure TFileFinderCollector.FileFound(Path: String; Info: TSearchRec);
begin
   FileList.Add(Path);
end;

procedure TFileFinderCollector.FolderFound(Path: String; Info: TSearchRec);
begin
   FolderList.Add(Path);
end;

function SolveEnv(const AFilenameInEnvironment: String): String;
var
   sFilename: String;
   pcFilename: pwidechar
   {$IFDEF FPC}
   = nil
   {$ENDIF FPC}
   ;
begin
   {$IFNDEF MSWindows}
   { TODO : implement linux environment stuff! }
   Result := AFilenameInEnvironment;
   {$ELSE MSWindows}
   sFilename := AFilenameInEnvironment;
   GetMem(pcFilename, MAX_PATH * SizeOf(widechar));
   try
      ExpandEnvironmentStringsW(pwidechar(sFilename), pcFilename, MAX_PATH);
      Result := pcFilename;
   finally
      FreeMem(pcFilename, MAX_PATH);
   end;
   {$ENDIF MSWindows}
end;

function RemoveQuotes(const AQuotedFilename: String): String;
const
   SQuotes: String   = '"';
   SExeBlank: String = 'EXE ';
var
   sFilename: String;
begin
   sFilename := AQuotedFilename;
   if Length(sFilename) > 0 then begin
      if sFilename[1] = SQuotes then begin
         sFilename := Copy(sFilename, 2, Length(sFilename) - 1);
         if Pos(SQuotes, sFilename) > 0 then begin
            sFilename := Copy(sFilename, 1, Pos(SQuotes, sFilename) - 1);
         end;
      end else begin
         if Pos(SExeBlank, UpperCase(sFilename)) > 0 then begin
            sFilename := Copy(sFilename, 1, Pos(SExeBlank, UpperCase(sFilename)) + 2);
         end;
      end;
      Result := sFilename;
   end else
      Result := '';
end;

function CommandLineToFilename(const CommandLine: ansistring): ansistring;
var
   sFilenameTemp, sPath, sWinDir, sSysDir: ansistring;
   iPosChar, iPosText, i: integer;
   bQuotesProcessed: boolean;
   pcFilename, pcPath: pansichar;
   slEnvPaths: TStringList;
begin
   Result := CommandLine;
   if FileExists(Result) then begin
      Exit;
   end;
   // service full path
   if Copy(Result, 1, 4) = '\??\' then begin
      Delete(Result, 1, 4);
   end;
   if FileExists(Result) then begin
      Exit;
   end;
   // process environment
   {$IFDEF MSWindows}
   GetMem(pcFilename, MAX_PATH);
   try
      ExpandEnvironmentStringsA(pansichar(Result), pcFilename, MAX_PATH);
      Result := pcFilename;
   finally
      FreeMem(pcFilename, MAX_PATH);
   end;
   {$ENDIF MSWindows}
   if FileExists(Result) then begin
      Exit;
   end;
   // check rundll32 stuff
   if Copy(LowerCase(Result), 1, 8) = 'rundll32' then begin
      iPosChar := Pos(' ', Result);
      if iPosChar > 0 then begin
         Delete(Result, 1, iPosChar);
         iPosChar := Pos(',', Result);
         if iPosChar > 0 then begin
            SetLength(Result, Pred(iPosChar));
         end;
      end;
      if FileExists(Result) then begin
         Exit;
      end;
   end;
   // check QUOTES
   bQuotesProcessed := False;
   if Length(Result) > 0 then begin
      if Result[1] = '"' then begin
         sFilenameTemp := Copy(Result, 2, Length(Result) - 1);
         iPosChar := Pos('"', sFilenameTemp);
         if iPosChar > 0 then begin
            Result := Copy(sFilenameTemp, 1, Pred(iPosChar));
         end else begin
            Result := sFilenameTemp;
         end;
         if FileExists(Result) then begin
            Exit;
         end;
         bQuotesProcessed := True;
      end;
   end;
   // check .EXE
   if (not bQuotesProcessed) then begin
      iPosText := Pos('.exe', LowerCase(Result));
      if (iPosText > 0) then begin
         SetLength(Result, iPosText + 3);
         if FileExists(Result) then begin
            Exit;
         end;
      end;
   end;
   {$IFDEF MSWindows}
   // check no full at all
   // if (Pos('\', Result)=0) then begin
   if Copy(Result, 2, 2) <> ':\' then begin
      GetMem(pcPath, MAX_PATH);
      try
         GetWindowsDirectoryA(pcPath, MAX_PATH);
         sWinDir := pcPath;
         GetSystemDirectoryA(pcPath, MAX_PATH);
         sSysDir := pcPath;
      finally
         FreeMem(pcPath, MAX_PATH);
      end;
      slEnvPaths := TStringList.Create; // get path
      try
         sPath := SolveEnv(ansistring('%PATH%'));
         while Pos(';', sPath) > 0 do begin
            i := Pos(';', sPath);
            System.Delete(sPath, i, 1);
            Insert(#13#10, sPath, i);
         end;
         slEnvPaths.CommaText := sPath;
         slEnvPaths.Add(sWinDir);
         slEnvPaths.Add(sSysDir);
         slEnvPaths.Add(IncludeTrailingPathDelimiter(sWinDir) + 'system');
         for i := 0 to Pred(slEnvPaths.Count) do slEnvPaths[i] := IncludeTrailingPathDelimiter(slEnvPaths[i]);
         for i := 0 to Pred(slEnvPaths.Count) do begin
            if FileExists(slEnvPaths[i] + Result) then begin
               Result := slEnvPaths[i] + Result;
               Exit;
            end;
            if FileExists(slEnvPaths[i] + Result + '.exe') then begin
               Result := slEnvPaths[i] + Result + '.exe';
               Exit;
            end;
         end;
      finally
         slEnvPaths.Free;
      end;
   end;
   {$ENDIF MSWindows}
end;

function ExpandComplicatedFilename(const AFilename: String): String;
var
   WinDir, SysDir: String;
   pc: pwidechar
   {$IFDEF FPC}
   = nil
   {$ENDIF FPC}
   ;
   EnvPaths: TStringList;
   sTemp, Path: String;
   i: integer;
begin
   // *** First, try the filename itself
   Result := AFilename;
   if FileExists(Result) then begin
      Exit;
   end;
   // *** Special RunDll32 construct:
   // i := Pos('RUNDLL32 ',UpperCase(Result));
   // if i>0 then Insert('.exe',Result,i+8);
   if (Copy(UpperCase(Result), 1, 8) = 'RUNDLL32') and (Pos(' ', Result) > 0) then begin
      System.Delete(Result, 1, Pos(' ', Result));
      if Pos(',', Result) > 0 then begin
         Result := Copy(Result, 1, Pos(',', Result) - 1);
      end;
   end;
   // *** then try to remove any command line quote stuff
   // Result := ExtractFilenameFromCommandLine(Result);
   Result := RemoveQuotes(Result);
   if FileExists(Result) then begin
      Exit;
   end;
   sTemp := Result;
   i := Pos('.EXE', UpperCase(sTemp));
   sTemp := Copy(sTemp, 1, i + 3);
   if FileExists(sTemp) then begin
      Result := sTemp;
   end;
   if FileExists(Result) then begin
      Exit;
   end;
   // *** if the file extension is missing, try to construct it
   if Pos('.', Result) < 1 then begin
      i := 1;
      while (i < Length(Result)) and (Result[i] <> '.') do begin
         Inc(i);
      end;
      while (i < Length(Result)) and (Result[i] <> ' ') do begin
         Inc(i);
      end;
      SetLength(Result, i);
      if not FileExists(Result) then begin
         Result := Result + '.exe';
      end;
   end;
   // *** see if environment entry is in path
   if Pos('%', Result) > 0 then begin
      Result := SolveEnv(Result);
   end;
   if FileExists(Result) then begin
      Exit;
   end;
   // *** try to locate the file somewhere in the path
   {$IFDEF MSWindows}
   GetMem(pc, MAX_PATH);
   try
      GetWindowsDirectoryW(pc, MAX_PATH);
      WinDir := pc; // get Win and Sys dir
      GetSystemDirectoryW(pc, MAX_PATH);
      SysDir := pc;
   finally
      FreeMem(pc, MAX_PATH);
   end;
   {$ENDIF MSWindows}
   EnvPaths := TStringList.Create; // get path
   try
      Path := SolveEnv('%PATH%');
      while Pos(';', Path) > 0 do begin
         i := Pos(';', Path);
         System.Delete(Path, i, 1);
         Insert(#13#10, Path, i);
      end;
      EnvPaths.CommaText := Path;
      EnvPaths.Add(WinDir);
      EnvPaths.Add(SysDir);
      EnvPaths.Add(IncludeTrailingPathDelimiter(WinDir) + 'system');
      for i := 0 to Pred(EnvPaths.Count) do EnvPaths[i] := IncludeTrailingPathDelimiter(EnvPaths[i]);
      for i := 0 to Pred(EnvPaths.Count) do begin
         if FileExists(EnvPaths[i] + Result) then begin
            Result := EnvPaths[i] + Result;
            Exit;
         end;
         if FileExists(EnvPaths[i] + Result + '.exe') then begin
            Result := EnvPaths[i] + Result + '.exe';
            Exit;
         end;
      end;
      if not FileExists(Result) then begin
         Result := '';
      end;
   finally
      EnvPaths.Free;
   end;
end;

function ExtractFilenameFromCommandLine(const AFilename: String): String;
   // 2006-09-15  pk  Please note that RemoveQuotes works better than this!
begin
   Result := AFilename;
   if (Copy(Result, 1, 1) = '"') then begin
      System.Delete(Result, 1, 1);
      if Pos('"', Result) > 0 then begin
         SetLength(Result, Pos('"', Result) - 1);
      end;
   end else if (Pos(' ', Result) > 0) and (Pos(' ', Result) > Pos('.', Result)) then begin
      SetLength(Result, Pos(' ', Result) - 1);
   end else if Pos('.EXE', UpperCase(Result)) < (Length(Result) - 5) then begin
      SetLength(Result, Pos('.EXE', UpperCase(Result)) + 3);
   end;
end;

function ExtractFilePathW(const Filename: String): String;
var
   iPos: integer;
begin
   iPos := LastDelimiter(PathDelim + DriveDelim, Filename);
   Result := Copy(Filename, 1, iPos);
end;

function GetFileSizeFromInt(iLow, iHigh: integer): int64;
begin
   Result := int64(iLow) + int64(int64(iHigh) shl 32);
end;

function GetFileSizeString(const AFilename: String): String;
begin
   Result := IntToStr(GetFileSizeInt64(AFilename));
end;

function GetFileSizeInt64(const AFilename: String): int64;
var
   h: integer;
   cSizeLow, cSizeHigh: cardinal;
   {$IFNDEF MSWindows}
   sr: TSearchRec;
   ec: integer;
   {$ENDIF MSWindows}
begin
   Result := 0;
   if FileExists(AFilename) then begin
      {$IFNDEF MSWindows}
      ec := FindFirst(AFilename, faAnyFile, sr);
      if ec = 0 then begin
         Result := sr.Size;
         FindClose(sr);
      end;
      {$ELSE MSWindows}
      h := FileOpen(AFilename, fmOpenRead or fmShareDenyNone);
      try
         if h <> -1 then begin
            cSizeLow := GetFileSize(h, @cSizeHigh);
            Result := cSizeLow + (cSizeHigh shl 32);
         end;
      finally
         FileClose(h);
      end;
      {$ENDIF MSWindows}
   end;
end;

function MyFileSizeToStringShort(AFilesize: int64): string;
const
   sDimension: array [0 .. 5] of string = (' b', ' k', ' M', ' G', ' T', ' P');
var
   iLevel: integer;
begin
   iLevel := 0;
   while (AFilesize > 1024) and (iLevel < 5) do begin
      AFilesize := AFilesize div 1024;
      Inc(iLevel);
   end;
   Result := IntToStr(AFilesize) + sDimension[iLevel];
end;

function GetFileDateString(const AFilename: String): String;
   // 2016-05-17  pk  Removed separate FreePascal version.
   // 2007-04-29  pk  For FPC it isn't deprecated though.
   // 2005-11-17  pk  Delphi 2006 says that FileAge is deprecated...
var
   dtFileAge: TDateTime;
begin
   Result := '';
   if FileAge(AFilename, dtFileAge) then begin
      Result := FormatDateTime('dddddd tt', dtFileAge);
   end;
end;

end.
