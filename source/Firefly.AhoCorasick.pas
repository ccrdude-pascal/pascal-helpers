{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Implements Aho-Corasick search algorithm.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2007-2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2016-08-31  pk  20m  Fixed #4013 by implementing case insensitivity.
// 2016-05-11  pk   5m  Removed custom THashedStringList since FreePascal now has one.
// 2016-05-04  pk  30m  Add documentation
// 2016-05-04  pk  10m  Moved to new PepiMKBase package
// 2010-10-14  ##   --  Beautified code (using Code Beautifier Wizard)
// 2007-09-05  pk  30m  Updated for FreePascal
// 2007-08-30  pk   2h  Added stream functions
// 2007-08-29  pk   4h  Started coding
// *****************************************************************************
// Credits to Pekka Kilpeläinen from the University of Kuopio, whose 2005 slides
// on the Aho-Corasick algoritm where the base for this.
// O(n+m+z)
// *****************************************************************************
   )
}

unit Firefly.AhoCorasick;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   SysUtils,
   Classes,
   IniFiles,
   Generics.Collections,
   Firefly.Strings.RegularExpressions;

type
   {
     Event that gets triggered on each found search term.

     @param(APosition identifies the position where a term was found.)
     @param(ASearchTerm contains the term that was found.)
     @param(AData is the data associated with the search term.)
   }
   TOnStringSearchResultEvent = procedure(APosition: int64; ASearchTerm: string; AData: TObject) of object;

   { TStringSearchResult }

   TStringSearchResult = class
   private
      FPosition: int64;
      FSearchTerm: string;
      FValue: string;
      FReplacer: string;
   public
      property Position: int64 read FPosition write FPosition;
      property SearchTerm: string read FSearchTerm write FSearchTerm;
      property Replacer: string read FReplacer write FReplacer;
      property Value: string read FValue write FValue;
   end;

   { TStringSearchResultList }

   TStringSearchResultList = class(TObjectList<TStringSearchResult>)
   public
      function AddResult(APosition: int64; ASearchTerm, AValue: string; AReplacer: string = ''): TStringSearchResult;
   end;

   { TStringSearchAlgorithmBase is an abstract base class for string search algorithms. }

   TStringSearchAlgorithmBase = class
   private
      FKeywords: TStringList;
      FOnStringSearchResult: TOnStringSearchResultEvent;
      FCaseSensitive: boolean;
   protected
      {
        Triggers an event about a found search term.

        @param(APosition is the position of search term in search data.)
        @param(ASearchTerm contains the term that was found.)
        @param(AData is the data associated with search term through TStringList.Objects.)
      }
      procedure FireStringSearchResultEvent(APosition: int64; ASearchTerm: string; AData: TObject); virtual;
   public
      constructor Create;
      destructor Destroy; override;
      procedure FindAll(const AText: ansistring; const AResult, APositions: TStrings); overload; virtual; abstract;
      procedure FindAll(const AText: ansistring; const AResult: TStringSearchResultList = nil); overload; virtual; abstract;
      procedure FindFirst(const AText: ansistring; out APosition: int64); overload; virtual; abstract;
      {
        Initiates a search for the first occurance of a keyword inside the search data.

        @param(AText is the text to search in.)
        @param(APosition will return position of search term, if found.)
        @param(AData is the data associated with search term through TStringList.Objects)
      }
      procedure FindFirst(const AText: ansistring; out APosition: int64; var Data: TObject); overload; virtual;
      function ContainsAny(const AText: ansistring): boolean; virtual; abstract;
      procedure FindInStream(const AStream: TStream; const AFindAll: boolean; const Result, APositions: TStrings; const Size: int64 = 0); overload; virtual; abstract;
      procedure FindInStream(const AStream: TStream; const AFindAll: boolean; const Result: TStringSearchResultList; const Size: int64 = 0); overload; virtual; abstract;
      property Keywords: TStringList read FKeywords;
      property CaseSensitive: boolean read FCaseSensitive write FCaseSensitive;
      property OnStringSearchResult: TOnStringSearchResultEvent read FOnStringSearchResult write FOnStringSearchResult;
   end;

   TACTreeNode = class;

   {
     TACTreeNodeArray is a helper array of TACTreeNode.
   }
   TACTreeNodeArray = array of TACTreeNode;

   { TACTreeNode represents one node in an Aho-Corasick search tree. }

   TACTreeNode = class
   protected
      FStreamId: integer;
      FStreamIdParent: integer;
      FStreamIdFailure: integer;
      FChar: ansichar;
      FParent, FFailure: TACTreeNode;
      FResults: TStringList;
      FResultsArray: TStringList;
      FTransitionsArray: TACTreeNodeArray;
      FTransHash: THashedStringList;
      FCaseSensitive: boolean;
   protected
   public
      {
        Constructor.

        @param(AParent is the parent node of this new one.)
        @param(ACharacter is the character this node is for.)
        @param(ACaseSensitive defines whether the search tree should be case sensitive.)
      }
      constructor Create(const AParent: TACTreeNode; const ACharacter: ansichar; ACaseSensitive: boolean);
      destructor Destroy; override; //< Destructor
      {
        Adds a result term to this node.

        @param(AResult is the result text that is available once this node is reached.)
      }
      procedure AddResult(const AResult: string); overload;
      {
        Adds a result term, and data, to this node.

        @param(AResult is the result text that is available once this node is reached.)
        @param(AData is the data associated with the result.)
      }
      procedure AddResult(const AResult: string; AData: TObject); overload;
      {
        Adds a transition to another node to this one.

        @param(ANode is another node a transition should lead to.)
      }
      procedure AddTransition(const ANode: TACTreeNode);
      {
        Gets transaction for specified next character to next node in tree.

        @param(ACharacter is the next character in the text that is searched in.)
      }
      function GetTransition(const ACharacter: ansichar): TACTreeNode;
      {
        Checks whether a transition exists for a net character.

        @param(ACharacter is the character to look up.)
        @returns 8True if a transaction for the character exists, meaning we're still within a possible search result.)
      }
      function ContainsTransition(const ACharacter: ansichar): boolean;
      {
        Saves an Aho-Corasick search tree to a stream

        @param(AStream is a stream object to write to.)
      }
      procedure SaveToStream(AStream: TStream);
      {
        Saves an Aho-Corasick search tree to a stream.

        @param(AStream is a stream object to load from.)
      }
      procedure LoadFromStream(AStream: TStream);
      property Character: ansichar read FChar write FChar;
      property Parent: TACTreeNode read FParent;
      property Failure: TACTreeNode read FFailure write FFailure;
      property Transitions: TACTreeNodeArray read FTransitionsArray;
      property Results: TStringList read FResultsArray;
      property StreamId: integer read FStreamId write FStreamId;
      property StreamIdParent: integer read FStreamIdParent write FStreamIdParent;
      property StreamIdFailure: integer read FStreamIdFailure write FStreamIdFailure;
   end;

   TACTreeNodes = class;

   { TACTreeNodeEnumerator allows to enumerate a Aho-Corasick search tree. }

   TACTreeNodeEnumerator = class
   private
      FIndex: integer;
      FList: TACTreeNodes;
   public
      {
        Constructor.

        @param(AList is the list to enumerate.)
      }
      constructor Create(AList: TACTreeNodes);
      {
        Returns the current node.

        @returns(The current node.)
      }
      function GetCurrent: Pointer;
      {
        Moves to next node in array.

        @returns(True if a next one was available.)
      }
      function MoveNext: boolean;
      property Current: Pointer read GetCurrent;
   end;

   { TACTreeNodes represents a list of nodes in an Aho-Corasick search tree. }

   TACTreeNodes = class(TList)
   private
      {
        Returns a subnode.

        @param(AIndex defines the index of the node to get.)
        @returns(Node defined by @code(AIndex).)
      }
      function Get(AIndex: integer): TACTreeNode;
      {
        Stores a subnode at specified index.

        @param(AIndex is the position where the node should be added.)
        @param(ANode is the node to add.)
      }
      procedure Put(AIndex: integer; const ANode: TACTreeNode);
   public
      {
        Adds a node to the tree of subnodes.

        @param(ANode is the node to process.)
      }
      function Add(ANode: TACTreeNode): integer;
      {
        Returns an enumerator for subnodes.
        @returns(Enumerator.)
      }
      function GetEnumerator: TACTreeNodeEnumerator;
      property Items[index: integer]: TACTreeNode read Get write Put; default;
   end;

   {
      TIdNodeCombi combines a node id with a node, used in loading and
      restoring nodes from files.
   }
   TIdNodeCombi = record
      Id: cardinal;
      Node: TACTreeNode;
   end;

   {
      TIdNodeCombis is a list of id/node combinations, used in loading and
      restoring nodes from files.
   }
   TIdNodeCombis = array of TIdNodeCombi;

   {
     TAhoCorasickReplacementObject is used to store replacement text in the
     data associated with a keyword.
   }
   TAhoCorasickReplacementObject = class(TObject)
      Replacer: ansistring;
   end;

   {
     TAhoCorasick implements the Aho-Corasick search algorithm.
   }
   TAhoCorasick = class(TStringSearchAlgorithmBase)
   private
      FRoot: TACTreeNode;
      FIdNodeCombis: TIdNodeCombis;
      FSeparatedList: boolean;
      FDynObjectList: TList;
      {
        Restores node Id after loading.

        @param(ANode is the node to process.)
      }
      procedure StoreNodeId(ANode: TACTreeNode);
      {
        Restores failure nodes after having loaded a tree.

        @seealso(LoadTreeFromFile)
      }
      procedure RestoreFailureNodes;
   public
      constructor Create; overload; //< Constructor.
      {
        Complete constructor.

        @param(AKeywords is a list of keywords to search for.)
        @param(ASeparatedList defines if the list contains searchterm=replaceterm pairs, otherwise just searchterms.)
      }
      constructor Create(const AKeywords: TStrings; ASeparatedList: boolean = False); overload;
      destructor Destroy; override; //< Destructor.
      {
        Builds the keyword list.

        @param(ASeparatedList defines if the list contains searchterm=replaceterm pairs, otherwise just searchterms.)
      }
      procedure BuildTree(ASeparatedList: boolean = False);
      {
        Loads a search tree from a file.

        @param(AFilename is the name to load the tree from.)
      }
      procedure LoadTreeFromFile(const AFilename: WideString);
      {
        Searches for all occurances of text
        @param(AText is the keyword to search for)
        @param(APosition will return position of search term, if found.)
        @param(AResult is the list that will receive the results.)
      }
      procedure FindAll(const AText: ansistring; const AResult, APositions: TStrings); override; overload;
      procedure FindAll(const AText: ansistring; const AResult: TStringSearchResultList = nil); override; overload;
      {
        Initiates a search for the first occurance of a keyword inside the search data.

        @param(AText is the keyword to search for)
        @param(APosition will return position of search term, if found.)
      }
      procedure FindFirst(const AText: ansistring; out APosition: int64); override;
      {
        Initiates a search for the first occurance of a keyword inside the search data.

        @param(AText is the keyword to search for)
        @param(APosition will return position of search term, if found.)
        @param(AData returns the data associated with search term through TStringList.Objects.)
      }
      procedure FindFirst(const AText: ansistring; out APosition: int64; var Data: TObject); override;
      {
        Searches inside a stream.

        @param(AStream is the stream of data to search in.)
        @param(AFindAll defines whether all or just one occurance shold be searched for.)
        @param(AResult returns all found instances.)
        @param(ASize limits the number of bytes to search, if larger than zero.)
      }
      procedure FindInStream(const AStream: TStream; const {%H-}AFindAll: boolean; const AResult, APositions: TStrings; const ASize: int64 = 0); override; overload;
      procedure FindInStream(const AStream: TStream; const {%H-}AFindAll: boolean; const AResult: TStringSearchResultList; const ASize: int64 = 0); override; overload;
      {
        Checks the specified text using the keywords, returning a simple state only.

        @param(AText is the text to search in.)
      }
      function ContainsAny(const AText: ansistring): boolean; override;
      property Root: TACTreeNode read FRoot;
   end;

   {
     TAhoCorasickReplacer implements the Aho-Corasick search algorithm, including search and replace functionality.
   }
   TAhoCorasickReplacer = class(TAhoCorasick)
   private
      FReplaceCount: integer;
      FInput, FResult: ansistring;
      FCopyStartPos: int64;
      FRegExpr: boolean;
   protected
      {
        Triggers an event about a found search term.
        @param(APosition identifies the position of search term in search data.)
        @param(ASearchTerm contains term that was found.)
        @param(AData passes data associated with search term through TStringList.Objects.)
      }
      procedure FireStringSearchResultEvent(APosition: int64; ASearchTerm: string; AData: TObject); override;
   public
      {
        Replace in text according to keyword list.

        @param(AText is the text to search in.)
      }
      function Replace(const Text: ansistring): integer;
      destructor Destroy; override;
      property Result: ansistring read FResult;
      property ReplaceCount: integer read FReplaceCount;
   end;

   {
     TAhoCorasickMultiReplacer implements the Aho-Corasick search algorithm, including search and replace functionality that can replace with multiple terms, causing multiple results.
   }
   TAhoCorasickMultiReplacer = class(TAhoCorasick)
   private
      FReplaceCount: integer;
      FInput: ansistring;
      FCopyStartPos: int64;
      FResults: TStringList;
      FRegExpr: boolean;
   protected
      {
        Triggers an event about a found search term.

        @param(APosition identifies the position of search term in search data.)
        @param(ASearchTerm contains term that was found.)
        @param(AData passes data associated with search term through TStringList.Objects.)
      }
      procedure FireStringSearchResultEvent(APosition: int64; ASearchTerm: string; AData: TObject); override;
   public
      constructor Create; overload;
      {
        Constructor.

        @param(AKeywords is a list of keywords.)
        @param(ASeparatedList defines if the list contains searchterm=replaceterm pairs, otherwise just searchterms.)
      }
      constructor Create(const AKeywords: TStrings; ASeparatedList: boolean = False); overload;
      {
        Replace in text according to keyword list.

        @param(AText is the text to search in.)
      }
      function Replace(const Text: ansistring): integer;
      destructor Destroy; override;
      property Results: TStringList read FResults;
      property ReplaceCount: integer read FReplaceCount;
   end;

implementation

{ TACTreeNode }

procedure TACTreeNode.AddResult(const AResult: string);
begin
   if FResults.IndexOf(AResult) > -1 then begin
      Exit;
   end;
   FResults.Add(AResult);
   FResultsArray.Assign(FResults);
end;

procedure TACTreeNode.AddResult(const AResult: string; AData: TObject);
begin
   if FResults.IndexOf(AResult) > -1 then begin
      Exit;
   end;
   FResults.AddObject(AResult, AData);
   FResultsArray.Assign(FResults);
end;

procedure TACTreeNode.AddTransition(const ANode: TACTreeNode);
var
   i: integer;
begin
   i := FTransHash.AddObject(string(char(ANode.Character)), ANode);
   SetLength(FTransitionsArray, FTransHash.Count);
   FTransitionsArray[i] := ANode;
end;

function TACTreeNode.ContainsTransition(const ACharacter: ansichar): boolean;
begin
   Result := Assigned(GetTransition(ACharacter));
end;

constructor TACTreeNode.Create(const AParent: TACTreeNode; const ACharacter: ansichar; ACaseSensitive: boolean);
begin
   FChar := ACharacter;
   FParent := AParent;
   FFailure := nil;
   FResults := TStringList.Create;
   FResultsArray := TStringList.Create;
   SetLength(FTransitionsArray, 0);
   FTransHash := THashedStringList.Create;
   FTransHash.CaseSensitive := ACaseSensitive;
   FCaseSensitive := ACaseSensitive;
end;

destructor TACTreeNode.Destroy;
var
   i: integer;
begin
   FResults.Free;
   FResultsArray.Free;
   SetLength(FTransitionsArray, 0);
   for i := 0 to Pred(FTransHash.Count) do begin
      if Assigned(FTransHash.Objects[i]) then begin
         TACTreeNode(FTransHash.Objects[i]).Free;
      end;
   end;
   FTransHash.Free;
   inherited;
end;

function TACTreeNode.GetTransition(const ACharacter: ansichar): TACTreeNode;
var
   i: integer;
begin
   Result := nil;
   i := FTransHash.IndexOf(string(char(ACharacter)));
   if i < 0 then begin
      Exit;
   end;
   Result := TACTreeNode(FTransHash.Objects[i]);
end;

procedure TACTreeNode.LoadFromStream(AStream: TStream);
var
   c, cCount: cardinal;
   s: ansistring = '';
begin
   cCount := 0;
   AStream.Read(FStreamId, 4);
   AStream.Read(FChar, 1);
   AStream.Read(FStreamIdFailure, 4);
   AStream.Read(cCount, 4);
   SetLength(s, cCount);
   AStream.Read(s[1], cCount);
   {$IFDEF Unicode}
   FResults.Text := UTF8Decode(s);
   {$ELSE Unicode}
   FResults.Text := s;
   {$ENDIF Unicode}
   AStream.Read(cCount, 4);
   SetLength(FTransitionsArray, cCount);
   if cCount > 0 then begin
      for c := 0 to Pred(cCount) do begin
         FTransitionsArray[c] := TACTreeNode.Create(Self, ' ', FCaseSensitive);
         FTransitionsArray[c].LoadFromStream(AStream);
      end;
   end;
end;

procedure TACTreeNode.SaveToStream(AStream: TStream);
var
   c, cCount: cardinal;
   s: ansistring;
begin
   if Assigned(Failure) then begin
      FStreamIdFailure := Failure.FStreamId;
   end else begin
      FStreamIdFailure := 0;
   end;
   // Size: 4  -  Content: ID
   AStream.Write(FStreamId, 4);
   // Size: 1  -  Content: Character
   AStream.Write(FChar, 1);
   // Size: 4  -  Content: Failure
   AStream.Write(FStreamIdFailure, 4);
   // Size: 4  -  Content: Results
   {$IFDEF Unicode}
   s := UTF8Encode(FResults.Text);
   {$ELSE Unicode}
   s := FResults.Text;
   {$ENDIF Unicode}
   c := Length(s);
   AStream.Write(c, 4);
   AStream.Write(s[1], c);
   // Size: 4  -  Content: Number of sub-nodes
   cCount := Length(FTransitionsArray);
   AStream.Write(cCount, 4);
   // Size: N  -  Content: Sub-nodes
   if cCount > 0 then
      for c := 0 to Pred(cCount) do begin
         FTransitionsArray[c].SaveToStream(AStream);
      end;
end;

{ TStringSearchResultList }

function TStringSearchResultList.AddResult(APosition: int64; ASearchTerm, AValue: string; AReplacer: string): TStringSearchResult;
begin
   Result := TStringSearchResult.Create;
   Result.Position := APosition;
   Result.SearchTerm := ASearchTerm;
   Result.Value := AValue;
   Result.Replacer := AReplacer;
   Self.Add(Result);
end;

{ TStringSearchAlgorithmBase }

constructor TStringSearchAlgorithmBase.Create;
begin
   inherited;
   FCaseSensitive := False;
   FKeywords := TStringList.Create;
end;

destructor TStringSearchAlgorithmBase.Destroy;
begin
   FKeywords.Free;
   inherited;
end;

procedure TStringSearchAlgorithmBase.FindFirst(const AText: ansistring; out APosition: int64; var Data: TObject);
begin
   FindFirst(AText, APosition);
   Data := nil;
end;

procedure TStringSearchAlgorithmBase.FireStringSearchResultEvent(APosition: int64; ASearchTerm: string; AData: TObject);
begin
   if Assigned(FOnStringSearchResult) then begin
      FOnStringSearchResult(APosition, ASearchTerm, AData);
   end;
end;

{ TAhoCorasick }

procedure TAhoCorasick.BuildTree(ASeparatedList: boolean);
var
   trans, nd, ndNew, r: TACTreeNode;
   cChar: ansichar;
   nodes, newNodes: TACTreeNodes;
   p: Pointer;
   sKeyword: ansistring;
   ro: TAhoCorasickReplacementObject;
   iResult, iKeyword, iChar, iTrans, iTransRoot, iNode, iChild: integer;
   cTreeId: cardinal;
begin
   // DONE : number tree to allow cross-references in saving
   // Build keyword tree and transition function
   FSeparatedList := ASeparatedList;
   cTreeId := 2;
   FRoot.Free;
   FRoot := TACTreeNode.Create(nil, ' ', CaseSensitive);
   FRoot.FStreamId := 1;
   for iKeyword := 0 to Pred(FKeywords.Count) do begin
      if ASeparatedList then begin
         {$IFDEF Unicode}
         sKeyword := UTF8Encode(FKeywords.Names[iKeyword]);
         {$ELSE Unicode}
         sKeyword := ansistring(FKeywords.Names[iKeyword]);
         {$ENDIF Unicode}
      end else begin
         {$IFDEF Unicode}
         sKeyword := UTF8Encode(FKeywords[iKeyword]);
         {$ELSE Unicode}
         sKeyword := ansistring(FKeywords[iKeyword]);
         {$ENDIF Unicode}
      end;
      // add pattern to tree
      nd := FRoot;
      for iChar := 1 to Length(sKeyword) do begin
         if FCaseSensitive then begin
            cChar := sKeyword[iChar];
         end else begin
            {$IFDEF FPC}
            cChar := LowerCase(sKeyword[iChar]);
            {$ELSE FPC}
            cChar := LowerCase(sKeyword[iChar])[1];
            {$ENDIF FPC}
         end;
         ndNew := nil;
         for iTrans := low(nd.Transitions) to high(nd.Transitions) do begin
            trans := nd.Transitions[iTrans];
            if trans.Character = cChar then begin
               ndNew := trans;
               break;
            end;
         end;
         if not Assigned(ndNew) then begin
            ndNew := TACTreeNode.Create(nd, cChar, CaseSensitive);
            ndNew.FStreamId := cTreeId;
            nd.AddTransition(ndNew);
            Inc(cTreeId);
         end;
         nd := ndNew;
      end;
      if ASeparatedList then begin
         ro := TAhoCorasickReplacementObject.Create;
         {$IFDEF Unicode}
         ro.Replacer := UTF8Encode(FKeywords.ValueFromIndex[iKeyword]);
         FDynObjectList.Add(ro);
         nd.AddResult(UTF8Decode(sKeyword), ro);
         {$ELSE Unicode}
         ro.Replacer := ansistring(FKeywords.ValueFromIndex[iKeyword]);
         FDynObjectList.Add(ro);
         nd.AddResult(sKeyword, ro);
         {$ENDIF Unicode}
      end else begin
         {$IFDEF Unicode}
         nd.AddResult(UTF8Decode(sKeyword), FKeywords.Objects[iKeyword]);
         {$ELSE Unicode}
         nd.AddResult(sKeyword, FKeywords.Objects[iKeyword]);
         {$ENDIF Unicode}
      end;
   end;
   // Find failure functions
   nodes := TACTreeNodes.Create;
   try
      // level 1 nodes - fail to root node
      for iTransRoot := low(FRoot.Transitions) to high(FRoot.Transitions) do begin
         nd := FRoot.Transitions[iTransRoot];
         nd.Failure := FRoot;
         for iTrans := low(nd.Transitions) to high(nd.Transitions) do begin
            nodes.Add(nd.Transitions[iTrans]);
         end;
      end;
      // other nodes - using BFS
      newNodes := nil;
      while (nodes.Count <> 0) do begin
         newNodes.Free;
         newNodes := TACTreeNodes.Create;
         for iNode := 0 to Pred(nodes.Count) do begin
            p := nodes[iNode];
            nd := TACTreeNode(p);
            r := nd.Parent.Failure;
            if FCaseSensitive then begin
               cChar := nd.Character;
            end else begin
               {$IFDEF FPC}
               cChar := LowerCase(nd.Character);
               {$ELSE FPC}
               cChar := LowerCase(nd.Character)[1];
               {$ENDIF FPC}
            end;
            while ((Assigned(r)) and (not r.ContainsTransition(cChar))) do begin
               r := r.Failure;
            end;
            if not Assigned(r) then begin
               nd.Failure := FRoot;
            end else begin
               nd.Failure := r.GetTransition(cChar);
               for iResult := 0 to Pred(nd.Failure.Results.Count) do begin
                  nd.AddResult(nd.Failure.Results[iResult], nd.Failure.Results.Objects[iResult]);
               end;
            end;
            // add child nodes to BFS list
            for iChild := low(nd.Transitions) to high(nd.Transitions) do begin
               newNodes.Add(nd.Transitions[iChild]);
            end;
         end;
         nodes.Free;
         nodes := newNodes;
         newNodes := nil;
      end;
   finally
      nodes.Free;
   end;
   FRoot.Failure := FRoot;
end;

function TAhoCorasick.ContainsAny(const AText: ansistring): boolean;
var
   ptr, trans: TACTreeNode;
   iIndex: integer;
   cChar: ansichar;
begin
   Result := False;
   ptr := FRoot;
   iIndex := 1;
   while iIndex < Length(AText) do begin
      trans := nil;
      while not Assigned(trans) do begin
         if FCaseSensitive then begin
            cChar := AText[iIndex];
         end else begin
            {$IFDEF FPC}
            cChar := LowerCase(AText[iIndex]);
            {$ELSE FPC}
            cChar := LowerCase(AText[iIndex])[1];
            {$ENDIF FPC}
         end;
         trans := ptr.GetTransition(cChar);
         if ptr = FRoot then begin
            break;
         end;
         if not Assigned(trans) then begin
            ptr := ptr.Failure;
         end;
         if not Assigned(ptr) then begin
            ptr := FRoot;
         end;
      end;
      if Assigned(trans) then begin
         ptr := trans;
      end;
      if ptr.Results.Count > 0 then begin
         Result := True;
         Exit;
      end;
      Inc(iIndex);
   end;
end;

constructor TAhoCorasick.Create(const AKeywords: TStrings; ASeparatedList: boolean);
var
   i: integer;
begin
   inherited Create;
   FRoot := nil;
   for i := 0 to Pred(AKeywords.Count) do begin
      FKeywords.AddObject(AKeywords[i], AKeywords.Objects[i]);
   end;
   BuildTree(ASeparatedList);
   SetLength(FIdNodeCombis, 0);
   FDynObjectList := TList.Create;
end;

destructor TAhoCorasick.Destroy;
var
   i: integer;
begin
   FRoot.Free;
   for i := 0 to Pred(FDynObjectList.Count) do begin
      try
         TAhoCorasickReplacementObject(FDynObjectList[i]).Free;
      except
         on E: Exception do begin
            //DebugLogger.LogException(E, 'TAhoCorasickReplacementObject(FDynObjectList[i]).Free');
         end;
      end;
   end;
   FDynObjectList.Free;
   inherited;
end;

procedure TAhoCorasick.FindAll(const AText: ansistring; const AResult, APositions: TStrings);
var
   ptr, trans: TACTreeNode;
   iIndex: integer;
   iResult: integer;
   sSearchTerm: string;
   cChar: ansichar;
   iPosition: int64;
begin
   ptr := FRoot;
   iIndex := 1;
   while iIndex <= Length(AText) do begin
      trans := nil;
      while not Assigned(trans) do begin
         if FCaseSensitive then begin
            cChar := AText[iIndex];
         end else begin
            {$IFDEF FPC}
            cChar := LowerCase(AText[iIndex]);
            {$ELSE FPC}
            cChar := LowerCase(AText[iIndex])[1];
            {$ENDIF FPC}
         end;
         trans := ptr.GetTransition(cChar);
         if ptr = FRoot then begin
            break;
         end;
         if not Assigned(trans) then begin
            ptr := ptr.Failure;
         end;
         // the following is a workaround that might not work around!
         // I only assume that it's the root reference that's nil only
         if not Assigned(ptr) then begin
            ptr := FRoot;
         end;
      end;
      if Assigned(trans) then begin
         ptr := trans;
      end;
      for iResult := 0 to Pred(ptr.Results.Count) do begin
         sSearchTerm := ptr.Results[iResult];
         iPosition := iIndex - Length(sSearchTerm) + 1;
         FireStringSearchResultEvent(iPosition, sSearchTerm, ptr.Results.Objects[iResult]);
         if Assigned(AResult) then begin
            if Assigned(APositions) then begin
               APositions.Add(IntToStr(iPosition));
            end;
            if FSeparatedList then begin
               AResult.Add(sSearchTerm + '=' + string(ansistring(pansichar(ptr.Results.Objects[iResult]))));
            end else begin
               AResult.Add(sSearchTerm);
            end;
         end;
      end;
      Inc(iIndex);
   end;
end;

procedure TAhoCorasick.FindAll(const AText: ansistring; const AResult: TStringSearchResultList);
var
   ptr, trans: TACTreeNode;
   iIndex: integer;
   iResult: integer;
   sSearchTerm: string;
   cChar: ansichar;
   iPosition: int64;
begin
   ptr := FRoot;
   iIndex := 1;
   while iIndex <= Length(AText) do begin
      trans := nil;
      while not Assigned(trans) do begin
         if FCaseSensitive then begin
            cChar := AText[iIndex];
         end else begin
            {$IFDEF FPC}
            cChar := LowerCase(AText[iIndex]);
            {$ELSE FPC}
            cChar := LowerCase(AText[iIndex])[1];
            {$ENDIF FPC}
         end;
         trans := ptr.GetTransition(cChar);
         if ptr = FRoot then begin
            break;
         end;
         if not Assigned(trans) then begin
            ptr := ptr.Failure;
         end;
         // the following is a workaround that might not work around!
         // I only assume that it's the root reference that's nil only
         if not Assigned(ptr) then begin
            ptr := FRoot;
         end;
      end;
      if Assigned(trans) then begin
         ptr := trans;
      end;
      for iResult := 0 to Pred(ptr.Results.Count) do begin
         sSearchTerm := ptr.Results[iResult];
         iPosition := iIndex - Length(sSearchTerm) + 1;
         FireStringSearchResultEvent(iPosition, sSearchTerm, ptr.Results.Objects[iResult]);
         if Assigned(AResult) then begin
            AResult.AddResult(iPosition, sSearchTerm, string(ansistring(pansichar(ptr.Results.Objects[iResult]))));
         end;
      end;
      Inc(iIndex);
   end;
end;

procedure TAhoCorasick.FindFirst(const AText: ansistring; out APosition: int64; var Data: TObject);
var
   ptr, trans: TACTreeNode;
   iIndex: integer;
   cChar: ansichar;
begin
   APosition := -1;
   Data := nil;
   ptr := FRoot;
   iIndex := 1;
   while iIndex < Length(AText) do begin
      trans := nil;
      while not Assigned(trans) do begin
         if FCaseSensitive then begin
            cChar := AText[iIndex];
         end else begin
            {$IFDEF FPC}
            cChar := LowerCase(AText[iIndex]);
            {$ELSE FPC}
            cChar := LowerCase(AText[iIndex])[1];
            {$ENDIF FPC}
         end;
         trans := ptr.GetTransition(cChar);
         if ptr = FRoot then begin
            break;
         end;
         if not Assigned(trans) then begin
            ptr := ptr.Failure;
         end;
         if not Assigned(ptr) then begin
            ptr := FRoot;
         end;
      end;
      if Assigned(trans) then begin
         ptr := trans;
      end;
      if ptr.Results.Count > 0 then begin
         APosition := iIndex - Length(ptr.Results[0]) + 1;
         Data := ptr.Results.Objects[0];
         FireStringSearchResultEvent(APosition, ptr.Results[0], Data);
         Exit;
      end;
      Inc(iIndex);
   end;
end;

procedure TAhoCorasick.FindFirst(const AText: ansistring; out APosition: int64);
var
   ptr, trans: TACTreeNode;
   iIndex: integer;
   cChar: ansichar;
begin
   APosition := -1;
   ptr := FRoot;
   iIndex := 1;
   while iIndex < Length(AText) do begin
      trans := nil;
      while not Assigned(trans) do begin
         if FCaseSensitive then begin
            cChar := AText[iIndex];
         end else begin
            {$IFDEF FPC}
            cChar := LowerCase(AText[iIndex]);
            {$ELSE FPC}
            cChar := LowerCase(AText[iIndex])[1];
            {$ENDIF FPC}
         end;
         trans := ptr.GetTransition(cChar);
         if ptr = FRoot then begin
            break;
         end;
         if not Assigned(trans) then begin
            ptr := ptr.Failure;
         end;
         if not Assigned(ptr) then begin
            ptr := FRoot;
         end;
      end;
      if Assigned(trans) then begin
         ptr := trans;
      end;
      if ptr.Results.Count > 0 then begin
         APosition := iIndex - Length(ptr.Results[0]) + 1;
         FireStringSearchResultEvent(APosition, ptr.Results[0], ptr.Results.Objects[0]);
         Exit;
      end;
      Inc(iIndex);
   end;
end;

procedure TAhoCorasick.FindInStream(const AStream: TStream; const AFindAll: boolean; const AResult, APositions: TStrings; const ASize: int64);
const
   BufferSize = 32768;
var
   ptr: TACTreeNode;
   trans: TACTreeNode;
   iCopySize: integer;
   iPositionBase: int64;
   iMax: int64;
   ms: TMemoryStream;
   c: ansichar;
   iFound: integer;
   ro: TAhoCorasickReplacementObject;
   iPosition: int64;
   sSearchTerm: string;
begin
   //DebugLogger.LogEnterMethod(Self, 'FindInStream');
   try
      c := #0;
      ptr := FRoot;
      if ASize = 0 then begin
         iMax := Pred(AStream.Size);
      end else begin
         iMax := Pred(AStream.Position + ASize);
      end;
      ms := TMemoryStream.Create;
      try
         // replace loop with stream reading loop
         // use memory buffer of size X
         while AStream.Position < iMax do begin
            iPositionBase := AStream.Position;
            ms.Clear;
            iCopySize := BufferSize;
            if iCopySize > (iMax - AStream.Position) then begin
               iCopySize := iMax - AStream.Position;
            end;
            ms.CopyFrom(AStream, iCopySize);
            ms.Seek(0, soFromBeginning);
            while ms.Position < ms.Size do begin
               ms.Read(c, 1);
               if not FCaseSensitive then begin
                  {$IFDEF FPC}
                  c := LowerCase(c);
                  {$ELSE FPC}
                  c := LowerCase(c)[1];
                  {$ENDIF FPC}
               end;
               trans := nil;
               while not Assigned(trans) do begin
                  trans := ptr.GetTransition(c);
                  if ptr = FRoot then begin
                     break;
                  end;
                  if not Assigned(trans) then begin
                     ptr := ptr.Failure;
                  end;
                  if not Assigned(ptr) then begin
                     ptr := FRoot;
                  end;
               end;
               if Assigned(trans) then begin
                  ptr := trans;
               end;
               for iFound := 0 to Pred(ptr.Results.Count) do begin
                  iPosition := iPositionBase + ms.Position - Length(ptr.Results[iFound]);
                  sSearchTerm := ptr.Results[iFound];
                  FireStringSearchResultEvent(iPosition, sSearchTerm, ptr.Results.Objects[iFound]);
                  {$IFDEF CPU64}
                  if FSeparatedList then begin
                     ro := TAhoCorasickReplacementObject(ptr.Results.Objects[iFound]);
                     {$IF DEFINED(FPC) AND DEFINED(Win64)}
                     AResult.Add(sSearchTerm + '=' + string(ro.Replacer));
                     {$ELSE}
                     AResult.Add(sSearchTerm + '=' + string(ro.Replacer));
                     {$IFEND}
                  end else begin
                     {$IF DEFINED(FPC) AND DEFINED(Win64)}
                     AResult.Add(sSearchTerm);
                     {$ELSE}
                     AResult.Add(sSearchTerm);
                     {$IFEND}
                  end;
                  {$ELSE CPU64}
                  if Assigned(APositions) then begin
                     APositions.Add(IntToStr(iPosition + 1));
                  end;
                  if FSeparatedList then begin
                     ro := TAhoCorasickReplacementObject(ptr.Results.Objects[iFound]);
                     AResult.Add(sSearchTerm + '=' + string(ro.Replacer));
                  end else begin
                     AResult.Add(sSearchTerm);
                  end;
                  {$ENDIF CPU64}
               end;
            end;
         end;
      finally
         ms.Free;
      end;
   finally
      //DebugLogger.LogLeaveMethod(Self, 'FindInStream');
   end;
end;

procedure TAhoCorasick.FindInStream(const AStream: TStream; const AFindAll: boolean; const AResult: TStringSearchResultList; const ASize: int64);
const
   BufferSize = 32768;
var
   ptr: TACTreeNode;
   trans: TACTreeNode;
   iCopySize: integer;
   iPositionBase: int64;
   iMax: int64;
   ms: TMemoryStream;
   c: ansichar;
   iFound: integer;
   ro: TAhoCorasickReplacementObject;
   iPosition: int64;
   sSearchTerm: string;
begin
   //DebugLogger.LogEnterMethod(Self, 'FindInStream');
   try
      c := #0;
      ptr := FRoot;
      if ASize = 0 then begin
         iMax := Pred(AStream.Size);
      end else begin
         iMax := Pred(AStream.Position + ASize);
      end;
      ms := TMemoryStream.Create;
      try
         // replace loop with stream reading loop
         // use memory buffer of size X
         while AStream.Position < iMax do begin
            iPositionBase := AStream.Position;
            ms.Clear;
            iCopySize := BufferSize;
            if iCopySize > (iMax - AStream.Position) then begin
               iCopySize := iMax - AStream.Position;
            end;
            ms.CopyFrom(AStream, iCopySize);
            ms.Seek(0, soFromBeginning);
            while ms.Position < ms.Size do begin
               ms.Read(c, 1);
               if not FCaseSensitive then begin
                  {$IFDEF FPC}
                  c := LowerCase(c);
                  {$ELSE FPC}
                  c := LowerCase(c)[1];
                  {$ENDIF FPC}
               end;
               trans := nil;
               while not Assigned(trans) do begin
                  trans := ptr.GetTransition(c);
                  if ptr = FRoot then begin
                     break;
                  end;
                  if not Assigned(trans) then begin
                     ptr := ptr.Failure;
                  end;
                  if not Assigned(ptr) then begin
                     ptr := FRoot;
                  end;
               end;
               if Assigned(trans) then begin
                  ptr := trans;
               end;
               for iFound := 0 to Pred(ptr.Results.Count) do begin
                  iPosition := iPositionBase + ms.Position - Length(ptr.Results[iFound]);
                  sSearchTerm := ptr.Results[iFound];
                  FireStringSearchResultEvent(iPosition, sSearchTerm, ptr.Results.Objects[iFound]);
                  if FSeparatedList then begin
                     ro := TAhoCorasickReplacementObject(ptr.Results.Objects[iFound]);
                     AResult.AddResult(iPosition + 1, sSearchTerm, '', ro.Replacer);
                  end else begin
                     AResult.AddResult(iPosition + 1, sSearchTerm, '', '');
                  end;
               end;
            end;
         end;
      finally
         ms.Free;
      end;
   finally
      //DebugLogger.LogLeaveMethod(Self, 'FindInStream');
   end;
end;

procedure TAhoCorasick.LoadTreeFromFile(const AFilename: WideString);
var
   ms: TMemoryStream;
begin
   FRoot.Free;
   FRoot := TACTreeNode.Create(nil, ' ', CaseSensitive);
   ms := TMemoryStream.Create;
   try
      try
         {$IFDEF FPC}
         ms.LoadFromFile(UTF8Encode(AFilename));
         {$ELSE FPC}
         ms.LoadFromFile(AFilename);
         {$ENDIF FPC}
         ms.Seek(0, soFromBeginning);
         FRoot.LoadFromStream(ms);
         SetLength(FIdNodeCombis, 0);
         StoreNodeId(FRoot);
         RestoreFailureNodes;
      finally
         ms.Free;
      end;
   except
      on E: Exception do begin
         //DebugLogger.LogException(E, 'TAhoCorasick.LoadTreeFromFile');
      end;
   end;
end;

procedure TAhoCorasick.RestoreFailureNodes;
var
   iNodeToSet, iNodeSearch: integer;
begin
   for iNodeToSet := 0 to Pred(Length(FIdNodeCombis)) do begin
      for iNodeSearch := 0 to Pred(Length(FIdNodeCombis)) do begin
         if integer(FIdNodeCombis[iNodeSearch].Id) = FIdNodeCombis[iNodeToSet].Node.FStreamIdFailure then begin
            FIdNodeCombis[iNodeToSet].Node.Failure := FIdNodeCombis[iNodeSearch].Node;
            break;
         end;
      end;
   end;
end;

procedure TAhoCorasick.StoreNodeId(ANode: TACTreeNode);
var
   i: integer;
begin
   i := Length(FIdNodeCombis);
   SetLength(FIdNodeCombis, Succ(i));
   FIdNodeCombis[i].Id := ANode.FStreamId;
   FIdNodeCombis[i].Node := ANode;
   if Length(ANode.FTransitionsArray) > 0 then begin
      for i := 0 to Pred(Length(ANode.FTransitionsArray)) do begin
         StoreNodeId(ANode.FTransitionsArray[i]);
      end;
   end;
end;

constructor TAhoCorasick.Create;
begin
   inherited;
   FRoot := nil;
   SetLength(FIdNodeCombis, 0);
   FDynObjectList := TList.Create;
end;

{ TACTreeNodes }

function TACTreeNodes.Add(ANode: TACTreeNode): integer;
begin
   Result := inherited Add(ANode);
end;

function TACTreeNodes.Get(AIndex: integer): TACTreeNode;
begin
   Result := TACTreeNode(inherited Get(AIndex));
end;

function TACTreeNodes.GetEnumerator: TACTreeNodeEnumerator;
begin
   Result := TACTreeNodeEnumerator.Create(Self);
end;

procedure TACTreeNodes.Put(AIndex: integer; const ANode: TACTreeNode);
begin
   inherited Put(AIndex, ANode);
end;

{ TACTreeNodeEnumerator }

constructor TACTreeNodeEnumerator.Create(AList: TACTreeNodes);
begin
   inherited Create;
   FIndex := -1;
   FList := AList;
end;

function TACTreeNodeEnumerator.GetCurrent: Pointer;
begin
   Result := FList[FIndex];
end;

function TACTreeNodeEnumerator.MoveNext: boolean;
begin
   Result := FIndex < FList.Count - 1;
   if Result then begin
      Inc(FIndex);
   end;
end;

{ TAhoCorasickReplacer }

destructor TAhoCorasickReplacer.Destroy;
begin
   inherited;
end;

procedure TAhoCorasickReplacer.FireStringSearchResultEvent(APosition: int64; ASearchTerm: string; AData: TObject);
var
   sReplacer: ansistring;
begin
   if FRegExpr then begin
      sReplacer := Firefly.Strings.RegularExpressions.StringToRegExpr(TAhoCorasickReplacementObject(AData).Replacer);
   end else begin
      sReplacer := TAhoCorasickReplacementObject(AData).Replacer;
   end;
   FResult := FResult + Copy(FInput, FCopyStartPos, APosition - FCopyStartPos) + sReplacer;
   FCopyStartPos := APosition + Length(ASearchTerm);
   Inc(FReplaceCount);
   inherited;
end;

function TAhoCorasickReplacer.Replace(const Text: ansistring): integer;
const
   SEqaulsRegExpr: ansistring = '=<regexpr>';
begin
   FCopyStartPos := 1;
   FInput := Text;
   FRegExpr := (Copy(FInput, 1, 9) = '<regexpr>') or (Pos(SEqaulsRegExpr, FInput) > 0);
   FReplaceCount := 0;
   FResult := '';
   FindAll(Text);
   FResult := FResult + Copy(FInput, FCopyStartPos, Length(FInput) - FCopyStartPos + 1);
   Result := FReplaceCount;
end;

{ TAhoCorasickMultiReplacer }

constructor TAhoCorasickMultiReplacer.Create;
begin
   inherited;
   FResults := TStringList.Create;
end;

constructor TAhoCorasickMultiReplacer.Create(const AKeywords: TStrings; ASeparatedList: boolean);
begin
   inherited;
   FResults := TStringList.Create;
end;

destructor TAhoCorasickMultiReplacer.Destroy;
begin
   FResults.Free;
   inherited;
end;

procedure TAhoCorasickMultiReplacer.FireStringSearchResultEvent(APosition: int64; ASearchTerm: string; AData: TObject);
var
   slReplacer: TStringList;
   sTextBetween, sReplacer: ansistring;
   iResult, iNew, iCount: integer;
   bOwnReplacer: boolean;
begin
   inherited;
   Inc(FReplaceCount);
   bOwnReplacer := not (AData is TStringList);
   if bOwnReplacer then begin
      slReplacer := TStringList.Create;
      if (AData is TStringList) then begin
         for iCount := 0 to Pred(TStringList(AData).Count) do begin
            slReplacer.Add(string(TStringList(AData)[iCount]));
         end;
      end;
   end else begin
      slReplacer := TStringList(AData);
   end;
   try
      sTextBetween := Copy(FInput, FCopyStartPos, APosition - FCopyStartPos);
      for iResult := 0 to Pred(FResults.Count) do begin
         FResults[iResult] := FResults[iResult] + string(sTextBetween);
      end;
      if not Assigned(slReplacer) then begin
         Exit;
      end;
      if slReplacer.Count = 0 then begin
         Exit;
      end;
      iCount := FResults.Count;
      // now multiply!
      if slReplacer.Count > 1 then begin
         for iNew := 1 to Pred(slReplacer.Count) do begin
            if FRegExpr then begin
               sReplacer := Firefly.Strings.RegularExpressions.StringToRegExpr(ansistring(slReplacer[iNew]));
            end else begin
               sReplacer := ansistring(slReplacer[iNew]);
            end;
            for iResult := 0 to Pred(iCount) do begin
               FResults.Add(FResults[iResult] + string(sReplacer));
            end;
         end;
      end;
      if FRegExpr then begin
         sReplacer := Firefly.Strings.RegularExpressions.StringToRegExpr(ansistring(slReplacer[0]));
      end else begin
         sReplacer := ansistring(slReplacer[0]);
      end;
      for iResult := 0 to Pred(iCount) do begin
         FResults[iResult] := FResults[iResult] + string(sReplacer);
      end;
      FCopyStartPos := APosition + Length(ASearchTerm);
   finally
      if bOwnReplacer then begin
         slReplacer.Free;
      end;
   end;
end;

function TAhoCorasickMultiReplacer.Replace(const Text: ansistring): integer;
const
   SEqaulsRegExpr: ansistring = '=<regexpr>';
var
   i: integer;
begin
   FCopyStartPos := 1;
   FInput := Text;
   FRegExpr := (Copy(FInput, 1, 9) = '<regexpr>') or (Pos(SEqaulsRegExpr, FInput) > 0);
   FReplaceCount := 0;
   FResults.Clear;
   FResults.Add('');
   FindAll(Text);
   for i := 0 to Pred(FResults.Count) do begin
      FResults[i] := FResults[i] + Copy(string(FInput), FCopyStartPos, Length(FInput) - FCopyStartPos + 1);
   end;
   Result := FReplaceCount;
end;

initialization
end.
