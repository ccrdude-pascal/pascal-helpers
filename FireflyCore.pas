{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyCore;

{$warn 5023 off : no warning about unused units}
interface

uses
  Firefly.AhoCorasick, Firefly.BoyerMoore, Firefly.Collections, 
  Firefly.ConsoleHelp, Firefly.Credits, Firefly.Date, Firefly.Drives, 
  Firefly.DynamicLinkLibrary.Base, Firefly.Events, Firefly.FileFinderThread, 
  Firefly.FilePaths, Firefly.FilePaths.FillersBase, 
  Firefly.FilePaths.WinConsts, Firefly.FileVersion, Firefly.i18n, 
  Firefly.Numbers, Firefly.Parser.Boolean, Firefly.Process, 
  Firefly.Serializer, Firefly.Streams, Firefly.StringCompare, 
  Firefly.Strings.RegularExpressions, Firefly.Strings.Versions, Firefly.Tests, 
  Firefly.Threads, Layers.Hashes.Base, Layers.Internet.DNS.Base, 
  Layers.JSON.Base, Layers.Transport.Base, Layers.Transport.Debugger.List, 
  SimpleFileReader.Base, Firefly.DynamicLinkLibrary.APIBase, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyCore', @Register);
end.
