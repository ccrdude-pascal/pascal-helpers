# Changes

## 2024-05-06

* Adding use of Firefly.Credits to some units
* Reducing compiler warnings and informations
* Added simple old test case for Firefly.Threads.

## 2024-04-26

* Improved FileTimeToDateTime in Firefly.Date, to support 64 bit data types.

## 2024-04-25

* Added new unit Firefly.Serializer that converts TObjects to and from JSON and XML.
* Added TDateTime type helper to Firefly.Date.
* Added header to Firefly.Process.
* Fixed return result and removed unused code from Layers.JSON.Base.
* Flagged parameters to not display a warning in SimpleFileReader.Base.pas
* Added this CHANGELOG.md.
